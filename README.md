
# 概要

Eponaは、新たなビジネスアイデアをサービス化する際に必要な開発環境、サービスを実際に稼働させる環境、
そしてこれらの環境へセキュアにアクセスできる手段をクラウド上に構築するためのツールキットです。

Eponaの提供するスクリプトを実行するだけで、開発環境およびサービス稼働環境を迅速に構築できます。
また、これらの環境の上でDevOpsの活動を回しやすいよう、後述するさまざまな機能を持っており、構築された環境の上でDevOps活動を行えるようにすることを意図しています。

<!-- TOC -->

- [概要](#概要)
  - [Repository Contents](#repository-contents)
    - [Terraform Modules](#terraform-modules)
    - [Guide](#guide)
  - [Overview](#overview)
  - [Concept](#concept)
    - [Self-Service](#self-service)
    - [Operation-Less](#operation-less)
    - [Switchable](#switchable)
  - [Target Cloud](#target-cloud)
    - [AWS](#aws)
  - [Activities](#activities)
  - [Metrics](#metrics)
  - [Getting Started](#getting-started)
    - [Epona AWS Getting Started](#epona-aws-getting-started)
    - [Epona AWS Starter CLI](#epona-aws-starter-cli)
  - [Version Up Policy](#version-up-policy)
  - [Why DevOps](#why-devops)

<!-- /TOC -->

## Repository Contents

本リポジトリは、Terraformモジュールおよびガイドで構成されています。

### Terraform Modules

Eponaは環境を構築する方法としてTerraformを使用することを前提とし、目的に応じたTerraformモジュールを提供しています。  
その詳細は、[こちら](./guide/patterns/README.md)を参照してください。

### Guide

Eponaでは、考え方や利用方法を理解するうえで、助けとなるような[ガイド](./guide/README.md)を提供しています。  
ガイドは[アーキテクチャ](./guide/README.md)、[Activities](./guide/activities/index.md)、Eponaが提供する[Terraformモジュール](./guide/patterns/README.md)を主として構成されています。

まずは本ページを見てEponaの概要を掴んでいただいてから、各ガイドを読み進めていくとよいでしょう。

## Overview

Eponaによって構築される環境の概念図は以下のようになります。
環境は、大きく分けてRuntime環境とDelivery環境に二分されます。

Runtime環境は、開発したサービスが実際に動く環境です。例えば、本番環境、ステージング環境、開発環境が該当します。Eponaを利用するサービスチームのテスト戦略等により、複数のRuntime環境が存在することになるでしょう。

Delivery環境は、Runtime環境にアプリケーションをデリバリするために使用する環境です。例えばソースリポジトリやコンテナリポジトリ等が配置されます。本環境は、サービス毎に1つのみ存在することを前提とします。

![Eponaによって構築される環境概念図](./guide/resources/abstract.png)

ガイドの[アーキテクチャ](./guide/README.md)をご参照ください。

## Concept

Eponaの開発コンセプトは以下の通りです。

### Self-Service

サービス開発チームの自律性を重視し、サービス開発チームが自ら開発、運用するための環境を構築する手段を提供します。
サービス開発チームは、Epona開発チームに何ら許可を求めることなく当該の環境を自由に使い、自由に運用が可能です。
自分たちの裁量でサービス開発・運用をすることで、そのアジリティは大きく高まると考えています。

一方で、自由は責任を伴います。
サービス開発チームはこれら環境の上で、安定したサービスを責任をもって提供しなければなりません。

そのためには、Eponaがどのようなアクティビティをカバーしているかを理解し、その実現方法として選定された技術を学び使いこなす必要があります。
そのようにして「自分たち自身で」サービスを開発・運用していくことは、慣れない開発チームにとっては大きなハードルとして立ちふさがるはずです。
しかしそれを超えた時、サービス、チームは双方ともに大きく成長するはずです。
その手助けは、Eponaのドキュメントが行います。

### Operation-Less

サービスチームはお客様の価値に直接結びつかないシステム自体の運用(監視システムの構築、セキュリティパッチの適用、etc.) について煩わされるべきではありません。
このため、EponaではSaaS、マネージドサービスを積極的に採用します。

これは、システム運用の仕組みをサービス開発チーム自身で構築しメンテナンスするコストよりも、SaaSやマネージドサービスに頼る方が総合的にコストパフォーマンスに優れる場合が多いためです。
とくにサービス開発に携わる人数が少ないほど、この傾向は顕著だと考えます。

SaaSやマネージドサービスでは要件を満たせなくような時にサービス開発チームが自身で構築を行い、そうでなければシステム運用を外部リソースに任せましょう。サービス自体の開発や、ビジネス運用に注力すべきです。

### Switchable

サービスごとにさまざまなユースケースがあり、本ツールキットでそれらすべてを満たすことはできません。
そのため、本ツールキットで構築される環境がサポートする機能はできる限り疎結合にし、サービス開発者によって以下を実現できるようにします。

- 不要であれば容易にそれを無効化できる
- 他の手段で実現する場合は、できる限り簡易に切り替えることができる

## Target Cloud

Eponaは、以下のクラウドサービスに適用できます。

1. [Amazon Web Services](https://aws.amazon.com/jp/) (AWS)

### AWS

AWSにEponaを適用した場合のアーキテクチャは以下の図[^1]のようになります。

![アーキテクチャ](./guide/resources/aws-architecture.png)

[^1]: GitLabのロゴは[GitLab](https://about.gitlab.com/)によって制作されたものであり、[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)の下に提供されています。

## Activities

Eponaを利用することで何ができるようになるのかは、[アクティビティ一覧](./guide/activities/index.md) を参照してください。

大きく分けて以下のアクティビティをサポートします。Eponaでは、これらのアクティビティを実現するための環境を、patternモジュールの組み合わせによって構築します。

- [コミュニケーション](./guide/activities/communication.md)
- [ITS/BTS](./guide/activities/its_bts.md)
- [VCS Provider](./guide/activities/vcs_provider.md)
- [デプロイメントパイプライン](./guide/activities/deployment_pipeline.md)
  - アプリケーション
  - インフラ
- [バックアップ](./guide/activities/backup.md)
- [仮想セキュアルーム](./guide/activities/virtual_secure_room.md)
- [インフラセキュリティ](./guide/activities/infra_security.md)
- [モニタリング](./guide/activities/monitoring.md)

## Metrics

多くのサービスはソフトウェアをデリバリし、運用する能力に支えられています。

それらの能力を測る指標として[State of DevOps 2019 Report](https://services.google.com/fh/files/misc/state-of-devops-2019.pdf)では
以下の5つの指標をSDO Performance [^2]として定義しています。
Thought WorksのTECHNOLOGY RADERにおいても、最初の4つの指標は
[Four key metrics](https://www.thoughtworks.com/radar/techniques/four-key-metrics)として`ADOPT`と評価されいます。

[^2]: Software Delivery and Operational Performance

1. Lead time for changes: コードがcommitされてから本番環境で正しく動作するまでにかかる時間
1. Deployment Frequency: 本番環境にデプロイする頻度
1. Change failure rate: デグレードし何らかの対応が必要になった本番環境変更の割合
1. Time torestore service: ユーザー影響のある障害が発生した際のサービス復旧にかかる時間
1. Availability: サービスをエンドユーザーに利用可能とする能力

Eponaの開発においても、これらの指標を如何に向上させるかに焦点を合わせて開発を進めます。
これらの指標はサービス開発時においても常に留意しておくべきものです。

## Getting Started

Eponaをはじめて使う人向けに、2つのコンテンツを提供しています。

- [Epona AWS Getting Started](https://eponas.gitlab.io/epona_aws_getting_started/)
- [Epona AWS Starter CLI](https://gitlab.com/eponas/epona-aws-starter-cli)

### Epona AWS Getting Started

[Epona AWS Getting Started](https://eponas.gitlab.io/epona_aws_getting_started/)は、Eponaが提供したモジュールを使用してサービスを構成したサンプルリポジトリとなります。

Epona AWS Getting Startedに含まれるソースコードを参照することで、Eponaの具体的な使い方の参考になるでしょう。  
また、お使いの環境に合わせてパラメーターを調整することで、Eponaを適用した環境を実際に動かすこともできます。

### Epona AWS Starter CLI

[Epona AWS Starter CLI](https://gitlab.com/eponas/epona-aws-starter-cli)は、Eponaを使用するための環境構築や初期セットアップの簡易化に特化したツールです。

Eponaが想定するTerraform実行環境や、Epona AWS Getting Startedにて実装されているアクティビティの一部の構築が可能です。

このツールは、本リポジトリ内の環境構築手順でも利用されています。

## Version Up Policy

ここでは、Epona自身のバージョンアップポリシーについて記述します。
まず、Eponaは後方互換性の維持を第一義とはせず、バージョンアップによって後方互換性を崩す場合があることに注意してください。

Eponaは、ユーザーの事情により大いに変動し、かつカスタマイズし得る「環境」を対象としています。結果として、ユーザーのすべての要望を取り込めるような自由度・拡張性をEponaに定義することは困難です。
このため、後方互換性を強く重視することによりEpona自身の発展を阻害することや、自由度が高すぎるモジュールを作成することにより利用者の学習コストを高くしてしまうことを避けた方が良いと考えています。

このためEponaを利用するときは、[Module Versions](https://www.terraform.io/docs/configuration/modules.html#module-versions)を参考に明示的にバージョンを指定してください。

また、Eponaの新しいバージョンを使用し新たなアクティビティやpatternを使いたい場合、構成を大きく変えることが想定されるため、環境の再プロビジョニングを必要とします。
もしくは、利用者自身が`pattern`を追加し、Eponaを拡張していくことになるでしょう。

## Why DevOps

[The DevOps ハンドブック 理論・原則・実践のすべて](https://www.nikkeibp.co.jp/atclpubmkt/book/17/P85480/)によれば、DevOpsの原則は以下の3つであるとされています。

1. 顧客に素早く価値を提供するために、開発から運用へのフローを高速かつスムーズにする
2. ITバリューストリーム全体で素早く頻繁なフィードバックループを回せるようにし、高品質・高信頼性・安全性を実現する
3. 継続的な学習と実験の文化を育てる

これらの原則に従うことができる環境を構築することで、ビジネスの価値をより高めるとともに、高めた価値を迅速にエンドユーザーへ届けることができると考えているからです。
