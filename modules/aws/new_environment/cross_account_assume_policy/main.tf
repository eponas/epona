# 各Runtime環境毎のTerraform実行ロールへのassume-role設定
data "aws_iam_policy_document" "assume_role" {
  for_each = var.execution_role_map

  statement {
    actions = [
      "sts:AssumeRole",
    ]
    resources = [
      each.value
    ]
  }
}

resource "aws_iam_policy" "assume_role" {
  for_each = var.execution_role_map

  description = "Allow Terraform Eexecution on ${each.key} environment"
  name        = format("%s%sTerraformAssumeRolePolicy", title(var.prefix), title(each.key))
  policy      = data.aws_iam_policy_document.assume_role[each.key].json
}

resource "aws_iam_user_policy_attachment" "assume_role" {
  for_each = var.execution_role_map

  user       = var.terraformer_users[each.key].name
  policy_arn = aws_iam_policy.assume_role[each.key].arn
}
