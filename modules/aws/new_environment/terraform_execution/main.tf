# Terraform実行用Role
resource "aws_iam_role" "execution" {
  name               = format("%sTerraformExecutionRole", title(var.prefix))
  assume_role_policy = data.aws_iam_policy_document.cross_account_assume_role.json
}

resource "aws_iam_role_policy_attachment" "execution" {
  role       = aws_iam_role.execution.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

# Terraform実行ユーザにのみ移譲するassume role policy
data "aws_iam_policy_document" "cross_account_assume_role" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "AWS"
      identifiers = var.principals
    }
  }
}
