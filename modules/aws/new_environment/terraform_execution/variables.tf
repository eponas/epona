variable "prefix" {
  description = "テスト時等でユニークな名前を付与したいとき、ユニークになるように付与する接頭語"
  type        = string
  default     = ""
}

variable "principals" {
  description = "Terraform実行権限を与えるprincipal"
  type        = list(string)
}
