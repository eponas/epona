variable "prefix" {
  description = "テスト時等でユニークな名前を付与したいとき、ユニークになるように付与する接頭語"
  type        = string
  default     = ""
}

variable "delivery_account_name" {
  description = "Delivery環境の名称"
  type        = string
  default     = "Delivery"
}

variable "runtime_accounts" {
  description = "Runtime環境の名前とAWSアカウントIDのマップ"
  type        = map(string)
}

variable "name" {
  description = "任意の名前"
  type        = string
}

variable "force_destroy" {
  description = "bucketが空でなくても強制的に削除するかどうか"
  type        = bool
  default     = false
}

variable "user_groups" {
  description = "Terraform実行用ユーザを追加する既存のユーザグループ"
  type        = list(string)
  default     = []
}
