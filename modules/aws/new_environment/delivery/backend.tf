resource "aws_s3_bucket" "tfstate" {
  for_each = local.environments

  bucket        = join("-", [var.name, lower(each.value), "terraform", "tfstate"])
  acl           = "private"
  force_destroy = var.force_destroy

  # tfstate 用の bucket バージョニング推奨
  # https://www.terraform.io/docs/backends/types/s3.html
  versioning {
    enabled = true
  }

  # SSE による暗号化
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    Name = join("_", [var.name, lower(each.key), "terraform", "tfstate"])
  }
}

resource "aws_s3_bucket_public_access_block" "tfstate" {
  for_each = local.environments

  bucket = aws_s3_bucket.tfstate[each.value].bucket

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_dynamodb_table" "tfstate_lock" {
  name           = join("_", [var.name, "terraform", "tfstate", "lock"])
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}
