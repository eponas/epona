output "iam_arn" {
  description = "Origin Access IdentityのARN"
  value       = aws_cloudfront_origin_access_identity.this.iam_arn
}

output "id" {
  description = "Origin Access IdentityのID"
  value       = aws_cloudfront_origin_access_identity.this.id
  sensitive   = true
}
