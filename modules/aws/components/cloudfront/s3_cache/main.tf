# 指定したS3バケットをCloudFrontディストリビューションで公開する設定の作成
resource "aws_cloudfront_distribution" "s3_cache" {
  enabled             = var.enabled
  default_root_object = var.default_root_object
  aliases             = var.aliases
  tags                = var.tags
  http_version        = var.http_version
  is_ipv6_enabled     = var.is_ipv6_enabled
  price_class         = var.price_class
  web_acl_id          = var.web_acl_id

  # 配信ソースの設定（本来は複数指定可能だが、現状では1つのみとする）
  origin {
    domain_name = var.origin.domain_name
    s3_origin_config {
      origin_access_identity = var.origin.s3_origin_config.origin_access_identity
    }
    origin_id   = var.origin.origin_id
    origin_path = lookup(var.origin, "origin_path", null)
    # アクセス時に配信先へ付与するヘッダ（0個以上で複数定義可能）
    dynamic "custom_header" {
      for_each = length(var.origin.custom_headers) > 0 ? var.origin.custom_headers : []
      content {
        name  = custom_header.value.name
        value = custom_header.value.value
      }
    }
  }

  # 国レベルでのアクセス制限（可変にすると実装が複雑化するため、必要になるまではすべて許可で固定とする）
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  # すべてのファイルにマッチするデフォルトのキャッシュ動作設定
  default_cache_behavior {
    target_origin_id       = var.default_cache_behavior.target_origin_id
    allowed_methods        = var.default_cache_behavior.allowed_methods
    cached_methods         = var.default_cache_behavior.cached_methods
    viewer_protocol_policy = var.default_cache_behavior.viewer_protocol_policy
    # この部分は可変にすると実装が複雑化するため、必要になるまでは固定とする
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
    compress    = lookup(var.default_cache_behavior, "compress", null)
    min_ttl     = lookup(var.default_cache_behavior, "min_ttl", null)
    default_ttl = lookup(var.default_cache_behavior, "default_ttl", null)
    max_ttl     = lookup(var.default_cache_behavior, "max_ttl", null)

    lambda_function_association {
      event_type = "viewer-response"
      lambda_arn = var.default_cache_behavior.viewer_response_lambda_arn
    }
  }

  # SSL化の設定（Route53使う前提のため、CloudFrontデフォルトの証明書は使わない）
  viewer_certificate {
    acm_certificate_arn      = var.viewer_certificate.acm_certificate_arn
    ssl_support_method       = var.viewer_certificate.ssl_support_method
    minimum_protocol_version = var.viewer_certificate.minimum_protocol_version
  }

  # エラーページアクセス時の動作設定（0個以上で複数定義可能）
  dynamic "custom_error_response" {
    for_each = length(var.custom_error_responses) > 0 ? var.custom_error_responses : []
    content {
      error_code            = custom_error_response.value.error_code
      response_code         = lookup(custom_error_response.value, "response_code", null)
      response_page_path    = lookup(custom_error_response.value, "response_page_path", null)
      error_caching_min_ttl = lookup(custom_error_response.value, "error_caching_min_ttl", null)
    }
  }

  # アクセスログの設定（未定義または1つ定義可能で、未定義の場合はロギングされない）
  dynamic "logging_config" {
    for_each = var.logging_config.bucket != null ? [var.logging_config] : []
    content {
      bucket          = logging_config.value.bucket
      include_cookies = lookup(logging_config.value, "include_cookies", null)
      prefix          = lookup(logging_config.value, "prefix", null)
    }
  }
}
