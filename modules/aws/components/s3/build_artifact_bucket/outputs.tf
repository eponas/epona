output "bucket_arns" {
  description = "S3バケットの名前とARNのマップ"
  value       = { for r in aws_s3_bucket.this : r.id => r.arn }
}
