output "bucket_regional_domain_name" {
  description = "ビルド済みファイルを配置するバケットのリージョンドメイン名"
  value       = aws_s3_bucket.cloudfront_origin_bucket.bucket_regional_domain_name
}

output "bucket_name" {
  description = "ビルド済みファイルを配置するバケットの名前"
  value       = aws_s3_bucket.cloudfront_origin_bucket.bucket
}
