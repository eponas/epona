# ビルド済みファイルを配置するバケットを作成
resource "aws_s3_bucket" "cloudfront_origin_bucket" {
  bucket        = var.s3_frontend_bucket_name
  acl           = "private"
  tags          = var.tags
  force_destroy = var.force_destroy

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  dynamic "logging" {
    for_each = var.enable_logging ? ["dummy"] : []

    content {
      target_bucket = var.logging_bucket
      target_prefix = var.logging_prefix
    }
  }
}

resource "aws_s3_bucket_public_access_block" "cloudfront_origin_bucket" {
  bucket                  = aws_s3_bucket.cloudfront_origin_bucket.bucket
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
  depends_on = [
    aws_s3_bucket_policy.cloudfront_origin_bucket
  ]
}

# バケットポリシーを作成
data "aws_iam_policy_document" "cloudfront_origin_bucket" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.cloudfront_origin_bucket.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [var.cloudfront_origin_access_identity_iam_arn]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.cloudfront_origin_bucket.arn]

    principals {
      type        = "AWS"
      identifiers = [var.cloudfront_origin_access_identity_iam_arn]
    }
  }
}

# 作成したバケットポリシーをビルド済みファイルを配置するバケットに設定
resource "aws_s3_bucket_policy" "cloudfront_origin_bucket" {
  bucket = aws_s3_bucket.cloudfront_origin_bucket.id
  policy = data.aws_iam_policy_document.cloudfront_origin_bucket.json
}
