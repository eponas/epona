resource "aws_s3_bucket" "this" {
  count  = var.create_bucket && var.bucket != null ? 1 : 0
  bucket = var.bucket
  acl    = var.acl
  dynamic "grant" {
    for_each = length(var.grants) > 0 ? var.grants : []
    content {
      type        = grant.value.type
      permissions = grant.value.permissions
      id          = grant.value.id
    }
  }
  tags          = var.tags
  force_destroy = var.force_destroy
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "this" {
  count                   = var.create_bucket && var.bucket != null ? 1 : 0
  bucket                  = aws_s3_bucket.this[(count.index)].bucket
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
  depends_on = [
    aws_s3_bucket_policy.this
  ]
}

resource "aws_s3_bucket_policy" "this" {
  count  = var.create_bucket && var.bucket_policy != null ? 1 : 0
  bucket = aws_s3_bucket.this[0].id
  policy = var.bucket_policy
}
