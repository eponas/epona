resource "aws_wafv2_web_acl" "this" {
  name  = var.name
  scope = var.scope

  default_action {
    dynamic "allow" {
      for_each = var.default_action == "allow" ? [null] : []
      content {}
    }
    dynamic "block" {
      for_each = var.default_action == "block" ? [null] : []
      content {}
    }
  }

  dynamic "rule" {
    for_each = var.rule_groups
    content {
      name     = "${var.rule_name_prefix}-${index(var.rule_groups, rule.value)}"
      priority = index(var.rule_groups, rule.value)

      override_action {
        dynamic "count" {
          for_each = rule.value.override_action == "count" ? [null] : []
          content {}
        }
        dynamic "none" {
          for_each = rule.value.override_action == "none" ? [null] : []
          content {}
        }
      }

      statement {
        rule_group_reference_statement {
          arn = rule.value.arn
        }
      }

      visibility_config {
        cloudwatch_metrics_enabled = rule.value.cloudwatch_metrics_enabled
        metric_name                = "${var.rule_name_prefix}-${index(var.rule_groups, rule.value)}-rule-metric-name"
        sampled_requests_enabled   = rule.value.sampled_requests_enabled
      }
    }
  }

  visibility_config {
    cloudwatch_metrics_enabled = var.cloudwatch_metrics_enabled
    metric_name                = var.metric_name
    sampled_requests_enabled   = var.sampled_requests_enabled
  }

  tags = var.tags
}

resource "aws_wafv2_web_acl_association" "this" {
  count        = var.resource_arn != null ? 1 : 0
  resource_arn = var.resource_arn
  web_acl_arn  = aws_wafv2_web_acl.this.arn
}
