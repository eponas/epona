variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "default_action" {
  description = "WebACLに設定するデフォルトアクション（`allow` or `block`）"
  type        = string
}

variable "name" {
  description = "作成するリソースに付ける名前"
  type        = string
}

variable "scope" {
  description = "CloudFront用かリージョンアプリケーション用かの指定（`CLOUDFRONT` or `REGIONAL`）"
  type        = string
}

variable "rule_name_prefix" {
  description = "WebACL内でのルールの名前に付与する接頭語"
  type        = string
  default     = "rule"
}

variable "rule_groups" {
  description = <<DESCRIPTION
WebACLに関連付けるルールグループ（List形式で複数指定可、記載した順に優先度が高くなる）
```
rule_groups = [
  {
    arn                        = "dummy"  # WebACLに設定するルールグループのARN（作成するWebACLと同一のリージョンに作成済みのルールグループのみ指定可）
    override_action            = "none"   # none: 元々のルールグループのアクションをそのまま適用する。count: 一致したWebリクエストのカウントのみを行うようにする。
    cloudwatch_metrics_enabled = false    # このルールグループのCloudWatchによる件数モニタリングを有効化する
    sampled_requests_enabled   = false    # このルールグループのルールに一致した過去3時間分のWebリクエストの保存を有効化する
  }
]
```
DESCRIPTION
  type        = list(map(any))
}

variable "cloudwatch_metrics_enabled" {
  description = "WebACLのCloudWatchによる件数モニタリングを有効化する"
  type        = bool
}

variable "metric_name" {
  description = "WebACLのメトリック名"
  type        = string
}

variable "sampled_requests_enabled" {
  description = "ルールに一致した過去3時間分のWebリクエストの保存を有効化する"
  type        = bool
}

variable "resource_arn" {
  description = "WAFを関連付けるリソースのARN（ここでのCloudFrontの指定は不可）"
  type        = string
  default     = null
}
