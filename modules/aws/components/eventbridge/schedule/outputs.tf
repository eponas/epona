output "rule_arn" {
  value       = aws_cloudwatch_event_rule.rule.arn
  description = "EventbridgeのルールのARNを指定する。"
}
