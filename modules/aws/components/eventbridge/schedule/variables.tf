variable "name" {
  type        = string
  description = "Eventbridgeルールの名称を指定する。"
}

variable "description" {
  type        = string
  default     = null
  description = "Eventbridgeのルールの説明を指定できる。"
}

variable "schedule_expression" {
  type        = string
  description = <<EOF
スケジュール式を指定する。`cron()`式あるいは`rate()`式で指定する。

詳細は(こちら)[https://docs.aws.amazon.com/ja_jp/eventbridge/latest/userguide/eb-create-rule-schedule.html]を参照。

EOF

}

variable "target_arn" {
  type        = string
  description = "ターゲットになるリソースのARNを指定する。"
}

variable "role_arn" {
  type        = string
  default     = null
  description = <<EOF

ターゲットになるリソースの実行ロールをARNで指定する。
Lambdaの場合、`aws_lambda_permission`リソースを使ってアクセスの権限設定をしている場合は指定は不要。
EOF

}

variable "is_enabled" {
  type        = bool
  default     = false
  description = "設定したスケジュールのオンオフを指定できる。デフォルはオフ。"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}
