variable "name" {
  type        = string
  description = "Eventbirdgeルールの名称を指定する。"

}

variable "event_pattern" {
  type        = string
  description = <<EOF
Eventbridgeのイベントパターンを指定する。
イベントパターンについては[Amazon EventBridge イベントパターン](https://docs.aws.amazon.com/ja_jp/eventbridge/latest/userguide/eb-event-patterns.html)を参照のこと。
EOF

}

variable "rule_description" {
  type        = string
  default     = null
  description = "Eventbridgeルールの説明を記載できる。"
}


variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}
