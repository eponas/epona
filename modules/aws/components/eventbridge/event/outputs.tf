output "name" {
  value       = aws_cloudwatch_event_rule.event_rule.name
  description = "イベントルールの名前を返す。"
}
