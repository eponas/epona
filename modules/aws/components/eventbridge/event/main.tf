#イベントターゲットの指定方法が例えばlambdaとstepfuntionで異なるので
#ここではルールのみ指定する。

resource "aws_cloudwatch_event_rule" "event_rule" {
  name          = var.name
  description   = var.rule_description
  event_pattern = var.event_pattern
  tags          = var.tags
}
