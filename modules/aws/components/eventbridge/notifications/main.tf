resource "aws_cloudwatch_event_connection" "event_connection" {
  name               = "${var.name_prefix}RestEventConn"
  authorization_type = var.authorization_type
  auth_parameters {
    api_key {
      key   = var.connection_parameters.apikey_key
      value = var.connection_parameters.apikey_value
    }
  }
}

resource "aws_cloudwatch_event_api_destination" "api_destination" {
  name                = "${var.name_prefix}ApiDest"
  http_method         = var.api_connection.method
  invocation_endpoint = var.api_connection.webhook_uri
  connection_arn      = aws_cloudwatch_event_connection.event_connection.arn
}

resource "aws_cloudwatch_event_rule" "event_rule" {
  name           = "${var.name_prefix}Rule"
  event_bus_name = var.event_bus_name
  event_pattern  = var.event_pattern
  description    = var.rule_description
  tags           = var.tags
}

data "aws_iam_policy_document" "assume_policy" {
  statement {
    sid     = "EventBridgeAssumeRolePolicy"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "events.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "api_destination" {
  statement {
    sid     = "InvokeApiDestination"
    actions = ["events:InvokeApiDestination"]
    resources = [
      aws_cloudwatch_event_api_destination.api_destination.arn
    ]
  }
}

resource "aws_iam_policy" "api_destination" {
  name   = "${var.name_prefix}ApiDestPolicy"
  path   = "/service-role/"
  policy = data.aws_iam_policy_document.api_destination.json
  tags   = var.tags
}

resource "aws_iam_role" "api_destination" {
  name               = "${var.name_prefix}ApiDestRole"
  assume_role_policy = data.aws_iam_policy_document.assume_policy.json
  path               = "/service-role/"
  managed_policy_arns = [
    aws_iam_policy.api_destination.arn
  ]
  tags = var.tags
}

resource "aws_cloudwatch_event_target" "target" {
  #target_id      = "ApiTarget"
  event_bus_name = var.event_bus_name
  rule           = aws_cloudwatch_event_rule.event_rule.name
  role_arn       = aws_iam_role.api_destination.arn
  arn            = aws_cloudwatch_event_api_destination.api_destination.arn
  http_target {
    header_parameters = {
      Content-type = "application/json"
    }
  }

  input_transformer {
    input_paths    = jsondecode(var.input_transformer.input_paths)
    input_template = var.input_transformer.input_template
  }
}
