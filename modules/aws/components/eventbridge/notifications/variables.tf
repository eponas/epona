variable "name_prefix" {
  type        = string
  default     = "Epona"
  description = "作成するリソースの名前のプレフィックスを指定する。"
}

variable "rule_description" {
  type        = string
  default     = null
  description = "イベントルールの説明を指定する。"
}


variable "authorization_type" {
  type        = string
  default     = "API_KEY"
  description = "通知先の認証タイプを指定する。指定できる値は[`authorization_type`](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_connection#authorization_type)を参照。"
}

variable "connection_parameters" {
  type = object({
    apikey_key   = string
    apikey_value = string
  })
  default = {
    apikey_key   = "dummy"
    apikey_value = "dummy"
  }
  description = "APIキーを渡すためのヘッダのキー名と値を指定する。SlackやMicrosoft Teamsに対してWebhook経由で通知する場合、APIキーは不要であるため本パラメータでの指定も不要。"
}

variable "api_connection" {
  type = object({
    webhook_uri = string
    method      = string
  })
  description = "通知時のWebhook URIへアクセスするための情報。`method`には、アクセスに必要なHTTP Method(`POST`等)を指定する。`webhook_uri`には通知先となるWebhookのURIを指定する。"
}

variable "input_transformer" {
  type = object({
    input_paths    = string
    input_template = string
  })
  description = "APIのボディの内容のための定義"
}

variable "event_pattern" {
  type        = string
  description = "イベントパターン。詳細は[Amazon EventBridge event patterns](https://docs.aws.amazon.com/eventbridge/latest/userguide/eb-event-patterns.html)を参照のこと"
}

variable "event_bus_name" {
  type        = string
  default     = "default"
  description = "イベントバスを指定する。デフォルトは`default`。"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}
