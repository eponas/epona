variable "project_name" {
  description = "CodeBuildのプロジェクト名"
  type        = string
}

variable "tags" {
  description = "CodeBuildに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "build_timeout" {
  description = "ビルドプロセスのタイムアウト時間（単位を分で指定）"
  type        = number
  default     = 60
}

variable "queued_timeout" {
  description = "ビルドキューのタイムアウト時間（単位を分で指定）"
  type        = number
  default     = 480
}

variable "compute_type" {
  description = "CodeBuildが利用するリソースタイプ。選択できる値については[Terraformドキュメントのcompute_type](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codebuild_project#compute_type)を参照。"
  type        = string
}

variable "base_image" {
  description = "CodeBuildで利用するコンテナのベースイメージ。値については[Terraformドキュメントのimage](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codebuild_project#image)を参照。"
  type        = string
}

variable "image_type" {
  description = "ベースイメージのリソースタイプ。選択できる値についてはTerraformドキュメントのtype](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codebuild_project#type)を参照。"
  type        = string
}

variable "image_pull_credentials_type" {
  description = "イメージをプルする際に用いるクレデンシャル情報。選択できる値については[Terraformドキュメントのimage_pull_credentials_type](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codebuild_project#image_pull_credentials_type)を参照。"
  type        = string
}

variable "artifact_store_bucket_name" {
  description = "Artifact Store用バケット名"
  type        = string
}

variable "cloudwatch_log_group_name" {
  description = "CodeBuildのログを保存する、CloudWatch Logsのロググループ名"
  type        = string
}
