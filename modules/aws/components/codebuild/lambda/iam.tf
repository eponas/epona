# CodeBuildのサービスロールを作成
data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codebuild" {
  name               = format("CodeBuildServiceRoleFor%s", title(var.project_name))
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

# CodeBuildのサービスロールにアタッチするポリシーを作成
data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_s3_bucket" "artifact_store_bucket" {
  bucket = var.artifact_store_bucket_name
}

data "aws_iam_policy_document" "codebuild_policy" {
  statement {
    sid = "AllowArtifactStoreAccess"

    effect = "Allow"
    actions = [
      "s3:PutObject",
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetBucketAcl",
      "s3:GetBucketLocation",
    ]

    resources = [
      data.aws_s3_bucket.artifact_store_bucket.arn,
      "${data.aws_s3_bucket.artifact_store_bucket.arn}/*"
    ]
  }

  statement {
    sid = "AllowUpdateLambdaFunctionCode"

    effect = "Allow"
    actions = [
      "lambda:UpdateFunctionCode",
      "lambda:GetAlias",
      "lambda:GetFunction",
      "lambda:PublishVersion"
    ]

    resources = ["*"]
  }

  statement {
    sid = "AllowCreateLogToCloudWatchLogs"

    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = [
      "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:/aws/codebuild/${var.project_name}",
      "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:/aws/codebuild/${var.project_name}:*"
    ]
  }
}

resource "aws_iam_policy" "codebuild_policy" {
  name   = format("CodeBuildPolicyFor%s", title(var.project_name))
  policy = data.aws_iam_policy_document.codebuild_policy.json
}

# CodeBuildのサービスロールにポリシーをアタッチ
resource "aws_iam_role_policy_attachment" "codebuild_policy" {
  role       = aws_iam_role.codebuild.name
  policy_arn = aws_iam_policy.codebuild_policy.arn
}
