resource "aws_codebuild_project" "this" {
  name           = var.project_name
  description    = "codebuild deploy lambda"
  build_timeout  = var.build_timeout
  queued_timeout = var.queued_timeout

  service_role = aws_iam_role.codebuild.arn

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    compute_type                = var.compute_type
    image                       = var.base_image
    type                        = var.image_type
    image_pull_credentials_type = var.image_pull_credentials_type

  }

  source {
    type = "CODEPIPELINE"
  }

  logs_config {
    cloudwatch_logs {
      status     = "ENABLED"
      group_name = var.cloudwatch_log_group_name
    }
  }

  tags = merge({
    Name = var.project_name
    },
    var.tags
  )
}
