resource "aws_codebuild_project" "this" {
  name           = var.project_name
  description    = "codebuild deploy to s3"
  build_timeout  = var.build_timeout
  queued_timeout = var.queued_timeout

  service_role = aws_iam_role.codebuild.arn

  artifacts {
    type = "CODEPIPELINE"
  }

  environment {
    compute_type                = var.compute_type
    image                       = var.base_image
    type                        = var.image_type
    image_pull_credentials_type = var.image_pull_credentials_type

    environment_variable {
      name  = "DEPLOY_BUCKET_NAME"
      value = var.deployment_bucket_name
    }

    dynamic "environment_variable" {
      for_each = length(var.deployment_object_path) > 0 ? ["dummy"] : []

      content {
        name  = "DEPLOY_OBJECT_PATH"
        value = var.deployment_object_path
      }
    }

    environment_variable {
      name  = "CACHE_INVALIDATION_FLAG"
      value = var.enable_cache_invalidation
    }

    dynamic "environment_variable" {
      for_each = var.enable_cache_invalidation ? ["dummy"] : []

      content {
        name  = "DISTRIBUTION_ID"
        value = var.cloudfront_distribution_id
      }
    }
  }

  source {
    type = "CODEPIPELINE"

    buildspec = file("${path.module}/buildspec.yml")
  }

  logs_config {
    cloudwatch_logs {
      status     = "ENABLED"
      group_name = var.cloudwatch_log_group_name
    }
  }

  tags = merge({
    Name = var.project_name
    },
    var.tags
  )
}
