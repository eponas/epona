output "codedeploy_arn" {
  description = "CodeDeployのARN"
  value       = aws_codedeploy_deployment_group.lambda.arn
}

output "codedeploy_service_role_arn" {
  description = "CodeDeployのサービスロールのARN"
  value       = aws_iam_role.codedeploy.arn
}
