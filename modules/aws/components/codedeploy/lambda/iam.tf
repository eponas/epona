# Codedeployのサービスロールを作成
data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codedeploy.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codedeploy" {
  name               = format("CodedeployServiceRoleFor%s", title(var.name))
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

# Codedeployのサービスロールにアタッチするポリシーを作成
data "aws_s3_bucket" "artifact_store_bucket" {
  bucket = var.artifact_store_bucket_name
}

resource "aws_iam_role_policy_attachment" "aws_code_deploy_role_for_lambda" {
  role       = aws_iam_role.codedeploy.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRoleForLambda"
}

data "aws_iam_policy_document" "codedeploy_policy" {
  statement {
    sid = "AllowArtifactStoreAccess"

    effect = "Allow"
    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetBucketAcl",
      "s3:GetBucketLocation",
    ]

    resources = [
      data.aws_s3_bucket.artifact_store_bucket.arn,
      "${data.aws_s3_bucket.artifact_store_bucket.arn}/*"
    ]
  }
}

resource "aws_iam_policy" "codedeploy_policy" {
  name   = format("CodedeployPolicyFor%s", title(var.name))
  policy = data.aws_iam_policy_document.codedeploy_policy.json
}

# Codedeployのサービスロールにポリシーをアタッチ
resource "aws_iam_role_policy_attachment" "codedeploy_policy" {
  role       = aws_iam_role.codedeploy.name
  policy_arn = aws_iam_policy.codedeploy_policy.arn
}
