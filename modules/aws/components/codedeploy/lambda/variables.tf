variable "name" {
  description = "CodeDeployのアプリケーション名"
  type        = string
}

variable "deployment_group_name_lambda" {
  description = "CodeDeployのデプロイメントグループ名"
  type        = string
}

variable "deployment_config_name" {
  description = "デプロイにおいてCodeDeployが使用するルール名。詳しくは[こちら](https://docs.aws.amazon.com/ja_jp/codedeploy/latest/userguide/deployment-configurations.html#deployment-configuration-lambda)。"
  default     = "CodeDeployDefault.LambdaAllAtOnce"
  type        = string
}

variable "artifact_store_bucket_name" {
  description = "Artifact Store用バケット名を指定する。Artifact Store用バケットへのアクセス権限を設定するために必要。"
  type        = string
}
