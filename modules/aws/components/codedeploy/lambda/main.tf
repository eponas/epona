resource "aws_codedeploy_app" "this" {
  compute_platform = "Lambda"
  name             = var.name
}

resource "aws_codedeploy_deployment_group" "lambda" {
  app_name               = aws_codedeploy_app.this.name
  deployment_group_name  = var.deployment_group_name_lambda
  service_role_arn       = aws_iam_role.codedeploy.arn
  deployment_config_name = var.deployment_config_name

  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "BLUE_GREEN"
  }

}
