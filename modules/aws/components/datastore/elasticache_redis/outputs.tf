output "elasticache_replication_group_id" {
  description = "ElastiCache for RedisのレプリケーショングループのID"
  value       = aws_elasticache_replication_group.this.id
}

output "elasticache_replication_group_port" {
  description = "ElastiCache for Redisへの接続ポート"
  value       = aws_elasticache_replication_group.this.port
}

output "elasticache_replication_group_configuration_endpoint_address" {
  description = "[クラスターモード有効時のエンドポイント（ConfigurationEndpoint）](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/Endpoints.html#Endpoints.Find.RedisCluster)のアドレス"
  value       = aws_elasticache_replication_group.this.configuration_endpoint_address
}

output "elasticache_replication_group_primary_endpoint_address" {
  description = "[クラスターモード無効時の読み書きを行うエンドポイント（PrimaryEndPoint）](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/Endpoints.html#Endpoints.Find.Redis)のアドレス"
  value       = aws_elasticache_replication_group.this.primary_endpoint_address
}
