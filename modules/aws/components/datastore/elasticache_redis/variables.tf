variable "replication_group_id" {
  description = "ElastiCache for RedisのレプリケーショングループID"
  type        = string
}

variable "tags" {
  description = "ElastiCache for Redisに関するリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "replication_group_description" {
  description = "ElastiCache for Redisのレプリケーショングループの説明"
  type        = string
}

variable "engine_version" {
  description = "ElastiCache for Redisのエンジンバージョン"
  type        = string
}

variable "port" {
  description = "ElastiCache for Redisが使用するポート"
  type        = number
}

variable "node_type" {
  description = "ElastiCache for Redisで使用するノードタイプ"
  type        = string
}

variable "cluster_mode_enabled" {
  description = "クラスターモードを有効にする場合、trueを設定する"
  type        = bool
  default     = false
}

variable "availability_zones" {
  description = "クラスターモードが有効の場合、ElastiCacheクラスターを配置するAZを指定する。number_cache_clustersの数と同じにする必要がある"
  type        = list(string)
}

variable "automatic_failover_enabled" {
  description = "自動フェイルオーバーを有効にする場合、trueを指定する。マルチAZ前提"
  type        = bool
}

variable "number_cache_clusters" {
  description = "ElastiCache for Redisのクラスター自体の数。クラスターモードの無効の場合は必須で、マルチAZを有効にする場合は少なくとも2とする必要がある"
  type        = number
  default     = null
}

variable "at_rest_encryption_enabled" {
  description = "保管時の暗号化を有効にする場合、trueを設定する"
  type        = bool
}

variable "transit_encryption_enabled" {
  ## 転送時の暗号化には、ノードタイプなどの条件がある: https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/in-transit-encryption.html#in-transit-encryption-constraints
  description = "データ転送における暗号化を有効にする場合、trueを設定する"
  type        = bool
}

variable "auth_token" {
  ## 認証トークンを使用する場合は、データ転送の暗号化が要件となる: https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/auth.html
  description = "認証トークン（パスワード）"
  type        = string
}

variable "kms_key_id" {
  description = "KMSキー"
  type        = string
}

variable "snapshot_name" {
  description = "スナップショット名"
  type        = string
}

variable "snapshot_retention_limit" {
  description = <<-DESCTIPTION
  バックアップ保管期間。0〜35(日)の範囲で指定すること。
  DESCTIPTION
  type        = number
}

variable "snapshot_window" {
  description = "スナップショットウィンドウ"
  type        = string
}

variable "maintenance_window" {
  description = "メンテナンスウィンドウ"
  type        = string
}

variable "notification_topic_arn" {
  description = "通知用のSNS ARN"
  type        = string
}

variable "apply_immediately" {
  description = "変更をすぐに適用する場合、trueを指定する（クラスターが再起動されることがある）"
  type        = bool
}

variable "cluster_mode_replicas_per_node_group" {
  # https://docs.aws.amazon.com/AmazonElastiCache/latest/red-ug/CacheNodes.NodeGroups.html
  description = "クラスターモード有効時の、クラスター内のノード数"
  type        = number
}

variable "cluster_mode_num_node_groups" {
  # https://docs.aws.amazon.com/AmazonElastiCache/latest/red-ug/CacheNodes.NodeGroups.html
  description = "クラスターモード有効時の、ノードグループ数"
  type        = number
}

variable "security_groups" {
  description = "ElastiCache for Redisに適用する、セキュリティグループ"
  type        = list(string)
}

variable "family" {
  description = "ElastiCache for Redisのパラメーターグループを作成する際のテンプレートとなる、パラメーターグループファミリー"
  type        = string
}

variable "parameters" {
  description = "パラメーターグループに指定する、パラメーターのリスト（name, value）"
  type        = list(map(string))
}

variable "subnets" {
  description = "ElastiCache for Redisの配置先となるサブネットID"
  type        = list(string)
}
