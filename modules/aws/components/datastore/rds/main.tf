resource "aws_db_instance" "this" {
  name = var.name

  identifier = var.identifier

  engine         = var.engine
  engine_version = var.engine_version

  port = var.port

  instance_class        = var.instance_class
  storage_type          = var.storage_type
  allocated_storage     = var.allocated_storage
  max_allocated_storage = var.max_allocated_storage
  iops                  = var.iops

  username = var.username
  password = var.password

  availability_zone   = var.availability_zone
  multi_az            = var.multi_az
  publicly_accessible = false

  vpc_security_group_ids = var.security_groups

  allow_major_version_upgrade = var.allow_major_version_upgrade
  auto_minor_version_upgrade  = var.auto_minor_version_upgrade

  maintenance_window = var.maintenance_window

  apply_immediately = var.apply_immediately

  skip_final_snapshot       = var.skip_final_snapshot
  final_snapshot_identifier = var.final_snapshot_identifier
  copy_tags_to_snapshot     = var.copy_tags_to_snapshot

  deletion_protection = var.deletion_protection

  db_subnet_group_name = aws_db_subnet_group.this.id
  parameter_group_name = aws_db_parameter_group.this.id
  option_group_name    = aws_db_option_group.this.id

  character_set_name = var.character_set_name

  ## TODO
  # replicate_source_db = var.replicate_source_db

  snapshot_identifier = var.snapshot_identifier

  # replicate_source_db = var.replicate_source_db  ## TODO: リードレプリカは現時点で未サポート

  monitoring_interval = var.monitoring_interval
  monitoring_role_arn = var.monitoring_role_arn

  performance_insights_enabled          = var.performance_insights_enabled
  performance_insights_kms_key_id       = var.performance_insights_enabled == true ? var.performance_insights_kms_key_id : null
  performance_insights_retention_period = var.performance_insights_enabled == true ? var.performance_insights_retention_period : null

  enabled_cloudwatch_logs_exports = var.exportable_cloudwatch_logs

  storage_encrypted = var.storage_encrypted
  kms_key_id        = var.kms_key_id

  ca_cert_identifier = var.ca_cert_identifier

  backup_retention_period = var.backup_retention_period
  backup_window           = var.backup_window

  # delete_automated_backups = var.delete_automated_backups

  timezone = var.mssql_timezone

  license_model = var.license_model

  tags = merge(
    {
      "Name" = var.identifier
    },
    var.tags
  )

  lifecycle {
    ignore_changes = [password] # マスターパスワード変更の差分は無視する
  }
}

resource "aws_db_subnet_group" "this" {
  name       = var.db_subnet_group_name
  subnet_ids = var.db_subnets

  tags = merge(
    {
      "Name" = var.db_subnet_group_name
    },
    var.tags
  )
}

resource "aws_db_parameter_group" "this" {
  name   = var.parameter_group_name
  family = var.parameter_group_family

  tags = merge(
    {
      "Name" = var.parameter_group_name
    },
    var.tags
  )

  dynamic "parameter" {
    for_each = var.parameters

    # TODO https://github.com/terraform-providers/terraform-provider-aws/issues/13551
    # 作成後のアップデートができない…

    content {
      name         = parameter.value.name
      value        = parameter.value.value
      apply_method = lookup(parameter.value, "apply_method", null)
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_db_option_group" "this" {
  name                 = var.option_group_name
  engine_name          = var.option_group_engine_name
  major_engine_version = var.option_group_major_engine_version

  tags = merge(
    {
      "Name" = var.option_group_name
    },
    var.tags
  )

  dynamic "option" {
    for_each = var.option_group_options

    content {
      option_name                    = option.value.option_name
      port                           = lookup(option.value, "port", null)
      version                        = lookup(option.value, "version", null)
      db_security_group_memberships  = lookup(option.value, "db_security_group_memberships", null)
      vpc_security_group_memberships = lookup(option.value, "vpc_security_group_memberships", null)

      dynamic "option_settings" {
        for_each = lookup(option.value, "option_settings", [])

        content {
          name  = lookup(option_settings.value, "name", null)
          value = lookup(option_settings.value, "value", null)
        }
      }
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}
