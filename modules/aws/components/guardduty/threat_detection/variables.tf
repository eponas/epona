variable "create" {
  description = "GuardDutyを作成する場合、true"
  type        = bool
  default     = true
}

variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "finding_publishing_frequency" {
  description = "既存の結果の再検出時に通知する頻度。選択可能な値は、[GuardDuty APIリファレンス](https://docs.aws.amazon.com/guardduty/latest/APIReference/API_CreateDetector.html#API_CreateDetector_RequestSyntax)を参照してください。"
  type        = string
  default     = "SIX_HOURS"
}
