variable "log_group_name" {
  type        = string
  description = "CloudWatch Logsのロググループの名前を指定する。"

}

variable "log_group_retention_in_days" {
  type        = number
  description = "CloudWatch Logsのロググループに記録したログの保持期間を指定する。"
}

variable "log_group_kms_key_id" {
  type        = string
  default     = null
  description = "CloudWatch Logsのログを記録する際に暗号化する場合、Key Management Sserviceのkeyのidを指定する。"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}

variable "policy_name_prefix" {
  type        = string
  description = "CloudWatch Logs用のIAMポリシーの名前のプレフィックスを指定する。"
}
