output "resource_arn" {
  value       = module.log.log_group_arn
  description = "CloudWatch LogsグループのARN"
}

output "policy_arn" {
  value       = aws_iam_policy.cloudwatch_logs_policy.arn
  description = "ステートマシンでCloudWatch Logsを操作するためにアタッチするポリシーのARN"
}
