data "aws_iam_policy_document" "put_cloudwatch_log_group" {
  statement {
    sid = "CloudWatchLogsGroup"
    actions = [
      "logs:CreateLogDelivery",
      "logs:GetLogDelivery",
      "logs:UpdateLogDelivery",
      "logs:DeleteLogDelivery",
      "logs:ListLogDeliveries",
      "logs:DescribeResourcePolicies",
      "logs:DescribeLogGroups",
      "logs:PutResourcePolicy",
    ]
    resources = ["*"]
  }

  statement {
    sid = "LogDeliveryWrite"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:PutLogEventBatch"
    ]
    resources = [
      "${module.log.log_group_arn}:*"
    ]
  }
}

resource "aws_iam_policy" "cloudwatch_logs_policy" {
  name   = "${var.policy_name_prefix}CloudWatchLogsPolicy"
  path   = "/service-role/"
  policy = data.aws_iam_policy_document.put_cloudwatch_log_group.json
  tags   = var.tags
}

module "log" {
  source                      = "../../cloudwatch/log"
  log_group_name              = var.log_group_name
  log_group_retention_in_days = var.log_group_retention_in_days
  log_group_kms_key_id        = var.log_group_kms_key_id
  tags                        = var.tags
}
