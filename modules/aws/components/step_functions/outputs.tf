output "role_arn" {
  description = "ステートマシンを実行する時に使うロールのARNを返す。"
  value       = aws_iam_role.sfn.arn
}

output "role_name" {
  description = "ステートマシンを実行する時に使うロールの名前を返す。"
  value       = aws_iam_role.sfn.name
}

output "arn" {
  description = "ステートマシンのARNを返す。"
  value       = aws_sfn_state_machine.sfn_state_machine.arn
}
