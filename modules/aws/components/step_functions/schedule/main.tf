data "aws_iam_policy_document" "assume_policy" {
  statement {
    sid     = "EventBridgeAssumeRolePolicy"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "events.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "start_sfn" {
  statement {
    sid     = "StartSfn"
    actions = ["states:StartExecution"]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "start_sfn" {
  count  = length(var.schedule_configs) == 0 ? 0 : 1
  name   = "StartStatePolicyFromSchedule_${var.sfn_name}"
  path   = "/service-role/"
  policy = data.aws_iam_policy_document.start_sfn.json
  tags   = var.tags
}

resource "aws_iam_role" "start_sfn" {
  count              = length(var.schedule_configs) == 0 ? 0 : 1
  name               = "StartStateRoleFromSchedule_${var.sfn_name}"
  assume_role_policy = data.aws_iam_policy_document.assume_policy.json
  path               = "/service-role/"
  managed_policy_arns = [
    aws_iam_policy.start_sfn[0].arn
  ]
  tags = var.tags
}


module "schedule" {
  source              = "../../../components/eventbridge/schedule"
  count               = length(var.schedule_configs)
  schedule_expression = lookup(var.schedule_configs[count.index], "schedule_expression")
  name                = "SfnStartSchedule_${var.sfn_name}_${format("%03d", count.index)}"
  target_arn          = var.sfn_arn
  role_arn            = aws_iam_role.start_sfn[0].arn
  is_enabled          = lookup(var.schedule_configs[count.index], "is_enabled", false)
  tags                = var.tags
}
