variable "sfn_name" {
  type        = string
  description = "実行したいステートマシンの名前を指定する。"
}

variable "sfn_arn" {
  type        = string
  description = "実行したいステートマシンのARNを指定する。"
}

variable "schedule_configs" {
  type        = list(any)
  default     = []
  description = <<EOF
スケジュールの設定を配列で指定する。"

例

```
   schedule_configs = [
     {
       schedule_expression = "rate(5 minutes)"
       is_enabled          = false
     },
     {
       schedule_expression = "rate(10 minutes)"
       is_enabled          = false
     }
  
   ]
```

EOF

}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}
