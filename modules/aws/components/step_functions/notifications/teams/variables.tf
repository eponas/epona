variable "name_prefix" {
  type        = string
  description = "EventBridge API Connectionsの名称のプレフィックスを指定する。"
}

variable "sfn_machine_arn" {
  type        = string
  description = "対象になるStep FunctionsステートマシンのARNを指定する。"
}

variable "webhook_uri" {
  type        = string
  description = "Microsoft Teamsのwebhook uriを指定する。"
}

variable "input_transformer_input_paths" {
  type        = string
  default     = null
  description = <<EOF
Eventを表現するJSONから値を抽出し、`input_transformer_input_template`で指定するテンプレートへ使用できる変数を定義する。

例:
```
{
  instance = "$.detail.instance",
  status   = "$.detail.status",
}
```

上記の例だと、`<instance>`、`<status>`がテンプレート中で使用できる。詳細は[Tutorial: Use Input Transformer to Customize What is Passed to the Event Target](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/CloudWatch-Events-Input-Transformer-Tutorial.html)を参照。

なお、本パラメータに`null`を指定した場合は以下が設定される。
```
{
  "id":"$.id",
  "status": "$.detail.status",
  "detail-type": "$.detail-type",
  "stateMachineArn": "$.detail.stateMachineArn",
  "time": "$.time"
}
```
EOF
}

variable "input_transformer_input_template" {
  type        = string
  default     = null
  description = <<EOF
  Microsoft Teamsに対する通知テンプレートを指定する。テンプレートには、`input_transformer_input_paths`で定義した変数が利用できる。
  詳細は[Tutorial: Use Input Transformer to Customize What is Passed to the Event Target](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/CloudWatch-Events-Input-Transformer-Tutorial.html)を参照。

  なお、Microsoft Teamsへの通知内容はさまざまなレイアウトをすることが可能。詳細は以下を参照。

  - [従来の操作可能なメッセージカードリファレンス](https://docs.microsoft.com/ja-jp/outlook/actionable-messages/message-card-reference)
  - [アダプティブカード形式が使用されるOutlookのアクション可能メッセージカードのデザイン](https://docs.microsoft.com/ja-jp/outlook/actionable-messages/adaptive-card)

  本パラメータが`null`の場合は、以下の内容が設定される。
```
{
    "@type": "MessageCard",
    "@context": "http://schema.org/extensions",
    "themeColor": "0076D7",
    "summary": "StepFunctions StateMachine Status",
    "sections": [{
        "activityTitle": "StepFunctions StateMachine Status",
        "activitySubtitle": "by Epona",
        "activityImage": "https://teamsnodesample.azurewebsites.net/static/img/image5.png",
        "facts": [{
            "name": "id",
            "value": "<id>"
        }, {
            "name": "State Machine Arn",
            "value": "<stateMachineArn>"
        }, {
            "name": "Date",
            "value": "<time>"
        }, {
            "name": "Status",
            "value": "<status>"
        }],
        "markdown": true
    }]
}
```
  EOF
}

variable "event_pattern_status" {
  type        = list(string)
  default     = ["SUCCEEDED", "FAILED", "TIMED_OUT", "ABORTED", "RUNNING"]
  description = <<EOF
イベントパターンで通知するイベントを指定する。
指定がない場合はすべてのイベントが発行された際に通知するようになる。
指定できるイベントは `SUCCEEDED`, `FAILED`, `TIMED_OUT`, `ABORTED`, `RUNNING`。複数指定も可能。

EOF

}

variable "event_bus_name" {
  type        = string
  default     = "default"
  description = "イベントバスを指定する。指定がない場合は `default` が使われる。"

}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}
