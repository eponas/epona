locals {

  event_pattern_status_tmp = var.event_pattern_status == null ? ["SUCCEEDED", "FAILED", "TIMED_OUT", "ABORTED", "RUNNING"] : var.event_pattern_status

  event_pattern_tmp = <<EOF
{
  "source": ["aws.states"],
  "detail-type": ["Step Functions Execution Status Change"],
  "detail": {
    "stateMachineArn": ["${var.sfn_machine_arn}"],
    "status": ${jsonencode(local.event_pattern_status_tmp)}
  }
}
EOF

  # 本設定値は variables.tf の input_transformer_input_paths の description と同期を取ること
  input_paths_tmp = <<EOF
{
  "id":"$.id",
  "status": "$.detail.status",
  "detail-type": "$.detail-type",
  "stateMachineArn": "$.detail.stateMachineArn",
  "time": "$.time"
}
EOF

  # 本設定値は variables.tf の input_transformer_input_template の description と同期を取ること
  input_template_tmp = <<EOF
{
    "attachments": [
        {
            "fallback": "<detail-type>",
            "color": "#0099FF",
            "title": "StateMachine Status",
            "fields": [
                {
                    "title": "id",
                    "value": "<id>"
                },
                {
                    "title": "arn",
                    "value": "`<stateMachineArn>`"
                },
                {
                    "title": "Status",
                    "value": "*<status>*"
                },
                {
                    "title": "time",
                    "value": "<time>"
                }
            ],
            "footer": "by Epona"
        }
    ]
}
EOF

}

module "slack" {
  source           = "../../../eventbridge/notifications"
  name_prefix      = var.name_prefix
  rule_description = "sfn slack notification(${data.aws_arn.sfn.resource})"
  api_connection = {
    webhook_uri = var.webhook_uri
    method      = "POST"
  }
  input_transformer = {
    input_paths    = var.input_transformer_input_paths == null ? local.input_paths_tmp : var.input_transformer_input_paths
    input_template = var.input_transformer_input_template == null ? local.input_template_tmp : var.input_transformer_input_template
  }
  event_pattern  = local.event_pattern_tmp
  event_bus_name = var.event_bus_name
  tags           = var.tags
}


data "aws_arn" "sfn" {
  arn = var.sfn_machine_arn
}
