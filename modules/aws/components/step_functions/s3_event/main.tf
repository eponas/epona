locals {

  prefix                 = var.filter_prefix == null ? "" : var.filter_prefix
  request_parameters_tmp = <<EOF
{
    "bucketName":  ["${var.bucket_name}"],
    "key":  [{ "prefix": "${local.prefix}"}]
  }
EOF

  bucket_only = <<EOF
{"bucketName": ["${var.bucket_name}"] }
EOF

  json = {
    source      = ["aws.s3"]
    detail-type = ["AWS API Call via CloudTrail"]
    detail = {
      errorCode         = [{ "exists" = false }]
      errorMessage      = [{ "exists" = false }]
      eventSource       = ["s3.amazonaws.com"]
      eventName         = var.events
      requestParameters = jsondecode(var.filter_prefix == null ? local.bucket_only : local.request_parameters_tmp)
    }
  }
}

module "start_event" {
  source        = "../../../components/eventbridge/event"
  name          = "StartStateFromS3Event_${var.name_uniq}"
  event_pattern = jsonencode(local.json)
  tags          = var.tags
}


data "aws_iam_policy_document" "assume_policy" {
  statement {
    sid     = "EventBridgeAssumeRolePolicy"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "events.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "start_sfn" {
  statement {
    sid     = "StartSfn"
    actions = ["states:StartExecution"]
    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "start_sfn" {
  name   = "StartStatePolicyFromS3Event_${var.name_uniq}"
  path   = "/service-role/"
  policy = data.aws_iam_policy_document.start_sfn.json
  tags   = var.tags
}

resource "aws_iam_role" "start_sfn" {
  name               = "StartStateRoleFromS3Event_${var.name_uniq}"
  assume_role_policy = data.aws_iam_policy_document.assume_policy.json
  path               = "/service-role/"
  managed_policy_arns = [
    aws_iam_policy.start_sfn.arn
  ]
  tags = var.tags
}

resource "aws_cloudwatch_event_target" "target" {
  target_id = "StateStateTarget_${var.name_uniq}"
  rule      = module.start_event.name
  arn       = var.step_functions_machine_arn
  role_arn  = aws_iam_role.start_sfn.arn
}
