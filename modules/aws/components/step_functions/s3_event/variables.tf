variable "bucket_name" {
  type        = string
  description = "ステートマシンを実行するトリガーにしたいS3バケットを指定する。"
}

variable "name_uniq" {
  type        = string
  description = "ロールなどに付ける名前をユニークにするための文字列を指定する。"
}

variable "step_functions_machine_arn" {
  type        = string
  description = "実行したいステートマシンのARNを指定する。"
}

variable "events" {
  type        = list(string)
  default     = []
  description = <<EOF
発火させるイベントを指定する。
イベントの種類は[AWS CloudTrailのログ記録によって追跡されるAmazon S3オブジェクトレベルのアクション](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/cloudtrail-logging-s3-info.html#cloudtrail-object-level-tracking)を参照。
EOF
}

variable "filter_prefix" {
  type        = string
  default     = null
  description = "S3オブジェクト名のプレフィックスを指定する。"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}
