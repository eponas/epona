variable "name_prefix" {
  description = "X-Ray実行ロール名のプレフィックスを指定する。"
  type        = string
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}
