output "arn" {
  description = "X-Ray実行ポリシーのARNを返す。"
  value       = aws_iam_policy.xray.arn
}

output "name" {
  description = "X-Ray実行ポリシーの名前を返す。"
  value       = aws_iam_policy.xray.name
}
