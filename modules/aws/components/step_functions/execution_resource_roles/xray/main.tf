# see: https://docs.aws.amazon.com/step-functions/latest/dg/xray-iam.html
data "aws_iam_policy_document" "xray" {
  statement {
    sid    = "SFNAssumeRolePolicy"
    effect = "Allow"
    actions = [
      "xray:PutTraceSegments",
      "xray:PutTelemetryRecords",
      "xray:GetSamplingRules",
      "xray:GetSamplingTargets"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "xray" {
  name   = "${var.name_prefix}-XrayExecutionPolicy"
  policy = data.aws_iam_policy_document.xray.json
  path   = "/service-role/"
  tags   = var.tags
}
