locals {
  # LambdaのARNにエイリアスがついている場合（splitの要素数が3の場合）、エイリアスを除いた部分を取得する
  temp_lambda_arn_list = split(":", data.aws_arn.this.resource)
  lambda_role_arn      = length(local.temp_lambda_arn_list) == 3 ? join(":", slice(local.temp_lambda_arn_list, 0, length(local.temp_lambda_arn_list) - 1)) : data.aws_arn.this.resource
}

data "aws_arn" "this" {
  arn = var.arn
}

module "iam_execution_role_lambda" {
  source        = "./lambda"
  count         = data.aws_arn.this.service == "lambda" ? 1 : 0
  arn           = var.arn
  sfn_role_name = var.sfn_role_name
  name_prefix   = "${var.sfn_name}_${replace(local.lambda_role_arn, "function:", "")}"
  tags          = var.tags
}

module "iam_execution_role_ecs" {
  source        = "./ecs"
  count         = data.aws_arn.this.service == "ecs" ? 1 : 0
  arn           = var.arn
  sfn_role_name = var.sfn_role_name
  region        = var.region
  name_prefix   = "${var.sfn_name}_${replace(data.aws_arn.this.resource, "task-definition/", "")}"
  tags          = var.tags
}

module "iam_execution_role_sns" {
  source        = "./sns"
  count         = data.aws_arn.this.service == "sns" ? 1 : 0
  arn           = var.arn
  sfn_role_name = var.sfn_role_name
  name_prefix   = "${var.sfn_name}_${data.aws_arn.this.resource}"
  tags          = var.tags
}

module "iam_execution_role_sqs" {
  source        = "./sqs"
  count         = data.aws_arn.this.service == "sqs" ? 1 : 0
  arn           = var.arn
  sfn_role_name = var.sfn_role_name
  name_prefix   = "${var.sfn_name}_${data.aws_arn.this.resource}"
  tags          = var.tags
}
