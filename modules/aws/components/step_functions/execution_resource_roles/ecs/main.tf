data "aws_caller_identity" "current" {}

locals {
  name_prefix = replace(var.name_prefix, "/\\W|_/", "")
}


# SA https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/ecs-iam.html
data "aws_iam_policy_document" "iam" {

  # for sync and  waitForTaskToken
  statement {
    sid = "synkTask${local.name_prefix}"
    actions = [
      "ecs:RunTask",
    ]
    resources = [
      var.arn
    ]
  }

  # for sync
  statement {
    sid = "stopTask${local.name_prefix}"
    actions = [
      "ecs:StopTask",
      "ecs:DescribeTasks",
    ]
    resources = [
      "*"
    ]
  }
  statement {
    sid = "events"
    actions = [
      "events:PutTargets",
      "events:PutRule",
      "events:DescribeRule",
    ]
    resources = [
      "arn:aws:events:${var.region}:${data.aws_caller_identity.current.account_id}:rule/StepFunctionsGetEventsForECSTaskRule"
    ]
  }

}

resource "aws_iam_policy" "ecs_execution_role" {
  policy = data.aws_iam_policy_document.iam.json
  name   = "StepFunctionsToEcs_${local.name_prefix}_Policy"
  tags   = var.tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_iam_role_policy_attachment" "policy" {
  role       = var.sfn_role_name
  policy_arn = aws_iam_policy.ecs_execution_role.arn
}
