locals {
  name_prefix = replace(var.name_prefix, "/\\W|_/", "")
}

# see: https://docs.aws.amazon.com/step-functions/latest/dg/sns-iam.html

resource "aws_iam_role_policy_attachment" "sns_publish" {
  role       = var.sfn_role_name
  policy_arn = aws_iam_policy.sns_publish.arn
}

resource "aws_iam_policy" "sns_publish" {
  name        = "${local.name_prefix}_SnsTopicPulishPolicy"
  path        = "/"
  description = "IAM policy StepFunction publish sns topic"

  policy = jsonencode({
    "Version" = "2012-10-17",
    "Statement" = [
      {
        "Action"   = ["sns:Publish"],
        "Resource" = var.arn
        "Effect"   = "Allow"
      }
    ] }
  )

  lifecycle {
    create_before_destroy = true
  }

  tags = var.tags
}
