variable "arn" {
  type        = string
  description = "SNS TopicのARNを指定する。"
}

variable "sfn_role_name" {
  type        = string
  description = "ステートマシンの実行IAMロール名を指定する。"
}

variable "name_prefix" {
  type        = string
  description = "IAMポリシーを作成する際の名前のプレフィックスを指定する。"

}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}
