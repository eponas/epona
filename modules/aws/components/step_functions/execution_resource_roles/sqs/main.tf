# see: https://docs.aws.amazon.com/step-functions/latest/dg/sqs-iam.html

locals {
  name_prefix = replace(var.name_prefix, "/\\W|_/", "")
}

resource "aws_iam_role_policy_attachment" "sqs" {
  role       = var.sfn_role_name
  policy_arn = aws_iam_policy.sqs.arn
}

resource "aws_iam_policy" "sqs" {
  name        = "${local.name_prefix}_SqsPolicy"
  path        = "/"
  description = "IAM policy StepFunction To Sqs"

  policy = jsonencode({
    "Version" = "2012-10-17",
    "Statement" = [
      {
        "Action"   = ["sqs:SendMessage"],
        "Resource" = var.arn
        "Effect"   = "Allow"
      }
    ] }
  )

  lifecycle {
    create_before_destroy = true
  }

  tags = var.tags
}
