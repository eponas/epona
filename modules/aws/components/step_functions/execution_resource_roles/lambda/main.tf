# see: https://docs.aws.amazon.com/step-functions/latest/dg/lambda-iam.html
resource "aws_iam_role_policy_attachment" "lambda_invoke" {
  role       = var.sfn_role_name
  policy_arn = aws_iam_policy.lambda_invoke.arn
}

resource "aws_iam_policy" "lambda_invoke" {
  name        = "${var.name_prefix}_LambdaInvokeFromSfnPolicy"
  path        = "/"
  description = "IAM policy StepFunction invokes Lambda Function"

  policy = jsonencode({
    "Version" = "2012-10-17",
    "Statement" = [
      {
        "Action"   = ["lambda:InvokeFunction"],
        "Resource" = var.arn
        "Effect"   = "Allow"
      }
    ] }
  )

  lifecycle {
    create_before_destroy = true
  }

  tags = var.tags
}
