variable "arn" {
  type        = string
  description = "ステートマシンから実行するリソースのARNを指定する。"
}

variable "sfn_name" {
  type        = string
  description = "ステートマシンの名前を指定する。"
}

variable "sfn_role_name" {
  type        = string
  description = "ステートマシンの実行IAMロールの名称を指定する。"
}

variable "region" {
  type        = string
  description = "リージョンを指定する。"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}
