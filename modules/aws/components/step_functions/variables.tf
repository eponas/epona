variable "sfn_name" {
  type        = string
  description = "Step Functionのステートマシンの名前を指定する。"
}

variable "definition_json" {
  type        = string
  description = "Step Functionsの実行対象となる、ステートマシンのJSON定義を指定する。"
}

variable "region" {
  type        = string
  description = "ステートマシンを実行するリージョンを指定する。"
}

variable "log_group_name" {
  type        = string
  description = "ロググループの名前を指定する。"
}

variable "log_level" {
  type        = string
  description = <<EOF
CloudWatch Logsに書き出すログのログレベルとして、`OFF`、`ALL`、`ERROR`、`FATAL`を指定可能。
詳細は[ログレベル](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/cloudwatch-log-level.html)を参照のこと。
EOF
}

variable "log_group_retention_in_days" {
  type        = number
  description = "CloudWatch Logsにログを残す日数を指定する。デフォルトは14日間"
  default     = 14
}

variable "enabled_xray" {
  type        = bool
  description = "X-Rayを利用するか指定する。デフォルトでは利用しない。"
  default     = false

}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}
