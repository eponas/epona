data "aws_iam_policy_document" "sfn" {
  statement {
    sid     = "SFNAssumeRolePolicy"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "states.${var.region}.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role" "sfn" {
  name               = "StateMachineExecutionRole_${var.sfn_name}"
  assume_role_policy = data.aws_iam_policy_document.sfn.json
  path               = "/service-role/"
  tags               = var.tags
}


# https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/concept-create-iam-advanced.html
data "aws_iam_policy_document" "sfn2" {
  statement {
    sid = "event"
    actions = [
      "events:PutTargets",
      "events:PutRule",
      "events:DescribeRule"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    sid = "passrole"
    actions = [
      "iam:PassRole"
    ]
    resources = [
      "arn:aws:iam::*:role/ecsTaskExecutionRole"
    ]
  }
}

resource "aws_iam_policy" "event" {
  policy = data.aws_iam_policy_document.sfn2.json
  name   = "AdditionalPolicyForSfn__${var.sfn_name}"
  tags   = var.tags
}

resource "aws_iam_role_policy_attachment" "sfn2" {
  role       = aws_iam_role.sfn.name
  policy_arn = aws_iam_policy.event.arn
}

module "xray_iam_policy" {
  source      = "./execution_resource_roles/xray"
  count       = var.enabled_xray == true ? 1 : 0
  name_prefix = "${var.sfn_name}-XrayRole"
  tags        = var.tags
}

resource "aws_iam_role_policy_attachment" "xray" {
  count      = var.enabled_xray == true ? 1 : 0
  role       = aws_iam_role.sfn.name
  policy_arn = module.xray_iam_policy[count.index].arn
}

module "cloudwatch_log_group" {
  count                       = var.log_group_name == null ? 0 : 1
  source                      = "./cloudwatch_logs"
  log_group_name              = var.log_group_name
  log_group_retention_in_days = var.log_group_retention_in_days
  log_group_kms_key_id        = ""
  tags                        = var.tags
  policy_name_prefix          = var.sfn_name
}

resource "aws_iam_role_policy_attachment" "log_group" {
  count      = var.log_group_name == null ? 0 : 1
  role       = aws_iam_role.sfn.name
  policy_arn = module.cloudwatch_log_group[0].policy_arn
  depends_on = [
    module.cloudwatch_log_group
  ]
}

resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = var.sfn_name
  role_arn = aws_iam_role.sfn.arn

  definition = var.definition_json

  # set log level to "OFF" if log_destination is null
  logging_configuration {
    log_destination        = var.log_group_name == null ? null : "${module.cloudwatch_log_group[0].resource_arn}:*"
    include_execution_data = true
    level                  = var.log_group_name == null ? "OFF" : var.log_level # ALL/ERROR/FATAL/OFF
  }


  tracing_configuration {
    enabled = var.enabled_xray
  }

  depends_on = [
    aws_iam_role_policy_attachment.log_group
  ]

  tags = var.tags
}
