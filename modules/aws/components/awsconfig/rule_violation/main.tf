resource "aws_config_configuration_recorder" "aws_config" {
  count    = var.create_config_recorder_delivery_channel ? 1 : 0
  name     = var.aws_config_recorder_name
  role_arn = var.aws_config_role_arn
  recording_group {
    all_supported                 = var.all_supported
    resource_types                = var.recording_resource_types
    include_global_resource_types = var.include_grobal_resources
  }
}

resource "aws_s3_bucket" "aws_config" {
  bucket        = var.s3_bucket_name
  acl           = "private"
  force_destroy = var.s3_bucket_force_destroy
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  dynamic "lifecycle_rule" {
    for_each = length(var.bucket_transitions) > 0 ? ["dummy"] : []
    content {
      enabled = true

      dynamic "transition" {
        for_each = var.bucket_transitions
        content {
          days          = transition.value["days"]
          storage_class = transition.value["storage_class"]
        }
      }
      # Versioning を有効化しているため、expiration は設定できない
    }
  }

  # ignore version argument
  lifecycle {
    ignore_changes = [
      versioning
    ]
  }
}

resource "aws_s3_bucket_public_access_block" "aws_config" {
  bucket                  = aws_s3_bucket.aws_config.bucket
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_config_delivery_channel" "aws_config" {
  count          = var.create_config_recorder_delivery_channel ? 1 : 0
  name           = var.aws_config_channel_name
  s3_bucket_name = aws_s3_bucket.aws_config.bucket
  sns_topic_arn  = aws_sns_topic.notification_topic.arn
  snapshot_delivery_properties {
    delivery_frequency = var.delivery_frequency
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
}

resource "aws_config_configuration_recorder_status" "aws_config" {
  count      = var.create_config_recorder_delivery_channel ? 1 : 0
  name       = var.aws_config_recorder_name
  is_enabled = var.recorder_status
  depends_on = [aws_config_configuration_recorder.aws_config, aws_config_delivery_channel.aws_config]
}

resource "aws_sns_topic" "notification_topic" {
  name = var.notification_topic_name
}

resource "aws_sns_topic_subscription" "aws_config_sub" {
  topic_arn = aws_sns_topic.notification_topic.arn
  protocol  = "lambda"
  endpoint  = var.aws_config_lambda_arn
}
