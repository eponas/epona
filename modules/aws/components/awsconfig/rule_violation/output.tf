
output "aws_config_bucket_arn" {
  description = "AWS Configがリソースの設定変更の詳細を送信するS3バケットのARNを設定する。"
  value       = aws_s3_bucket.aws_config.arn
}

output "aws_config_bucket_id" {
  description = "AWS Configがリソースの設定変更の詳細を送信するS3バケットのIDを設定する。"
  value       = aws_s3_bucket.aws_config.id
}

output "aws_config_bucket" {
  description = "AWS Configがリソースの設定変更の詳細を送信するS3バケット名を設定する。"
  value       = aws_s3_bucket.aws_config.bucket
}

output "notification_topic_arn" {
  description = "AWS Configがリソースの設定変更またはルール違反を通知するSNSトピックのARNを設定する。"
  value       = aws_sns_topic.notification_topic.arn
}
