data "aws_route53_zone" "this" {
  name = var.zone_name
}

resource "aws_route53_record" "this" {
  name    = var.record_name
  type    = "A"
  zone_id = data.aws_route53_zone.this.id

  alias {
    evaluate_target_health = true
    name                   = lookup(var.alias_target, "dns_name", null)
    zone_id                = lookup(var.alias_target, "zone_id", null)
  }
}
