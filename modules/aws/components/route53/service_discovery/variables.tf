variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
}

variable "vpc_id" {
  description = "Service Discoveryを関連付けるVPCのID"
  type        = string
}

variable "namespace_name" {
  description = "Service DiscoveryのNamespaceの名前（ドメイン名になる）"
  type        = string
}

variable "service_name" {
  description = "Service DiscoveryのNamespaceに作成するサービス名（サブドメイン名になる）"
  type        = string
}
