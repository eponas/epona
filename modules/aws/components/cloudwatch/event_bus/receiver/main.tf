# 他のAWSアカウントからevent bue経由でのイベント受信を許可する
resource "aws_cloudwatch_event_permission" "accounts" {
  principal    = var.principal
  statement_id = "EventBusAccessFromAccount${var.principal}"
}
