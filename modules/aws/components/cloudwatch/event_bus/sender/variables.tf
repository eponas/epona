variable "target_event_bus_arn" {
  description = "送信先となるEventBusのARN"
  type        = string
}

variable "cloudwatch_event_rule_names" {
  description = "イベントルール名のリスト。これらのイベントが発火すると、EventBusに当該イベントが送信される"
  type        = list(string)
}

variable "role_arn" {
  description = "EventBusに送信する際に利用されるRoleのARN"
  type        = string
}
