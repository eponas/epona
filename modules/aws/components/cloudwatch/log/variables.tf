variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "log_group_name" {
  description = "ロググループ名"
  type        = string
  default     = null
}

variable "lambda_function_name" {
  description = "Lambda関数名。Lambda用のロググループ作成する場合、Lambda関数名を設定すると、命名規則に則ったロググループ名を生成する"
  type        = string
  default     = null
}

variable "log_group_retention_in_days" {
  description = <<-DESCRIPTION
  ログをCloudWatch Logsに出力する場合の保持期間を設定する。

  値は、次の範囲の値から選ぶこと： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.
  [Resource: aws_cloudwatch_log_group / retention_in_days](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group#retention_in_days)
  DESCRIPTION
  type        = number
  default     = null
}

variable "log_group_kms_key_id" {
  description = "ログデータを暗号化するための、KMS CMKのARNを指定する"
  type        = string
}