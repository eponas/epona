variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "name_suffix" {
  description = "Kinesis Data Firehoseの名前に付ける接尾語"
  type        = string
}

variable "prefix" {
  description = "S3に配置する際にパスに付与するPrefix"
  type        = string
  default     = null
}

variable "compression_format" {
  description = "S3に配置するログの圧縮フォーマット（`無圧縮(null)` or `GZIP` or `ZIP` or `Snappy`）"
  type        = string
  default     = null
}

variable "iam_role_arn" {
  description = "Kinesis Data FirehoseにアタッチするIAM RoleのARN"
  type        = string
}

variable "bucket_arn" {
  description = "Kinesis Data Firehoseの出力先にするS3バケットのARN"
  type        = string
}

variable "waf_webacl_arn" {
  description = "Kinesis Data Firehoseに関連付けるWebACLのARN"
  type        = string
}
