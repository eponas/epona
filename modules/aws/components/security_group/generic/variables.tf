variable "name" {
  description = "作成するセキュリティグループの名前"
  type        = string
}

variable "tags" {
  description = "セキュリティグループに付与するタグ"
  type        = map(string)
}

variable "create" {
  description = "セキュリティグループを作成する場合、true"
  type        = bool
  default     = true
}

variable "vpc_id" {
  description = "セキュリティグループの作成対象となる、VPCのID"
  type        = string
}

variable "ingresses" {
  description = "CIDR BLOCKを対象とするingressのリスト"
  type = list(object({
    port        = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  default = []
}

variable "sg_ingresses" {
  description = "SecurityGroupを対象とするingressのリスト"
  type = list(object({
    port              = number
    protocol          = string
    security_group_id = string
    description       = string
  }))
  default = []
}

variable "egresses" {
  description = "CIDR BLOCKを対象とするegressのリスト"
  type = list(object({
    port        = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  default = []
}

variable "sg_egresses" {
  description = "SecurityGroupを対象とするegressのリスト"
  type = list(object({
    port              = number
    protocol          = string
    security_group_id = string
    description       = string
  }))
  default = []
}
