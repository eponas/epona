resource "aws_security_group" "this" {
  count = var.create ? 1 : 0

  name   = var.name
  vpc_id = var.vpc_id

  tags = merge(
    {
      "Name" = var.name
    },
    var.tags
  )
}

resource "aws_security_group_rule" "ingress" {
  for_each = var.create ? { for ingress in var.ingresses : join(":", [ingress.protocol, ingress.port, join("-", ingress.cidr_blocks)]) => ingress } : {}

  type              = "ingress"
  from_port         = each.value.port
  to_port           = each.value.port
  protocol          = each.value.protocol
  cidr_blocks       = each.value.cidr_blocks
  description       = each.value.description
  security_group_id = aws_security_group.this[0].id
}

# security groupを指定するingress
resource "aws_security_group_rule" "sg_ingress" {
  for_each = var.create ? { for ingress in var.sg_ingresses : join(":", [ingress.protocol, ingress.port, ingress.security_group_id]) => ingress } : {}

  type                     = "ingress"
  from_port                = each.value.port
  to_port                  = each.value.port
  protocol                 = each.value.protocol
  source_security_group_id = each.value.security_group_id
  description              = each.value.description
  security_group_id        = aws_security_group.this[0].id
}

resource "aws_security_group_rule" "egress" {
  for_each = var.create ? { for egress in var.egresses : join(":", [egress.protocol, egress.port, join("-", egress.cidr_blocks)]) => egress } : {}

  type              = "egress"
  from_port         = each.value.port
  to_port           = each.value.port
  protocol          = each.value.protocol
  cidr_blocks       = each.value.cidr_blocks
  description       = each.value.description
  security_group_id = aws_security_group.this[0].id
}

# security groupを指定するegress定義
resource "aws_security_group_rule" "sg_egress" {
  for_each = var.create ? { for egress in var.sg_egresses : join(":", [egress.protocol, egress.port, egress.security_group_id]) => egress } : {}

  type                     = "egress"
  from_port                = each.value.port
  to_port                  = each.value.port
  protocol                 = each.value.protocol
  source_security_group_id = each.value.security_group_id
  description              = each.value.description
  security_group_id        = aws_security_group.this[0].id
}
