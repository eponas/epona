data "aws_caller_identity" "current" {}

locals {
  # Terraform実行ロールの"role-id"を抽出
  terraformer_role_id = split(":", data.aws_caller_identity.current.user_id)[0]
}

# CloudTrailがBucketにログファイルを書き込むことを許可する
# see: https://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/create-s3-bucket-policy-for-cloudtrail.html
# 上記から、セキュリティ向上の設定を追加
data "aws_iam_policy_document" "cloudtrail" {
  statement {
    sid = "AWSCloudTrailAclCheck20150319"
    actions = [
      "s3:GetBucketAcl",
    ]
    resources = [
      "arn:aws:s3:::${var.bucket_name}",
    ]

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }

  statement {
    sid = "AWSCloudTrailWrite20150319"
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${var.bucket_name}/AWSLogs/${data.aws_caller_identity.current.account_id}/*",
    ]

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values = [
        "bucket-owner-full-control",
      ]
    }
  }

  # MFAが設定されていないユーザからのバケット設定の変更や削除操作などを無効にする
  statement {
    effect = "Deny"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions = [
      "s3:PutBucket*",
      "s3:Delete*",
    ]

    resources = [
      "arn:aws:s3:::${var.bucket_name}",
      "arn:aws:s3:::${var.bucket_name}/*",
    ]

    # terraformを実行するロールを除外
    condition {
      test     = "StringNotLike"
      variable = "aws:userid"
      values   = ["${local.terraformer_role_id}:*"]
    }

    condition {
      test     = "StringLike"
      variable = "aws:principaltype"
      values = [
        "User",
        "AssumedRole",
      ]
    }

    condition {
      test     = "BoolIfExists"
      variable = "aws:MultiFactorAuthPresent"
      values   = [false]
    }
  }
}

resource "aws_s3_bucket" "cloudtrail" {
  bucket        = var.bucket_name
  acl           = "private"
  force_destroy = var.bucket_force_destroy
  policy        = join("", data.aws_iam_policy_document.cloudtrail.*.json)

  versioning {
    enabled    = true
    mfa_delete = var.bucket_mfa_delete
  }

  dynamic "lifecycle_rule" {
    for_each = !var.bucket_mfa_delete && length(var.bucket_transitions) > 0 ? ["dummy"] : []
    content {
      enabled = true

      dynamic "transition" {
        for_each = var.bucket_transitions
        content {
          days          = transition.value["days"]
          storage_class = transition.value["storage_class"]
        }
      }
      # Versioning を有効化しているため、expiration は設定できない
    }
  }

  # ignore version argument
  lifecycle {
    ignore_changes = [
      versioning
    ]
  }
}

resource "aws_s3_bucket_public_access_block" "cloudtrail" {
  bucket = aws_s3_bucket.cloudtrail.bucket

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

