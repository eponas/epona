variable "tags" {
  description = "KMSキーに付与するタグ"
  type        = map(string)
}

variable "kms_keys" {
  description = "KMSで作成するキーに指定するパラメーター（alias_name, description, key_usage, customer_master_key_spec, policy, deletion_window_in_days, is_enabled, enable_key_rotation"
  # https://www.terraform.io/docs/providers/aws/r/kms_key.html
  # https://www.terraform.io/docs/providers/aws/r/kms_alias.html
  type = list(map(any))
}
