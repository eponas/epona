variable "tags" {
  description = "ECRリポジトリに関するリソースに、共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "repositories" {
  description = "ECRリポジトリの定義（name,image_scan_on_push,image_tag_mutability,lifecycle_policy,repository_policy）"
  type        = list(map(string))
  #  name                 = string
  #  image_scan_on_push   = bool
  #  image_tag_mutability = bool
  #  lifecycle_policy     = string
  #  repository_policy    = string
}
