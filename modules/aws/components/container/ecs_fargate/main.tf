resource "aws_ecs_cluster" "this" {
  name = var.cluster_name

  tags = merge(
    {
      "Name" = format("%s-%s", var.name, var.cluster_name)
    },
    var.tags
  )
}

resource "aws_ecs_task_definition" "this" {
  family                   = "${var.cluster_name}-task"
  cpu                      = var.cpu
  memory                   = var.memory
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]

  task_role_arn      = var.task_role_arn
  execution_role_arn = var.execution_role_arn

  container_definitions = var.container_definitions

  tags = merge(
    {
      "Name" = format("%s-%s-task", var.name, var.cluster_name)
    },
    var.tags
  )
}

# NOTE: ignore_changesには動的な値を指定できないため、code_deployの使用有無で実行されるリソースを分岐させる
# https://www.terraform.io/docs/configuration/resources.html#ignore_changes
resource "aws_ecs_service" "code_deploy_type" {
  count = var.enable_code_deploy ? 1 : 0

  name                              = "${var.cluster_name}-service"
  cluster                           = aws_ecs_cluster.this.arn
  task_definition                   = aws_ecs_task_definition.this.arn
  desired_count                     = var.desired_count
  launch_type                       = "FARGATE"
  platform_version                  = var.platform_version
  health_check_grace_period_seconds = var.service_association == "LoadBalancer" ? 60 : null

  network_configuration {
    assign_public_ip = false
    security_groups  = var.security_groups

    subnets = var.subnets
  }

  dynamic "load_balancer" {
    for_each = var.service_association == "LoadBalancer" ? [null] : []
    content {
      target_group_arn = var.load_balancer_target_group_arn
      container_name   = "${var.cluster_name}-task"
      container_port   = var.container_port
    }
  }

  dynamic "service_registries" {
    for_each = var.service_association == "ServiceDiscovery" ? [null] : []
    content {
      registry_arn = var.service_registry_arn
    }
  }

  deployment_controller {
    type = "CODE_DEPLOY"
  }

  lifecycle {
    ignore_changes = [task_definition, load_balancer]
  }
}

resource "aws_ecs_service" "ecs_type" {
  count = var.enable_code_deploy ? 0 : 1

  name                              = "${var.cluster_name}-service"
  cluster                           = aws_ecs_cluster.this.arn
  task_definition                   = aws_ecs_task_definition.this.arn
  desired_count                     = var.desired_count
  launch_type                       = "FARGATE"
  platform_version                  = var.platform_version
  health_check_grace_period_seconds = var.service_association == "LoadBalancer" ? 60 : null

  network_configuration {
    assign_public_ip = false
    security_groups  = var.security_groups

    subnets = var.subnets
  }

  dynamic "load_balancer" {
    for_each = var.service_association == "LoadBalancer" ? [null] : []
    content {
      target_group_arn = var.load_balancer_target_group_arn
      container_name   = "${var.cluster_name}-task"
      container_port   = var.container_port
    }
  }

  dynamic "service_registries" {
    for_each = var.service_association == "ServiceDiscovery" ? [null] : []
    content {
      registry_arn = var.service_registry_arn
    }
  }
}
