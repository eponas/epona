variable "name" {
  description = "VPCピアリングの名前"
  type        = string
}

variable "tags" {
  description = "VPCピアリングに関するリソースに、共通的に付与するタグ"
  type        = map(string)
}

variable "requester_vpc_id" {
  description = "ピアリングを要求するVPC（リクエスタ）のID"
  type        = string
}

variable "accepter_vpc_id" {
  description = "ピアリングを承諾するVPC（アクセプタ）のID"
  type        = string
}
