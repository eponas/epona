variable "name" {
  description = "プライベートリンクに関するリソースの、Nameタグの一部に使用する"
  type        = string
}

variable "tags" {
  description = "プライベートリンクに関するリソースに、共通的に付与するタグ"
  type        = map(string)
}

variable "vpc_id" {
  description = "プライベートリンクで接続するVPCのID"
  type        = string
}

variable "private_subnet_ids" {
  description = "プライベートリンクで接続するプライベートサブネットのID"
  type        = list(string)
}

variable "cidr_block" {
  description = "VPCに割り当てるCIDRブロック"
  type        = string
}
