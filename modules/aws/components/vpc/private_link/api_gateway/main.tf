data "aws_region" "current" {}

module "api_gateway_private_link_sg" {
  source = "../../../../components/security_group/generic"

  name = "${var.name}-api-gateway-private-link-security-group"
  tags = var.tags

  create = true

  vpc_id = var.vpc_id

  ingresses = [{
    port        = 0
    protocol    = "-1"
    cidr_blocks = [var.cidr_block]
    description = "API Gateway Private Link inbound SG Rule"
  }]

  egresses = [{
    port        = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow any"
  }]
}

resource "aws_vpc_endpoint" "api_gateway" {
  service_name        = "com.amazonaws.${data.aws_region.current.name}.execute-api"
  vpc_endpoint_type   = "Interface"
  vpc_id              = var.vpc_id
  subnet_ids          = var.private_subnet_ids
  security_group_ids  = [module.api_gateway_private_link_sg.security_group_id]
  private_dns_enabled = true

  tags = merge(
    {
      "Name" = "${var.name}-APIGateway-Endpoint"
    },
    var.tags
  )
}
