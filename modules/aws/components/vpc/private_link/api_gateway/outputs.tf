output "endpoints" {
  value = {
    api_gateway = aws_vpc_endpoint.api_gateway.id
  }
}
