data "aws_region" "current" {}

resource "aws_vpc_endpoint" "ecr_api" {
  service_name        = "com.amazonaws.${data.aws_region.current.name}.ecr.api"
  vpc_endpoint_type   = "Interface"
  vpc_id              = var.vpc_id
  subnet_ids          = var.private_subnet_ids
  security_group_ids  = [var.ecr_private_link_sg]
  private_dns_enabled = true

  tags = merge(
    {
      "Name" = "${var.name}-ECR-API-Endpoint"
    },
    var.tags
  )
}

resource "aws_vpc_endpoint" "ecr_dkr" {
  service_name        = "com.amazonaws.${data.aws_region.current.name}.ecr.dkr"
  vpc_endpoint_type   = "Interface"
  vpc_id              = var.vpc_id
  subnet_ids          = var.private_subnet_ids
  security_group_ids  = [var.ecr_private_link_sg]
  private_dns_enabled = true

  tags = merge(
    {
      "Name" = "${var.name}-ECR-DKR-Endpoint"
    },
    var.tags
  )
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id          = var.vpc_id
  service_name    = "com.amazonaws.${data.aws_region.current.name}.s3"
  route_table_ids = var.private_route_tables

  tags = merge(
    {
      "Name" = "${var.name}-S3-Endpoint"
    },
    var.tags
  )
}

resource "aws_vpc_endpoint" "cloudwatch_logs" {
  service_name        = "com.amazonaws.${data.aws_region.current.name}.logs"
  vpc_endpoint_type   = "Interface"
  vpc_id              = var.vpc_id
  subnet_ids          = var.private_subnet_ids
  security_group_ids  = [var.cloudwatch_logs_private_link_sg]
  private_dns_enabled = true

  tags = merge(
    {
      "Name" = "${var.name}-CloudWatch-Logs-Endpoint"
    },
    var.tags
  )
}
