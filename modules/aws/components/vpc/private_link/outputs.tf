output "endpoints" {
  value = {
    ecr_api         = aws_vpc_endpoint.ecr_api.id
    ecr_dkr         = aws_vpc_endpoint.ecr_dkr.id
    s3              = aws_vpc_endpoint.s3.id
    cloudwatch_logs = aws_vpc_endpoint.cloudwatch_logs.id
  }
}
