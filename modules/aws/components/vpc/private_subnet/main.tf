resource "aws_subnet" "this" {
  vpc_id = var.vpc_id

  count = length(var.subnets)

  cidr_block        = var.subnets[count.index]
  availability_zone = var.availability_zones[count.index]

  tags = merge(
    {
      "Name" = format("%s-private-%s-%d", var.name, var.availability_zones[count.index], count.index)
    },
    var.tags
  )
}

resource "aws_route_table" "this" {
  vpc_id = var.vpc_id

  count = length(var.subnets) # TODO nat gateway count

  tags = merge(
    {
      "Name" = format("%s-private-%s-%d", var.name, var.availability_zones[count.index], count.index)
    },
    var.tags
  )
}

resource "aws_route_table_association" "this" {
  count = length(var.subnets)

  subnet_id      = aws_subnet.this[count.index].id
  route_table_id = aws_route_table.this[count.index].id
}

resource "aws_eip" "nat_gateway" {
  count = length(var.nat_gateway_deploy_subnets)

  vpc = true

  tags = merge(
    {
      "Name" = format(
        "%s-eip-%s",
        var.name,
        count.index + 1
      )
    },
    var.tags
  )
}

resource "aws_nat_gateway" "this" {
  count = length(var.nat_gateway_deploy_subnets)

  allocation_id = aws_eip.nat_gateway[count.index].id
  subnet_id     = element(var.public_subnet_ids, index(var.public_subnets, var.nat_gateway_deploy_subnets[count.index]))
}

resource "aws_route" "private" {
  count = length(var.nat_gateway_deploy_subnets)

  route_table_id         = element(aws_route_table.this.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.this.*.id, count.index)
}

resource "aws_route" "peering" {
  count = var.peering_id == null ? 0 : length(var.subnets)

  route_table_id            = element(aws_route_table.this.*.id, count.index)
  destination_cidr_block    = var.peering_cidr
  vpc_peering_connection_id = var.peering_id
}