variable "name" {
  description = "プライベートサブネットに関するリソースの、Nameタグの一部に使用する"
  type        = string
}

variable "tags" {
  description = "プライベートサブネットに関するリソースに、共通的に付与するタグ"
  type        = map(string)
}

variable "vpc_id" {
  description = "プライベートサブネットを配置先となる、VPCのID"
  type        = string
}

variable "availability_zones" {
  description = "プライベートサブネットを配置先となる、AZのリスト"
  type        = list(string)
}

variable "subnets" {
  description = "プライベートサブネットに割り当てる、CIDRブロックのリスト"
  type        = list(string)
}

variable "public_subnets" {
  description = "パブリックサブネットが配置されている、CIDRブロックのリスト"
  type        = list(string)
}

variable "public_subnet_ids" {
  description = "NAT ゲートウェイを配置する、パブリックサブネットIDのリスト"
  type        = list(string)
}

variable "nat_gateway_deploy_subnets" {
  description = "NAT ゲートウェイを配置するサブネットの、CIDRブロックのリスト。パブリックサブネットのCIDRブロックを指定する"
  type        = list(string)
}

variable "peering_id" {
  description = "VPCピアリングのID"
  type        = string
  default     = null
}

variable "peering_cidr" {
  description = "VPCピアリングする対象VPCのCIDR（peering_idを指定する場合は設定必須）"
  type        = string
  default     = null
}