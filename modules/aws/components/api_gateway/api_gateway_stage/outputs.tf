output "stages" {
  description = "API Gatewayに設定したステージに関する情報（名称、ARN、URL）"
  value = {
    stage_name = aws_api_gateway_stage.this.stage_name
    stage_arn  = aws_api_gateway_stage.this.arn
    stage_url  = aws_api_gateway_stage.this.invoke_url
  }
}