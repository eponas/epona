locals {
  create_custom_access_log = lookup(var.stage_settings, "custom_access_log_group_name", null) != null ? true : false
}

resource "aws_api_gateway_deployment" "this" {
  rest_api_id = var.rest_api_id

  triggers = {
    redeployment = "${sha1(var.rest_api_body)}${try(var.stage_settings["force_redeployment_string"], "")}"
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "this" {
  rest_api_id   = var.rest_api_id
  deployment_id = aws_api_gateway_deployment.this.id
  stage_name    = var.stage_name
  variables     = lookup(var.stage_settings, "stage_variables", null)
  dynamic "access_log_settings" {
    for_each = local.create_custom_access_log ? [1] : []
    content {
      destination_arn = module.log[0].log_group_arn
      format          = replace(file(lookup(var.stage_settings, "custom_access_log_format_path", null)), "\n", "")
    }
  }
  xray_tracing_enabled = lookup(var.stage_settings, "xray_tracing_enabled", null)
  # キャッシュ設定はデフォルトのまま（無効）とする
  tags = merge(
    {
      "Name" = var.stage_name
    },
    var.tags
  )
  depends_on = [module.log]
}

module "log" {
  count                       = local.create_custom_access_log ? 1 : 0
  source                      = "../../cloudwatch/log"
  log_group_name              = lookup(var.stage_settings, "custom_access_log_group_name", null)
  log_group_retention_in_days = 7
  log_group_kms_key_id        = ""
}

module "api_gateway_stage_method_settings" {
  for_each                       = lookup(var.stage_settings, "log_throttling", null) != null ? var.stage_settings.log_throttling : {}
  source                         = "../../api_gateway/api_gateway_stage_method_setting"
  rest_api_id                    = var.rest_api_id
  stage_name                     = var.stage_name
  resource_method_name           = each.key
  stage_resource_method_settings = each.value
  depends_on = [
    aws_api_gateway_stage.this
  ]
}
