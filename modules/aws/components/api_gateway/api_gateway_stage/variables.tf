variable "rest_api_id" {
  description = "API GatewayのID"
  type        = string
}
variable "rest_api_body" {
  description = "API GatewayにインポートしたOpenAPIのbody"
  type        = string
}
variable "stage_name" {
  description = "API Gateway Stageの名称"
  type        = string
}
variable "stage_settings" {
  description = "API Gateway Stageに関する設定項目"
  type        = any
}
variable "tags" {
  description = "API Gateway Stageに付与するタグ"
  type        = map(string)
  default     = {}
}
