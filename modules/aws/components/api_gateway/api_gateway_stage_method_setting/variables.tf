variable "rest_api_id" {
  type = string
}
variable "stage_name" {
  type = string
}
variable "resource_method_name" {
  type = string
}
variable "stage_resource_method_settings" {
  type = any
}
