locals {
  # /で分割したリストの最後の要素をHTTPメソッドとして特定する
  method_name = split("/", var.resource_method_name)[length(split("/", var.resource_method_name)) - 1]
  # /で分割したリストから、メソッド名に一致する要素を除外する
  resource_name = compact([for x in split("/", var.resource_method_name) : x == local.method_name ? "" : x])
}

resource "aws_api_gateway_method_settings" "this" {
  rest_api_id = var.rest_api_id
  stage_name  = var.stage_name
  # /で分割したリストを再度結合する。メソッド名は大文字にして連結する。/*/*は*/*として使用する
  method_path = var.resource_method_name == "/*/*" ? "*/*" : "${join("/", local.resource_name)}/${upper(local.method_name)}"

  settings {
    # data_trace_enabled、caching_enabledはデフォルト（無効）とする
    logging_level          = try(var.stage_resource_method_settings["logging_level"], "OFF")
    metrics_enabled        = try(var.stage_resource_method_settings["enable_metrics"], false)
    throttling_burst_limit = try(var.stage_resource_method_settings["throttling_burst_limit"], -1)
    throttling_rate_limit  = try(var.stage_resource_method_settings["throttling_rate_limit"], -1)
  }
}
