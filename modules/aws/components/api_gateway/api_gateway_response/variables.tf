variable "response_type" {
  description = "レスポンスを設定したいレスポンスタイプ"
  type        = string
}
variable "status_response_settings" {
  description = "レスポンスに関する設定項目"
  type        = any
}
variable "rest_api_id" {
  description = "API GatewayのID"
  type        = string
}