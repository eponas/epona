resource "aws_api_gateway_gateway_response" "this" {
  rest_api_id   = var.rest_api_id
  response_type = var.response_type

  status_code = lookup(var.status_response_settings, "status_code", null) != null ? var.status_response_settings.status_code : null

  response_templates  = lookup(var.status_response_settings, "response_templates", null) != null ? var.status_response_settings.response_templates : null
  response_parameters = lookup(var.status_response_settings, "response_parameters", null) != null ? var.status_response_settings.response_parameters : null
}