data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "api_gateway_cloudwatch_role" {
  name               = format("%sCloudWatchRole", title(var.api_name))
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

# API Gatewayのサービスロールにポリシーをアタッチ
# マネージドポリシーを使用する
resource "aws_iam_role_policy_attachment" "api_gateway_policy" {
  role       = aws_iam_role.api_gateway_cloudwatch_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
}
