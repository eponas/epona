output "root_resource_id" {
  description = "API Gateway配下のリソースパスの最上位に当たるリソースパスのID"
  value       = aws_api_gateway_rest_api.this.root_resource_id
}
output "rest_api_id" {
  description = "API GatewayのID"
  value       = aws_api_gateway_rest_api.this.id
}
output "rest_api_arn" {
  description = "API GatewayのARN"
  value       = aws_api_gateway_rest_api.this.arn
}
output "rest_api_body" {
  description = "API GatewayにインポートしたOpenAPIのbody"
  value       = aws_api_gateway_rest_api.this.body
}