variable "api_resources" {
  description = "API Gatewayのリソース/メソッドに関する設定項目"
  type        = any
}
variable "api_security_schemes" {
  description = "API GatewayのLambdaオーソライザーに関する設定項目"
  type        = any
  default     = null
}
variable "api_name" {
  description = "API Gatewayの名称"
  type        = string
}
variable "description" {
  description = "API Gatewayの説明"
  type        = string
  default     = ""
}
variable "api_endpoint_config" {
  description = "API Gatewayのエンドポイント。`vpc_endpoint_ids`は、`endpoint_types`が`PRIVATE`の場合のみ使用。それ以外の場合はnullを設定してください"
  type = object({
    endpoint_types   = list(string)
    vpc_endpoint_ids = list(string)
  })
  default = null
}
variable "api_policy_path" {
  description = "API Gatewayに設定するリソースポリシーを定義しているファイルのパス。リソースポリシーを設定しない場合はnullを設定してください"
  type        = string
  default     = null
}
variable "accept_binary_media_types" {
  description = "API Gatewayでのバイナリサポート有無。サポートする場合は[公式ドキュメントのサンプル](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-swagger-extensions-binary-media-types.html)を参考に設定してください。バイナリサポートが不要な場合はnullを設定してください"
  type        = list(string)
  default     = null
}
variable "minimum_compression_size" {
  description = "API Gatewayでコンテンツのエンコード（圧縮）を行うかどうかを設定する項目。この項目をしきい値として、設定値以上のボディサイズが設定されていた場合に圧縮がトリガーされます。圧縮不要の場合は-1を設定してください"
  type        = number
  default     = -1
}
variable "api_key_source" {
  description = "API GatewayでAPIキーを使用する場合に、どこからAPIキーを取得するかを設定する項目。`HEADER`または`AUTHORIZER`が指定可能です。（デフォルトは`HEADER`）"
  type        = string
  default     = "HEADER"
}
variable "disable_execute_api_endpoint" {
  description = "API Gatewayでの自動生成URLを作成しないかを設定する項目。不要な場合は`true`としてください。（デフォルトは`false`）"
  type        = bool
  default     = false
}
variable "tags" {
  description = "API Gatewayに付与するタグ"
  type        = map(string)
  default     = {}
}

