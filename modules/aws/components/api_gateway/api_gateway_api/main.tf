data "aws_region" "region" {}
data "aws_caller_identity" "identity" {}
locals {
  open_api_paths = {
    for resource, spec in var.api_resources : resource => {
      # method名がenable_corsの場合はoptionsに、anyの場合はx-amazon-apigateway-any-methodに置換
      for method, lambda in spec : replace(replace(method, "enable_cors", "options"), "any", "x-amazon-apigateway-any-method") => {
        # security に以下を設定する。指定している仕様は https://swagger.io/specification/ の「Security Requirement Object」を参照。
        # - authorizer 指定あり、api_key_required 指定あり: { api_key = [] }, { authorizer指定 = [] }
        # - authorizer 指定あり、api_key_required 指定なし: { authorizer指定 = [] }
        # - authorizer 指定なし、api_key_required 指定あり: { api_key = [] }
        # - authorizer 指定なし、api_key_required 指定なし: { }
        security = lookup(lambda, "authorizer", null) != null && lookup(lambda, "api_key_required", false) ? [{ api_key = [] }, { lookup(lambda, "authorizer") = [] }] : (lookup(lambda, "authorizer", null) != null ? [{ lookup(lambda, "authorizer") = [] }] : (lookup(lambda, "api_key_required", false) ? [{ api_key = [] }] : [{}]))
        # CORS有効化のために固定レスポンスが必要
        responses = method == "enable_cors" ? {
          200 = {
            "description" = "200 response",
            "headers" = {
              "Access-Control-Allow-Origin" = {
                "schema" = {
                  "type" = "string"
                }
              },
              "Access-Control-Allow-Methods" = {
                "schema" = {
                  "type" = "string"
                }
              },
              "Access-Control-Allow-Headers" = {
                "schema" = {
                  "type" = "string"
                }
              }
            },
            "content" = {}
          }
          } : lookup(lambda, "lambda", null) == "mock" ? {
          lookup(lambda, "status_code", 200) = {
            "description" = "fixed response"
            # content_typeはある前提
            # allow_originがあった場合
            "headers" = lookup(lambda, "allow_origin", null) != null && lookup(lambda, "strict_transport_security", null) == null ? {
              "Content-Type" = {
                "schema" = {
                  "type" = "string"
                }
              }
              "Access-Control-Allow-Origin" = {
                "schema" = {
                  "type" = "string"
                }
              },
              # strict_transport_securityがあった場合
              } : lookup(lambda, "allow_origin", null) == null && lookup(lambda, "strict_transport_security", null) != null ? {
              "Content-Type" = {
                "schema" = {
                  "type" = "string"
                }
              }
              "Strict-Transport-Security" = {
                "schema" = {
                  "type" = "string"
                }
              },
              # 両方あった場合
              } : lookup(lambda, "allow_origin", null) != null && lookup(lambda, "strict_transport_security", null) != null ? {
              "Content-Type" = {
                "schema" = {
                  "type" = "string"
                }
              }
              "Access-Control-Allow-Origin" = {
                "schema" = {
                  "type" = "string"
                }
              },
              "Strict-Transport-Security" = {
                "schema" = {
                  "type" = "string"
                }
              },
              # 両方ない場合（content_typeのみセット）
              } : {
              "Content-Type" = {
                "schema" = {
                  "type" = "string"
                }
              }
            }
            "content" = {}
          }
        } : null,
        # OPTIONSかそうでないかに応じて x-amazon-apigateway-integrationの内容を切り替える
        # methodに「enable_cors」が設定されていたら、OPTIONSメソッドを作成する
        x-amazon-apigateway-integration = method == "enable_cors" ? {
          type       = "mock"
          httpMethod = null
          uri        = null
          responses = {
            default = {
              statusCode = 200,
              responseParameters = {
                "method.response.header.Access-Control-Allow-Methods" = lookup(lambda, "allow_methods", null) != null ? "'${lambda["allow_methods"]}'" : null
                "method.response.header.Access-Control-Allow-Headers" = lookup(lambda, "allow_headers", null) != null ? "'${lambda["allow_headers"]}'" : null
                "method.response.header.Access-Control-Allow-Origin"  = lookup(lambda, "allow_origin", null) != null ? "'${lambda["allow_origin"]}'" : null
              }
              responseTemplates = null
            }
          }
          requestTemplates = {
            "application/json" = "{\"statusCode\": 200}"
          }
          # methodが「enable_cors」以外
          # lambdaが「mock」の場合、固定レスポンス
          } : lookup(lambda, "lambda", null) == "mock" ? {
          type       = "mock"
          httpMethod = null
          uri        = null
          responses = {
            default = {
              statusCode = lookup(lambda, "status_code", 200),
              # content_typeはある前提
              # allow_originがあった場合
              responseParameters = lookup(lambda, "allow_origin", null) != null && lookup(lambda, "strict_transport_security", null) == null ? {
                "method.response.header.Content-Type"                = "'${lambda["content_type"]}'"
                "method.response.header.Access-Control-Allow-Origin" = "'${lambda["allow_origin"]}'"
                # strict_transport_securityがあった場合
                } : lookup(lambda, "allow_origin", null) == null && lookup(lambda, "strict_transport_security", null) != null ? {
                "method.response.header.Content-Type"              = "'${lambda["content_type"]}'"
                "method.response.header.Strict-Transport-Security" = "'${lambda["strict_transport_security"]}'"
                # 両方あった場合
                } : lookup(lambda, "allow_origin", null) != null && lookup(lambda, "strict_transport_security", null) != null ? {
                "method.response.header.Content-Type"                = "'${lambda["content_type"]}'"
                "method.response.header.Access-Control-Allow-Origin" = "'${lambda["allow_origin"]}'"
                "method.response.header.Strict-Transport-Security"   = "'${lambda["strict_transport_security"]}'"
                # 両方ない場合（content_typeのみ設定）
                } : {
                "method.response.header.Content-Type" = "'${lambda["content_type"]}'"
              }
              responseTemplates = lookup(lambda, "response_template", null) != null ? {
                lookup(lambda, "content_type", null) = lookup(lambda, "response_template", null)
              } : {}
            }
          }
          requestTemplates = lookup(lambda, "status_code", null) != null ? {
            "application/json" = "{\"statusCode\": ${tostring(lookup(lambda, "status_code", 200))}}"
          } : {}
          # lambdaが「mock」でない場合、lambdaに設定された文字列でLambdaとのインテグレーションを設定
          } : {
          type             = "aws_proxy"
          httpMethod       = "POST"
          uri              = "arn:aws:apigateway:${data.aws_region.region.name}:lambda:path/2015-03-31/functions/arn:aws:lambda:${data.aws_region.region.name}:${data.aws_caller_identity.identity.account_id}:function:${lambda["lambda"]}/invocations"
          responses        = null
          requestTemplates = null
        }
      }
    }
  }
  open_api_security_schemes = var.api_security_schemes != null ? {
    for security, spec in var.api_security_schemes : security => {
      type                         = "apiKey"
      name                         = spec["authorizer_parameter_name"]
      in                           = spec["authorizer_parameter_in"]
      x-amazon-apigateway-authtype = "custom",
      x-amazon-apigateway-authorizer = {
        type                         = spec["request_or_token_type"]
        identitySource               = lookup(spec, "request_identity_source", null)
        authorizerUri                = "arn:aws:apigateway:${data.aws_region.region.name}:lambda:path/2015-03-31/functions/arn:aws:lambda:${data.aws_region.region.name}:${data.aws_caller_identity.identity.account_id}:function:${spec["authorizer_function_name"]}/invocations"
        authorizerCredentials        = spec["authorizer_credentials"]
        identityValidationExpression = lookup(spec, "token_validation_regex", null)
        authorizerResultTtlInSeconds = lookup(spec, "authorizer_ttl", null)
      }
    }
  } : {}

  open_api_api_key = {
    "api_key" : {
      "type" : "apiKey",
      "name" : "x-api-key",
      "in" : "header"
    }
  }

  open_api_empty_schema = {
    "Empty" : {
      "title" : "Empty Schema",
      "type" : "object"
    }
  }


  lambda_list = flatten(
    [for resource, spec in var.api_resources :
      [for method, lambda in spec :
        # api_resources内に「lambda_aliases」がある場合は、「lambda関数名:エイリアス名」の形に編集する
        lookup(lambda, "lambda_aliases", null) != null ? [for alias in lambda.lambda_aliases :
          "${split(":$", lambda.lambda)[0]}:${alias}" if lookup(lambda, "lambda", null) != null && lookup(lambda, "lambda", null) != "mock"
          # 「lambda_aliases」がない場合は、「lambda関数名」を取得する
        ] : [lambda.lambda] if lookup(lambda, "lambda", null) != null && lookup(lambda, "lambda", null) != "mock"
      ]
    ]
  )
  authorizer_list = var.api_security_schemes != null ? flatten(
    [for scheme_name, spec in var.api_security_schemes :
      # api_security_schemes内に「authorizer_alias」がある場合は、「authorizer関数名:エイリアス名」の形に編集する
      lookup(spec, "authorizer_aliases", null) != null ? [for alias in spec.authorizer_aliases :
        "${split(":$", spec.authorizer_function_name)[0]}:${alias}" if lookup(spec, "authorizer_function_name", null) != null
        # 「authorizer_aliases」がない場合は、「authorizer関数名」の形に編集する
      ] : [spec.authorizer_function_name] if lookup(spec, "authorizer_function_name", null) != null
    ]
  ) : []
}

resource "aws_api_gateway_rest_api" "this" {
  name = var.api_name
  body = jsonencode({
    openapi = "3.0.1"
    paths   = local.open_api_paths
    components = {
      securitySchemes = merge(local.open_api_security_schemes, local.open_api_api_key)
      schemas         = local.open_api_empty_schema
    }
  })
  description = var.description
  endpoint_configuration {
    types            = var.api_endpoint_config.endpoint_types
    vpc_endpoint_ids = var.api_endpoint_config.vpc_endpoint_ids
  }
  binary_media_types           = var.accept_binary_media_types
  minimum_compression_size     = var.minimum_compression_size
  api_key_source               = var.api_key_source
  disable_execute_api_endpoint = var.disable_execute_api_endpoint
  tags = merge(
    {
      "Name" = var.api_name
    },
    var.tags
  )
}
resource "aws_api_gateway_rest_api_policy" "this" {
  count       = var.api_policy_path != null ? 1 : 0
  rest_api_id = aws_api_gateway_rest_api.this.id

  policy = file(var.api_policy_path)
}
resource "aws_lambda_permission" "this" {
  for_each      = toset(local.lambda_list)
  statement_id  = "${var.api_name}_${replace(each.key, ":", "_")}_LambdaInvoke"
  action        = "lambda:InvokeFunction"
  function_name = split(":", each.key)[0]
  principal     = "apigateway.amazonaws.com"
  qualifier     = try(split(":", each.key)[1], null)

  source_arn = "${aws_api_gateway_rest_api.this.execution_arn}/*/*/*"
}
resource "aws_lambda_permission" "authorizer" {
  for_each      = toset(local.authorizer_list)
  statement_id  = "${var.api_name}_${replace(each.key, ":", "_")}_LambdaInvoke"
  action        = "lambda:InvokeFunction"
  function_name = split(":", each.key)[0]
  principal     = "apigateway.amazonaws.com"
  qualifier     = try(split(":", each.key)[1], null)

  source_arn = "${aws_api_gateway_rest_api.this.execution_arn}/authorizers/*"
}
resource "aws_api_gateway_account" "this" {
  cloudwatch_role_arn = aws_iam_role.api_gateway_cloudwatch_role.arn
}
