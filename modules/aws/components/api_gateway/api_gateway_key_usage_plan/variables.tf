variable "rest_api_id" {
  description = "API GatewayのID"
  type        = string
}
variable "api_key_name" {
  description = "APIキーの名称"
  type        = string
}
variable "api_key_settings" {
  description = "APIキーに関する設定項目"
  type        = any
}
variable "tags" {
  description = "APIキー、使用量プランに付与するタグ"
  type        = map(string)
  default     = {}
}
