locals {
  api_usage_plan              = lookup(var.api_key_settings, "api_usage_plan", null)
  api_usage_plan_apply_stages = local.api_usage_plan != null ? lookup(local.api_usage_plan, "api_usage_plan_apply_stages", null) : null
  quota_settings              = local.api_usage_plan != null ? lookup(local.api_usage_plan, "quota", null) : null
  throttle_settings           = local.api_usage_plan != null ? lookup(local.api_usage_plan, "throttling", null) : null
}

resource "aws_api_gateway_api_key" "this" {
  name    = var.api_key_name
  enabled = lookup(var.api_key_settings, "api_key_enabled", true)
  tags = merge(
    {
      "Name" = var.api_key_name
    },
    var.tags
  )
}

resource "aws_api_gateway_usage_plan" "this" {
  count = local.api_usage_plan != null ? 1 : 0
  name  = "${var.api_key_name}_usage_plan"

  dynamic "api_stages" {
    for_each = local.api_usage_plan_apply_stages != null ? local.api_usage_plan_apply_stages : []
    content {
      api_id = var.rest_api_id
      stage  = api_stages.value
    }
  }

  dynamic "quota_settings" {
    for_each = local.quota_settings != null ? [1] : []
    content {
      limit  = lookup(local.quota_settings, "limit", null)
      offset = lookup(local.quota_settings, "offset", null)
      period = lookup(local.quota_settings, "period", null)
    }
  }

  dynamic "throttle_settings" {
    for_each = local.throttle_settings != null ? [1] : []
    content {
      burst_limit = lookup(local.throttle_settings, "burst_limit", null)
      rate_limit  = lookup(local.throttle_settings, "rate_limit", null)
    }
  }

  tags = merge(
    {
      "Name" = "${var.api_key_name}_usage_plan"
    },
    var.tags
  )
}
resource "aws_api_gateway_usage_plan_key" "this" {
  count         = local.api_usage_plan != null ? 1 : 0
  key_id        = aws_api_gateway_api_key.this.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.this[0].id
}