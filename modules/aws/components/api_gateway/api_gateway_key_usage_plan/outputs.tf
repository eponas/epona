output "api_key_arn" {
  description = "APIキーのARN"
  value       = aws_api_gateway_api_key.this.arn
}
output "api_key_name" {
  description = "APIキーの名称"
  value       = aws_api_gateway_api_key.this.name
}
output "api_key_value" {
  description = "APIキーの値"
  value       = aws_api_gateway_api_key.this.value
}
output "api_stages" {
  description = "APIキーを設定するステージ名のリスト"
  value       = aws_api_gateway_usage_plan.this[0].api_stages
}
