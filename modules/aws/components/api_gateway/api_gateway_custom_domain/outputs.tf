output "custom_domain" {
  description = "カスタムドメインに関する情報"
  value       = aws_api_gateway_base_path_mapping.this
}