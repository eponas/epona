variable "rest_api_id" {
  description = "API GatewayのID"
  type        = string
}
variable "rest_api_endpoint_type" {
  description = "API Gatewayのエンドポイントタイプ"
  type        = string
}
variable "custom_domain_name" {
  description = "カスタムドメインに使用するドメイン名"
  type        = string
}
variable "custom_domain_settings" {
  description = "カスタムドメインに関する設定項目"
  type        = any
}
variable "tags" {
  description = "カスタムドメインに付与するタグ"
  type        = map(string)
  default     = {}
}
