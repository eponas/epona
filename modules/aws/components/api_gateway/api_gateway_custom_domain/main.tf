provider "aws" {
  alias = "cloudfront_provider"
}

# EDGEとREGIONALで別々に作成する必要あり
# （providersの条件分岐が不可であること、aws_api_gateway_domain_nameではEDGEとREGIONALで設定項目名が異なることから）
# EDGE用のリソース
module "acm_with_validate_dns_edge" {
  count  = var.rest_api_endpoint_type == "EDGE" ? 1 : 0
  source = "../../acm/certificate_with_validate_dns"

  create = true

  providers = {
    aws = aws.cloudfront_provider
  }

  validation_method = "DNS"
  zone_name         = lookup(var.custom_domain_settings, "custom_zone_name", null)
  domain_name       = var.custom_domain_name
  ttl               = 60 # ALIASレコードと同じ
}

resource "aws_api_gateway_domain_name" "edge" {
  count           = var.rest_api_endpoint_type == "EDGE" ? 1 : 0
  domain_name     = var.custom_domain_name
  certificate_arn = module.acm_with_validate_dns_edge[0].certificate_arn
  security_policy = "TLS_1_2"
  tags = merge(
    {
      "Name" = var.custom_domain_name
    },
    var.tags
  )
  depends_on = [module.acm_with_validate_dns_edge]
}

# REGIONAL用のリソース
module "acm_with_validate_dns_regional" {
  count  = var.rest_api_endpoint_type == "REGIONAL" ? 1 : 0
  source = "../../acm/certificate_with_validate_dns"

  create = true

  validation_method = "DNS"
  zone_name         = lookup(var.custom_domain_settings, "custom_zone_name", null)
  domain_name       = var.custom_domain_name
  ttl               = 60 # ALIASレコードと同じ
}

resource "aws_api_gateway_domain_name" "regional" {
  count                    = var.rest_api_endpoint_type == "REGIONAL" ? 1 : 0
  domain_name              = var.custom_domain_name
  security_policy          = "TLS_1_2"
  regional_certificate_arn = module.acm_with_validate_dns_regional[0].certificate_arn

  endpoint_configuration {
    types = ["REGIONAL"]
  }

  tags = merge(
    {
      "Name" = var.custom_domain_name
    },
    var.tags
  )
  depends_on = [module.acm_with_validate_dns_regional]
}

module "route53_alias" {
  source = "../../route53/alias"

  zone_name   = lookup(var.custom_domain_settings, "custom_zone_name", null)
  record_name = var.custom_domain_name
  alias_target = {
    "dns_name" = try(aws_api_gateway_domain_name.edge[0].cloudfront_domain_name, aws_api_gateway_domain_name.regional[0].regional_domain_name)
    "zone_id"  = try(aws_api_gateway_domain_name.edge[0].cloudfront_zone_id, aws_api_gateway_domain_name.regional[0].regional_zone_id)
  }
}

resource "aws_api_gateway_base_path_mapping" "this" {
  for_each  = var.custom_domain_settings["base_path_mapping"]
  base_path = join("/", slice(split("/", each.key), 1, length(split("/", each.key))))

  api_id      = var.rest_api_id
  stage_name  = each.value["stage_name"]
  domain_name = var.custom_domain_name
  depends_on  = [aws_api_gateway_domain_name.edge, aws_api_gateway_domain_name.regional]
}
