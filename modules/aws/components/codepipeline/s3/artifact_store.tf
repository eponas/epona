# Pipeline の Artifact を保存する S3 バケットの設定
resource "aws_s3_bucket" "s3_artifact_store" {
  bucket        = var.artifact_store_bucket_name
  acl           = "private"
  force_destroy = var.artifact_store_bucket_force_destroy
  tags = merge(
    {
      "Name" = var.artifact_store_bucket_name
    },
    var.tags
  )

  versioning {
    enabled = true
  }

  lifecycle_rule {
    enabled = true

    dynamic "noncurrent_version_transition" {
      for_each = var.artifact_store_bucket_transitions
      content {
        days          = noncurrent_version_transition.value["days"]
        storage_class = noncurrent_version_transition.value["storage_class"]
      }
    }
    # Versioning を有効化しているため、expiration は設定できない
  }
}

resource "aws_s3_bucket_public_access_block" "s3_artifact_store" {
  bucket                  = aws_s3_bucket.s3_artifact_store.bucket
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
  depends_on = [
    aws_s3_bucket_policy.s3_artifact_store
  ]
}

resource "aws_s3_bucket_policy" "s3_artifact_store" {
  bucket = aws_s3_bucket.s3_artifact_store.id
  policy = data.aws_iam_policy_document.s3_artifact_store.json
}

# クロスアカウントロールに artifact store に対する読み書きの権限を与える
data "aws_iam_policy_document" "s3_artifact_store" {
  statement {
    sid    = "ReadAndWriteFromDeliveryAccount"
    effect = "Allow"
    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:PutObject",
      "s3:PutObjectAcl"
    ]
    resources = [
      "${aws_s3_bucket.s3_artifact_store.arn}/*"
    ]

    principals {
      type        = "AWS"
      identifiers = [var.cross_account_codepipeline_access_role_arn]
    }
  }

  statement {
    sid    = "ListBucketFromDeliveryAccount"
    effect = "Allow"
    actions = [
      "s3:ListBucket"
    ]
    resources = [
      aws_s3_bucket.s3_artifact_store.arn
    ]

    principals {
      type        = "AWS"
      identifiers = [var.cross_account_codepipeline_access_role_arn]
    }
  }
}
