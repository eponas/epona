# CloudWatch Event に対するRoleのAssumeを許可する
data "aws_iam_policy_document" "cloudwatch_events_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "cloudwatch_events" {
  description        = "Role to start pipeline ${var.pipeline_name}"
  name               = format("StartPipelineRole-%s", var.pipeline_name)
  assume_role_policy = data.aws_iam_policy_document.cloudwatch_events_assume_role.json
}

# デプロイ用のPipelineを起動可能とするポリシー
data "aws_iam_policy_document" "cloudwatch_events" {
  statement {
    effect = "Allow"
    resources = [
      aws_codepipeline.this.arn
    ]
    actions = [
      "codepipeline:StartPipelineExecution"
    ]
  }
}

resource "aws_iam_policy" "cloudwatch_events" {
  name   = format("StartPipelinePolicy-%s", title(var.pipeline_name))
  policy = data.aws_iam_policy_document.cloudwatch_events.json
}

resource "aws_iam_role_policy_attachment" "cloudwatch_events" {
  role       = aws_iam_role.cloudwatch_events.name
  policy_arn = aws_iam_policy.cloudwatch_events.arn
}

resource "aws_cloudwatch_event_rule" "s3" {
  name          = "CodePipeline-${var.pipeline_name}-${var.source_bucket_name}"
  description   = "Amazon CloudWatch Events rule to automatically start your pipeline when a PUSH occurs in the Amazon S3."
  event_pattern = <<-JSON
  {
    "source": [
      "aws.s3"
    ],
    "detail-type": [
      "AWS API Call via CloudTrail"
    ],
    "detail": {
        "eventSource": [
          "s3.amazonaws.com"
        ],
        "eventName": [
          "PutObject"
        ],
        "requestParameters": {
          "bucketName": [
            ${jsonencode(var.source_bucket_name)}
          ],
          "key": [
            ${jsonencode(var.source_object_key)}
          ]
        }
    }
  }
  JSON

  depends_on = [aws_codepipeline.this]
}

resource "aws_cloudwatch_event_target" "s3" {
  rule      = aws_cloudwatch_event_rule.s3.name
  target_id = aws_cloudwatch_event_rule.s3.name
  arn       = aws_codepipeline.this.arn
  role_arn  = aws_iam_role.cloudwatch_events.arn
}
