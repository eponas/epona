variable "pipeline_name" {
  description = "パイプライン名"
  type        = string
}

variable "tags" {
  description = "パイプラインに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "source_bucket_name" {
  description = "CodePipelineのデプロイ元となるS3バケット名"
  type        = string
}

variable "source_object_key" {
  description = "CodePipelineのデプロイ元となるS3 Object Key"
  type        = string
}

variable "require_approval_before_build" {
  description = "ビルドに管理者の承認を必要とするか"
  type        = bool
  default     = true
}

variable "require_approval_before_deploy" {
  description = "デプロイに管理者の承認を必要とするか"
  type        = bool
  default     = true
}


variable "deployment_codebuild_project_name" {
  description = "デプロイを実施するCodeBuildのプロジェクト名"
  type        = string
}

variable "artifact_store_bucket_name" {
  description = "アーティファクトストアとして利用するS3 bucket名"
  type        = string
}

variable "artifact_store_bucket_force_destroy" {
  description = "Artifact Storeとして使っているS3を強制的に削除可能にするか否か"
  type        = bool
  default     = false
}

variable "artifact_store_bucket_transitions" {
  description = "Artifact Storeの移行に対するポリシー設定。最新でなくなったファイルに対して適用される。未設定の場合は30日後にAmazon S3 Glacierへ移行する"
  type        = list(map(string))
  default = [{
    days          = 30
    storage_class = "GLACIER"
  }]
}

variable "artifact_store_bucket_encryption_key_arn" {
  description = "Artifact Storeの暗号化に使うCMKのARN"
  type        = string
}

variable "cross_account_codepipeline_access_role_arn" {
  description = "デプロイ元オブジェクトを読み取るためのS3バケットへアクセスする際に利用するRoleのARN。Delivery環境上のS3バケットにアクセスするためには、クロスアカウントのロールを指定する必要がある。"
  type        = string
}

variable "codedeploy_app_name" {
  type        = string
  description = "CodeDeployのアプリケーション名を指定する。"
}


variable "deployment_group_name" {
  type        = string
  description = "CodeDeployのデプロイメントグループ名を指定する。"
}

variable "sns_topic_arn_approval_before_build" {
  type        = string
  default     = null
  description = "ビルド前の承認リクエストの送信先となるSNSトピックのARNを指定する。"
}

variable "sns_topic_arn_approval_before_deploy" {
  type        = string
  default     = null
  description = "デプロイ前の承認リクエストの送信先となるSNSトピックのARNを指定する。"
}

variable "require_codedeploy" {
  type        = bool
  default     = true
  description = "CodeDeployをパイプラインに含めるかどうか。デフォルトは含める(`true`)"
}
