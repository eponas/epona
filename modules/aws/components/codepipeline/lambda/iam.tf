# CodePipeline 用のサービスロール
# artifact storeとデプロイするbucketへのRead/WriteとListBucketの権限を設定
data "aws_iam_policy_document" "codepipeline" {
  statement {
    sid    = "EnableReadWriteArtifactStoreBucket"
    effect = "Allow"
    resources = [
      format("%s/*", aws_s3_bucket.s3_artifact_store.arn)
    ]
    actions = [
      "s3:PutObject",
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetBucketVersioning",
    ]
  }

  statement {
    sid    = "EnableListArtifactStoreBucket"
    effect = "Allow"
    resources = [
      format("%s", aws_s3_bucket.s3_artifact_store.arn)
    ]
    actions = [
      "s3:ListBucket"
    ]
  }

  statement {
    sid       = "CodePipelineLambdaPolicy"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "codebuild:BatchGetBuilds",
      "codebuild:StartBuild",
      "iam:PassRole",
      "codedeploy:CreateDeployment",
      "codedeploy:GetDeployment",
      "codedeploy:GetApplication",
      "codedeploy:GetApplicationRevision",
      "codedeploy:RegisterApplicationRevision",
      "codedeploy:GetDeploymentConfig"
    ]
  }

  statement {
    sid    = "EnableCodePipelineAssumesCrossAccountRole"
    effect = "Allow"
    resources = [
      var.cross_account_codepipeline_access_role_arn,
    ]
    actions = ["sts:AssumeRole"]
  }

  dynamic "statement" {
    for_each = compact([var.sns_topic_arn_approval_before_build])

    content {
      sid    = "SNSPublishApplovalBuild"
      effect = "Allow"
      resources = [
        var.sns_topic_arn_approval_before_build
      ]
      actions = [
        "SNS:Publish"
      ]
    }
  }

  dynamic "statement" {
    for_each = compact([var.sns_topic_arn_approval_before_deploy])
    content {
      sid    = "SNSPublishApplovalDeploy"
      effect = "Allow"
      resources = [
        var.sns_topic_arn_approval_before_deploy
      ]
      actions = [
        "SNS:Publish"
      ]
    }
  }
}

resource "aws_iam_policy" "codepipeline" {
  name   = format("CodePipelinePolicyFor%s", title(var.pipeline_name))
  policy = data.aws_iam_policy_document.codepipeline.json
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codepipeline" {
  name               = format("CodePipelineServiceRole-%s", title(var.pipeline_name))
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_role_policy_attachment" "codepipeline" {
  role       = aws_iam_role.codepipeline.name
  policy_arn = aws_iam_policy.codepipeline.arn
}
