variable "name" {
  description = "パイプライン名"
  type        = string
}

variable "bucket_name" {
  description = "デプロイ用の設定ファイルを配置するバケット名"
  type        = string
}

variable "force_destroy" {
  description = "バケットを強制的に削除可能にするか否か"
  type        = bool
  default     = false
}

variable "tags" {
  description = "パイプラインに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "noncurrent_version_transitions" {
  description = "バケットの移行に対するポリシー設定。最新でなくなったファイルに対して適用される。未設定の場合は30日後にAmazon S3 Glacierへ移行する"
  type        = list(map(string))
  default = [{
    days          = 30
    storage_class = "GLACIER"
  }]
}
