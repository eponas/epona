# CodePipeline 用のデプロイ設定ファイル(taskdef.json、appspec.yamlを
# 圧縮したzipファイル)を配置する S3 バケット
resource "aws_s3_bucket" "this" {
  bucket        = var.bucket_name
  acl           = "private"
  force_destroy = var.force_destroy
  tags = merge(
    {
      "Name" = format("%s-pipeline-source", var.name)
    },
    var.tags
  )

  versioning {
    enabled = true
  }

  lifecycle_rule {
    enabled = true

    dynamic "noncurrent_version_transition" {
      for_each = var.noncurrent_version_transitions
      content {
        days          = noncurrent_version_transition.value["days"]
        storage_class = noncurrent_version_transition.value["storage_class"]
      }
    }
    # Versioning を有効化しているため、expiration は設定できない
  }
}

resource "aws_s3_bucket_public_access_block" "this" {
  bucket = aws_s3_bucket.this.bucket

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
