output "arn" {
  description = "bucketのARN"
  value       = aws_s3_bucket.this.arn
}

output "name" {
  description = "bucketの名前"
  value       = aws_s3_bucket.this.bucket
}
