# CodePipeline 用のロール
data "aws_iam_policy_document" "codepipeline" {
  statement {
    sid       = "CodePipelineECSPolicy"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "s3:PutObject",
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetBucketVersioning",
      "ecs:DescribeServices",
      "ecs:DescribeTaskDefinition",
      "ecs:DescribeTasks",
      "ecs:ListTasks",
      "ecs:RegisterTaskDefinition",
      "ecs:UpdateService",
      "iam:PassRole",
      "codedeploy:CreateDeployment",
      "codedeploy:GetDeployment",
      "codedeploy:GetApplication",
      "codedeploy:GetApplicationRevision",
      "codedeploy:RegisterApplicationRevision",
      "codedeploy:GetDeploymentConfig"
    ]
  }

  statement {
    sid    = "EnableAccessToSourceBucketOnDeliveryEnvironment"
    effect = "Allow"
    actions = [
      "s3:Get*"
    ]
    resources = [
      "arn:aws:s3:::${var.source_bucket_name}/*",
      "arn:aws:s3:::${var.source_bucket_name}"
    ]
  }

  statement {
    sid       = "EnableCodePipelineAssumesCrossAccountRole"
    effect    = "Allow"
    resources = [var.cross_account_codepipeline_access_role_arn]
    actions   = ["sts:AssumeRole"]
  }
}

resource "aws_iam_policy" "codepipeline" {
  name   = format("CodePipelinePolicyFor%s", title(var.name))
  policy = data.aws_iam_policy_document.codepipeline.json
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codepipeline" {
  name               = format("CodePipelineRoleFor%s", title(var.name))
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_role_policy_attachment" "codepipeline" {
  role       = aws_iam_role.codepipeline.name
  policy_arn = aws_iam_policy.codepipeline.arn
}
