# 各アクションのアウトプットであるアーティファクトを保存するS3バケット
resource "aws_s3_bucket" "artifact_store_bucket" {
  bucket        = var.artifact_store_bucket_name
  acl           = "private"
  force_destroy = var.artifact_store_bucket_force_destroy
  tags = merge(
    {
      "Name" = var.artifact_store_bucket_name
    },
    var.tags
  )

  versioning {
    enabled = true
  }

  lifecycle_rule {
    enabled = true

    dynamic "noncurrent_version_transition" {
      for_each = var.artifact_store_bucket_transitions
      content {
        days          = noncurrent_version_transition.value["days"]
        storage_class = noncurrent_version_transition.value["storage_class"]
      }
    }
    # Versioning を有効化しているため、expiration は設定できない
  }
}

resource "aws_s3_bucket_public_access_block" "artifact_store_bucket" {
  bucket = aws_s3_bucket.artifact_store_bucket.bucket

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true

  depends_on = [
    aws_s3_bucket_policy.artifact_store_bucket
  ]
}

resource "aws_s3_bucket_policy" "artifact_store_bucket" {
  bucket = aws_s3_bucket.artifact_store_bucket.id
  policy = data.aws_iam_policy_document.artifact_store_bucket.json
}

# クロスアカウントロールに artifact store に対する読み書きの権限を与える
data "aws_iam_policy_document" "artifact_store_bucket" {
  statement {
    sid    = "ReadAndWriteFromDeliveryAccount"
    effect = "Allow"
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:PutObjectAcl"
    ]
    resources = [
      "${aws_s3_bucket.artifact_store_bucket.arn}/*"
    ]

    principals {
      type        = "AWS"
      identifiers = [var.cross_account_codepipeline_access_role_arn]
    }
  }

  statement {
    sid    = "ListBucketFromDeliveryEnvironment"
    effect = "Allow"
    actions = [
      "s3:ListBucket"
    ]
    resources = [
      aws_s3_bucket.artifact_store_bucket.arn
    ]

    principals {
      type        = "AWS"
      identifiers = [var.cross_account_codepipeline_access_role_arn]
    }
  }
}
