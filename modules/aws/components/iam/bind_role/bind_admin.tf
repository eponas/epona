locals {
  admin_role_name = format("%sAdminRole", title(var.system_name))
}

resource "aws_iam_policy" "admin_switch_policy" {
  name        = format("%s%sAdminSwitchPolicy", title(var.system_name), title(var.environment_name))
  description = "IAM Policy for admin"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "sts:AssumeRole"
      ],
      "Resource": [
        "arn:aws:iam::${var.account_id}:role/${local.admin_role_name}"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "admin_attach" {
  name       = format("%s%sAdminAttach", title(var.system_name), title(var.environment_name))
  users      = var.admins
  policy_arn = aws_iam_policy.admin_switch_policy.arn
}
