
resource "aws_iam_user" "ses_smtp" {
  name = var.username

  tags = merge(
    {
      "Name" = var.username
    },
    var.tags
  )
}

resource "aws_iam_user_policy" "ses_smtp_user" {
  name   = var.policy_name
  user   = aws_iam_user.ses_smtp.name
  policy = var.policy
}

resource "aws_iam_access_key" "ses_smtp_key" {
  user = aws_iam_user.ses_smtp.name
}
