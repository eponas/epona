output "arn" {
  description = "Runtime環境から利用できるロールのARN"
  value       = aws_iam_role.this.arn
}
