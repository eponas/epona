variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "create" {
  description = "RDS拡張モニタリング用のIAMロールを作成するか否か"
  type        = bool
}

variable "rds_enhanced_monitoring_role_name" {
  default = "RDS拡張モニタリング用のIAMロール名"
  type    = string
}
