variable "create" {
  description = "このモジュールでIAMロール、IAMポリシーを作成する場合、true"
  type        = bool
}

variable "ecs_task_iam_role_name" {
  description = "タスク用のIAMロール名"
  type        = string
}

variable "ecs_task_iam_policy_name" {
  description = "タスク用のIAMポリシー名"
  type        = string
}