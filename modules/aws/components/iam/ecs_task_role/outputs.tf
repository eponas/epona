output "ecs_task_iam_role_arn" {
  description = "タスク用IAMロールのARN"
  value       = concat(aws_iam_role.ecs_task.*.arn, [""])[0]
}
