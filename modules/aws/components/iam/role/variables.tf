
variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
}

variable "admins" {
  description = "adminへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "approvers" {
  description = "approverへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "costmanagers" {
  description = "costmanagerへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "developers" {
  description = "developerへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "operators" {
  description = "operatorへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "viewers" {
  description = "viewerへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "additional_admin_role_policies" {
  description = "adminロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "additional_approver_role_policies" {
  description = "approverロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "additional_costmanager_role_policies" {
  description = "costmanagerロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "additional_developer_role_policies" {
  description = "developerロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "additional_operator_role_policies" {
  description = "operatorロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "additional_viewer_role_policies" {
  description = "viewerロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "account_id" {
  description = "信頼関係を作成するユーザのAWSアカウントID"
  type        = string
}

variable "system_name" {
  description = "システム名"
  type        = string
}
