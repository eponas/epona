locals {
  operator_entities = [for user in var.operators : "arn:aws:iam::${var.account_id}:user/${user}"]
}

resource "aws_iam_role" "operator_role" {
  tags               = var.tags
  name               = format("%sOperatorRole", title(var.system_name))
  assume_role_policy = data.aws_iam_policy_document.operator_policy.json
}

data "aws_iam_policy_document" "operator_policy" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type        = "AWS"
      identifiers = local.operator_entities
    }

    # MFAが有効になっているリクエストのみAssumeRoleを許可する
    condition {
      test     = "Bool"
      variable = "aws:MultiFactorAuthPresent"

      values = [true]
    }
  }
}

resource "aws_iam_policy" "operator_policy" {
  name        = format("%sOperatorRolePolicy", title(var.system_name))
  description = "IAM Policy for operator"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Deny",
      "Action": [
        "aws-portal:*"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "operator_role_attach" {
  role       = aws_iam_role.operator_role.name
  policy_arn = aws_iam_policy.operator_policy.arn
}

resource "aws_iam_role_policy_attachment" "operator_role_attach2" {
  role       = aws_iam_role.operator_role.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

resource "aws_iam_role_policy_attachment" "additional_operator_role_attach" {
  count = var.additional_operator_role_policies != null ? length(var.additional_operator_role_policies) : 0

  role       = aws_iam_role.operator_role.name
  policy_arn = var.additional_operator_role_policies[count.index]
}
