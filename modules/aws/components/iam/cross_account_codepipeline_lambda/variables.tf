variable "pipeline_name" {
  description = "パイプライン名"
  type        = string
}

variable "runtime_account_id" {
  description = "Runtime環境のアカウントID"
  type        = string
}

variable "artifact_store_bucket_arn" {
  description = "本ロールがアクセスするRuntime環境のArtifact storeのARN"
  type        = string
}

variable "source_bucket_arn" {
  description = "本ロールがアクセスするDelivery環境のsourceバケットのARN"
  type        = string
}

variable "artifact_store_bucket_encryption_key_arn" {
  description = "本ロールがアクセスするRuntime環境のArtifact store暗号化用CMKのARN"
  type        = string
  default     = null
}
