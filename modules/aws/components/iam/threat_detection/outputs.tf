output "iam_role_threat_detection_arn" {
  description = "通知先へ検知を送信するLambda関数に設定するIAM RoleのARN"
  value       = aws_iam_role.threat_detection.arn
  sensitive   = true
}
