data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

resource "aws_iam_role" "security_header" {
  name               = "lambda-${var.lambda_function_name}-role"
  tags               = var.tags
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "lambda.amazonaws.com",
          "edgelambda.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "security_header" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数に設定するIAM Roleのポリシー"
  name        = "lambda-${var.lambda_function_name}-policy"
  path        = "/"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${var.lambda_function_name}:*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "security_header" {
  role       = aws_iam_role.security_header.name
  policy_arn = aws_iam_policy.security_header.arn
}
