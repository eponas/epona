output "arn" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数に設定するIAM RoleのARN"
  value       = aws_iam_role.security_header.arn
}
