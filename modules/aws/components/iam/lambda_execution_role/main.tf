data "aws_iam_policy_document" "lambda" {
  statement {
    sid     = "LambdaAssumeRolePolicy"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role" "lambda" {
  name               = "${var.name_prefix}LambdaExecutionRole"
  assume_role_policy = data.aws_iam_policy_document.lambda.json
  path               = "/service-role/"
}

data "aws_iam_policy" "admin" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "iam" {
  role       = aws_iam_role.lambda.name
  policy_arn = data.aws_iam_policy.admin.arn
}

#xray
data "aws_iam_policy" "xray" {
  arn = "arn:aws:iam::aws:policy/AWSXRayDaemonWriteAccess"
}

resource "aws_iam_role_policy_attachment" "xray" {
  role       = aws_iam_role.lambda.name
  policy_arn = data.aws_iam_policy.xray.arn
}

#with vpc
data "aws_iam_policy" "vpc" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_role_policy_attachment" "vpc" {
  role       = aws_iam_role.lambda.name
  policy_arn = data.aws_iam_policy.vpc.arn
}
