output "arn" {
  description = "Lambda関数実行ロールIAMのARNを返す。"
  value       = aws_iam_role.lambda.arn
}

output "name" {
  description = "Lambda関数実行ロールIAMの名称を返す。"
  value       = aws_iam_role.lambda.name
}
