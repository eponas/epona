output "arn" {
  description = "Kinesis Data Firehose用のIAM RoleのARN"
  value       = aws_iam_role.waf_logging_firehose.arn
  sensitive   = true
}
