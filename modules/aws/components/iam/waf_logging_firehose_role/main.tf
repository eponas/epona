data "aws_iam_policy_document" "waf_logging_firehose_role" {
  statement {
    sid     = "WafLoggingFirehoseRole"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["firehose.amazonaws.com"]
    }
    effect = "Allow"
  }
}

resource "aws_iam_role" "waf_logging_firehose" {
  description        = "For Kinesis Data Firehose"
  name               = "KinesisFirehoseServiceRole-${var.name_suffix}"
  assume_role_policy = data.aws_iam_policy_document.waf_logging_firehose_role.json
  tags               = var.tags
}

# see: https://docs.aws.amazon.com/ja_jp/firehose/latest/dev/controlling-access.html#using-iam-s3
data "aws_iam_policy_document" "waf_logging_firehose_policy" {
  statement {
    sid = "WafLoggingFirehosePolicy"
    actions = [
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:PutObject"
    ]
    resources = [
      "arn:aws:s3:::${var.allow_bucket_name}",
      "arn:aws:s3:::${var.allow_bucket_name}/*"
    ]
    effect = "Allow"
  }
}

resource "aws_iam_policy" "waf_logging_firehose" {
  description = "For Kinesis Data Firehose"
  name        = "KinesisFirehoseServicePolicy-${var.name_suffix}"
  policy      = data.aws_iam_policy_document.waf_logging_firehose_policy.json
}

resource "aws_iam_role_policy_attachment" "waf_logging_firehose" {
  role       = aws_iam_role.waf_logging_firehose.name
  policy_arn = aws_iam_policy.waf_logging_firehose.arn
}
