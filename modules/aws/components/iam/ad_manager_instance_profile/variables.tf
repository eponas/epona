variable "role_name" {
  description = "IAMロール名"
  type        = string
}

variable "instance_profile_name" {
  description = "AD管理用EC2に付与するIAMインスタンスプロファイル名"
  type        = string
}

variable "tags" {
  description = "AD管理用EC2に付与するに付与するIAMロール関連のリソースに、共通的に付与するタグ"
  type        = map(string)
  default     = {}
}
