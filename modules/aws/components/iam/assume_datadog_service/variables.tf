variable "tags" {
  description = "Datadogと連携するIAMロール関連のリソースに、共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "aws_integration_external_id" {
  description = "Datadogと連携するためのExternal ID"
  type        = string
}

variable "allow_actions" {
  description = <<DESCRIPTION
  Datadogに実行許可するアクションを、リストで指定する。
  どのようなアクションが指定可能かは、[DatadogのAWSインテグレーション](https://docs.datadoghq.com/ja/integrations/amazon_web_services/?tab=cloudformation)を参照すること。
  DESCRIPTION
  type        = list(string)
}

variable "integration_policy_name" {
  description = "Datadog連携のために作成するポリシーの名前"
  type        = string
}

variable "integration_role_name" {
  description = "Datadog連携のために作成するIAMロール名"
  type        = string
}
