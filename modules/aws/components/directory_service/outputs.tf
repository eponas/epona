output "security_group_id" {
  description = "Directory Service の SG"
  value       = aws_directory_service_directory.this.security_group_id
}

output "dns_ip_addresses" {
  description = "Directory Service の DNS IP"
  value       = aws_directory_service_directory.this.dns_ip_addresses
}

output "id" {
  description = "Directory Service の ID"
  value       = aws_directory_service_directory.this.id
}