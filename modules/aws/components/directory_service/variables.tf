variable "tags" {
  description = "Directory Serviceに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "name" {
  description = "ディレクトリのFQDN。例: corp.example.com"
  type        = string
}

variable "password" {
  description = "ディレクトリのadminのパスワード"
  type        = string
}

variable "short_name" {
  description = "ディレクトリの短縮ドメイン名"
  type        = string
}

variable "edition" {
  description = "type:MicrosoftADのedition(Standard or Enterprise)"
  type        = string
  default     = "Standard"
}

variable "vpc_id" {
  description = "ディレクトリをデプロイするVPCのID"
  type        = string
}

variable "subnet_ids" {
  description = "ディレクトリをデプロイするサブネットIDのリスト。異なるAZにあるサブネットIDを指定すること。"
  type        = list(string)
}
