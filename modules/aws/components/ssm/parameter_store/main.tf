resource "aws_ssm_parameter" "this" {
  count = length(var.parameters)

  name  = var.parameters[count.index].name
  type  = var.parameters[count.index].type
  value = var.parameters[count.index].value

  description = lookup(var.parameters[count.index], "description", null)
  tier        = lookup(var.parameters[count.index], "tier", null)
  key_id      = lookup(var.parameters[count.index], "key_id", null)

  overwrite = lookup(var.parameters[count.index], "overwrite", true) # not default

  allowed_pattern = lookup(var.parameters[count.index], "allowed_pattern", null)

  tags = merge(
    {
      "Name" = var.parameters[count.index].name
    },
    var.tags
  )
}