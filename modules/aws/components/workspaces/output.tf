output "security_group_id" {
  value = aws_workspaces_directory.this.workspace_security_group_id
}

output "ip_addresses" {
  value = values(aws_workspaces_workspace.this)[*].ip_address
}

output "states" {
  value = values(aws_workspaces_workspace.this)[*].state
}

output "registration_code" {
  value = aws_workspaces_directory.this.registration_code
}
