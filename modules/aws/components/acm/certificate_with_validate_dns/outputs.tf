output "certificate_arn" {
  value = concat(aws_acm_certificate.this.*.arn, [""])[0]
}
