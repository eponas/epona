resource "aws_acm_certificate" "this" {
  count = var.create ? 1 : 0

  domain_name       = var.domain_name
  validation_method = var.validation_method

  tags = merge(
    {
      "Name" = var.domain_name
    },
    var.tags
  )

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "this" {
  name = var.zone_name
}

locals {
  domain_validation_options = tolist(var.create ? aws_acm_certificate.this[0].domain_validation_options : null)
}

# SSL/TLS証明書検証用のDNSレコード（CNAME）
resource "aws_route53_record" "this" {
  count = var.create ? length(local.domain_validation_options) : 0

  name    = local.domain_validation_options[count.index].resource_record_name
  type    = local.domain_validation_options[count.index].resource_record_type
  zone_id = data.aws_route53_zone.this.zone_id

  records = [local.domain_validation_options[count.index].resource_record_value]

  ttl = var.ttl
}

resource "aws_acm_certificate_validation" "this" {
  count                   = var.create ? 1 : 0
  certificate_arn         = aws_acm_certificate.this[0].arn
  validation_record_fqdns = [aws_route53_record.this[0].fqdn]
}