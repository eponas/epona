variable "name" {
  description = "ロードバランサーの名前"
  type        = string
}

variable "tags" {
  description = "ロードバランサーに関連するリソースに、共通的に付与するタグ"
  type        = map(string)
}

variable "vpc_id" {
  description = "ターゲットグループを配置するVPCのIC"
  type        = string
}

variable "load_balancer_type" {
  description = "ロードバランサーのタイプ"
  type        = string
}

variable "access_logs_enabled" {
  description = "ロードバランサーのアクセスログを有効にする場合、trueを指定する"
  type        = bool
}

variable "access_logs_bucket" {
  description = "ロードバランサーのアクセスログを保存するS3バケット名"
  type        = string
}

variable "access_logs_prefix" {
  description = "アクセスログをS3バケットに保存する時のprefix。指定しない場合は、ログはS3のルートに保存される"
  type        = string
}

variable "subnets" {
  description = "ロードバランサーに割り当てるサブネットIDのリスト"
  type        = list(string)
}

variable "security_groups" {
  description = "ロードバランサーに割り当てるセキュリティグループのリスト"
  type        = list(string)
}

variable "public_traffic_protocol" {
  description = "ロードバランサーが受け付けるトラフィックのプロトコル"
  type        = string
}

variable "public_traffic_port" {
  description = "ロードバランサーが受け付けるトラフィックのポート"
  type        = number
}

variable "public_traffic_certificate_arn" {
  description = "ロードバランサーが使用する、SSL/TLS証明書のARN"
  type        = string
}

variable "public_traffic_ssl_policy" {
  description = "ロードバランサーのSSL/TLSネゴシエーションポリシー"
  type        = string
}

variable "public_traffic_test_protocol" {
  description = "テスト用リスナーがListenするプロトコル。テスト用リスナーを使用する場合は、public_traffic_test_portと同時に指定すること"
  type        = string
  default     = null
}

variable "public_traffic_test_port" {
  description = "テスト用リスナーがListenするポート。テスト用リスナーを使用する場合は、public_traffic_test_protocolと同時に指定すること"
  type        = number
  default     = null
}

variable "backend_protocol" {
  description = "ロードバランサーのバックエンドのプロトコル"
  type        = string
}

variable "backend_port" {
  description = "ロードバランサーのバックエンドのポート"
  type        = number
}

variable "backend_health_check_path" {
  description = "ロードバランサーのバックエンドに対する、ヘルスチェックパス"
  type        = string
}

variable "alb_forward_listener_rules" {
  description = <<DESCRIPTION
ロードバランサーのリスナーに設定するフォワードアクションルールのリスト
[
  {
    priority = ルールの優先順位。値が小さいルールから順に評価される。 (例: "10") # Required
    path_pattern = ルールのアクションが実行される条件。カンマ区切りで複数指定が可能。 (単一の場合の例: "/*" 複数の場合の例: "/foo/*,/bar/*") # Required
  }
]
DESCRIPTION
  type        = list(map(string))
}

variable "alb_fixed_response_listener_rules" {
  description = <<DESCRIPTION
ロードバランサーのリスナーに設定する固定レスポンスルールのリスト
レスポンスコードとオプションのメッセージを返すことができる
[
  {
    priority = ルールの優先順位。値が小さいルールから順に評価される。(例: "10") # Required
    content_type = コンテントタイプ (例: "text/plain") # Required content_typeに設定できる値については[content_type](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener_rule#content_type)を参照してください
    message_body = メッセージボディ (例: "403 Forbidden") # Required
    status_code = HTTPレスポンスコード (例: "403") # Required 2XX,4XX,5XXのいずれかを設定してください
    path_pattern = ルールのアクションが実行される条件。カンマ区切りで複数指定が可能。 (単一の場合の例: "/*" 複数の場合の例: "/foo/*,/bar/*") # Required
  }
]
DESCRIPTION
  type        = list(map(string))
}

variable "alb_redirect_listener_rules" {
  description = <<DESCRIPTION
ロードバランサーのリスナーに設定するリダイレクトルールのリスト
`host`、`path`、`port、`protocol`、`query`に関しては `#{host}` のように記述することで、元のURLの値を再利用できます
詳細については[リダイレクトアクション](https://docs.aws.amazon.com/ja_jp/elasticloadbalancing/latest/application/load-balancer-listeners.html#redirect-actions)を参照してください
[
  {
    priority = ルールの優先順位 (例: "10") # Required
    host = リダイレクト先のホスト (例: "example.com") # Optional 省略した場合 `"#{host}"` が設定されます
    path = リダイレクト先のURLパス (例: "/bar") # Optional 省略した場合 `"/#{path}"` が設定されます
    port = リダイレクト先のポート番号 (例: "8080") # Optional 省略した場合 `"#{port}"` が設定されます
    protocol = プロトコル HTTPもしくはHTTPS (例: "HTTPS") # Optional 省略した場合 `"#{protocol}"` が設定されます
    query = クエリパラメータ (例: "key=aaa") # Optional 省略した場合 `"#{query}"` が設定されます
    status_code = HTTPリダイレクトコード (例: "HTTP_301") # Required "HTTP_301"か"HTTP_302"のいずれかを設定してください
    path_pattern = ルールのアクションが実行される条件。カンマ区切りで複数指定が可能。 (単一の場合の例: "/*" 複数の場合の例: "/foo/*,/bar/*") # Required
  }
]
DESCRIPTION
  type        = list(map(string))
}
