locals {
  target_group_names = toset(["blue", "green"])
}

resource "aws_lb" "this" {
  name                       = var.name
  load_balancer_type         = var.load_balancer_type
  internal                   = false
  idle_timeout               = 60
  enable_deletion_protection = false # TODO

  subnets = var.subnets

  access_logs {
    enabled = var.access_logs_enabled
    bucket  = var.access_logs_bucket
    prefix  = var.access_logs_prefix
  }

  security_groups = var.security_groups

  tags = merge(
    {
      "Name" = var.name
    },
    var.tags
  )
}

resource "aws_lb_listener" "prod" {
  load_balancer_arn = aws_lb.this.arn
  port              = var.public_traffic_port
  protocol          = var.public_traffic_protocol

  certificate_arn = upper(var.public_traffic_protocol) == "HTTPS" ? var.public_traffic_certificate_arn : null
  ssl_policy      = upper(var.public_traffic_protocol) == "HTTPS" ? var.public_traffic_ssl_policy : null

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this["green"].arn
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [default_action] # for CodeDeploy update
  }
}

resource "aws_lb_listener" "test" {
  count = (var.public_traffic_test_port != null && var.public_traffic_test_protocol != null) ? 1 : 0

  load_balancer_arn = aws_lb.this.arn
  port              = var.public_traffic_test_port
  protocol          = var.public_traffic_test_protocol

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this["blue"].arn
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [default_action] # for CodeDeploy update
  }
}

resource "aws_lb_target_group" "this" {
  for_each = local.target_group_names

  # Error deleting Target Group: ResourceInUse: Target group 'xxxx' is currently in use by a listener or a rule status code
  # エラーの回避
  # https://github.com/terraform-providers/terraform-provider-aws/issues/636#issuecomment-637761075
  name_prefix          = each.value
  target_type          = "ip"
  vpc_id               = var.vpc_id
  port                 = var.backend_port
  protocol             = var.backend_protocol
  deregistration_delay = 30

  health_check {
    path                = var.backend_health_check_path
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 60
    matcher             = 200
    port                = var.backend_port
    protocol            = var.backend_protocol
  }

  tags = merge(
    {
      "Name" = format("%s-target-group", each.value)
    },
    var.tags
  )

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [aws_lb.this]
}

resource "aws_lb_listener_rule" "this" {
  count        = length(var.alb_forward_listener_rules) != 0 ? 0 : 1
  listener_arn = aws_lb_listener.prod.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_listener.prod.default_action[0].target_group_arn
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }

  lifecycle {
    ignore_changes = [action] # for CodeDeploy update
  }
}

resource "aws_lb_listener_rule" "forward_rules" {
  for_each     = { for forward_rule in var.alb_forward_listener_rules : forward_rule["path_pattern"] => forward_rule }
  listener_arn = aws_lb_listener.prod.arn
  priority     = tonumber(each.value["priority"])

  action {
    type             = "forward"
    target_group_arn = aws_lb_listener.prod.default_action[0].target_group_arn
  }

  condition {
    path_pattern {
      values = split(",", each.value["path_pattern"])
    }
  }

  lifecycle {
    ignore_changes = [action] # for CodeDeploy update
  }
}

resource "aws_lb_listener_rule" "fixed_response_rules" {
  for_each     = { for fixed_response_rule in var.alb_fixed_response_listener_rules : fixed_response_rule["path_pattern"] => fixed_response_rule }
  listener_arn = aws_lb_listener.prod.arn
  priority     = tonumber(each.value["priority"])

  action {
    type = "fixed-response"
    fixed_response {
      content_type = each.value["content_type"]
      message_body = each.value["message_body"]
      status_code  = each.value["status_code"]
    }
  }

  condition {
    path_pattern {
      values = split(",", each.value["path_pattern"])
    }
  }
}

resource "aws_lb_listener_rule" "optional_redirect_rules" {
  for_each     = { for redirect_rule in var.alb_redirect_listener_rules : redirect_rule["path_pattern"] => redirect_rule }
  listener_arn = aws_lb_listener.prod.arn
  priority     = tonumber(each.value["priority"])

  action {
    type = "redirect"
    redirect {
      host        = lookup(each.value, "host", "#{host}")
      path        = lookup(each.value, "path", "/#{path}")
      port        = lookup(each.value, "port", "#{port}")
      protocol    = lookup(each.value, "protocol", "#{protocol}")
      query       = lookup(each.value, "query", "#{query}")
      status_code = each.value["status_code"]
    }
  }

  condition {
    path_pattern {
      values = split(",", each.value["path_pattern"])
    }
  }
}
