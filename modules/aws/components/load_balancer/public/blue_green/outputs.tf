output "load_balancer_dns_name" {
  value = aws_lb.this.dns_name
}

output "load_balancer_zone_id" {
  value = aws_lb.this.zone_id
}

output "load_balancer_arn" {
  value = aws_lb.this.arn
}

output "target_group_arn" {
  value = aws_lb_target_group.this["green"].arn
}

output "prod_lister_arn" {
  description = "本番環境用トラフィックを受け持つ、ロードバランサーのリスナーARN"
  value       = aws_lb_listener.prod.arn
}

output "test_lister_arn" {
  description = "テストトラフィックを受け持つ、ロードバランサーのリスナーARN"
  value       = length(aws_lb_listener.test) > 0 ? aws_lb_listener.test[0].arn : null
  # see: https://github.com/hashicorp/terraform/issues/23222#issuecomment-547399016
}

output "target_group_names" {
  description = "LoadBalancerのターゲットグループ名のリスト"
  value       = values(aws_lb_target_group.this)[*].name
}
