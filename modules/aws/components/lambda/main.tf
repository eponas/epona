data "archive_file" "lambda" {
  type        = "zip"
  source_dir  = var.source_dir
  output_path = "${path.root}/.terraform/.tmp/components/lambda/lambda_source.zip"
  excludes    = var.source_dir_exclude_files
}

resource "aws_lambda_function" "lambda" {
  filename                       = data.archive_file.lambda.output_path
  description                    = var.description
  function_name                  = var.function_name
  role                           = var.role_arn
  handler                        = var.handler
  source_code_hash               = var.ignore_change_source ? null : data.archive_file.lambda.output_base64sha256
  memory_size                    = var.memory_size
  reserved_concurrent_executions = var.reserved_concurrent_executions
  publish                        = var.publish
  runtime                        = var.runtime
  timeout                        = var.timeout

  # environment で空mapを渡すと以下のエラーが発生するため、空mapでない場合にのみ
  # environment blockを生成する
  # Error: At least one field is expected inside environment
  dynamic "environment" {
    for_each = length(var.environment) > 0 ? [1] : []
    content {
      variables = var.environment
    }
  }

  vpc_config {
    subnet_ids         = var.vpc_config_subnet_ids
    security_group_ids = var.vpc_config_security_group_ids
  }

  tracing_config {
    mode = var.tracing_mode
  }

  tags = var.tags
}
