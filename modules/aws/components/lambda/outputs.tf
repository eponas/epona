output "arn" {
  value       = aws_lambda_function.lambda.arn
  description = "Lambda関数のarnを返します。"
}

output "function_name" {
  value       = aws_lambda_function.lambda.function_name
  description = "Lambda関数の名前を返します。"
}

output "qualified_arn" {
  value       = aws_lambda_function.lambda.qualified_arn
  description = "Lambda関数のバージョンのついたARNを返す。"
}

output "version" {
  value       = aws_lambda_function.lambda.version
  description = "Lambda関数のバージョンを返す。"
}

output "invoke_arn" {
  value       = aws_lambda_function.lambda.invoke_arn
  description = "API Gatewayで使う発火用のARNを返す。"
}
