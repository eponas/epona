variable "role_name" {
  type        = string
  description = "成功/失敗の通知をSNS topicで送るLambda関数の実行ロールの名前を指定する。"
}

variable "function_name" {
  type        = string
  description = "成功/失敗の通知をSNS topicで送るLambda関数の名前を指定する。"
}

variable "on_success_sns_arn" {
  type        = string
  default     = null
  description = "Lambda関数がsuccessを返した時に通知したいSNS topicのARNを指定する。"
}

variable "on_failure_sns_arn" {
  type        = string
  default     = null
  description = "Lambda関数がfailureを返した時に通知したいSNS topicのARNを指定する。"
}
