data "aws_iam_policy_document" "sns_topic_policy" {
  statement {
    effect  = "Allow"
    actions = ["SNS:Publish"]
    sid     = "LambdaToSns"
    resources = compact([
      var.on_success_sns_arn,
      var.on_failure_sns_arn
    ])
  }
}

resource "aws_iam_policy" "sns" {
  count  = var.on_success_sns_arn == null && var.on_failure_sns_arn == null ? 0 : 1
  name   = "LambdaToSnsPolicy${var.function_name}"
  path   = "/service-role/"
  policy = data.aws_iam_policy_document.sns_topic_policy.json
}

resource "aws_iam_role_policy_attachment" "lambda_to_sns" {
  count      = var.on_success_sns_arn == null && var.on_failure_sns_arn == null ? 0 : 1
  role       = var.role_name
  policy_arn = aws_iam_policy.sns[0].arn
}

# one lambda, one sns event invoke config
resource "aws_lambda_function_event_invoke_config" "sns" {
  count         = var.on_success_sns_arn == null && var.on_failure_sns_arn == null ? 0 : 1
  function_name = var.function_name

  destination_config {

    dynamic "on_success" {
      for_each = compact([var.on_success_sns_arn])
      content {
        destination = on_success.value
      }
    }

    dynamic "on_failure" {
      for_each = compact([var.on_failure_sns_arn])
      content {
        destination = on_failure.value
      }
    }
  }
  depends_on = [aws_iam_role_policy_attachment.lambda_to_sns]
}


