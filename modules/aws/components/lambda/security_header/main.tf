# Lambda関数のコードをTerraformで送るためにZIP圧縮をする
data "archive_file" "security_header" {
  type        = "zip"
  source_dir  = var.lambda_source_dir != null ? var.lambda_source_dir : "${path.module}/codes/add_security_header"
  output_path = "${path.root}/.terraform/.tmp/components/lambda/security_header/codes/add_security_header.zip"
}

resource "aws_lambda_function" "security_header" {
  description      = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数"
  filename         = data.archive_file.security_header.output_path
  function_name    = var.lambda_function_name
  role             = var.iam_role_arn
  handler          = var.lambda_handler
  source_code_hash = data.archive_file.security_header.output_base64sha256
  runtime          = var.lambda_runtime
  timeout          = var.lambda_timeout
  publish          = true
  tags             = var.tags
}
