output "arn" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数のARN"
  value       = aws_lambda_function.security_header.arn
}

output "qualified_arn" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数のバージョン付きARN"
  value       = aws_lambda_function.security_header.qualified_arn
}
