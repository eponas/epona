terraform {
  required_providers {
    archive = {
      source  = "hashicorp/archive"
      version = ">= 2.2.0, < 3.0.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.37.0, < 4.0.0"
    }
  }
  required_version = "~> 0.14.10"
}
