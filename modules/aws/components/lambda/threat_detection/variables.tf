variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "lambda_function_name" {
  description = "検出結果を送信するLambda関数の名前"
  type        = string
}

variable "lambda_timeout" {
  description = "Lambdaが停止するまでのタイムアウト設定。単位は秒で指定してください。"
  type        = number
  default     = 3
}

variable "notification_lambda_config" {
  description = <<-EOT
検出結果を送信するLambda関数の設定値。
`type`: 通知先サービス["slack" | "teams"]
endpoints: エンドポイントのurl情報
`low`/`medium`/`high`: それぞれの脅威度毎のIncoming Webhookのパス（通知しない場合は未設定もしくは`null`を指定）

以下、設定例です。

```
{
  type   = "slack"
  endpoints = {
    medium = "https://hooks.slack.com/xxxx/xxxx"
    high   = "https://hooks.slack.com/yyyy/yyyy"
  }
}
```
EOT
  type = object({
    type      = string,
    endpoints = map(string)
  })
  default = {
    type = "slack"
    endpoints = {
      low    = null,
      medium = null,
      high   = null
    }
  }
}

variable "notification_lambda_source_dir" {
  description = "検出結果を送信するLambda関数のソースコードが配置されたディレクトリのパス"
  type        = string
}

variable "notification_lambda_handler" {
  description = "検出結果を送信するLambda関数のエントリーポイント"
  type        = string
}

variable "notification_lambda_runtime" {
  description = "検出結果を送信するLambda関数のランタイム"
  type        = string
}

variable "iam_role_threat_detection_arn" {
  description = "検出結果を送信するLambda関数に設定するIAM RoleのARN"
  type        = string
}

variable "cloudwatch_event_rule_threat_detection_arn" {
  description = "検出結果を送信するLambda関数に設定するCloudWatch EventsのARN"
  type        = string
}

variable "lambda_subnet_ids" {
  description = "Lambdaを配置するサブネットのIDリスト。参考：[高可用性関数のために](https://aws.amazon.com/jp/premiumsupport/knowledge-center/internet-access-lambda-function/)"
  type        = list(string)
}

variable "lambda_security_group_arns" {
  description = "Lambdaに割り当てるセキュリティグループのARNリスト"
  type        = list(string)
}
