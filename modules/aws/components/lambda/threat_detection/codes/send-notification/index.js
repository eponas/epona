const https = require('https');
const url = require('url');

/** Severity毎の閾値（それぞれの下限を定義） */
const SEVERITY_THRESHOLD = {
  LOW: 1.0,
  MEDIUM: 4.0,
  HIGH: 7.0,
};

/** サポート対象のサービスタイプ文字列 */
const SUPPORT_SERVICES = {
  SLACK: 'slack',
  TEAMS: 'teams',
};

/**
 * GuardDutyで定義されたEventデータをSlack通知に使用する文字列に変換する.
 * @param {Object} event GuardDutyが送信するEventデータ
 * @return {string} JSON形式の通知文字列
 */
const createSlackData = (event) => {
  const data = JSON.stringify({
    'username': event['detail-type'],
    'text': '```' + JSON.stringify({
      'Time': event['time'],
      'AWS Account': event['detail']['accountId'],
      'AWS Region': event['detail']['region'],
      'Severity': event['detail']['severity'],
      'Type': event['detail']['type'],
      'Description': event['detail']['description'],
    }, null, 2) + '```',
  });
  return data;
};

/**
 * GuardDutyで定義されたEventデータをTeams通知に使用する文字列に変換する.
 * @details Teams向けの書式設定として、アダプティブカードを利用
 * ref: https://docs.microsoft.com/ja-jp/microsoftteams/platform/task-modules-and-cards/cards/cards-format?tabs=adaptive-md%2Cconnector-html
 * @param {Object} event GuardDutyが送信するEventデータ
 * @return {string} JSON形式の通知文字列
 */
const createTeamsData = (event) => {
  const data = JSON.stringify({
    '@type': 'MessageCard',
    '@context': 'https://schema.org/extensions',
    'summary': 'Notification from Amazon GuardDuty',
    'title': event['detail-type'],
    'sections': [{
      'facts': [{
        'name': 'Time',
        'value': event['time'],
      }, {
        'name': 'AWS Account',
        'value': event['detail']['accountId'],
      }, {
        'name': 'AWS Region',
        'value': event['detail']['region'],
      }, {
        'name': 'Severity',
        'value': event['detail']['severity'],
      }, {
        'name': 'Type',
        'value': event['detail']['type'],
      }, {
        'name': 'Description',
        'value': event['detail']['description'],
      }],
    }],
  });
  return data;
};

/**
 * GuardDutyで定義されたEventデータを通知に使用する文字列に変換する.
 * @param {string} type 送信するサービスタイプ
 * @param {Object} event GuardDutyが送信するEventデータ
 * @return {string} JSON形式の通知文字列
 */
const createData = (type, event) => {
  switch (type) {
    case SUPPORT_SERVICES.SLACK:
      return createSlackData(event);
    case SUPPORT_SERVICES.TEAMS:
      return createTeamsData(event);
  }
};

/**
 * endpointで定義された送信先へTLS通信でデータを送信する.
 * @description 'Content-Type'として'application/json'の送信を想定
 * @param {string} endpoint 送信先エンドポイントURL
 * @param {string} data 送信データ
 * @return {Promise} 送信結果
 */
const sendData = async (endpoint, data) => {
  const endpointUrl = url.parse(endpoint);
  const options = {
    hostname: endpointUrl.hostname,
    path: endpointUrl.path,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(data),
    },
  };

  const req = https.request(options, (res) => {
    if (res.statusCode === 200) {
      console.log('OK:' + res.statusCode);
    } else {
      console.log('Error:' + res.statusCode);
    }
  });
  req.write(data);
  req.end();
  return new Promise((resolve) => req.on('response', () => resolve()));
};

/**
 * 通知を送信するメインハンドラ
 * @param {Object} event GuardDutyが送信するEventデータ
 * @return {Promise} 送信結果
 */
exports.handler = async (event) => {
  const severity = event['detail']['severity'];

  let endpoint = undefined;
  if (SEVERITY_THRESHOLD.LOW <= severity &&
      severity < SEVERITY_THRESHOLD.MEDIUM) {
    endpoint = process.env['ENDPOINT_LOW'];
  } else if (SEVERITY_THRESHOLD.MEDIUM <= severity &&
      severity < SEVERITY_THRESHOLD.HIGH) {
    endpoint = process.env['ENDPOINT_MEDIUM'];
  } else {
    endpoint = process.env['ENDPOINT_HIGH'];
  }

  if (endpoint === undefined) {
    console.log('This severity\'s endpoint is undefined');
    return null;
  }

  // 各サービスに合わせたデータを作成
  const data = createData(process.env['SERVICE_TYPE'], event);

  return await sendData(endpoint, data);
};
