output "lambda_function_threat_detection_arn" {
  description = "通知先へ検知を送信するLambda関数のARN"
  value       = aws_lambda_function.threat_detection.arn
}
