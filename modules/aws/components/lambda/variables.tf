variable "function_name" {
  type        = string
  description = "Lambda関数の名称を指定する。"
}

variable "source_dir" {
  type        = string
  description = "Lambda関数のソースが配置されたディレクトリを指定する。"
}

variable "source_dir_exclude_files" {
  type        = list(string)
  description = <<EOF
`var.source_dir`で指定したディレクトリ配下の除外したいファイルやディレクトリを指定できる。
`
例えば、var.source_dir`が`node`だった場合、
`node/test.js`ファイルを除外したい場合は、`test.js`と指定。`*`は使えません。

実際は、
```
source_dir_excludes = [
  "test.js",
  "node_modules/eslint-config-eslint/package.json"
]
```
のようにlist(string)で指定する。

EOF

}

variable "description" {
  type        = string
  description = "Lambda関数の説明を記載できる。"
  default     = null
}

variable "runtime" {
  type        = string
  description = <<EOF
利用するランタイムを指定する。
指定できる値については[こちら](https://docs.aws.amazon.com/lambda/latest/dg/API_CreateFunction.html#SSS-CreateFunction-request-Runtime)を参照
EOF

}

variable "handler" {
  type        = string
  description = <<EOF
ハンドラエントリーポイントを指定する。
[Node.jsの例](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/nodejs-handler.html)
EOF

}

variable "environment" {
  type        = map(string)
  description = "環境変数をkey-value形式で指定できる。"
}

variable "vpc_config_subnet_ids" {
  type        = list(string)
  default     = []
  description = "VPC内に配置する場合はサブネットIDを指定する。"
}

variable "vpc_config_security_group_ids" {
  type        = list(string)
  default     = []
  description = "VPC内に配置する場合はセキュリティグループをアタッチする必要がある。"
}

variable "memory_size" {
  type        = number
  default     = 128
  description = <<EOF
Lambda関数で使用するメモリをMB単位で指定する。
指定できる値の制約については[Lambda quotas](https://docs.aws.amazon.com/lambda/latest/dg/gettingstarted-limits.html)を参照。

EOF

}

variable "timeout" {
  type        = number
  default     = null
  description = "タイムアウトを秒で指定する。最大900秒。指定しなかった場合は3秒。"
}

variable "tracing_mode" {
  type        = string
  default     = "PassThrough"
  description = "トレースの種類を指定できる。`PassThrough`か`Active`を指定。デフォルトは`PassThrough`"
}

variable "role_arn" {
  type        = string
  description = "Lambda関数を実行するIAMロールのARNを指定する。"
}

variable "ignore_change_source" {
  type        = bool
  description = "source_dirで指定されたディレクトリの中身が変更されていても、Lambda関数を更新しないようにする場合は`true`を指定する。"
}

variable "reserved_concurrent_executions" {
  type        = number
  description = <<EOF
Lambda関数の[リザーブド同時実行数](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-concurrency.html)。
指定できる値については[`reserved_concurrent_executions`](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function#reserved_concurrent_executions)を参照。
EOF

  default = -1
}


variable "publish" {
  type        = bool
  default     = true
  description = <<EOF
Lambda関数の新しいバージョンを作成するかどうかを指定する。trueで新しいバージョンを作成する。
Lambda関数のバージョンについては、[Lambda関数のバージョン](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-versions.html)を参照。

EOF

}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Lambdaリソースに付与するtagを指定できる。"
}
