variable "s3_bucket_name" {
  type        = string
  description = "イベント通知設定を行いたいS3バケットの名前を指定する。"
}

variable "s3_bucket_arn" {
  type        = string
  description = "イベント通知設定を行いたいS3バケットのARNを指定する。"
}

variable "arn" {
  type        = string
  description = "S3オブジェクトのイベントを受信した時に起動したいLambda関数のarnを指定する。"
}

variable "function_name" {
  type        = string
  description = "S3オブジェクトのイベントを受信した時に起動したいLambda関数の名前を指定する。"
}

variable "s3_object_events" {
  type        = list(string)
  description = <<EOF
Lambda関数起動イベント発火時に対象となるイベントを指定できる。デフォルトは作成時。

指定する内容については[Amazon S3 イベント通知](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/NotificationHowTo.html#notification-how-to-event-types-and-destinations)を参照
EOF

  default = [
    "s3:ObjectCreated:*"
  ]
}

variable "filter_prefix" {
  type        = string
  default     = null
  description = "Lambda関数の起動イベント発生条件を絞り込むためのS3オブジェクトのプレフィックスを指定できる。例 `AWSLogs/` `image_`"
}

variable "filter_suffix" {
  type        = string
  default     = null
  description = "Lambda関数の起動イベント発生条件を絞り込むためのS3オブジェクト名のサフィックスを指定できる。例 `.log` `.jpg`"
}
