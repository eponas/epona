resource "aws_lambda_permission" "schedule" {
  statement_id  = "AllowExecutionFromEventbridgeScheduler"
  action        = "lambda:InvokeFunction"
  function_name = var.function_name
  principal     = "events.amazonaws.com"
  source_arn    = module.schedule.rule_arn
}


module "schedule" {
  source              = "../../eventbridge/schedule"
  name                = "EventBridgeScheduleRuleForLambda_${var.function_name}"
  description         = "target for lambda invoke rule (${var.function_name})"
  is_enabled          = var.is_enabled
  schedule_expression = var.schedule_expression
  target_arn          = var.lambda_arn
}
