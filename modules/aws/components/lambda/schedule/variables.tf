variable "function_name" {
  type        = string
  description = "起動したいLambda関数の名前を指定する。"
}

variable "lambda_arn" {
  type        = string
  description = "起動したいLambda関数のARNを指定する。"
}

variable "schedule_expression" {
  type        = string
  description = <<EOF
スケジュール式を指定する。`cron()`式あるいは`rate()`式で指定する。

記述方法については[RateまたはCronを使用したスケジュール式](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/services-cloudwatchevents-expressions.html)を参照。
EOF

}

variable "is_enabled" {
  type        = bool
  description = "設定したスケジュールのオンオフを指定できる。"
}
