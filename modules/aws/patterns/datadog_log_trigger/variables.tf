variable "datadog_forwarder_lambda_arn" {
  description = "datadog_forwarder patternでデプロイした、Lambda関数のARN"
  type        = string
}

variable "cloudwatch_log_subscription_filters" {
  description = <<DESCRIPTION
  CloudWatch Logsに紐付けるサブスクリプションフィルター定義のリスト。

  フィルター定義はmapで表現する。最小定義は、以下のようになる。
  {
    log_group_name = "サブスクライブするロググループ名"
  }

  任意項目を含めたmapの完全な定義は、以下のようになる。
  {
    name = "サブスクリプションフィルター名。未指定の場合、モジュール内で自動設定する"
    log_group_name = "サブスクライブするロググループ名"
    filter_pattern = "ログイベントをサブスクライブするパターン。未指定の場合、すべてのイベントを対象とする"
    destination_arn = "ログイベントの送信先ARN。未指定の場合は、datadog_forwarder_lambda_arnに指定した値が使用される"
    role_arn = "CloudWatch Logsがイベントを送信先に送るために必要な権限を含んだIAMロールARN"
  }

  これらの定義を含んだリストを設定する。
  [
    {
      log_group_name = "log_group1"
    },
    {
      log_group_name = "log_group2"
    }
  ]
  DESCRIPTION
  type        = list(map(string))
  default     = []
}

variable "logging_s3_bucket_notifications" {
  description = <<DESCRIPTION
  ログ用のS3バケットに紐付ける通知定義のリスト。

  通知定義は、mapで表現する。最小定義は、以下のようになる。
  {
    bucket = "通知を紐付けるS3バケット名"
  }

  任意項目を含めたのmapの完全な定義は、以下のようになる。
  {
    bucket = "通知を紐付けるS3バケット名"
    id = "通知設定のユニークなID"
    lambda_function_arn = "通知を受け取るLambdaのARN。未指定の場合は、datadog_forwarder_lambda_arnに指定した値が使用される"
    events = "どのイベントに対して通知を行うか。未指定の場合は、\"s3:ObjectCreated:*\"が指定される。複数のイベントを指定する場合は、,区切りの文字列で指定すること"
    filter_prefix = "通知をオブジェクトのキー名のprefixで絞り込む場合に使用"
    filter_suffix = "通知をオブジェクトのキー名のsuffixで絞り込む場合に使用"
  }

  これらの定義を含んだリストを設定する。
  [
    {
      bucket = "bucket1"
    },
    {
      bucket = "bucket2"
    }
  ]
  DESCRIPTION

  type    = list(map(string))
  default = []
}