output "arn" {
  value       = module.step_functions.arn
  description = "Step FunctionsのステートマシンのARN。"
}

output "role_arn" {
  value       = module.step_functions.role_arn
  description = "Step Functionsのステートマシンを実行するロールのARN。"
}

output "role_name" {
  value       = module.step_functions.role_name
  description = "Step Functionsのステートマシンを実行するロールの名前を返す。"
}
