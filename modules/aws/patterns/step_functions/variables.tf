variable "sfn_name" {
  type        = string
  description = "Step Functionsのステートマシンの名前を指定する。ロールなどのリソース名の編集にも使用している。文字数超過エラーが起きた場合は名称の長さを調整すること。"
}

variable "definition_json" {
  type        = string
  description = "Step Functionsの実行対象となる、ステートマシンのJSON定義を指定する。"
}

variable "region" {
  type        = string
  default     = null
  description = <<EOF
Step Functionsのステートマシンを実行するリージョンを指定する。指定しなかった場合は、現在Terraformを実行中のリージョンが指定される。
EOF

}

variable "log_group_name" {
  type        = string
  description = "ロググループの名前を指定する。"
}

variable "log_level" {
  type        = string
  description = <<EOF
CloudWatch Logsに書き出すログのログレベルとして、`OFF`、`ALL`、`ERROR`、`FATAL`のどれかを指定可能。
詳細は[ログレベル](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/cloudwatch-log-level.html)を参照のこと。デフォルトは`ALL`。
EOF
  default     = "ALL"
}


variable "log_group_retention_in_days" {
  type        = number
  description = "CloudWatch Logsにログを残す日数を指定する。デフォルトは14日間。"
  default     = 14
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}

variable "enabled_xray" {
  type        = bool
  description = "X-Rayを利用するか指定する。デフォルトでは利用しない。"
  default     = false
}

variable "slack_notification_configs" {
  type        = list(any)
  default     = []
  description = <<EOF
本設定があった場合はSlackに対して通知を行う。詳細は`step_functions pattern`の「通知」を参照。

example:
```
slack_notification_configs = [
  {
    webhook_uri = "https://webhook.example.com/webhook"
    input_transformer_input_paths = null
    input_transformer_input_template = null
    event_bus = "default"
    event_pattern_status = ["SUCCEEDED"]
  },
    webhook_uri = "https://another-webhook.example.com/webhook"
    input_transformer_input_paths = null
    input_transformer_input_template = null
    event_bus = "default"
    event_pattern_status = ["RUNNING"]
  }
]
```
EOF
}

variable "teams_notification_configs" {
  type        = list(any)
  default     = []
  description = <<EOF
本設定があった場合はMicrosoft Teamsに対して通知を行う。詳細は`step_functions pattern`の「通知」を参照。

example:
```
teams_notification_configs = [
  {
    webhook_uri = "https://webhook.example.com/webhook"
    input_transformer_input_paths = null
    input_transformer_input_template = null
    event_bus = "default"
    event_pattern_status = ["SUCCEEDED"]
  },
    webhook_uri = "https://another-webhook.example.com/webhook"
    input_transformer_input_paths = null
    input_transformer_input_template = null
    event_bus = "default"
    event_pattern_status = ["RUNNING"]
  }
]
```
EOF
}

variable "execution_resource_arns" {
  type        = list(string)
  default     = []
  description = <<EOF
ステートマシンのタスクが連携(統合)するAWSリソースのARNをリストで指定する。それにより、ステートマシンのIAM実行ロールに必要なIAMポリシーがアタッチされる。

対応しているAWSリソースは以下の通り。

- [AWS Lambda](https://aws.amazon.com/jp/lambda/)関数
- [Amazon Elastic Container Service](https://aws.amazon.com/jp/ecs/)(ECS)タスク
- [Amazon Simple Notification Service](https://aws.amazon.com/jp/sns/)(SNS)トピック
- [Amazon Simple Queue Service](https://aws.amazon.com/jp/sqs/)(SQS)キュー

これら以外のAWSリソースを使用する場合は、利用者側で必要な権限を持ったIAMポリシーを作成し、ステートマシンの実行ロールにアタッチしていただく必要がある。
EOF
}

variable "s3_event_configs" {
  type        = list(any)
  default     = []
  description = <<EOF
S3バケットのイベント経由でステートマシンを開始したい場合に指定する。

- `bucket_name`: ステートマシンを開始するトリガーとなるS3バケットの名前。(Required)
- `events`: ステートマシンを起動するS3イベント。イベントの種類は[AWS CloudTrailのログ記録によって追跡されるAmazon S3オブジェクトレベルのアクション](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/cloudtrail-logging-s3-info.html#cloudtrail-object-level-tracking)を参照。(Required)
- `filter_prefix`: S3バケット側のイベント発行の対象になるS3オブジェクトのプレフィックス。(Optional)

example:
```
s3_event_configs = [
  {
      bucket_name   = YOUR_BUCKET_NAME
      events        = ["PutObject"]
      filter_prefix = "prefix"
  },
]
```
EOF

}

variable "schedule_configs" {
  type        = list(any)
  default     = []
  description = <<EOF

EeventBridgeのスケジュールを使ってステートマシンを開始したい場合に指定する。

`schedule_expression` にはスケジュールを`rate()`か`cron()`で指定する。
記載方法は[RateまたはCronを使用したスケジュール式](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/services-cloudwatchevents-expressions.html)を参照。

`is_enabled`が`true`の時はスケジューリングを有効にする。デフォルトは`false`。

example:
```
schedule_configs = [
  {
    schedule_expression = "rate(5 minutes)"
    is_enabled = true
  },
  {
    schedule_expression = "cron(0/5 * * ? *)"
    is_enabled = false
  }

]
```

EOF

}
