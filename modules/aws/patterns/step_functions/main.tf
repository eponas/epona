# FIXME: コンポーネントからコンポーネントへの依存関係は今後排除していく

data "aws_region" "current" {}
locals {
  region = var.region == null ? data.aws_region.current.name : var.region
}

module "step_functions" {
  source                      = "../../components/step_functions/"
  region                      = local.region
  sfn_name                    = var.sfn_name
  definition_json             = var.definition_json
  enabled_xray                = var.enabled_xray
  log_level                   = var.log_level
  log_group_name              = var.log_group_name
  log_group_retention_in_days = var.log_group_retention_in_days
  tags                        = var.tags
}


module "slack_notification" {
  count                            = length(var.slack_notification_configs)
  source                           = "../../components/step_functions/notifications/slack"
  name_prefix                      = "SfnSlackNotification${var.sfn_name}_${format("%04d", count.index)}_"
  webhook_uri                      = lookup(var.slack_notification_configs[count.index], "webhook_uri")
  input_transformer_input_paths    = lookup(var.slack_notification_configs[count.index], "input_transformer_input_paths", null)
  input_transformer_input_template = lookup(var.slack_notification_configs[count.index], "input_transformer_input_template", null)
  event_pattern_status             = lookup(var.slack_notification_configs[count.index], "event_pattern_status", null)
  event_bus_name                   = lookup(var.slack_notification_configs[count.index], "event_bus_name", null)
  sfn_machine_arn                  = module.step_functions.arn
  tags                             = var.tags
}


module "teams_notification" {
  count                            = length(var.teams_notification_configs)
  source                           = "../../components/step_functions/notifications/teams"
  name_prefix                      = "SfnTeamsNotification${var.sfn_name}_${format("%04d", count.index)}_"
  webhook_uri                      = lookup(var.teams_notification_configs[count.index], "webhook_uri")
  input_transformer_input_paths    = lookup(var.teams_notification_configs[count.index], "input_transformer_input_paths", null)
  input_transformer_input_template = lookup(var.teams_notification_configs[count.index], "input_transformer_input_template", null)
  event_pattern_status             = lookup(var.teams_notification_configs[count.index], "event_pattern_status", null)
  event_bus_name                   = lookup(var.teams_notification_configs[count.index], "event_bus_name", null)
  sfn_machine_arn                  = module.step_functions.arn
  tags                             = var.tags
}


module "execution_resource_roles" {
  count         = length(var.execution_resource_arns)
  source        = "../../components/step_functions/execution_resource_roles"
  region        = local.region
  arn           = var.execution_resource_arns[count.index]
  sfn_role_name = module.step_functions.role_name
  sfn_name      = var.sfn_name
  tags          = var.tags
}

module "s3_event_to_start_sfn" {
  count                      = length(var.s3_event_configs)
  source                     = "../../components/step_functions/s3_event"
  bucket_name                = lookup(var.s3_event_configs[count.index], "bucket_name")
  name_uniq                  = "${lookup(var.s3_event_configs[count.index], "bucket_name")}_${format("%04d", count.index)}"
  filter_prefix              = lookup(var.s3_event_configs[count.index], "filter_prefix", null)
  events                     = lookup(var.s3_event_configs[count.index], "events", null)
  step_functions_machine_arn = module.step_functions.arn
  tags                       = var.tags
}

module "schedule" {
  source           = "../../components/step_functions/schedule"
  sfn_name         = var.sfn_name
  sfn_arn          = module.step_functions.arn
  schedule_configs = var.schedule_configs
  tags             = var.tags
}
