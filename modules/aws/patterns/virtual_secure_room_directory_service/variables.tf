# common
variable "tags" {
  description = "仮想セキュアルームに関するリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

# Directory Service
variable "directory_service_domain_name" {
  description = "ディレクトリのFQDN。例: corp.example.com"
  type        = string
}

variable "directory_service_domain_password" {
  description = "Directory Serviceのadminのパスワード"
  type        = string
}

variable "directory_service_short_name" {
  description = "ディレクトリの短縮ドメイン名"
  type        = string
}

variable "directory_service_ou" {
  description = "ディレクトリ内の組織(OU=<短縮名>,DC=<第2レベルドメイン>,DC=<第1レベルドメイン>)"
  type        = string
}

variable "directory_service_vpc_id" {
  description = "Directory ServiceをデプロイするVPCのID"
  type        = string
}

variable "directory_service_subnet_ids" {
  description = "ディレクトリをデプロイするサブネットIDのリスト。異なるAZにあるサブネットIDを指定すること。"
  type        = list(string)
}

# AD Manager
## EC2
variable "ad_manager_name" {
  description = "AD管理ツールを導入するEC2インスタンスのNameタグの値"
  type        = string
}

variable "ad_manager_administrator_password" {
  description = "AD管理ツールを導入するEC2インスタンスのAdministratorのパスワード"
  type        = string
}

variable "ad_manager_ami" {
  description = "AD管理ツールを導入するEC2インスタンスのAMI。指定なしの場合、最新のWindows2019Base日本語版のAMIを使用"
  type        = string
  default     = null
}

variable "ad_manager_instance_type" {
  description = "AD管理ツールを導入するEC2インスタンスのインスタンスタイプ"
  type        = string
  default     = "t2.micro"
}

variable "ad_manager_subnet_id" {
  description = "AD管理ツールを導入するEC2インスタンスを配置する、サブネットのID"
  type        = string
}

variable "ad_manager_root_block_volume_size" {
  description = "AD管理ツールを導入するEC2インスタンスの、ルートデバイスボリュームのサイズ"
  type        = string
  default     = "30"
}

## IAM
variable "ad_manager_role_name" {
  description = "AD管理ツールを導入するEC2インスタンスに付与するIAMロール名"
  type        = string
}

variable "ad_manager_instance_profile_name" {
  description = "AD管理ツールを導入するEC2インスタンスに付与するインスタンスプロファイル名"
  type        = string
}

## SG
variable "ad_manager_sg_name" {
  description = "AD管理ツールを導入するEC2インスタンスに付与するSGの名前"
  type        = string
}

variable "ad_manager_sg_ingresses" {
  description = <<-DESC
  AD管理ツールを導入するEC2インスタンスへのアクセス許可。記述例は以下の通り。
  ```
  rules = [{
    port        = 3389
    protocol    = "TCP"
    cidr_blocks = ["192.0.2.0/24"]
    description = "AD Manager inbound SG Rule"
  }]
  ```
  DESC
  type = list(object({
    port        = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
}
