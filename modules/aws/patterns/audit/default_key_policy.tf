data "aws_caller_identity" "self" {}

data "aws_iam_policy_document" "trail_kms_access_policy" {
  version   = "2012-10-17"
  policy_id = "Default CloudTrail Key Policy"
  statement {
    sid    = "Enable IAM User ReadOnly Permissions"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.self.account_id}:root"]
    }
    actions = [
      "kms:DescribeKey",
      "kms:GetKeyPolicy",
      "kms:GetKeyRotationStatus",
      "kms:GetPublicKey",
      "kms:ListKeys",
      "kms:ListAliases",
      "kms:ListKeyPolicies"
    ]
    resources = ["*"]
  }
  statement {
    sid    = "Enable IAM User Key Management Permissions"
    effect = "Allow"
    principals {
      type = "AWS"
      identifiers = concat(
        [
          ## FIXME: 権限管理用のpatternができたら、デフォルトとして管理グループをremote_stateから取得する
        ],
        var.kms_key_management_arns
      )
    }
    actions = [
      "kms:*"
    ]
    resources = ["*"]
  }
  statement {
    sid    = "Allow CloudTrail to encrypt logs"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["kms:GenerateDataKey*"]
    resources = ["*"]
    condition {
      test     = "StringLike"
      variable = "kms:EncryptionContext:aws:cloudtrail:arn"
      values   = ["arn:aws:cloudtrail:*:${data.aws_caller_identity.self.account_id}:trail/*"]
    }
  }
  statement {
    sid    = "Allow CloudTrail to describe key"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["kms:DescribeKey"]
    resources = ["*"]
  }
  statement {
    sid    = "Allow principals in the account to decrypt log files"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = [data.aws_caller_identity.self.account_id]
    }
    actions = [
      "kms:Decrypt",
      "kms:ReEncryptFrom"
    ]
    resources = ["*"]
    condition {
      test     = "StringLike"
      variable = "kms:EncryptionContext:aws:cloudtrail:arn"
      values   = ["arn:aws:cloudtrail:*:${data.aws_caller_identity.self.account_id}:trail/*"]
    }
  }
}
