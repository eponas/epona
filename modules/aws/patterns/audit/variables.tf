variable "trail_name" {
  description = "Trailの名称"
  type        = string
}

variable "is_multi_region_trail" {
  description = "全リージョンの証跡を記録するか否か。<https://aws.amazon.com/jp/cloudtrail/faqs/> を参照"
  type        = bool
  default     = true
}

variable "trail_tags" {
  description = "Trailに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "event_read_write_type" {
  description = "管理イベントのうち読み取りイベント、書き込みイベントを証跡として残すかどうかの設定。読み取りイベントのみを証跡とする場合は\"ReadOnly\"、書き込みイベントのみの場合は \"WriteOnly\"、双方の場合は \"All\" を指定する"
  type        = string
  default     = "All"
}

variable "event_data_resources" {
  description = <<-DESCRIPTION
CloudTrailで監視可能な、データイベントを設定する。データイベントとして設定できるのは、S3とLambdaに関するイベントのみ。
ただし、デフォルトではどちらも記録されない。
記録するように設定する場合は、それぞれ以下のように設定すること。

S3のイベントをロギングする場合、以下を本パラメータのmapに追加すること。
```
"s3_event" = {
  type   = "AWS::S3::Object"
  values = ["arn:aws:s3:::"]
}
```

Lambdaのイベントをロギングする場合、以下を本パラメータのmapに追加すること。
```
"lambda_event" = {
  type   = "AWS::Lambda::Function"
  values = ["arn:aws:lambda"]
}
```

（typeおよびvaluesの意味については、[DataResource - AWS CloudTrail](https://docs.aws.amazon.com/awscloudtrail/latest/APIReference/API_DataResource.html)を参照。
また、mapのキーについては任意の名称を指定）
DESCRIPTION
  type = map(object({
    type   = string
    values = list(string)
  }))
  default = {}
}

variable "bucket_name" {
  description = "証跡を保存するS3 bucket名"
  type        = string
}

variable "bucket_force_destroy" {
  description = "bucketとともに保存されているデータを強制的に削除可能にする。証跡は監査上重要なものであるため、削除可能にする(trueを設定する)のは開発用途に限定すること"
  type        = bool
  default     = false
}

variable "bucket_mfa_delete" {
  description = "証跡が保存されているS3 Bucketを削除するとき、ルートアカウントのMFAを要求する([Reserved for future] terraform単体では設定できないため、設定手順はpatternドキュメントを参照のこと)"
  type        = bool
  default     = false
}

variable "bucket_transitions" {
  description = "証跡の移行に対するポリシー設定"
  type        = list(map(string))
  default     = []
}

variable "kms_key_alias_name" {
  description = "CloudTrailのログを暗号化するためのKMSキーのエイリアス名。"
  type        = string
  default     = "alias/trail-encryption-key"
}

variable "kms_custom_key_policy" {
  description = "CloudTrailのログを暗号化に用いるKMSのキーポリシーに、追加のキーポリシーを指定する。"
  type        = string
  default     = null
}

variable "kms_key_deletion_in_days" {
  description = "CloudTrailを暗号化するKMSキーを削除する際の待機日数。7以上30以下で設定してください。(<https://docs.aws.amazon.com/kms/latest/APIReference/API_ScheduleKeyDeletion.html#KMS-ScheduleKeyDeletion-request-PendingWindowInDays> を参照)"
  type        = number
  default     = 30
}

variable "kms_key_tags" {
  description = "CloudTrailのログを暗号化するKMSキーに付与するタグ。"
  type        = map(string)
  default     = {}
}

variable "kms_key_management_arns" {
  description = "KMSキーの管理をするIAMエンティティのARN一覧"
  type        = list(string)
}
