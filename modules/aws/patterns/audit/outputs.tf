output "cloudtrail_arn" {
  description = "TrailのARN"
  value       = module.cloudtrail.cloudtrail_arn
}

output "bucket_arn" {
  description = "証跡用bucketのARN"
  value       = module.cloudtrail.bucket_arn
}

output "bucket_id" {
  description = "証跡用bucketのID"
  value       = module.cloudtrail.bucket_id
}

output "bucket" {
  description = "証跡用bucket名"
  value       = module.cloudtrail.bucket
}