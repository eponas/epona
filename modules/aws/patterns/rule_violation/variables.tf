variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグを設定する。"
  type        = map(string)
  default     = {}
}

variable "function_name" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数の名前を設定する。"
  type        = string
  default     = "ruleViolationFunction"
}

variable "aws_config_role_name" {
  description = "AWS Configがリソースの情報を収集するために使用するIAMロールの名称を設定する。"
  type        = string
  default     = "ruleViolation"
}

variable "s3_bucket_name" {
  description = "AWS Configがリソースの設定変更の詳細を送信するS3バケット名を設定する。"
  type        = string
}

variable "notification_type" {
  description = "結果の通知先となるチャットツールの種類（`slack`: Slack | `teams`: Microsoft Teams）を設定する。"
  type        = string
}

variable "notification_channel" {
  description = "結果の通知先となるSlack channelのIncoming Webhook URLを設定する。`changeDetectionNotice`キーに対する値としては、対象リソース変更時の通知先、`ruleViolationNotice`キーに対する値としては、ルール違反情報の通知先を設定すること"
  type        = map(string)
  # 検知結果の範囲ごとに通知先のチャンネルのパスを指定（通知しない場合は未定義またはnullを指定）
  default = {
    changeDetectionNotice = null
    ruleViolationNotice   = null
  }
}

variable "notification_lambda_source_dir" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数のソースコードが配置されたディレクトリのパスを設定する。"
  type        = string
  default     = null
}

variable "notification_lambda_handler" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数の、エントリーポイントとなる関数名を設定する。"
  type        = string
  default     = "index.handler"
}

variable "notification_lambda_runtime" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数のランタイムを設定する。設定内容については[AWS Lambdaランタイム](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/lambda-runtimes.html)を参照してください。"
  type        = string
  default     = "nodejs12.x"
}

variable "notification_lambda_timeout" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数のタイムアウト時間（秒）を設定する。デフォルトでは10秒。"
  type        = number
  default     = 10
}

variable "notification_topic_name" {
  description = "AWS Configがリソースの設定変更またはルール違反を通知するトピック名を設定する。"
  type        = string
  default     = "rule-violation-topic"
}

variable "aws_config_recorder_name" {
  type        = string
  description = "AWS Configがリソースの設定変更を検出するための「設定レコーダー」の名称を設定する。"
  default     = "rule-violation-recorder"
}

variable "aws_config_channel_name" {
  type        = string
  description = "AWS Configがリソースの状態を配信するための「配信チャネル」の名称を設定する。"
  default     = "rule-violation-channel"
}
variable "s3_bucket_force_destroy" {
  description = "AWS Configが使用するS3バケットを強制的に削除するかどうかを設定する。"
  type        = bool
  default     = false
}

variable "bucket_transitions" {
  description = "AWS Configが使用するS3バケットに対するポリシーを設定する。未設定の場合は30日後にAmazon S3 Glacierへ移行する。"
  type        = list(map(string))
  default = [{
    days          = 30
    storage_class = "GLACIER"
  }]
}

variable "recorder_status" {
  description = "AWS Configを有効にするかどうかを設定する。"
  type        = bool
  default     = true
}

variable "delivery_frequency" {
  description = "AWSConfigが設定スナップショットを配信する頻度を設定する。"
  type        = string
  default     = "Six_Hours"
}

variable "configrule_prefix" {
  description = "既存ルールとの重複を避けるため、ルール名に付与するプレフィックスを設定する。"
  type        = string
  default     = "rule-violation"
}
variable "configrule_access_key_max_age" {
  description = "ACCESS_KEYS_ROTATEDルールのアクセスキーのローテーション期間を設定する。"
  type        = string
  default     = "90"
}
variable "configrule_access_key_frequency" {
  description = "ACCESS_KEYS_ROTATEDルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_acm_days_to_expiration" {
  description = "ACM_CERTIFICATE_EXPIRATION_CHECKルールのACM証明書の有効期限を設定する。"
  type        = string
  default     = "30"
}
variable "configrule_acm_certificate_frequency" {
  description = "ACM_CERTIFICATE_EXPIRATION_CHECKルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_cloud_trail_encryption_frequency" {
  description = "CLOUD_TRAIL_ENCRYPTION_ENABLEDルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_cloud_trail_enabled_frequency" {
  description = "CLOUD_TRAIL_ENABLEDルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_cloud_trail_validation_frequency" {
  description = "CLOUD_TRAIL_LOG_FILE_VALIDATION_ENABLEDルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_password_max_age" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、パスワードの有効期限を指定する場合に設定する。"
  type        = string
  default     = "90"
}
variable "configrule_password_min_length" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、パスワードの最小の長さを指定する場合に設定する。"
  type        = string
  default     = "14"
}
variable "configrule_password_reuse_prevention" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、パスワードの再使用を禁止する世代数を指定する場合に設定する。"
  type        = string
  default     = "24"
}
variable "configrule_password_require_lower_case" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、小文字の使用有無を指定する場合に設定する。"
  type        = string
  default     = "true"
}
variable "configrule_password_require_numbers" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、数字の使用有無を指定する場合に設定する。"
  type        = string
  default     = "true"
}
variable "configrule_password_require_symbols" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、記号の使用有無を指定する場合に設定する。"
  type        = string
  default     = "true"
}
variable "configrule_password_require_upper_case" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、大文字の使用有無を指定する場合に設定する。"
  type        = string
  default     = "true"
}
variable "configrule_password_policy_frequency" {
  description = "IAM_PASSWORD_POLICYルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_iam_root_access_key_frequency" {
  description = "IAM_ROOT_ACCESS_KEY_CHECKルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_iam_mfa_enabled_frequency" {
  description = "MFA_ENABLED_FOR_IAM_CONSOLE_ACCESSルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_cloud_trail_multi_region_frequency" {
  description = "MULTI_REGION_CLOUD_TRAIL_ENABLEDルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_root_mfa_frequency" {
  description = "ROOT_ACCOUNT_HARDWARE_MFA_ENABLEDルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_s3_public_read_frequency" {
  description = "S3_BUCKET_PUBLIC_READ_PROHIBITEDルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_s3_public_write_frequency" {
  description = "S3_BUCKET_PUBLIC_WRITE_PROHIBITEDルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "configrule_waf_logging_frequency" {
  description = "WAFV2_LOGGING_ENABLEDルールのチェック頻度を設定する。"
  type        = string
  default     = "TwentyFour_Hours"
}
variable "enable_default_rule" {
  description = "デフォルトルールを適用するかどうかを設定する。"
  type        = bool
  default     = true
}
variable "create_config_recorder_delivery_channel" {
  description = "AWS Configのリソースのうち、Config Recorder, Deliverly Channelを作成するかどうかを設定する。すでにAWS Configを使用しているリージョンにパターンを適用する場合はfalseとする。"
  type        = bool
  default     = true
}
variable "include_grobal_resources" {
  description = "グローバルリソースを監視対象に含めるかどうかを設定する。"
  type        = bool
  default     = true
}
variable "all_supported" {
  description = "リージョン内の全リソースを監視対象にするかどうかを設定する。`recording_resource_types`を設定する場合は`false`にする必要がある。"
  type        = bool
  default     = true
}
variable "recording_resource_types" {
  description = "監視対象とするリソースを指定する。このパラメータを設定する場合は`all_supported`は`false`にする必要がある。"
  type        = list(string)
  default     = []
}

variable "log_group_retention_in_days" {
  description = <<-DESCRIPTION
  ログをCloudWatch Logsに出力する場合の保持期間を設定する。

  値は、次の範囲の値から選ぶこと： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.
  [Resource: aws_cloudwatch_log_group / retention_in_days](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group#retention_in_days)
  DESCRIPTION
  type        = number
  default     = null
}

variable "log_group_kms_key_id" {
  description = "CloudWatch Logsを暗号化するためのKMS CMKのARN"
  type        = string
  default     = null
}
