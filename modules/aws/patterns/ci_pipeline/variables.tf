variable "name" {
  description = "GitLab Runnerに関するリソースに対して、共通的に付与する名前"
  type        = string
}

variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "vpc_id" {
  description = "このモジュールで作成するリソースが配置される、VPCのID"
  type        = string
}

variable "static_resource_buckets" {
  description = "静的リソースを配置するS3バケットに関する設定を指定する（bucket_name, force_destroy, runtime_account_id）。このバケットに出力された静的リソースは、`cd_pipeline_frontend`でのデプロイ対象とする"
  type        = list(map(any))
  default     = []
  # bucket_name        = string
  # force_destroy      = bool
  # runtime_account_id = string
}

variable "artifact_buckets" {
  description = "ビルドしたアーティファクトを配置するバケットに関する設定を指定する（bucket_name, force_destroy, runtime_account_id）。このバケットに出力されたアーティファクトは、利用者自身でデプロイする"
  type        = list(map(any))
  default     = []
  # bucket_name        = string
  # force_destroy      = bool
  # runtime_account_id = string
}

variable "container_image_repositories" {
  description = "作成するECRに関する設定を指定する（name, image_scan_on_push, image_tag_mutability, lifecycle_policy, repository_policy）"
  type        = list(map(any))
  default     = []
  #  name                 = string
  #  image_scan_on_push   = bool
  #  image_tag_mutability = bool
  #  lifecycle_policy     = string
  #  repository_policy    = string
}

variable "ci_runner_ami" {
  description = "GitLab Runnerを稼働させるEC2インスタンスインスタンスのAMI"
  type        = string
}

variable "ci_runner_instance_type" {
  description = "GitLab Runnerを稼働させるEC2インスタンスのインスタンスタイプ"
  type        = string
}

variable "ci_runner_root_block_volume_size" {
  description = "GitLab Runnerを稼働させるEC2インスタンスの、ルートデバイスボリュームのサイズ"
  type        = string
}

variable "ci_runner_subnet" {
  description = "GitLab Runnerを稼働させるEC2インスタンスを配置する、サブネットのID"
  type        = string
}

variable "gitlab_runner_gitlab_url" {
  description = "GitLab Runnerのアクセス先となる、GitLabのURL"
  type        = string
  default     = "https://gitlab.com"
}

variable "gitlab_runner_registration_token" {
  description = "GitLab RunnerのGitLabに登録するためのトークン。ci_runner_cloud_init_scriptを指定する場合は不要"
  type        = string
  default     = null # ci_runner_cloud_init_script を利用する場合は、registration-tokenは不要になるため
}

variable "gitlab_runner_pull_image_always" {
  description = <<DESCRIPTION
GitLab RunnerがCIジョブでコンテナイメージを取得する際に常に最新のイメージを利用するかどうかのフラグ。
値がtrueである場合、`pull_policy`に`always`が設定され、コンテナイメージは常に最新のイメージをpullする。
false(もしくはデフォルト値)を指定した場合、`pull_policy`に`always`が設定され、ローカルにイメージがある場合はローカルイメージを利用する。
pull_policyの詳細は、[How pull policies work](https://docs.gitlab.com/runner/executors/docker.html#how-pull-policies-work)を参照してください。
DESCRIPTION
  type        = bool
  default     = false
}

variable "ci_runner_cloud_init_script" {
  description = "GitLab Runnerを稼働させるEC2インスタンスの構築時に適用する、Cloud Initスクリプト。GitLab RunnerおよびCIで必要なパッケージのインストールなどに使用する"
  type        = string
  default     = null
}

variable "ci_runner_security_groups" {
  description = "GitLab Runnerを稼働させるEC2インスタンスに適用する、セキュリティグループID"
  type        = list(string)
  default     = []
}
