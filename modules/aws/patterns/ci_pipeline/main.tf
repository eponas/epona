module "build_artifact_buckets" {
  source = "../../components/s3/build_artifact_bucket"

  tags = var.tags

  buckets = concat(var.static_resource_buckets, var.artifact_buckets)
}

module "container_image_repositories" {
  source = "../../components/container/repositories"

  tags = var.tags

  repositories = var.container_image_repositories
}

module "gitlab_runner_instance_profile" {
  source = "../../components/iam/gitlab_runner_instance_profile"

  iam_role_name             = format("%sGitLabRunnerRole", title(var.name))
  iam_instance_profile_name = format("%sGitLabRunnerInstanceProfile", title(var.name))
}

module "gitlab_runner_simple_security_group" {
  source = "../../components/security_group/generic"

  name = "${var.name}-security-group"
  tags = var.tags

  create = length(var.ci_runner_security_groups) == 0

  vpc_id = var.vpc_id

  ingresses = []

  egresses = [{
    port        = -1
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "GitLab Runner inbound SG Rule"
  }]
}

module "gitlab_runner" {
  source = "../../components/ec2/gitlab_runner"

  name = var.name
  tags = var.tags

  ami                              = var.ci_runner_ami
  instance_type                    = var.ci_runner_instance_type
  root_block_volume_size           = var.ci_runner_root_block_volume_size
  iam_instance_profile             = module.gitlab_runner_instance_profile.iam_instance_profile_name
  subnet                           = var.ci_runner_subnet
  security_groups                  = length(var.ci_runner_security_groups) > 0 ? var.ci_runner_security_groups : [module.gitlab_runner_simple_security_group.security_group_id]
  gitlab_runner_gitlab_url         = var.gitlab_runner_gitlab_url
  gitlab_runner_registration_token = var.gitlab_runner_registration_token != null ? var.gitlab_runner_registration_token : null
  gitlab_runner_pull_image_always  = var.gitlab_runner_pull_image_always
  init_script                      = var.ci_runner_cloud_init_script
}
