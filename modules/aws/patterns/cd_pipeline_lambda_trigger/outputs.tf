output "cross_account_access_role_arn" {
  description = "CodePipelineからのクロスアカウントアクセスに使用するRoleのARN"
  value       = module.cross_account_codepipeline_role_lambda.arn
}
