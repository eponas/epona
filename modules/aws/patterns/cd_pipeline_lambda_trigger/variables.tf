variable "pipeline_name" {
  description = "CodePipelineの名前"
  type        = string
}

variable "source_bucket_arn" {
  description = "デプロイ元となるS3バケットのARN"
  type        = string
}

variable "runtime_account_id" {
  description = "Runtime環境のアカウントID"
  type        = string
}

variable "artifact_store_bucket_arn" {
  description = "本ロールがアクセスするRuntime環境のArtifact storeのARN。artifact storeは`cd_pipeline_lambda pattern`のインスタンス作成時に作成される。形式は`arn:aws:s3:::[アーティファクトストアのバケット名]`となるため、バケット名さえ決定すれば`cd_pipeline_lambda pattern`の適用を待たずに設定可能"
  type        = string
}

variable "artifact_store_bucket_encryption_key_arn" {
  description = "本ロールがアクセスするRuntime環境のArtifact store暗号化用CMKのARN。artifact storeは`cd_pipeline_lambda pattern`のインスタンス作成時に作成される"
  type        = string
  default     = null
}

variable "deployment_source_object_key" {
  description = "CodePipelineでデプロイされるLambda関数定義が圧縮されたzipファイルのS3 Object Key"
  type        = string
  default     = "source.zip"
}

variable "target_event_bus_arn" {
  description = "送信先のEventBusのARN"
  type        = string
}
