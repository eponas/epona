# Epona の思想上、Terraform を実行するのは Delivery 環境上の Terraformer であるため、
# 取得できるのは Delivery 環境のアカウント ID 等になる
data "aws_caller_identity" "delivery" {}

locals {
  cross_account_deploy = var.runtime_account_id != data.aws_caller_identity.delivery.account_id
}

module "source_bucket" {
  source = "../../components/codepipeline/source/s3/"

  name                           = var.name
  bucket_name                    = var.bucket_name
  force_destroy                  = var.bucket_force_destroy
  tags                           = var.tags
  noncurrent_version_transitions = var.bucket_noncurrent_version_transitions
}

# Deliviery環境上におけるCodePipeline用ロールを作成する
module "cross_account_codepipeline_role" {
  source = "../../components/iam/cross_account_codepipeline/"

  pipeline_name      = var.name
  runtime_account_id = var.runtime_account_id
  repository_arns    = values(var.ecr_repositories)[*].arn
  source_bucket_arn  = module.source_bucket.arn

  # 以下は cd_pipeline_backend を動かしてから設定する
  artifact_store_bucket_arn                = var.artifact_store_bucket_arn
  artifact_store_bucket_encryption_key_arn = var.artifact_store_bucket_encryption_key_arn
}

module "event_bus_role" {
  source = "../../components/iam/event_bus/sender/"

  # Delivery 環境と Runtime 環境が同一の場合、Event Bus は不要
  count = local.cross_account_deploy ? 1 : 0

  source_name          = var.name
  target_event_bus_arn = var.target_event_bus_arn
}

# ECR に PUSH されたら Event Bus にイベントを送信する
module "ecr_event_bus_sender" {
  source = "../../components/cloudwatch/event_bus/sender/"

  # Delivery 環境と Runtime 環境が同一の場合、Event Bus は不要
  count = local.cross_account_deploy ? 1 : 0

  target_event_bus_arn        = var.target_event_bus_arn
  cloudwatch_event_rule_names = values(aws_cloudwatch_event_rule.ecr)[*].name
  role_arn                    = module.event_bus_role[0].arn
}

# S3 に PutObject されたら Event Bus にイベントを送信する
module "s3_event_bus_sender" {
  source = "../../components/cloudwatch/event_bus/sender/"

  # Delivery 環境と Runtime 環境が同一の場合、Event Bus は不要
  count = local.cross_account_deploy ? 1 : 0

  target_event_bus_arn        = var.target_event_bus_arn
  cloudwatch_event_rule_names = [aws_cloudwatch_event_rule.s3[0].name]
  role_arn                    = module.event_bus_role[0].arn
}

resource "aws_cloudwatch_event_rule" "ecr" {
  # Delivery 環境と Runtime 環境が同一の場合、Event Bus への Cloudwatch Events の送出は不要
  for_each = local.cross_account_deploy ? toset(keys(var.ecr_repositories)) : []

  name          = "Fire${var.name}-PushedToECR-${each.key}"
  description   = "Amazon CloudWatch Events rule to automatically start your pipeline when a change occurs in the Amazon ECR ${each.key} image tag."
  event_pattern = <<-JSON
  {
    "source": [
      "aws.ecr"
    ],
    "detail-type": [
      "ECR Image Action"
    ],
    "detail": {
        "action-type": [
          "PUSH"
        ],
        "image-tag": ${jsonencode(var.ecr_repositories[each.key].tags)},
        "repository-name": [
          ${jsonencode(each.key)}
        ],
        "result": [
          "SUCCESS"
        ]
    }
  }
  JSON
}

resource "aws_cloudwatch_event_rule" "s3" {

  # Delivery 環境と Runtime 環境が同一の場合、Event Bus への Cloudwatch Events の送出は不要
  count = local.cross_account_deploy ? 1 : 0

  name          = "Fire${var.name}-change-deploy-setting"
  description   = "Amazon CloudWatch Events rule to automatically start your pipeline when a PUSH occurs in the Amazon S3."
  event_pattern = <<-JSON
  {
    "source": [
      "aws.s3"
    ],
    "detail-type": [
      "AWS API Call via CloudTrail"
    ],
    "detail": {
        "eventSource": [
          "s3.amazonaws.com"
        ],
        "eventName": [
          "PutObject"
        ],
        "requestParameters": {
          "bucketName": [
            ${jsonencode(module.source_bucket.name)}
          ],
          "key": [
            ${jsonencode(var.deployment_setting_key)}
          ]
        }
    }
  }
  JSON
}
