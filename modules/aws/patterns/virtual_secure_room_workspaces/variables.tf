variable "tags" {
  description = "workspacesに関連するリソースに、共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "workspaces_directory_id" {
  description = "参加するDirectoryServiceのID"
  type        = string
}

variable "workspaces_subnet_ids" {
  description = "DirectoryServiceが存在するサブネットIDのリスト"
  type        = list(string)
}

variable "workspaces_user_names" {
  description = "Workspacesを利用するDirectory Serviceのユーザーのリスト"
  type        = list(string)
}

variable "workspaces_bundle_id" {
  description = "WorkspacesのOS種別。とくに指定しない場合「Standard with Windows 10(Japanese)」で作られる。"
  type        = string
  default     = null
}

variable "workspaces_kms_key" {
  description = "Workspacesのボリューム暗号化に使用するCMKのエイリアス名"
  type        = string
}

variable "workspaces_compute_type_name" {
  description = "Workspacesのタイプ(VALUE, STANDARD, PERFORMANCE, POWER, GRAPHICS, POWERPRO and GRAPHICSPRO)。それぞれのタイプの違いについては[Amazon WorkSpaces の特徴](https://aws.amazon.com/jp/workspaces/features/#Amazon_WorkSpaces_Bundles)を参照。"
  type        = string
}

variable "workspaces_user_volume_size_gib" {
  description = "Workspacesのユーザーボリューム(Dドライブ)の容量"
  type        = number
}

variable "workspaces_root_volume_size_gib" {
  description = "Workspacesのルートボリューム(Cドライブ)の容量"
  type        = number
}

variable "workspaces_running_mode" {
  description = "Workspacesの起動モード(AUTO_STOP or ALWAYS_ON)"
  type        = string
  default     = "AUTO_STOP"
}

variable "workspaces_running_mode_auto_stop_timeout_in_minutes" {
  description = "Workspaces起動から自動停止するまでの分数を60分単位で指定する。例：60, 120, 180"
  type        = number
}

variable "workspaces_access_source_addresses" {
  description = <<-DESC
  Workspacesへの接続を許可する送信元IPアドレス (CIDRで指定可能)、およびその説明。記述例は以下の通り。
  ```
  rules = [{
    source_ip   = "192.0.2.0/24",
    description = "運用者が所属するネットワーク"
  }]
  ```
  DESC
  type        = list(map(string))
}
