output "security_group_id" {
  value = module.workspaces.security_group_id
}

output "ip_addresses" {
  value = module.workspaces.ip_addresses
}

output "states" {
  value = module.workspaces.states
}

output "registration_code" {
  value = module.workspaces.registration_code
}
