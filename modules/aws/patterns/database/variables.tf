variable "name" {
  description = "RDSのデータベース名"
  type        = string
}

variable "tags" {
  description = "このモジュールで作成するリソースに、共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "vpc_id" {
  description = "VPC ID"
  type        = string
}

variable "identifier" {
  description = "RDSのインスタンス名"
  type        = string
}

variable "engine" {
  description = "RDSのエンジンの種類"
  type        = string
}

variable "engine_version" {
  description = "RDSのエンジンのバージョン"
  type        = string
}

variable "port" {
  description = "RDSのポートを指定する"
  type        = number
}

variable "instance_class" {
  description = "RDSのDBインスタンスクラス"
  type        = string
}

variable "storage_type" {
  description = "RDSのストレージタイプ"
  type        = string
  default     = null
}

variable "allocated_storage" {
  description = "RDSに割り当てるストレージ容量（GB単位）"
  type        = number
}

variable "iops" {
  description = "プロビジョンドIOPS SSDストレージを使用した場合の、IOPSを指定する <https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/CHAP_Storage.html#USER_PIOPS>"
  type        = number
  default     = null
}

variable "username" {
  description = "RDSのマスタユーザー名"
  type        = string
}

variable "password" {
  description = "RDSのマスタユーザーのパスワード"
  type        = string
}

variable "availability_zone" {
  description = "このRDSインスタンスを配置するAZ。マルチAZ構成とする場合は、nullを指定します"
  type        = string
  default     = null
}

variable "multi_az" {
  description = "マルチAZを有効にする場合、true"
  type        = bool
  default     = false
}

variable "allow_major_version_upgrade" {
  description = "RDSエンジンのメジャーアップグレードを許可する場合、true"
  type        = bool
  default     = false
}

variable "auto_minor_version_upgrade" {
  description = "メンテナンスウィンドウ中に、RDSエンジンのマイナーバージョンアップグレードを許可する場合、true"
  type        = bool
}

variable "maintenance_window" {
  description = "メンテナンスを実行する時間帯を指定する <https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_UpgradeDBInstance.Maintenance.html#AdjustingTheMaintenanceWindow>"
  type        = string
}

variable "apply_immediately" {
  description = "データベースの変更をすぐに反映する場合、trueを指定する。falseを指定した場合、次のメンテナンス期間に適用される"
  type        = bool
  default     = false
}

variable "skip_final_snapshot" {
  description = "RDSのインスタンスを削除する際に、最後のスナップショットを作成しない場合はtrueを指定する"
  type        = bool
  default     = false
}

variable "final_snapshot_identifier" {
  description = "最終スナップショットに指定する識別子"
  type        = string
  default     = null
}

variable "copy_tags_to_snapshot" {
  description = "最終スナップショットに、インスタンスのタグをコピーして付与するか否か"
  type        = bool
  default     = true
}

variable "max_allocated_storage" {
  description = "この値が指定されている場合、RDSのストレージが不足した場合にこの値（GBで指定）まで自動的にスケーリングする"
  type        = number
  default     = null
}

variable "deletion_protection" {
  description = "RDSインスタンスの削除保護を有効にする場合、trueを指定する。有効にすると、RDSインスタンスを削除できなくなる"
  type        = bool
  default     = true
}

variable "security_groups" {
  description = "RDSインスタンスに割り当てるセキュリティグループのリスト"
  type        = list(string)
  default     = []
}

variable "character_set_name" {
  description = "OracleおよびMicrosoft SQL Server使用時の、エンコーディングに使用する文字集合名"
  type        = string
  default     = null
}

variable "mssql_timezone" {
  description = "Microsoft SQL Server使用時のタイムゾーンを指定する"
  type        = string
  default     = null
}

variable "db_subnet_group_name" {
  description = "DBサブネットグループ名"
  type        = string
}

variable "snapshot_identifier" {
  description = "Snapshotから復元する場合、Snapshot IDを入力"
  type        = string
  default     = null
}

variable "storage_encrypted" {
  description = "ストレージの暗号化を行う場合、true"
  type        = bool
  default     = true
}

variable "kms_key_id" {
  description = "暗号化に使用するKMSキーを指定"
  type        = string
}

variable "ca_cert_identifier" {
  description = "RDSインスタンスに適用するSSL/TLS証明書の識別子"
  type        = string
  default     = "rds-ca-2019"
}

variable "db_subnets" {
  description = "DBサブネットに割り当てる、サブネットIDのリスト"
  type        = list(string)
}

variable "parameter_group_name" {
  description = "DBパラメーターグループ名"
  type        = string
}

variable "parameter_group_family" {
  description = "DBパラメーターグループのファミリー"
  # aws rds describe-db-engine-versions --query "DBEngineVersions[].DBParameterGroupFamily"
  type = string
}

variable "parameters" {
  description = "DBパラメーターグループに割り当てる、パラメーターのリストを指定する。各パラメーターには、name, value, apply_methodの3つを指定する"
  type        = list(map(string))
  default     = []
}

variable "option_group_name" {
  description = "DBオプショングループ名"
  type        = string
}

variable "option_group_engine_name" {
  description = "DBオプショングループに関連付けるエンジンの名前"
  type        = string
}

variable "option_group_major_engine_version" {
  description = "DBオプショングループに関連付けるエンジンのメジャーバージョン"
  type        = string
}

variable "option_group_options" {
  description = "DBオプショングループに割り当てる、オプションのリスト"
  type        = list(map(any))
  default     = []
}

variable "license_model" {
  description = "OracleまたはMicrosoft SQL Serverを使用する際のライセンスモデル"
  type        = string
  default     = null
}

variable "enabled_cloudwatch_logs_exports" {
  description = <<-DESCRIPTION
  CloudWatch Logsに出力するログの種類をリストで指定する。空のリストを指定した場合、ログはCloudWatch Logsに出力されない。
  なにも指定しない場合、RDSMSごとのデフォルトのログが出力される。

  指定可能なログの種類はRDBMSごとに異なるので、[Amazon RDS データベースログファイル](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_LogAccess.html)、[Resource: aws_db_instance#enabled_cloudwatch_logs_exports](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance#enabled_cloudwatch_logs_exports)を参照すること
  DESCRIPTION
  type        = list(string)
  default     = null
}

variable "cloudwatch_logs_retention_in_days" {
  description = <<-DESCRIPTION
  ログをCloudWatch Logsに出力する場合の保持期間を設定する。

  値は、次の範囲の値から選ぶこと： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.
  [Resource: aws_cloudwatch_log_group / retention_in_days](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group#retention_in_days)
  DESCRIPTION
  type        = number
  default     = null
}

variable "cloudwatch_logs_kms_key_id" {
  description = "CloudWatch Logsを暗号化するためのKMS CMKのARN"
  type        = string
  default     = null
}

variable "monitoring_interval" {
  description = "拡張モニタリングにおける詳細度（収集間隔）を秒単位で指定する。0, 1, 5, 10, 15, 30, 60のいずれかが指定可能"
  type        = number
  default     = 60
}

variable "monitoring_role_arn" {
  description = "拡張モニタリング用のIAMロールのARNを指定する"
  type        = string
  default     = null
}

variable "performance_insights_enabled" {
  description = "パフォーマンスインサイトを有効にするか否か"
  type        = bool
  default     = true
}

variable "performance_insights_kms_key_id" {
  description = "パフォーマンスインサイトに保存する際の、KMSキー"
  type        = string
}

variable "performance_insights_retention_period" {
  description = "パフォーマンスインサイトのデータ保持期間を日数で指定する。`performance_insights_enabled`が`trueの時のみ設定可能で、7 または 731 （2年）のいずれかを指定する"
  type        = number
  default     = 7
}
variable "backup_retention_period" {
  description = <<-DESCTIPTION
  バックアップ保管期間。0〜35(日)の範囲で指定すること。
  なお、このパラメータを0から0以外の値、0以外の値から0に変更した場合、機能停止が発生する点に注意。詳細はドキュメントを参照のこと。
  [バックアップの使用](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_WorkingWithAutomatedBackups.html)
  DESCTIPTION
  type        = string
}

variable "backup_window" {
  description = <<-DESCTIPTION
  DBスナップショット(バックアップ)を作成する時間帯を09:46-10:16(UTC)という形式で指定する。
  Single-AZ DBインスタンスの場合、スナップショット取得中はI/Oが短時間中断する点に注意。また、maintenance_windowとは重複させないこと。
  DESCTIPTION
  type        = string
}

variable "source_security_group_ids" {
  description = "RDSに設定するセキュリティグループのインバウンドに指定するSecurity Group IDリスト"
  type        = list(string)
  default     = []
}
