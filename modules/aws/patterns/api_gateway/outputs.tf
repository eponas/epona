output "api_gateway_id" {
  description = "API GatewayのID"
  value       = module.api_gateway_rest_api.rest_api_id
}
output "api_gateway_arn" {
  description = "API GatewayのARN"
  value       = module.api_gateway_rest_api.rest_api_arn
}
output "api_gateway_stage" {
  description = "API Gatewayのステージに関する情報（ステージ名、ARN、URL）"
  value       = module.api_gateway_stage
}
output "api_gateway_custom_domain" {
  description = "API Gatewayのカスタムドメイン、パスマッピングに関する情報"
  value       = module.api_gateway_custom_domain
}
output "api_gateway_key" {
  description = "APIキーに関する情報（APIキー名、値、ARN、設定されているAPI GatewayのID、ステージ名）"
  value       = module.api_gateway_key_usage_plan
}
