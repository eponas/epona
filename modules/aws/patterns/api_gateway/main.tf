provider "aws" {
  alias = "default_provider"
}
provider "aws" {
  alias = "cloudfront_provider"
}
module "api_gateway_rest_api" {
  source = "../../components/api_gateway/api_gateway_api"
  providers = {
    aws = aws.default_provider
  }
  api_name                     = var.api_gateway_settings.api_name
  description                  = lookup(var.api_gateway_settings, "api_description", null)
  api_endpoint_config          = var.api_gateway_settings.api_endpoint_config
  api_policy_path              = try(var.api_gateway_settings.api_option_config["api_policy_path"], null)
  accept_binary_media_types    = try(split(",", var.api_gateway_settings.api_option_config["accept_binary_media_types"]), [])
  minimum_compression_size     = try(var.api_gateway_settings.api_option_config["minimum_compression_size"], -1)
  api_key_source               = try(var.api_gateway_settings.api_option_config["api_key_source"], "HEADER")
  disable_execute_api_endpoint = try(var.api_gateway_settings.api_option_config["disable_execute_api_endpoint"], false)
  tags                         = var.tags
  api_resources                = var.api_gateway_settings.api_resources
  api_security_schemes         = lookup(var.api_gateway_settings, "api_security_schemes", null)
}
module "api_gateway_stage" {
  # stagesの数だけ作成
  for_each = var.stages
  source   = "../../components/api_gateway/api_gateway_stage"
  providers = {
    aws                     = aws.default_provider
    aws.cloudfront_provider = aws.cloudfront_provider
  }
  rest_api_id   = module.api_gateway_rest_api.rest_api_id
  rest_api_body = module.api_gateway_rest_api.rest_api_body

  stage_name     = each.key
  stage_settings = each.value
  tags           = var.tags
  depends_on     = [module.api_gateway_rest_api]
}
module "api_gateway_custom_domain" {
  # custom_domainsの数だけ作成
  for_each = var.custom_domains
  source   = "../../components/api_gateway/api_gateway_custom_domain"
  providers = {
    aws                     = aws.default_provider
    aws.cloudfront_provider = aws.cloudfront_provider
  }
  rest_api_id            = module.api_gateway_rest_api.rest_api_id
  rest_api_endpoint_type = var.api_gateway_settings.api_endpoint_config.endpoint_types[0]
  custom_domain_name     = each.key
  custom_domain_settings = each.value
  tags                   = var.tags

  depends_on = [module.api_gateway_rest_api, module.api_gateway_stage]
}
module "api_gateway_key_usage_plan" {
  # api_keysの数だけ作成
  for_each = var.api_keys
  source   = "../../components/api_gateway/api_gateway_key_usage_plan"
  providers = {
    aws = aws.default_provider
  }
  rest_api_id      = module.api_gateway_rest_api.rest_api_id
  api_key_name     = each.key
  api_key_settings = each.value
  tags             = var.tags
  depends_on = [
    module.api_gateway_rest_api, module.api_gateway_stage
  ]
}
module "api_gateway_response" {
  for_each = var.api_responses
  source   = "../../components/api_gateway/api_gateway_response"
  providers = {
    aws = aws.default_provider
  }
  rest_api_id              = module.api_gateway_rest_api.rest_api_id
  response_type            = each.key
  status_response_settings = each.value
  depends_on = [
    module.api_gateway_rest_api
  ]
}


