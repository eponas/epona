variable "api_gateway_settings" {
  description = <<DESCRIPTION
  API Gatewayおよびリソース、メソッドの設定
```
api_gateway_settings = {
  api_name        = API Gatewayの名称
  api_description = API Gatewayの説明
  api_endpoint_config = {
    endpoint_types   = API Gatewayのエンドポイントの配置場所。["EDGE"],["REGIONAL"],["PRIVATE"]のいずれかを設定してください
    vpc_endpoint_ids = プライベートAPIの場合にAPI Gatewayと接続するVPCエンドポイントのID。["vpce-xxxxxxxxxxxx"]の形式で指定してください
  }
  api_option_config = { # API Gatewayに関するオプション設定です。設定不要の場合はこのMapごと削除でも構いません
    api_policy_path              = API Gatewayに設定するリソースポリシーを記載したファイルのパス
    minimum_compression_size     = APIのペイロードの圧縮を有効にする場合、圧縮するしきい値（バイト単位の数値）を設定してください。[こちら](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-enable-compression.html)もあわせてご参照ください
    api_key_source               = APIキーの取得元。"HEADER", "AUTHORIZER"のいずれかを設定してください
    disable_execute_api_endpoint = API GatewayのデフォルトのURLを無効にするかどうかを指定してください。デフォルトではfalse（無効にしない＝生成する）
    accept_binary_media_types    = API Gatewayでバイナリデータを扱う場合、メディアタイプを指定してください
  }
  api_resources = { # リソース及びメソッドを設定するブロックです。このブロックは必ず指定してください
    "/リソースパス" = { # リソースパスを指定してください。配下のパスを一括で指定する場合は"{proxy+}"を指定してください。詳細は[こちら](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-method-settings-method-request.html#api-gateway-proxy-resource)を参照してください。
      "HTTPメソッド" = { # このメソッドは小文字で指定してください。すべてのHTTPメソッドに対して設定を適用する場合は"any"を指定してください。詳細は[こちら](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-method-settings-method-request.html#setup-method-add-http-method)を参照してください。
        # 以下のいずれかの形式で指定してください
          # 固定レスポンスの場合
          lambda    = "mock"
          status_code = 返却するステータスコード
          content_type = レスポンスのContent-Type
          allow_origin = CORS対応時、アクセスを許可するオリジン。CORS対応不要の場合は設定不要（項目自体を削除しておく）
          strict_transport_security = ブラウザにHTTPSアクセスを強制する場合に設定するヘッダー。不要の場合は設定不要（項目自体を削除しておく）
          response_template = レスポンス本文

          # Lambdaと統合する場合
          lambda           = 統合するLambda関数名。事前に作成されている必要があります
          lambda_aliases   = 統合するLambda関数のエイリアス名。エイリアス不要の場合は設定不要（項目自体を削除しておく）
          authorizer       = オーソライザーの名称。api_security_schemesブロックのオーソライザー名と一致する必要があります。未指定の場合はLambdaオーソライザーは使用されません。
          api_key_required = このHTTPメソッドを呼び出すためにAPIキーを必須とする場合はtrueとしてください
      }
      enable_cors = { # リソースパス内でCORSを有効にする場合に設定してください
        allow_methods = 有効にするHTTPメソッドをカンマ区切りで指定してください。例："'DELETE,GET,OPTIONS,PATCH'"
        allow_headers = 有効にするHTTPヘッダーをカンマ区切りで指定してください。例："'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'"
        allow_origin = 有効にするオリジンを指定してください。例："'*'"
      }
    }
  }
  api_security_schemes = { # オーソライザーに関する設定を行うブロックです。不要の場合はブロックごと未設定でも構いません。このブロックの設定内容については[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-swagger-extensions-authorizer.html)もあわせてご参照ください
    "オーソライザー名" = { # api_resources内で指定したauthorizerと一致する必要があります
      authorizer_parameter_name = トークンとして使用するパラメータの名称
      authorizer_parameter_in   = トークンをどこから取得するかを指定します。tokenタイプのオーソライザーの場合は"header", requestタイプのオーソライザーの場合は"header"または"query"を指定してください
      request_or_token_type     = オーソライザーのタイプ。"token"または"request"を指定してください
      request_identity_source   = ID ソースとして、リクエストパラメータの式をマッピングするカンマ区切りのリスト。requestタイプのオーソライザーのみ使用します
      authorizer_function_name  = オーソライザーとなるLambda関数名。事前に作成されている必要があります
      authorizer_aliases        = オーソライザーとなるLambda関数のエイリアス名。
      authorizer_credentials    = Lambda関数を実行するのに必要なロールのARN。
      token_validation_regex    = トークンを検証するための正規表現。たとえば、"^x-[a-z]+" などです。
      authorizer_ttl            = オーソライザーの結果がキャッシュされる秒数（数値）。
    }
  }
}
```
DESCRIPTION
  type        = any
}

variable "tags" {
  description = "API Gatewayに付与するタグ"
  type        = map(string)
  default     = {}
}
variable "stages" {
  description = <<DESCRIPTION
  API Gatewayのステージの設定
```
stages = { # stageに関する設定を行うブロックです。このブロックは必ず設定してください
  "ステージ名" = {
    stage_variables = このステージに当てはめるステージ変数。Map形式（{ key = value }）で指定してください
    xray_tracing_enabled = このステージでX-Rayによるトレースを有効にするかどうかを指定します。有効にする場合はtrueを指定してください
    custom_access_log_group_name  = カスタムアクセスログを出力する場合のロググループ名称
    custom_access_log_format_path = カスタムアクセスログを出力する場合の書式を記載したファイルパス。ファイルの記載例については[こちら](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/set-up-logging.html)を参照してください
    log_throttling = { # このステージでのログ設定、スロットリング設定を行うブロックです。設定不要の場合はブロックごと未設定でも構いません
      "/リソースパス/HTTPメソッド" = { # ステージ全体に適用する場合は"/*/*"としてください
        enable_metrics         = 詳細CloudWatchメトリクスを有効とするかどうか。有効にする場合はtrueとしてください
        logging_level          = ログ取得対象とするログレベル。"OFF", "INFO", "ERROR"のいずれかを設定してください
        throttling_burst_limit = 1ミリ秒あたりのアクセス数上限（数値）) を設定してください。これを超えた場合、レートの範囲内で処理されます
        throttling_rate_limit  = 1秒あたりのリクエスト数上限（数値））を設定してください。これを超えた場合、429エラーが返されます
      }
    }
    force_redeployment_string = terraform apply時、ステージを再デプロイする場合に設定してください。前回のterraform apply時と異なる値にすると再デプロイされます
  }
}
```
DESCRIPTION
  type        = any
  default     = {}
}
variable "custom_domains" {
  description = <<DESCRIPTION
  API Gatewayのカスタムドメインの設定
```
custom_domains = { # カスタムドメインに関する設定を行うブロックです。カスタムドメインを使用しない場合はブロックごと未設定でも構いません
  "ドメイン名" = {
    "ベースパス" = { # カスタムドメインを細分化するためのパス。たとえば、"/dev"、"/prod"のように指定し、それぞれ別のステージに関連付けるような設定が可能です。細分化不要の場合は"/"を指定してください
      custom_zone_name   = カスタムドメインのホストゾーン
      stage_name  = カスタムドメインを設定するステージ名。stages内で指定したステージ名と一致する必要があります
    }
  }
}
```
DESCRIPTION
  type        = any
  default     = {}
}
variable "api_keys" {
  description = <<DESCRIPTION
  API GatewayのAPIキーの設定
```
api_keys = { # APIキーに関する設定を行うブロックです。設定不要の場合はブロックごと未設定でも構いません
  "APIキー名" = {
    api_key_enabled = APIキーを有効にするかを設定してください
    api_usage_plan = { # APIキーについてクォータ・スロットリングを設定する場合に設定してください
      quota = { # APIキーに設定するクォータ
        limit  = "10" # APIの呼出を行える最大回数
        offset = "2" # 期間の始まる日。WEEKの場合、0:日曜日、1:月曜日...
        period = "WEEK" # 呼出回数をカウントする期間。 "DAY", "WEEK" or "MONTH"のいずれかを設定してください
      }
      throttling = { # APIキーに設定するスロットリング
        burst_limit = 1ミリ秒あたりのアクセス上限。これを超えた場合、レートの範囲内で処理されます
        rate_limit  = 1秒あたりのアクセス上限。これを超えた場合、429エラーが返されます
      }
      api_usage_plan_apply_stages = # APIキーおよび使用量プランを設定するステージ。["stage1", "stage2", ...]のように設定してください
    }
  }
}
```
DESCRIPTION
  type        = map(any)
  default     = {}
}

variable "api_responses" {
  description = <<DESCRIPTION
  API GatewayのAPIキーの設定
```
api_responses = { # API Gatewayから返却するエラーレスポンスを設定するブロックです。設定不要の場合はブロックごと未設定でも構いません
  "レスポンスタイプ" = { # 設定したいレスポンスタイプを指定してください。詳細は[こちら](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/supported-gateway-response-types.html)をご参照ください
    response_templates = { # レスポンス本文に使用されるテンプレートを指定するマップ。メディアタイプ = "マッピングテンプレート"のように指定してください。マッピングテンプレートについては[こちら](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-gatewayResponse-definition.html)を参照してください
    response_parameters = { # レスポンスに指定するパラメータ（ヘッダー、クエリ文字列、パスなど）を設定するマップ。パラメータ名 = "パラメータ値"のように指定してください。[こちら](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/request-response-data-mappings.html)もあわせて参照してください
  }
}
```
DESCRIPTION
  type        = map(any)
  default     = {}
}
