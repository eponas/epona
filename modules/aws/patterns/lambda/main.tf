module "lambda" {
  source                         = "../../components/lambda"
  source_dir                     = var.source_dir
  source_dir_exclude_files       = var.source_dir_exclude_files
  function_name                  = var.function_name
  description                    = var.description
  runtime                        = var.runtime
  handler                        = var.handler
  memory_size                    = var.memory_size
  environment                    = var.environment
  vpc_config_subnet_ids          = var.vpc_config_subnet_ids
  vpc_config_security_group_ids  = var.vpc_config_security_group_ids
  role_arn                       = module.execution_role.arn
  timeout                        = var.timeout
  ignore_change_source           = var.ignore_change_source
  reserved_concurrent_executions = var.reserved_concurrent_executions
  tags                           = var.tags
  tracing_mode                   = var.tracing_mode
}

# iam 
module "execution_role" {
  source      = "../../components/iam/lambda_execution_role"
  name_prefix = var.function_name
}

# cloudwatch logs
module "log" {
  source                      = "../../components/cloudwatch/log"
  log_group_name              = "/aws/lambda/${var.function_name}"
  log_group_retention_in_days = var.log_group_retention_in_days
  log_group_kms_key_id        = null
}

# notify sns
module "notify_sns" {
  count              = var.notify_sns_topic_on_success == null && var.notify_sns_topic_on_failure == null ? 0 : 1
  source             = "../../components/lambda/sns_notification"
  function_name      = module.lambda.function_name
  on_success_sns_arn = var.notify_sns_topic_on_success
  on_failure_sns_arn = var.notify_sns_topic_on_failure
  role_name          = module.execution_role.name
}

# schedule
module "lambda_schedule" {
  count               = var.schedule_expression == null ? 0 : 1
  source              = "../../components/lambda/schedule"
  lambda_arn          = module.lambda.arn
  function_name       = module.lambda.function_name
  schedule_expression = var.schedule_expression
  is_enabled          = var.is_enabled_schedule
}

module "lambda_invoke_s3_object_event" {
  count            = var.invoke_bucket_name == null || var.invoke_bucket_arn == null ? 0 : 1
  source           = "../../components/lambda/s3_object_event"
  s3_bucket_name   = var.invoke_bucket_name
  s3_bucket_arn    = var.invoke_bucket_arn
  arn              = module.lambda.arn
  function_name    = module.lambda.function_name
  s3_object_events = var.invoke_object_events
  filter_prefix    = var.invoke_object_filter_prefix
  filter_suffix    = var.invoke_object_filter_suffix
}

# alias
resource "aws_lambda_alias" "lambda_alias" {
  count            = length(var.aliases)
  name             = var.aliases[count.index]
  function_name    = module.lambda.arn
  function_version = module.lambda.version

  lifecycle {
    ignore_changes = [
      function_version
    ]
  }
}
