variable "function_name" {
  type        = string
  description = "Lambda関数の名称を指定する。"
}

variable "source_dir" {
  type        = string
  description = "Lambda関数のソースが配置されたディレクトリを指定する。"
}

variable "source_dir_exclude_files" {
  type        = list(string)
  default     = []
  description = <<EOF
`source_dir`で指定したディレクトリ配下で、zipファイルに含めないファイルを指定する。

例えば、`source_dir`が`node`で、その中の`test.js`を除外したい場合は、`test.js`と指定する（`source_dir`のディレクトリ名は含めない。なお、`*`は使えない）。

実際は、
```
source_dir_exclude_files = [
  "test.js",
  "node_modules/eslint-config-eslint/package.json"
]
```
のようにlist(string)で指定する。

EOF

}

variable "runtime" {
  type        = string
  description = <<EOF
利用するランタイムを指定する。
指定できる値については[こちら](https://docs.aws.amazon.com/lambda/latest/dg/API_CreateFunction.html#SSS-CreateFunction-request-Runtime)を参照
EOF

}

variable "description" {
  type        = string
  description = "Lambda関数の説明を記載する。"
  default     = null
}

variable "handler" {
  type        = string
  description = <<EOF
Lambda関数のエントリーポイントとなる、関数内のメソッド名を指定する。
[Node.jsの例](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/nodejs-handler.html)
EOF

}

variable "environment" {
  type        = map(string)
  default     = {}
  description = "Lambda関数が使用する環境変数を{key = value}の形式で指定する。"
}

variable "vpc_config_subnet_ids" {
  type        = list(string)
  default     = []
  description = "Lambda関数をVPC内に配置する場合、Lambdaを接続するサブネットIDを指定する。"
}
variable "vpc_config_security_group_ids" {
  type        = list(string)
  default     = []
  description = "Lambda関数をVPC内に配置する場合、Lambdaに許容するルールを設定したセキュリティグループをアタッチする。"
}

variable "memory_size" {
  type        = number
  default     = 128
  description = <<EOF
Lambda関数に割り当てるメモリサイズをMB単位で指定する。
指定できる値の制約については[Lambda quotas](https://docs.aws.amazon.com/lambda/latest/dg/gettingstarted-limits.html)を参照。

EOF

}

variable "timeout" {
  type        = number
  default     = null
  description = "Lambda関数に許可する実行時間（タイムアウト）を秒単位で指定する。デフォルトでは3秒、最大は900秒となる。"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "タグを指定する。"
}

variable "tracing_mode" {
  type        = string
  default     = "PassThrough"
  description = <<EOF
`AWS X-Ray`を使用して実行状況をトレースする場合、トレースの種類を指定する。`PassThrough`か`Active`を指定する。デフォルトは`PassThrough`。

詳細は[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/services-xray.html)および[Terraformドキュメント](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function#reserved_concurrent_executions)を参照。

EOF

}

variable "ignore_change_source" {
  type        = bool
  default     = false
  description = "`source_dir`で指定されたディレクトリの中身が変更されていても、Lambda関数を更新しないようにする場合は`true`を指定する。デフォルトは`false`。なお、`false`の場合、`source_dir`で指定されたディレクトリの中身と、AWS環境上にデプロイ済みのLambda関数を比較し、差分があれば`source_dir`の内容でLambda関数を上書きする。"
}

variable "reserved_concurrent_executions" {
  type        = number
  description = <<EOF
Lambda関数の同時実行数を設定する。[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-concurrency.html)の`リザーブド同時実行数`に該当する。
指定できる値については[`reserved_concurrent_executions`](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function#reserved_concurrent_executions)を参照。
EOF

  default = -1
}

variable "log_group_retention_in_days" {
  type        = number
  default     = 14
  description = "CloudWatch Logsのロググループでログを保持する日数を指定する。デフォルトは14日。"
}

variable "schedule_expression" {
  type        = string
  default     = null
  description = <<EOF
Lambdaをスケジュール実行する場合、スケジュール式を`cron()`式あるいは`rate()`式で指定する。

記述方法については[RateまたはCronを使用したスケジュール式](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/services-cloudwatchevents-expressions.html)を参照。
EOF
}

variable "is_enabled_schedule" {
  type        = bool
  default     = true
  description = "Lambda関数をスケジュール実行する場合で、一時的にスケジュール実行を止める場合に設定する。デフォルトは`true`（実行する）であり、止める場合は`false`（実行しない）と設定する。"
}

variable "invoke_bucket_name" {
  type        = string
  default     = null
  description = "Lambda関数の実行トリガーとなるS3バケットの名称を指定する。`invoke_bucket_arn`と同時に指定する必要がある。"
}

variable "invoke_bucket_arn" {
  type        = string
  default     = null
  description = "Lambda関数の実行トリガーとなるS3バケットのARNを指定する。`invoke_bucket_name`と同時に指定する必要がある。"
}

variable "invoke_object_events" {
  type        = list(string)
  description = <<EOF
Lambda関数の実行トリガーとなるS3バケットのイベントを指定できる。デフォルトは作成時。

指定する内容については[Amazon S3 イベント通知](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/NotificationHowTo.html#notification-how-to-event-types-and-destinations)を参照。
このパラメータを設定する際は、`invoke_bucket_arn`、`invoke_bucket_name`もあわせて設定する必要がある。
EOF

  default = [
    "s3:ObjectCreated:*"
  ]
}

variable "invoke_object_filter_prefix" {
  type        = string
  description = <<EOF
Lambda関数をS3バケットのイベントで実行する場合で、対象となるオブジェクトをプレフィックスで絞り込む際に設定する。

詳細は[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/notification-how-to-filtering.html)を参照。
このパラメータを設定する際は、`invoke_bucket_arn`、`invoke_bucket_name`もあわせて設定する必要がある。
EOF

  default = null
}

variable "invoke_object_filter_suffix" {
  type        = string
  description = <<EOF
Lambda関数をS3バケットのイベントで実行する場合で、対象となるオブジェクトをサフィックスで絞り込む際に設定する。

詳細は[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/notification-how-to-filtering.html)を参照。
このパラメータを設定する際は、`invoke_bucket_arn`、`invoke_bucket_name`もあわせて設定する必要がある。
EOF

  default = null
}

variable "notify_sns_topic_on_success" {
  type        = string
  description = "Lambdaから実行成功を通知する際の通知先となるSNSトピックのARNを指定する。"
  default     = null
}

variable "notify_sns_topic_on_failure" {
  type        = string
  description = "Lambdaから実行失敗を通知する際の通知先となるSNSトピックのARNを指定する。"
  default     = null
}

variable "aliases" {
  type        = list(string)
  description = "Lambda関数に付与するエイリアスを指定する。複数指定が可能。"
  default     = []
}
