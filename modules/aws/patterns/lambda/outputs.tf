output "role_arn" {
  value       = module.execution_role.arn
  description = "Lambda実行ロールのARN"
}

output "role_name" {
  value       = module.execution_role.name
  description = "Lambda実行ロールの名称。他のAWSリソースへのアクセス権限が必要な場合は、このロールにアタッチする"
}

output "arn" {
  value       = module.lambda.arn
  description = "Lambda関数のARN"
}

output "function_name" {
  value       = module.lambda.function_name
  description = "Lambda関数の名称"
}

output "qualified_arn" {
  value       = module.lambda.qualified_arn
  description = "Lambda関数のバージョンのついたARN"
}

output "version" {
  value       = module.lambda.version
  description = "Lambda関数のバージョン"
}

output "invoke_arn" {
  value       = module.lambda.invoke_arn
  description = "API GatewayからLambda関数を呼び出すために使用されるARN"
}
