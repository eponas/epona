output "ecs_cluster_name" {
  description = "ECSクラスター名"
  value       = module.fargate_container_service.cluster_name
}
output "ecs_service_name" {
  description = "ECSサービス名"
  value       = module.fargate_container_service.service_name
}

output "load_balancer_arn" {
  description = "ロードバランサーのARN"
  value       = module.public_traffic_load_balancer.load_balancer_arn
}

output "load_balancer_prod_listener_arn" {
  description = "本番環境用トラフィックを受け持つ、ロードバランサーのListenerのARN"
  value       = module.public_traffic_load_balancer.prod_lister_arn
}

output "load_balancer_target_group_names" {
  description = "LoadBalancerのターゲットグループ名のリスト"
  value       = module.public_traffic_load_balancer.target_group_names
}

output "load_balancer_access_logs_bucket_id" {
  description = "ロードバランサーのアクセスログ出力用のS3バケットのID"
  value       = var.public_traffic_access_logs_enabled ? local.logging_bucket_name : ""
}

output "load_balancer_access_logs_bucket" {
  description = "ロードバランサーのアクセスログ出力用のS3バケット名"
  value       = var.public_traffic_access_logs_enabled ? local.logging_bucket_name : ""
}

output "cloudwatch_container_log_names" {
  description = "コンテナのログ出力先となる、CloudWatch Logsのロググループ名のリスト"
  value       = [for log in module.container_logs : log.log_group_name]
}