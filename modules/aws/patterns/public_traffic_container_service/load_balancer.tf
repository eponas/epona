module "public_traffic_load_balancer_security_group" {
  source = "../../components/security_group/generic"

  name = "${var.name}-lb-security-group"
  tags = var.tags

  vpc_id = var.vpc_id
  ingresses = [{
    port        = var.public_traffic_port
    protocol    = var.public_traffic_inbound_protocol
    cidr_blocks = var.public_traffic_inbound_cidr_blocks
    description = "Public Traffic ALB inbound SG Rule"
  }]

  egresses = [{
    port        = -1
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Public Traffic ALB outbound SG Rule"
  }]
}

module "acm_with_validate_dns" {
  source = "../../components/acm/certificate_with_validate_dns"

  create = var.create_public_traffic_certificate && upper(var.public_traffic_protocol) == "HTTPS"

  validation_method = "DNS"
  zone_name         = lookup(var.dns, "zone_name", null)
  domain_name       = lookup(var.dns, "record_name", null)
  ttl               = 60 # ALIASレコードと同じ
}

locals {
  logging_bucket_name = var.public_traffic_access_logs_bucket != null ? var.public_traffic_access_logs_bucket : "${var.name}-lb-access-log"
}

data "aws_elb_service_account" "this" {}

data "aws_iam_policy_document" "access_lob_bucket_policy" {
  statement {
    effect    = "Allow"
    actions   = ["s3:PutObject"]
    resources = ["arn:aws:s3:::${local.logging_bucket_name}/*"]

    principals {
      type        = "AWS"
      identifiers = [data.aws_elb_service_account.this.arn]
    }
  }
}

module "loadbalancer_access_logs_bucket" {
  source = "../../components/s3/logging_bucket"

  create_bucket = var.public_traffic_access_logs_enabled && var.public_traffic_access_logs_create_bucket ? true : false

  bucket        = local.logging_bucket_name
  bucket_policy = data.aws_iam_policy_document.access_lob_bucket_policy.json
  force_destroy = var.public_traffic_access_logs_force_destroy
  tags          = var.tags
}

module "public_traffic_load_balancer" {
  source = "../../components/load_balancer/public/blue_green/"

  name = var.name
  tags = var.tags

  vpc_id = var.vpc_id

  load_balancer_type = "application"

  access_logs_enabled = var.public_traffic_access_logs_enabled
  access_logs_bucket  = module.loadbalancer_access_logs_bucket.bucket
  access_logs_prefix  = var.public_traffic_access_logs_prefix

  # Blue-Green Deployment においては、テスト用リスナーの利用を想定しない
  public_traffic_protocol = var.public_traffic_protocol
  public_traffic_port     = var.public_traffic_port

  public_traffic_certificate_arn = var.create_public_traffic_certificate ? module.acm_with_validate_dns.certificate_arn : var.public_traffic_certificate_arn
  public_traffic_ssl_policy      = var.public_traffic_ssl_policy

  subnets         = var.public_subnets
  security_groups = [module.public_traffic_load_balancer_security_group.security_group_id]

  backend_protocol          = var.container_protocol
  backend_port              = var.container_port
  backend_health_check_path = var.container_health_check_path

  alb_forward_listener_rules        = var.alb_forward_listener_rules
  alb_fixed_response_listener_rules = var.alb_fixed_response_listener_rules
  alb_redirect_listener_rules       = var.alb_redirect_listener_rules
}

module "route53_alias" {
  source = "../../components/route53/alias"

  zone_name   = lookup(var.dns, "zone_name", null)
  record_name = lookup(var.dns, "record_name", null)
  alias_target = {
    "dns_name" = module.public_traffic_load_balancer.load_balancer_dns_name
    "zone_id"  = module.public_traffic_load_balancer.load_balancer_zone_id
  }
}

module "container_service_security_group" {
  source = "../../components/security_group/generic"

  name = "${var.name}-container-service-security-group"
  tags = var.tags

  vpc_id = var.vpc_id

  ingresses = [{
    port        = var.container_port
    protocol    = var.container_traffic_inbound_protocol
    cidr_blocks = var.container_traffic_inbound_cidr_blocks
    description = "Public Traffic Container Service inbound SG Rule"
  }]

  egresses = [{
    port        = -1
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Public Traffic Container Service outbound SG Rule"
  }]
}
