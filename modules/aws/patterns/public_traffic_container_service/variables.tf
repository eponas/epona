variable "name" {
  description = "コンテナサービス、ロードバランサーに関する名前"
  type        = string
}

variable "tags" {
  description = "このモジュールで作成するリソースに、共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "vpc_id" {
  description = "VPC ID"
  type        = string
}

variable "public_traffic_access_logs_enabled" {
  description = "ロードバランサーのアクセスログを有効にする場合、trueを指定する。デフォルトでtrueが指定されたものとして振る舞う"
  type        = bool
  default     = true
}

variable "public_traffic_access_logs_create_bucket" {
  description = "bucketに指定したS3バケットをこのモジュールで作成する場合、trueを指定する。デフォルトでtrueが指定されたものとして振る舞う"
  type        = bool
  default     = true
}

variable "public_traffic_access_logs_bucket" {
  description = "ロードバランサーのアクセスログを保存するS3バケット名。public_traffic_access_logs_enabledがtrueで、この変数を指定しない場合、nameから自動導出する"
  type        = string
  default     = null
}

variable "public_traffic_access_logs_force_destroy" {
  description = "destroy時、データがあったとしても強制的にアクセスログ用S3バケットを削除する"
  type        = bool
  default     = false
}

variable "public_traffic_access_logs_prefix" {
  description = "アクセスログをS3バケットに保存する時のprefix。指定しない場合は、ログはS3のルートに保存される"
  type        = string
  default     = null
}

variable "public_subnets" {
  description = "ロードバランサーに割り当てる、パブリックサブネットのIDのリスト"
  type        = list(string)
}

variable "public_traffic_protocol" {
  description = "ロードバランサーが受け付けるトラフィックのプロトコル"
  type        = string
}

variable "public_traffic_port" {
  description = "ロードバランサーが受け付けるトラフィックのポート"
  type        = number
}

variable "public_traffic_inbound_protocol" {
  description = "ロードバランサーが受け付けるトラフィックのプロトコル。セキュリティグループの指定に利用"
  type        = string
  default     = "tcp"
}

variable "public_traffic_inbound_cidr_blocks" {
  description = "ロードバランサーが受け付けを許可するトラフィックのCIDRブロックのリスト"
  type        = list(string)
}

variable "alb_forward_listener_rules" {
  description = <<DESCRIPTION
ロードバランサーのリスナーに設定するフォワードアクションルールのリスト
[
  {
    priority = ルールの優先順位。値が小さいルールから順に評価される。 (例: "10") # Required
    path_pattern = ルールのアクションが実行される条件。カンマ区切りで複数指定が可能。 (単一の場合の例: "/*" 複数の場合の例: "/foo/*,/bar/*") # Required
  }
]
DESCRIPTION
  type        = list(map(string))
  default     = []
}

variable "alb_fixed_response_listener_rules" {
  description = <<DESCRIPTION
ロードバランサーのリスナーに設定する固定レスポンスルールのリスト
レスポンスコードとオプションのメッセージを返すことができる
[
  {
    priority = ルールの優先順位。値が小さいルールから順に評価される。(例: "10") # Required
    content_type = コンテントタイプ (例: "text/plain") # Required content_typeに設定できる値については[content_type](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener_rule#content_type)を参照してください
    message_body = メッセージボディ (例: "403 Forbidden") # Required
    status_code = HTTPレスポンスコード (例: "403") # Required 2XX,4XX,5XXのいずれかを設定してください
    path_pattern = ルールのアクションが実行される条件。カンマ区切りで複数指定が可能。 (単一の場合の例: "/*" 複数の場合の例: "/foo/*,/bar/*") # Required
  }
]
DESCRIPTION
  type        = list(map(string))
  default     = []
}

variable "alb_redirect_listener_rules" {
  description = <<DESCRIPTION
ロードバランサーのリスナーに設定するリダイレクトルールのリスト
`host`、`path`、`port`、`protocol`、`query`に関しては `#{host}` のように記述することで、元のURLの値を再利用できます
詳細については[リダイレクトアクション](https://docs.aws.amazon.com/ja_jp/elasticloadbalancing/latest/application/load-balancer-listeners.html#redirect-actions)を参照してください
[
  {
    priority = ルールの優先順位 (例: "10") # Required
    host = リダイレクト先のホスト (例: "example.com") # Optional 省略した場合 `"#{host}"` が設定されます
    path = リダイレクト先のURLパス (例: "/bar") # Optional 省略した場合 `"/#{path}"` が設定されます
    port = リダイレクト先のポート番号 (例: "8080") # Optional 省略した場合 `"#{port}"` が設定されます
    protocol = プロトコル HTTPもしくはHTTPS (例: "HTTPS") # Optional 省略した場合 `"#{protocol}"` が設定されます
    query = クエリパラメータ (例: "key=aaa") # Optional 省略した場合 `"#{query}"` が設定されます
    status_code = HTTPリダイレクトコード (例: "HTTP_301") # Required "HTTP_301"か"HTTP_302"のいずれかを設定してください
    path_pattern = ルールのアクションが実行される条件。カンマ区切りで複数指定が可能。 (単一の場合の例: "/*" 複数の場合の例: "/foo/*,/bar/*") # Required
  }
]
DESCRIPTION
  type        = list(map(string))
  default     = []
}

variable "dns" {
  description = <<DESCRIPTION
ロードバランサーのDNSに関する設定（zone_name, record_name）。
zone_name = record_nameを登録するホストゾーン名
record_name = Amazon Route 53に登録するレコードセットの名前
DESCRIPTION
  type        = map(any)
}

variable "create_public_traffic_certificate" {
  description = "ロードバランサーで利用するSSL/TLS証明書を作成する場合、true"
  type        = bool
  default     = true
}

variable "public_traffic_certificate_arn" {
  description = "ロードバランサーに与えるSSL/TLS証明書のARN。このpattern外部で証明書を作成する場合に使用する"
  type        = string
  default     = null
}

variable "public_traffic_ssl_policy" {
  description = "ロードバランサーのデフォルトのセキュリティポリシー"
  type        = string
  default     = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
}

variable "container_cluster_name" {
  description = "コンテナサービスのクラスター名"
  type        = string
}

variable "container_traffic_inbound_protocol" {
  description = "コンテナサービスが受け付けるトラフィックのプロトコル"
  type        = string
  default     = "tcp"
}

variable "container_traffic_inbound_cidr_blocks" {
  description = "コンテナサービスが受け付けるトラフィックの、CIDRブロックのリスト"
  type        = list(string)
}

variable "container_subnets" {
  description = "コンテナサービスを配置する、サブネットIDのリスト"
  type        = list(string)
}

variable "container_protocol" {
  description = "ロードバランサーがコンテナに転送する際のプロトコル"
  type        = string
}

variable "container_port" {
  description = "ロードバランサーがコンテナに転送する際のポート"
  type        = number
}

variable "container_health_check_path" {
  description = "ロードバランサーの、コンテナに対するヘルスチェックのパス"
  type        = string
}

variable "container_log_group_names" {
  description = "コンテナのログ出力先と成るCloudWatch Logsのグループ名のリスト"
  type        = list(string)
  default     = []
}

variable "container_log_group_retention_in_days" {
  description = "CloudWatch Logsに保存した、コンテナログの保持日数"
  # https://www.terraform.io/docs/providers/aws/r/cloudwatch_log_group.html#retention_in_days
  # この範囲の値から選ぶ： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653. 
  type    = number
  default = null
}

variable "container_log_group_kms_key_id" {
  description = "CloudWatch Logsを暗号化するためのKMS CMKのARN"
  type        = string
  default     = null
}

variable "container_service_desired_count" {
  description = "コンテナサービス内の、タスクのインスタンス数"
  type        = number
}

variable "container_service_platform_version" {
  description = "コンテナサービスを実行するプラットフォームのバージョン"
  # https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/developerguide/platform_versions.html
  type = string
}

variable "container_task_cpu" {
  description = "コンテナサービス内で実行されるタスクに割り当てるCPU"
  type        = number
}

variable "container_task_memory" {
  description = "コンテナサービス内で実行されるタスクに割り当てるメモリ"
  type        = string
}

variable "container_task_role_arn" {
  description = "コンテナサービスのタスクが、他のAWSサービスを呼び出せるように割り当てるIAMロールのARN"
  type        = string
  default     = null
}

variable "container_task_execution_role_arn" {
  description = "コンテナサービスのタスクに割り当てるタスク実行IAMロールのARN。create_default_ecs_task_execution_roleをfalseにする場合に指定すること"
  type        = string
  default     = null
}

variable "create_default_ecs_task_execution_role" {
  description = "デフォルトのタスク実行ロールを作成する場合、trueを指定する"
  type        = bool
  default     = true
}

variable "default_ecs_task_execution_iam_role_name" {
  description = "デフォルトのタスク実行ロールを作成する場合のIAMロール名"
  type        = string
  default     = "DefaultContainerServiceTaskExecutionRole"
}

variable "default_ecs_task_execution_iam_policy_name" {
  description = "デフォルトのタスク実行ロールを作成する場合のIAMポリシー名"
  type        = string
  default     = "DefaultContainerServiceTaskExecutionRolePolicy"
}

variable "create_default_ecs_task_role" {
  description = "デフォルトのタスク実行ロールを作成する場合、trueを指定する"
  type        = bool
  default     = true
}

variable "default_ecs_task_iam_role_name" {
  description = "デフォルトのタスクロールを作成する場合のIAMロール名"
  type        = string
  default     = "DefaultContainerServiceTaskRole"
}

variable "default_ecs_task_iam_policy_name" {
  description = "デフォルトのタスクロールを作成する場合のIAMポリシー名"
  type        = string
  default     = "DefaultContainerServiceTaskRolePolicy"
}

variable "container_definitions" {
  description = "コンテナサービスのタスクで実行する、コンテナ定義のリスト"
  type        = string
}
