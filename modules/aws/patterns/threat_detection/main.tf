module "iam_role" {
  source = "../../components/iam/threat_detection"

  lambda_function_name = var.lambda_function_name
  tags                 = var.tags
}

module "security_group" {
  source = "../../components/security_group/generic"

  name = "${var.lambda_function_name}-security-group"
  tags = var.tags

  vpc_id = var.vpc_id

  ingresses = []

  egresses = [{
    port        = -1
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Lambda ${var.lambda_function_name} outbound SG Rule"
  }]
}

module "lambda" {
  source = "../../components/lambda/threat_detection"

  lambda_function_name                       = var.lambda_function_name
  lambda_timeout                             = var.lambda_timeout
  notification_lambda_config                 = var.notification_lambda_config
  notification_lambda_source_dir             = var.notification_lambda_source_dir
  notification_lambda_handler                = var.notification_lambda_handler
  notification_lambda_runtime                = var.notification_lambda_runtime
  iam_role_threat_detection_arn              = module.iam_role.iam_role_threat_detection_arn
  cloudwatch_event_rule_threat_detection_arn = module.cloudwatch.cloudwatch_event_rule_threat_detection_arn

  tags = var.tags

  # vpc_config
  lambda_subnet_ids          = var.lambda_subnet_ids
  lambda_security_group_arns = [module.security_group.security_group_id]
}

module "cloudwatch" {
  source = "../../components/cloudwatch/threat_detection"

  lambda_function_name                 = var.lambda_function_name
  lambda_function_threat_detection_arn = module.lambda.lambda_function_threat_detection_arn
  tags                                 = var.tags
}

module "log" {
  source = "../../components/cloudwatch/log"

  lambda_function_name        = var.lambda_function_name
  log_group_retention_in_days = var.log_retention_in_days
  log_group_kms_key_id        = var.log_kms_key_id
  tags                        = var.tags
}

module "guardduty" {
  source = "../../components/guardduty/threat_detection"

  create = var.create_guardduty

  finding_publishing_frequency = var.finding_publishing_frequency
  tags                         = var.tags
}
