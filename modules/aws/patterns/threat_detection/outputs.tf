output "lambda_function_threat_detection_arns" {
  description = "検出結果を送信するLambda関数のARN一覧"
  value       = module.lambda
}

output "lambda_function_thread_detection_log_group_name" {
  description = "Lambda関数のログ出力先となる、CloudWatch Logsロググループ名"
  value       = module.log.log_group_name
}
