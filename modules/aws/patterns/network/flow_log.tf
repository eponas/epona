module "flow_log_bucket" {
  source = "../../components/s3/logging_bucket"

  create_bucket = var.flow_log_enabled && var.flow_log_destination_arn == null && var.flow_log_destination_type == "s3"
  bucket        = var.flow_log_s3_bucket == null ? "${var.name}-flow-log" : var.flow_log_s3_bucket

  tags = var.tags
}

module "flow_log_group" {
  source = "../../components/cloudwatch/log"

  count = var.flow_log_enabled && var.flow_log_destination_arn == null && var.flow_log_destination_type == "cloud-watch-logs" ? 1 : 0

  log_group_name              = var.flow_log_cloudwatch_log_group == null ? "${var.name}-flow-log" : var.flow_log_cloudwatch_log_group
  log_group_retention_in_days = var.flow_log_cloudwatch_log_retention_in_days
  log_group_kms_key_id        = var.flow_log_cloudwatch_log_kms_key_id

  tags = var.tags
}

data "aws_iam_policy_document" "cloudwatch_flow_log_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["vpc-flow-logs.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "cloudwatch_flow_log_policy_document" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams"
    ]

    resources = ["*"]
  }
}

resource "aws_iam_policy" "cloudwatch_flow_log_role_policy" {
  count  = var.flow_log_destination_type == "cloud-watch-logs" && var.flow_log_iam_role_arn == null ? 1 : 0
  name   = "${title(var.name)}FlowLogPolicy"
  policy = data.aws_iam_policy_document.cloudwatch_flow_log_policy_document.json
}

resource "aws_iam_role" "cloudwatch_flow_log_role" {
  count              = var.flow_log_destination_type == "cloud-watch-logs" && var.flow_log_iam_role_arn == null ? 1 : 0
  name               = "${title(var.name)}FlowLogRole"
  assume_role_policy = data.aws_iam_policy_document.cloudwatch_flow_log_assume_role.json

  tags = merge(
    {
      "Name" = "${title(var.name)}FlowLogRole"
    },
    var.tags
  )
}

resource "aws_iam_role_policy_attachment" "cloudwatch_flow_log_role_policy_attachment" {
  count      = var.flow_log_destination_type == "cloud-watch-logs" && var.flow_log_iam_role_arn == null ? 1 : 0
  role       = aws_iam_role.cloudwatch_flow_log_role[0].name
  policy_arn = aws_iam_policy.cloudwatch_flow_log_role_policy[0].arn
}

locals {
  computed_flow_log_destination_arn = var.flow_log_enabled && var.flow_log_destination_arn == null ? (var.flow_log_destination_type == "s3" ? module.flow_log_bucket.arn : module.flow_log_group[0].log_group_arn) : var.flow_log_destination_arn
}

resource "aws_flow_log" "flow_log" {
  count                = var.flow_log_enabled ? 1 : 0
  vpc_id               = aws_vpc.this.id
  iam_role_arn         = var.flow_log_iam_role_arn != null ? var.flow_log_iam_role_arn : (var.flow_log_destination_type == "cloud-watch-logs" ? aws_iam_role.cloudwatch_flow_log_role[0].arn : null)
  traffic_type         = var.flow_log_traffic_type
  log_destination_type = var.flow_log_destination_type
  log_destination      = local.computed_flow_log_destination_arn

  tags = merge(
    {
      "Name" = "${var.name}-${aws_vpc.this.id}-flow-log"
    },
    var.tags
  )
}
