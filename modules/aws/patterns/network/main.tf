resource "aws_vpc" "this" {
  cidr_block = var.cidr_block

  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    {
      "Name" = format("%s", var.name)
    },
    var.tags
  )
}

module "public_subnet" {
  source = "../../components/vpc/public_subnet"

  name = var.name
  tags = var.tags

  vpc_id             = aws_vpc.this.id
  availability_zones = var.availability_zones

  subnets = var.public_subnets
}

module "private_subnet" {
  source = "../../components/vpc/private_subnet"

  name = var.name
  tags = var.tags

  vpc_id             = aws_vpc.this.id
  availability_zones = var.availability_zones

  subnets = var.private_subnets

  public_subnets             = var.public_subnets
  public_subnet_ids          = module.public_subnet.subnets
  nat_gateway_deploy_subnets = var.nat_gateway_deploy_subnets

  peering_id   = var.peering_id
  peering_cidr = var.peering_cidr
}

module "ecr_private_link_sg" {
  source = "../../components/security_group/generic"

  name = "${var.name}-ecr-private-link-security-group"
  tags = var.tags

  create = true

  vpc_id = aws_vpc.this.id

  ingresses = [{
    port        = 443
    protocol    = "tcp"
    cidr_blocks = [var.cidr_block]
    description = "ECR Private Link inbound SG Rule"
  }]

  egresses = [{
    port        = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow any"
  }]
}

module "cloudwatch_logs_private_link_sg" {
  source = "../../components/security_group/generic"

  name = "${var.name}-cloudwatch-logs-private-link-security-group"
  tags = var.tags

  create = true

  vpc_id = aws_vpc.this.id

  ingresses = [{
    port        = 0
    protocol    = "-1"
    cidr_blocks = [var.cidr_block]
    description = "CloudWatch Logs Private Link inbound SG Rule"
  }]

  egresses = [{
    port        = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow any"
  }]
}

module "private_link" {
  source = "../../components/vpc/private_link"

  name = var.name
  tags = var.tags

  vpc_id                          = aws_vpc.this.id
  private_subnet_ids              = module.private_subnet.subnets
  ecr_private_link_sg             = module.ecr_private_link_sg.security_group_id
  cloudwatch_logs_private_link_sg = module.cloudwatch_logs_private_link_sg.security_group_id
  private_route_tables            = module.private_subnet.private_route_tables
}

# API Gateway用のVPCエンドポイントは必要時のみ作成
module "private_link_api_gateway" {
  count  = var.api_gateway_endpoint_enabled == true ? 1 : 0
  source = "../../components/vpc/private_link/api_gateway"

  name = var.name
  tags = var.tags

  vpc_id             = aws_vpc.this.id
  private_subnet_ids = module.private_subnet.subnets
  cidr_block         = var.cidr_block
}
