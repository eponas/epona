
variable "name" {
  description = "作成するVPC、ネットワークリソースに関する名前"
  type        = string
}

variable "tags" {
  description = "作成するVPC、ネットワークリソースに共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "flow_log_enabled" {
  description = "VPCフローログを出力する場合、trueに設定する"
  type        = bool
  default     = true
}

variable "flow_log_iam_role_arn" {
  description = "VPCフローログに付与するIAMロールのARN"
  type        = string
  default     = null
}

variable "flow_log_traffic_type" {
  description = "VPCフローログに記録するトラフィックの種類を、ACCEPT、REJECT、ALLのいずれかから指定する"
  type        = string
  default     = "ALL"
}

variable "flow_log_destination_type" {
  description = "VPCフローログの出力先を、s3またはcloud-watch-logsで指定する"
  type        = string
  default     = "cloud-watch-logs"
}

variable "flow_log_destination_arn" {
  description = "VPCフローログの出力先となる、S3またはCloudWatch LogsのARNを指定する。指定しない場合は、本モジュールで自動的に出力先を作成する"
  type        = string
  default     = null
}

variable "flow_log_s3_bucket" {
  description = "VPCフローログの出力先をS3にする場合のバケット名。指定しない場合は、本モジュールで自動的に決定する"
  type        = string
  default     = null
}

variable "flow_log_cloudwatch_log_group" {
  description = "VPCフローログの出力先をCloudWatch Logsにする場合のロググループ名。指定しない場合は、本モジュールで自動的に決定する"
  type        = string
  default     = null
}

variable "flow_log_cloudwatch_log_retention_in_days" {
  description = <<-DESCRIPTION
  VPCフローログをCloudWatch Logsに出力する場合の保持期間を設定する。

  値は、次の範囲の値から選ぶこと： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.
  [Resource: aws_cloudwatch_log_group / retention_in_days](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group#retention_in_days)
  DESCRIPTION
  type        = number
  default     = null
}

variable "flow_log_cloudwatch_log_kms_key_id" {
  description = "VPCフローログの出力先をCloudWatch Logsにする場合、CloudWatch Logsを暗号化するためのKMS CMKのARN"
  type        = string
  default     = null
}

variable "cidr_block" {
  description = "VPCに割り当てるCIDRブロック"
  type        = string
}

variable "availability_zones" {
  description = "パブリックサブネット、プライベートサブネットを配置するAZ"
  type        = list(string)
}

variable "public_subnets" {
  description = "パブリックサブネットに割り当てる、CIDRブロックのリスト"
  type        = list(string)
}


variable "private_subnets" {
  description = "プライベートサブネットに割り当てる、CIDRブロックのリスト"
  type        = list(string)
}

variable "nat_gateway_deploy_subnets" {
  description = "NAT ゲートウェイを配置するサブネットの、CIDRブロックのリスト。パブリックサブネットのCIDRブロックを指定する"
  type        = list(string)
}

variable "peering_id" {
  description = "VPCピアリングのID"
  type        = string
  default     = null
}

variable "peering_cidr" {
  description = "VPCピアリングする対象VPCのCIDR（peering_idを指定する場合は設定必須）"
  type        = string
  default     = null
}

variable "api_gateway_endpoint_enabled" {
  description = "API Gateway用のVPCエンドポイントを作成する場合、trueに設定する"
  type        = bool
  default     = false
}
