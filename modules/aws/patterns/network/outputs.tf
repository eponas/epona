output "vpc_id" {
  value = aws_vpc.this.id
}

output "public_subnets" {
  value = module.public_subnet.subnets
}

output "private_subnets" {
  value = module.private_subnet.subnets
}

output "private_link" {
  value = merge(module.private_link.endpoints, try(module.private_link_api_gateway[0].endpoints, {}))
  #  value = merge(module.private_link.endpoints)
}

output "flow_log_bucket_id" {
  description = "フローログの出力先となるS3バケットID"
  value       = var.flow_log_enabled && var.flow_log_destination_arn == null && var.flow_log_destination_type == "s3" ? module.flow_log_bucket.bucket_id : ""
}

output "flow_log_bucket" {
  description = "フローログの出力先となるS3バケット名"
  value       = var.flow_log_enabled && var.flow_log_destination_arn == null && var.flow_log_destination_type == "s3" ? module.flow_log_bucket.bucket : ""
}

output "flow_log_cloudwatch_log_group_name" {
  description = "フローログの出力先となるCloudWatch Logsロググループ名"
  value       = var.flow_log_enabled && var.flow_log_destination_arn == null && var.flow_log_destination_type == "cloud-watch-logs" ? module.flow_log_group[0].log_group_name : ""
}