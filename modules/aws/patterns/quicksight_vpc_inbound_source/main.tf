locals {
  # 各種リソースからの戻りを許可するため、全ての通信を許可
  # https://aws.amazon.com/jp/blogs/news/amazon-quicksight-private-vpc-access/
  sg_ingresses = [for integration_security_group in var.integration_security_groups : {
    port              = 0
    protocol          = "all"
    description       = integration_security_group.description
    security_group_id = integration_security_group.security_group_id
  }]
}

module "quicksight_simple_security_group" {
  source = "../../components/security_group/generic"

  name = var.security_group_name
  tags = var.tags

  vpc_id = var.vpc_id

  ingresses = []

  sg_ingresses = local.sg_ingresses

  egresses = []
}
