module "service_discovery" {
  source = "../../components/route53/service_discovery"

  vpc_id         = var.vpc_id
  namespace_name = var.service_discovery_namespace_name
  service_name   = var.service_discovery_service_name
  tags           = var.tags
}

module "security_group" {
  source = "../../components/security_group/generic"

  create = true
  name   = "${var.name_prefix}-proxy-security-group"
  vpc_id = var.vpc_id
  tags   = var.tags

  ingresses = [{
    port        = 3128
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Proxy inbound SG Rule"
  }]

  egresses = [{
    port        = -1
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Proxy outbound SG Rule"
  }]
}

module "default_ecs_task_execution_role" {
  source = "../../components/iam/ecs_task_execution_role"

  create = var.create_default_ecs_task_execution_role

  ecs_task_execution_iam_role_name   = var.default_ecs_task_execution_iam_role_name
  ecs_task_execution_iam_policy_name = var.default_ecs_task_execution_iam_policy_name
}

module "fargate_container_service" {
  source = "../../components/container/ecs_fargate"

  enable_code_deploy = false

  name = var.name_prefix
  tags = var.tags

  cluster_name = "${var.name_prefix}-proxy-cluster"

  subnets = var.container_subnets
  security_groups = [
    module.security_group.security_group_id
  ]

  service_association  = "ServiceDiscovery"
  service_registry_arn = module.service_discovery.service_arn

  desired_count      = var.container_service_desired_count
  platform_version   = var.container_service_platform_version
  cpu                = var.container_task_cpu
  memory             = var.container_task_memory
  task_role_arn      = null
  execution_role_arn = var.container_task_execution_role_arn != null ? var.container_task_execution_role_arn : module.default_ecs_task_execution_role.ecs_task_execution_iam_role_arn

  container_definitions = <<EOL
[
  {
    "name": "proxy",
    "image": "${var.repository_url}:${var.proxy_image_tag}",
    "portMappings": [
      {
        "containerPort": 3128
      }
    ]
  }
]
EOL
}
