data "aws_arn" "source_bucket" {
  arn = var.source_bucket_arn
}

# Epona の思想上、Terraform を実行するのは Delivery 環境上の Terraformer であるため、
# 取得できるのは Delivery 環境のアカウント ID 等になる
data "aws_caller_identity" "delivery" {}

locals {
  cross_account_deploy = var.runtime_account_id != data.aws_caller_identity.delivery.account_id
}

# Deliviery環境上におけるCodePipeline用ロール
module "cross_account_codepipeline_role_frontend" {
  source = "../../components/iam/cross_account_codepipeline_frontend/"

  pipeline_name      = var.pipeline_name
  runtime_account_id = var.runtime_account_id
  source_bucket_arn  = var.source_bucket_arn

  artifact_store_bucket_arn                = var.artifact_store_bucket_arn
  artifact_store_bucket_encryption_key_arn = var.artifact_store_bucket_encryption_key_arn
}

module "event_bus_role" {
  source = "../../components/iam/event_bus/sender/"

  # Delivery 環境と Runtime 環境が同一の場合、Event Bus は不要
  count = local.cross_account_deploy ? 1 : 0

  source_name          = var.pipeline_name
  target_event_bus_arn = var.target_event_bus_arn
}

# S3 に PutObject されたら Event Bus にイベントを送信する
module "s3_event_bus_sender" {
  source = "../../components/cloudwatch/event_bus/sender/"

  # Delivery 環境と Runtime 環境が同一の場合、Event Bus への Cloudwatch Events の送出は不要
  count = local.cross_account_deploy ? 1 : 0

  target_event_bus_arn        = var.target_event_bus_arn
  cloudwatch_event_rule_names = [aws_cloudwatch_event_rule.s3[0].name]
  role_arn                    = module.event_bus_role[0].arn
}

resource "aws_cloudwatch_event_rule" "s3" {
  # Delivery 環境と Runtime 環境が同一の場合、Event Bus への Cloudwatch Events の送出は不要
  count = local.cross_account_deploy ? 1 : 0

  name          = format("FireCodePipeline-%s", var.pipeline_name)
  description   = "Amazon CloudWatch Events rule to automatically start your pipeline when a PUSH occurs in the Amazon S3."
  event_pattern = <<-JSON
  {
    "source": [
      "aws.s3"
    ],
    "detail-type": [
      "AWS API Call via CloudTrail"
    ],
    "detail": {
        "eventSource": [
          "s3.amazonaws.com"
        ],
        "eventName": [
          "PutObject"
        ],
        "requestParameters": {
          "bucketName": [
            ${jsonencode(data.aws_arn.source_bucket.resource)}
          ],
          "key": [
            ${jsonencode(var.deployment_source_object_key)}
          ]
        }
    }
  }
  JSON
}
