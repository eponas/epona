locals {
  # var.artifact_store_bucket_name が設定されていない場合、自動的にアーティファクトストアの名称を設定
  artifact_store_bucket_name      = length(var.artifact_store_bucket_name) > 0 ? var.artifact_store_bucket_name : format("%s-artifact-store", var.pipeline_name)
  artifact_store_bucket_key_alias = format("alias/%s_key", lower(local.artifact_store_bucket_name))
  codebuild_project_name          = format("%sCodeBuild", var.pipeline_name)
  codebuild_log_group_name        = format("/aws/codebuild/%s", local.codebuild_project_name)
}

module "codebuild_cloudwatch_log" {
  source = "../../components/cloudwatch/log"

  log_group_name              = local.codebuild_log_group_name
  log_group_retention_in_days = var.deployment_cloudwatch_logs_retention_in_days
  log_group_kms_key_id        = var.deployment_cloudwatch_logs_kms_key_id

  tags = merge(
    {
      "Name" = "${local.codebuild_project_name}-logs"
    },
    var.tags
  )
}

module "codebuild" {
  source = "../../components/codebuild/deploy_s3/"

  project_name = local.codebuild_project_name
  tags         = var.tags

  artifact_store_bucket_name = module.codepipeline.artifact_store_bucket_name

  deployment_bucket_name = var.deployment_bucket_name
  deployment_object_path = var.deployment_object_path

  cloudwatch_log_group_name = local.codebuild_log_group_name

  enable_cache_invalidation  = var.cache_invalidation_config.enable
  cloudfront_distribution_id = var.cache_invalidation_config.cloudfront_distribution_id

  compute_type                = var.codebuild_compute_type
  base_image                  = "aws/codebuild/standard:4.0"
  image_type                  = "LINUX_CONTAINER"
  image_pull_credentials_type = "CODEBUILD"
}

module "codepipeline" {
  source = "../../components/codepipeline/s3/"

  pipeline_name = var.pipeline_name
  tags          = var.tags

  artifact_store_bucket_name               = local.artifact_store_bucket_name
  artifact_store_bucket_force_destroy      = var.artifact_store_bucket_force_destroy
  artifact_store_bucket_transitions        = var.artifact_store_bucket_transitions
  artifact_store_bucket_encryption_key_arn = module.kms_key.keys[local.artifact_store_bucket_key_alias].key_arn

  cross_account_codepipeline_access_role_arn = var.cross_account_codepipeline_access_role_arn

  source_bucket_name = var.source_bucket_name
  source_object_key  = var.source_object_key

  require_approval = var.deployment_require_approval

  deployment_codebuild_project_name = local.codebuild_project_name
}

# EventBus へのアクセスを許可
module "cloudwatch_events_receiver" {
  source = "../../components/cloudwatch/event_bus/receiver/"

  principal = var.delivery_account_id
}

# artifact store 用の bucket を Delivery 環境から参照させるために必要な KMS CMK
module "kms_key" {
  source = "../../components/kms/"

  tags = var.tags
  kms_keys = [
    {
      alias_name              = local.artifact_store_bucket_key_alias
      deletion_window_in_days = 30
      is_enabled              = true
      enable_key_rotation     = false
      policy                  = data.aws_iam_policy_document.cross_account_key_policy.json
    }
  ]
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "cross_account_key_policy" {
  statement {
    sid    = "RootUserKeyManagement"
    effect = "Allow"
    # actions を "kms.*" にすると以下のエラーが発生する
    # "The new key policy will not allow you to update the key policy in the future."
    actions = [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion"
    ]
    resources = ["*"]

    principals {
      type = "AWS"
      identifiers = [
        format("arn:aws:iam::%s:root", data.aws_caller_identity.current.account_id),
      ]
    }
  }

  # see: https://aws.amazon.com/jp/premiumsupport/knowledge-center/cross-account-access-denied-error-s3/
  statement {
    sid    = "EnableUseOfArtifactStoreEncryptionKey"
    effect = "Allow"
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    resources = ["*"]

    principals {
      type = "AWS"
      identifiers = [
        format("arn:aws:iam::%s:root", var.delivery_account_id), # Delivery環境からartifact storeへのアクセスのために必要
        module.codepipeline.codepipeline_service_role_arn,       # CodePipelineからのartifact storeへのアクセスに必要
        module.codebuild.codebuild_service_role_arn              # CodeBuildからのartifact storeへのアクセスに必要
      ]
    }
  }

  statement {
    sid    = "EnableGrantOfArtifactStoreEncryptionKey"
    effect = "Allow"
    actions = [
      "kms:CreateGrant",
      "kms:ListGrants",
      "kms:RevokeGrant"
    ]
    resources = ["*"]

    principals {
      type = "AWS"
      identifiers = [
        format("arn:aws:iam::%s:root", var.delivery_account_id),
        module.codepipeline.codepipeline_service_role_arn,
        module.codebuild.codebuild_service_role_arn
      ]
    }
    condition {
      test     = "Bool"
      variable = "kms:GrantIsForAWSResource"
      values   = [true]
    }
  }
}
