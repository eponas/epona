variable "pipeline_name" {
  description = "CodePipelineの名前"
  type        = string
}

variable "tags" {
  description = "CodePipelineに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "artifact_store_bucket_name" {
  description = <<DESCRIPTION
CodePipelineの[アーティファクト](https://docs.aws.amazon.com/ja_jp/codepipeline/latest/userguide/concepts.html#concepts-artifacts)を管理するS3 bucket名
未設定の場合、 "[pipeline_name]-artifact-store" でS3 bucketを作成する。
DESCRIPTION
  type        = string
  default     = ""
}

variable "artifact_store_bucket_force_destroy" {
  description = "Artifact Storeとして使っているS3 bucketを強制的に削除可能にするか否か"
  type        = bool
  default     = false
}

variable "artifact_store_bucket_transitions" {
  description = "Artifact Storeの移行に対するポリシー設定。最新でなくなったファイルに対して適用される。未設定の場合は30日後にAmazon S3 Glacierへ移行する"
  type        = list(map(string))
  default = [{
    days          = 30
    storage_class = "GLACIER"
  }]
}

variable "cross_account_codepipeline_access_role_arn" {
  description = "Runtime環境のCodePipelineから利用するクロスアカウントアクセス用RoleのARN。"
  type        = string
}

variable "source_bucket_name" {
  description = "デプロイ用のzip化されたソースファイルを配置するバケット名"
  type        = string
}

variable "source_object_key" {
  description = "デプロイ用のzip化されたソースファイルのObject Key"
  type        = string
  default     = "source.zip"
}

variable "deployment_require_approval_before_build" {
  description = "CodeBuildの実行前に承認を必要とするか"
  type        = bool
  default     = true
}

variable "deployment_require_approval_before_deploy" {
  description = "CodeDeployの実行前に承認を必要とするか"
  type        = bool
  default     = true
}


variable "codebuild_compute_type" {
  description = "デプロイ用CodeBuildのコンピューティングリソースタイプ。選択できる値については[Terraformドキュメントのcompute_type](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codebuild_project#compute_type)を参照。"
  type        = string
  default     = "BUILD_GENERAL1_SMALL"
}

variable "delivery_account_id" {
  description = "Delivery環境のアカウントID"
  type        = string
}

variable "deployment_cloudwatch_logs_retention_in_days" {
  description = <<-DESCRIPTION
デプロイ用CodeBuildのログをCloudWatch Logsで保持する期間を設定する。

値は、次の範囲の値から選ぶこと： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.
[Resource: aws_cloudwatch_log_group / retention_in_days](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group#retention_in_days)
DESCRIPTION
  type        = number
  default     = null
}

variable "deployment_cloudwatch_logs_kms_key_id" {
  description = "デプロイ用CodeBuildのログデータを暗号化するためのKMS CMKのARN"
  type        = string
  default     = null
}

variable "codedeploy_name" {
  description = "CodeDeployのアプリケーション名"
  type        = string
}

variable "codedeploy_deployment_group_name" {
  description = "CodeDeployのデプロイメントグループ名"
  type        = string
}

variable "sns_topic_arn_approval_before_build" {
  type        = string
  default     = null
  description = "ビルド前の承認リクエストの送信先となるSNSトピックのARNを指定する。"
}

variable "sns_topic_arn_approval_before_deploy" {
  type        = string
  default     = null
  description = "デプロイ前の承認リクエストの送信先となるSNSトピックのARNを指定する。"
}

variable "require_codedeploy" {
  type        = bool
  default     = true
  description = "CodeDeployをパイプラインに含めるかどうか。デフォルトは含める(`true`)"
}
