output "elasticache_redis_replication_group_id" {
  description = "ElastiCache for RedisのレプリケーショングループのID"
  value       = module.elasticache_redis.elasticache_replication_group_id
}

output "elasticache_redis_port" {
  description = "ElastiCache for Redisへの接続ポート"
  value       = module.elasticache_redis.elasticache_replication_group_port
}

output "elasticache_redis_configuration_endpoint_address" {
  description = "[クラスターモード有効時のエンドポイント（ConfigurationEndpoint）](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/Endpoints.html#Endpoints.Find.RedisCluster)のアドレス"
  value       = module.elasticache_redis.elasticache_replication_group_configuration_endpoint_address
}

output "elasticache_redis_primary_endpoint_address" {
  description = "[クラスターモード無効時の読み書きを行うエンドポイント（PrimaryEndPoint）](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/Endpoints.html#Endpoints.Find.Redis)のアドレス"
  value       = module.elasticache_redis.elasticache_replication_group_primary_endpoint_address
}
