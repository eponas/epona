locals {
  # var.artifact_store_bucket_name が設定されていない場合、自動的にアーティファクトストアの名称を設定
  artifact_store_bucket_name      = length(var.artifact_store_bucket_name) > 0 ? var.artifact_store_bucket_name : format("%s-artifact-store", var.pipeline_name)
  artifact_store_bucket_key_alias = format("alias/%s_key", lower(local.artifact_store_bucket_name))
}

module "codedeploy" {
  source = "../../components/codedeploy/ecs/"

  name                             = var.codedeploy_app_name
  deployment_group_name_ecs        = var.deployment_group_name_ecs
  deployment_config_name           = var.deployment_config_name
  auto_rollback_events             = var.deployment_auto_rollback_events
  action_on_timeout                = var.deployment_action_on_timeout
  wait_time_in_minutes             = var.deployment_wait_time_in_minutes
  termination_wait_time_in_minutes = var.termination_wait_time_in_minutes

  # Blue-Green Deployment時、テスト用リスナーについては利用を想定しない
  cluster_name       = var.cluster_name
  service_name       = var.service_name
  prod_listener_arns = var.prod_listener_arns
  target_group_names = var.target_group_names
}

module "codepipeline" {
  source = "../../components/codepipeline/ecs/"

  name = var.pipeline_name
  tags = var.tags

  codedeploy_app_name           = var.codedeploy_app_name
  deployment_group_name         = var.deployment_group_name_ecs
  deployment_setting_key        = var.deployment_setting_key
  require_approval              = var.deployment_require_approval
  task_definition_template_file = var.task_definition_template_file
  appspec_template_file         = var.appspec_template_file

  source_bucket_name                       = var.source_bucket_name
  artifact_store_bucket_name               = local.artifact_store_bucket_name
  artifact_store_bucket_force_destroy      = var.artifact_store_bucket_force_destroy
  artifact_store_bucket_transitions        = var.artifact_store_bucket_transitions
  artifact_store_bucket_encryption_key_arn = module.kms_key.keys[local.artifact_store_bucket_key_alias].key_arn

  ecr_repositories                           = var.ecr_repositories
  cross_account_codepipeline_access_role_arn = var.cross_account_codepipeline_access_role_arn
}

# EventBus へのアクセスを許可
module "cloudwatch_events_receiver" {
  source = "../../components/cloudwatch/event_bus/receiver/"

  principal = var.delivery_account_id
}

# artifact store 用の bucket を Delivery 環境から参照させるために必要な KMS CMK
module "kms_key" {
  source = "../../components/kms/"

  tags = var.tags
  kms_keys = [
    {
      alias_name              = local.artifact_store_bucket_key_alias
      deletion_window_in_days = 30
      is_enabled              = true
      enable_key_rotation     = false
      policy                  = data.aws_iam_policy_document.cross_account_key_policy.json
    }
  ]
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "cross_account_key_policy" {
  statement {
    sid    = "RootUserKeyManagement"
    effect = "Allow"
    # actions を "kms.*" にすると以下のエラーが発生する
    # "The new key policy will not allow you to update the key policy in the future."
    actions = [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion"
    ]
    resources = ["*"]

    principals {
      type = "AWS"
      identifiers = [
        format("arn:aws:iam::%s:root", data.aws_caller_identity.current.account_id),
      ]
    }
  }

  # see: https://aws.amazon.com/jp/premiumsupport/knowledge-center/cross-account-access-denied-error-s3/
  statement {
    sid    = "EnableUseOfArtifactStoreEncryptionKey"
    effect = "Allow"
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    resources = ["*"]

    principals {
      type = "AWS"
      identifiers = [
        format("arn:aws:iam::%s:root", var.delivery_account_id), # Delivery環境からartifact storeへのアクセスのために必要
        module.codepipeline.codepipeline_service_role_arn        # CodePipelineからのartifact storeへのアクセスに必要
      ]
    }
  }

  statement {
    sid    = "EnableGrantOfArtifactStoreEncryptionKey"
    effect = "Allow"
    actions = [
      "kms:CreateGrant",
      "kms:ListGrants",
      "kms:RevokeGrant"
    ]
    resources = ["*"]

    principals {
      type = "AWS"
      identifiers = [
        format("arn:aws:iam::%s:root", var.delivery_account_id),
        module.codepipeline.codepipeline_service_role_arn
      ]
    }
    condition {
      test     = "Bool"
      variable = "kms:GrantIsForAWSResource"
      values   = [true]
    }
  }
}
