#!/bin/bash -eu

# guide/templates/patterns/aws 配下の Markdown をドキュメントのテンプレートとして
# guide/patterns/aws 配下に出力する
repo_home=$(
    cd $(dirname $0)
    echo "$(pwd)/../../"
)
for template_file in $(find ${repo_home}/guide/templates/patterns/aws -type f -name \*.md ! -name README.md); do
    echo "generates terraform document from ${template_file}"

    pattern=$(basename ${template_file} .md)
    doc_file="${repo_home}/guide/patterns/aws/${pattern}.md"

    if [ -n "${CHECK_UPDATED-}" -a ! -f "${doc_file}" ]; then
        echo "${doc_file} が配置されていません。$0 を実行してドキュメントを生成してください"
        exit 1
    elif [ -n "${CHECK_UPDATED-}" ]; then
        # CI 実行時は、後段のドキュメント最新化チェックのためにドキュメントを一時待避
        orig=$(mktemp)
        cp ${doc_file} ${orig}
    fi

    # in/out の parameter を含めたドキュメントを生成
    export TF_DOC="$(
        terraform-docs \
            --no-providers \
            --sort-by-required \
            markdown table ${repo_home}/modules/aws/patterns/${pattern}/
    )"
    envsubst '${TF_DOC}' <${template_file} >${doc_file}

    if [ -n "${CHECK_UPDATED-}" ] && ! diff -u ${orig} ${doc_file}; then
        # 差分がある場合は、ドキュメントが最新化されていないので異常終了させる
        echo "${doc_file} が最新化されていません。$0 を実行してドキュメントを最新化してください" 1>&2
        exit 1
    fi
done
