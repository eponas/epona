#!/bin/bash -eu

# lab コマンドがインストールされていない場合はコマンド実行を終了する
function is_lab_installed() {
    if ! which lab >/dev/null; then
        echo "lab (https://github.com/zaquestion/lab) command is missing." 1>&2
        exit 1
    fi
}

# MR 作成の前提として develop branch を最新化する
function prepare() {
    git checkout develop
    git pull
}

# develop -> master への MR を作成する
function create_bump_version() {
    local version=${1:?"version is missing"}
    local branch="bump/${version}"

    # トップレベルのディレクトリへ移動
    pushd $(git rev-parse --show-toplevel)

    git checkout -b ${branch} # MR 用の branch を作成
    git rm -r --ignore-unmatch tests/
    git commit --allow-empty -m "bump version to ${version}"
    git push origin ${branch}

    # MR の作成
    lab mr create origin master \
        --label release \
        --remove-source-branch \
        --message "Checking in changes prior to tagging of version ${version}" \
        --message "# History" \
        --message "<ul>$(git log --oneline master..${branch} | perl -ple 's/^/<li>/' | tr -d '\n')</ul>"

    popd
}

VERSION=${1:?"version (ex., v1.0.0) is missing"}

is_lab_installed
prepare
create_bump_version ${VERSION}
