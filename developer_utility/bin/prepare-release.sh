#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0); pwd)
PROJECT_DIR=$(cd ${SCRIPT_DIR}; cd ../../; pwd)

VERSION=$1

for FILE in `find ${PROJECT_DIR}/guide/templates ${PROJECT_DIR}/guide/patterns -name '*.tf' -o -name '*.md' -not -name 'README.md' | grep -v '/\.terraform/' | xargs grep -l "git::https://gitlab.com/eponas/epona.git"`
do
    perl -wpi -e 's!(.+git::https://gitlab.com/eponas/epona.git//.+?ref=)[^"]+(".*)$!$1'${VERSION}'$2!g' ${FILE}
    echo "updated... ${FILE}"
done

VERSION_FOR_CHANGELOG=`echo "${VERSION}" | perl -pe 's!v([0-9.]+)!$1!'`
DATE=`date "+%Y-%m-%d"`

perl -wpi -e 's!^(## '${VERSION_FOR_CHANGELOG}').*Unreleased!$1 - '${DATE}'!' ${PROJECT_DIR}/CHANGELOG.md
echo "updated... ${PROJECT_DIR}/CHANGELOG.md"
