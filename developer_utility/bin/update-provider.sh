#!/bin/bash

## usage
# $ ./update-provider.sh aws [target-version]
#
# or
#
# $ ./update-provider.sh archive [target-version]

SCRIPT_DIR=$(cd $(dirname $0); pwd)
PROJECT_DIR=$(cd ${SCRIPT_DIR}; cd ../../; pwd)

PROVIDER=$1
VERSION=$2

shift
shift

FIX_VERSIONS_DIRS="${PROJECT_DIR}/tests ${PROJECT_DIR}/guide/templates/new_envionment"

for FILE in `find ${FIX_VERSIONS_DIRS} -name '*.tf' | grep -v '/\.terraform/'`
do
    tfupdate provider ${PROVIDER} -v ${VERSION} ${FILE}
    echo "updated... ${FILE}"
done


LOWER_BOUND_VERSIONS_DIRS="${PROJECT_DIR}/modules"

MAJOR_VERSION=`echo ${VERSION} | perl -wp -e 's!(\d+)\..*!$1!'`
NEXT_MAJOR_VERSION=`expr ${MAJOR_VERSION} + 1`

for FILE in `find ${LOWER_BOUND_VERSIONS_DIRS} -name 'versions.tf' | grep -v '/\.terraform/'`
do
    tfupdate provider ${PROVIDER} -v ">= ${VERSION}, < ${NEXT_MAJOR_VERSION}.0.0" ${FILE}
    echo "updated... ${FILE}"
done

