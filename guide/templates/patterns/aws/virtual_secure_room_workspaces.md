# virtual_secure_room_workspaces pattern

## 概要

`virtual_secure_room_workspaces pattern`モジュールは仮想セキュアルームにおける操作端末となる仮想デスクトップを構築します。

## 想定する適用対象環境

`virtual_secure_room_workspaces pattern`は、Runtime環境で使用することを想定しています。

## 依存するpattern

`virtual_secure_room_workspaces pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                                                                                   | 利用する情報                       |
| :------------------------------------------------------------------------------------------ | :--------------------------------- |
| [network pattern](./network.md)                                                             | プライベートサブネットのID         |
| [virtual_secure_room_directory_service pattern](./virtual_secure_room_directory_service.md) | DirectoryServiceのID               |
| [encryption_key pattern](./encryption_key.md)                                               | 暗号化に使用するキーのエイリアス名 |

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                | 説明                                                     |
| :-------------------------------------------------------- | :------------------------------------------------------- |
| [Amazon WorkSpaces](https://aws.amazon.com/jp/workspaces) | 入力変数で指定された設定で、仮想デスクトップを構築します |

## モジュールの理解に向けて

仮想セキュアルームは``Directory Service``と``WorkSpaces``で実現します。以下に各サービスの概要を簡単に記載します。

![仮想セキュアルーム](../../resources/virtual_secure_room.png)

**Directory Service** :ディレクトリ(AD)サービス。WorkSpacesにログインするユーザーを管理する。  
**WorkSpaces** :VDIサービス。仮想セキュアルームの端末となる。

`virtual_secure_room_workspaces pattern`は以下の部分を作成するモジュールになります。

![WorkSpacesパターン作成範囲](../../resources/virtual_secure_room_workspaces.png)

以下のリソースを作成します。

### WorkSpaces

- 指定したDirectory ServiceをWorkSpacesに登録する
- リストで指定したユーザーごとにWorkSpacesを作成する。ユーザーはDirectoryServiceで作成したユーザーのログオン名を指定する
- ルートボリュームおよびユーザーボリュームを指定したCMKで暗号化する
- WorkSpacesプロパティで指定した以下を設定する
  - コンピュータタイプ
  - ルートボリュームサイズ
  - ユーザーボリュームサイズ
  - 実行モード
  - 停止時間
- リストで指定したIPアドレスからのアクセスを許可する

## WorkSpacesへの接続方法

デプロイした仮想デスクトップへは以下の手順で接続できます。

1. モジュールのoutputまたはマネジメントコンソールでWorkSpacesの画面を開き対象端末の`登録コード`(registration code)を確認する
2. 以下、接続方法により手順が異なる

### デスクトップアプリで接続

デスクトップアプリで接続するにはポート4172を使用します。社内などプロキシ環境下では接続できない場合もあるので注意してください。

1. [クライアントダウンロードの画面](https://clients.amazonworkspaces.com/)にアクセスし自身の端末にあったクライアントツールをダウンロードする
2. WorkSpacesツールを起動、``registration code``を入力する
3. Directory Serviceに追加したドメインユーザーのログオン名、パスワードを入力する。ドメインユーザーの追加については[こちら](./virtual_secure_room_directory_service.md)の`ディレクトリユーザーの追加について`をご参照ください
4. WorkSpacesにログインできる

### webアクセス

webブラウザで接続します。

1. マネジメントコンソールで[WorkSpaces]-[ディレクトリ]をひらく
2. 対象ディレクトリを選択して[アクション]-[詳細の更新]
3. [アクセス制御のオプション]-[その他のプラットフォーム]で``Web Access``をチェックし[更新と終了]
4. [クライアントダウンロードの画面](https://clients.amazonworkspaces.com/)にアクセスし[Web Access]-[Launch]を選択
5. ``registration code``を入力して[Register]
6. Directory Serviceに追加したドメインユーザーのログオン名、パスワードを入力する。ドメインユーザーの追加については[こちら](./virtual_secure_room_directory_service.md)の`ディレクトリユーザーの追加について`をご参照ください
7. WorkSpacesにログインできる

## WorkSpacesへのアクセス許可について

WorkSpacesへの接続を許可する送信元IPアドレス(CIDR)は、リポジトリにコミットしたくない場合もあります。その場合は、.tfファイルにハードコードするのではなく、入力変数ファイル(.tfvarsファイル)に定義して、terraformコマンドのオプションで指定してください。管理方針については、合わせて[こちら](./parameter_store.md)もご参照ください。

## WorkSpacesから、サービス用VPC内のリソースへのアクセスについて

仮想セキュアルーム用とサービス用でそれぞれVPCを分けている場合、そのままではWorkSpacesからサービス用VPCへの通信はできません。
[vpc_peering pattern](vpc_peering.md)を適用しVPCピアリングを構成してください。

さらに、WorkSpacesからサービス用のVPC内のリソースへアクセスするためには、対象のリソースに対する通信許可を行う必要があります。

たとえば、Eponaがサポートするデータストア用の以下のpatternではインバウンドとしてセキュリティグループを設定できます。

- [database pattern](./database.md)
- [redis pattern](./redis.md)

VPCピアリングを行ったうえで、本patternを適用した際に出力される`security_group_ids`を各種patternのインバウンドに設定してください。

このようにして、WorkSpacesから確認する必要があるリソースに対して、通信許可を設定しましょう。

## WorkSpacesの自動停止について

仮想セキュアルームのWorkSpacesは常時起動している必要はありません。
また、利用料を抑えるためにも必要な時だけ起動する方が望ましいです。
そのため、Eponaで提供する仮想セキュアルームのWorkSpacesは時間経過で自動停止するように設定します。
デフォルトでは2時間（120分）としていますが停止までの時間は利用者で調整可能です。
なお、WorkSpacesを停止してもWorkSpacesに保存されたデータは保持されます。

## WorkSpacesとの各種共有設定について

WorkSpacesはデフォルトで、クライアント端末とWorkSpaces双方のクリップボード共有やプリンターによる印刷が有効になっています。
ドメインに対して各種共有設定の有効/無効を変更できます。
詳しい設定方法は[virtual_secure_room_directory_service pattern](./virtual_secure_room_directory_service.md)のドキュメントを参照ください。

## 利用イメージ

Epona利用者は、本patternで構築される仮想デスクトップを用いて本番運用におけるデータベース参照やAWSマネジメントコンソールへのログイン等を行います。

本番運用は注意を要する作業になります。本patternで構築した仮想デスクトップを用いてどのようなフローで本番運用作業を行うかについては、各サービスごとに慎重に検討してください。

また、仮想デスクトップに対するルールについても各サービスごとに慎重に検討してください。たとえば、機密情報の漏洩を防ぐために仮想デスクトップからのアウトバウンドに制限をかけるなどです。

本番運用に必要なアプリケーションについては仮想デスクトップにインストールすれば利用可能です。

## サンプルコード

`virtual_secure_room_workspaces pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "workspaces" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/virtual_secure_room_workspaces?ref=v0.2.6"

  tags = {
    Owner              = "test"
    Environment        = "virtual-secure-room"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }

  workspaces_directory_id                              = data.terraform_remote_state.production_directory_service.outputs.directory_service.directory_id
  workspaces_subnet_ids                                = data.terraform_remote_state.network.outputs.network.private_subnets
  workspaces_user_names                                = ["bobmarley", "alicecooper"]
  workspaces_kms_key                                   = data.terraform_remote_state.production_kms.outputs.keys["alias/workspaces-key"].key_arn
  workspaces_compute_type_name                         = "VALUE"
  workspaces_user_volume_size_gib                      = 50
  workspaces_root_volume_size_gib                      = 80
  workspaces_running_mode                              = "AUTO_STOP"
  workspaces_running_mode_auto_stop_timeout_in_minutes = 120

  workspaces_access_source_addresses = var.source_addresses
```

## 関連するpattern

`virtual_secure_room_workspaces pattern`に関連するpatternを、以下に記載します。

| pattern名 | 説明 |
| :-------- | :--- |
| [virtual_secure_room_directory_service pattern](./virtual_secure_room_directory_service.md) | WorkSpacesに接続するユーザーおよびパスワードを管理するDirectoryService |

## 入出力リファレンス

${TF_DOC}
