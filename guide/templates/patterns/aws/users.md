# users pattern

## 概要

`users pattern`モジュールでは、AWS(Delivery環境)のAWS Identity and Access Management(IAM)環境を構築します。

## 想定する適用対象環境

`users pattern`は、Delivery環境で使用されることを想定しています。

## 依存するpattern

`users pattern`モジュールは`bind_role pattern`モジュールと`roles pattern`モジュールとセットで実行します。各モジュールの役割、実行順序は以下のとおりです。

1. `users pattern`：Delivery環境にIAMユーザーを作成します
1. `bind_role pattern`：Runtime環境のIAMロールへスイッチロールするIAMポリシーを作成し、Delivery環境のIAMユーザーへアタッチします
1. `roles pattern`：Runtime環境のIAMロールを作成します

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                 | 説明                                                |
| :----------------------------------------------------------------------------------------- | :-------------------------------------------------- |
| [IAMユーザー](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_users.html)        | Delivery環境のIAMユーザーを作成します               |
| [IAMポリシー](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/access_policies.html) | IAMユーザー/ロールへ付与するIAMポリシーを作成します |
| [IAMロール](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_roles.html)          | Delivery環境のIAMロールを作成します                 |

## モジュールの理解に向けて

`users pattern`はDelivery環境にIAMユーザー、IAMユーザーに付与するIAMポリシー、IAMユーザーからスイッチするIAMロール、IAMロールに付与するIAMポリシーを作成します。

本patternでは以下のロールを用意しています。どのロールに対し、どのユーザーを所属させるかを指定してterraformを実行してください。

なおロール名の`%s`は、本patternの入力パラメータである`system_name`の値をCamelCaseとして変換した文字列に置き換えてください。例えば`system_name`に`"epona"`を指定した場合、管理者ロールの名前は`EponaAdminRole`となります。

| ロール名            | 説明                                                                           |
| :------------------ | :----------------------------------------------------------------------------- |
| `%sAdminRole`       | 管理者ロール(全ての作業を行う権限をもちます)                                   |
| `%sViewerRole`      | 閲覧者ロール(全リソースの閲覧のみできる権限をもちます)                         |
| `%sOperatorRole`    | 作業者ロール(読み取り宣言+運用で必要な権限をもちます)                          |
| `%sDeveloperRole`   | 開発者ロール(NWなどインフラ系サービスの操作や秘匿パラメータの参照を除いたリソースを操作する権限をもちます) |
| `%sCostManagerRole` | コスト管理者ロール(課金情報の操作のみできる権限をもちます)                     |
| `%sApproverRole`    | 承認者ロール(CI/CDの承認のみ可能な権限をもちます)                              |

IAMユーザーは各権限のIAMロールへスイッチロールすることで、必要な権限を取得します。

:warning: IAMユーザーは特定のロールを割り当てられるだけでは、そのロール上の権限を行使できません。
権限を行使するためには、明示的にそのロールに切り替える（スイッチする）ことが必要となります。
詳細については[Switching to a Role (Console)](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_roles_use_switch-role-console.html)を参照ください。

セキュリティ向上のため、IAMユーザーがスイッチロールするにはAWSマネジメントコンソールへのログイン時にMFA認証が有効になっていることを必須としています。
MFAの有効化については、[MFA デバイスの有効化](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_credentials_mfa_enable.html)を参照してください。

`users pattern`はDelivery環境内でスイッチロールする環境を構築します。
Runtime環境へスイッチロールする場合は、`bind_role pattern`と`roles pattern`を実行する必要があります。

![IAMユーザ関連のpattern](../../resources/users.png)

## 公開鍵設定について

TerraformからIAMユーザーを作成すると初期パスワードが生成されますが、このパスワードは本来漏らしてはならないものです。
このため、Terraformでは、初期パスワードを暗号化して出力する仕様となっています。
本patternでもその機能を利用しているため、暗号化に必要な暗号化鍵を指定いただく必要があります。

よって、本patternを実行するためには、鍵を生成する環境を整える必要があります。

本章では、利用者の環境に合わせて鍵の作成手順をガイドします。

### Linux OSの場合

1. gpgをインストール

      ```bash
      $ yum install gpg
      ```

1. gpgで公開鍵/秘密鍵を作成

    以下で登場する `test_name` については、`gpg --gen-key` 実行時に尋ねられる「本名」で指定した内容に置き換えてください。  
    なお、キーの作成時に乱数の生成が行われますが、環境によってはこの処理で非常に時間のかかることがあります。  
    この場合は[Rng-tools](https://wiki.archlinux.jp/index.php/Rng-tools)を利用する等の方法をご検討ください。

      ```bash
      $ gpg --gen-key
      $ gpg -o ./test_name.public.gpg  --export test_name
      $ gpg -o ./test_name.private.gpg --export-secret-key test_name
      ```

1. 公開鍵情報をエンコードし、改行を取り除いてファイル出力しておきます

      ```bash
      $ cat test_name.public.gpg | base64 | tr -d '\n' > test_name.public.gpg.base64
      $ cat test_name.public.gpg.base64
      ```

1. 最後にcatされた文章をコピーして、`main.tf`の`pgp_key`変数に代入します

### Windows OSの場合

1. [GnuPG公式サイト](https://www.gnupg.org/) へ移動します
1. `Download`をクリックし、画面下部`Gpg4win`をクリックします
1. `Gpg4win X.X.X`と書かれた緑色のダウンロードアイコンをクリックします
1. ダウンロードメニュが表示されます。donateしない様に「$0」を選択した状態で、`Download`ボタンをクリックします
1. ダウンロードしたファイルを実行し、インストールを開始します
1. インストールは全てデフォルト値で実行します
1. インストール完了後、「Kleopatra」が自動で起動するので、「New Key Pair」をクリックします
1. 名前とメールアドレスに任意の情報を入れて「次へ」をクリックします
1. `Create`をクリックします
1. 任意のパスフレーズを入力します
1. 「完了」をクリックします
1. `Kleopatra`画面で作成した鍵を選択して右クリック「エクスポート」をクリックします
1. エクスポートした鍵を任意のエディタで起動し、末尾の改行コードを削除します
1. 鍵情報をコピーして`main.tf`の`pgp_key`変数に代入します(以下の`<鍵情報>`の範囲のみコピーください)

    >-----BEGIN PGP PUBLIC KEY BLOCK-----
    >
    >`<鍵情報>`
    >
    >=eq0E
    >
    >-----END PGP PUBLIC KEY BLOCK-----

## IAMユーザー初期パスワード復号方法

`users pattern`実行時に、各IAMユーザーのパスワード情報がoutputされます。

パスワード情報は暗号化されているため、事前に作成した秘密鍵で復号する必要があります。

本章で、各環境での復号方法をガイドします。

### Linux OSの場合

1. `users pattern`実行後、以下コマンドでoutput情報を再出力します

      ```bash
      $ terraform output password
      ```

1. 各IAMユーザーのパスワード情報が、ダブルクォーテーションで区切られて出力します。ダブルクォーテーションの中を選択し、以下コマンドでbase64デコードして保存します(test_nameは任意に設定した鍵の名前)

      ```bash
      $ echo "暗号文" | base64 -d | gpg -r test_name
      ```

1. パスフレーズの入力が求められるので、暗号時に設定したパスフレーズを入力します

    :warning: 環境によってはパスフレーズの入力が求められず、復号に失敗することがあります。  
    その際は以下のコマンドをお試しください。

      ```bash
      $ echo "暗号文" | base64 -d | gpg --passphrase=<設定したパスフレーズ> --batch -r test_name
      ```

1. パスワードが平文に復号されるので、IAMユーザーの初期パスワードとして利用します

### Windows OSの場合

1. `users pattern`実行後、以下コマンドでoutput情報を再出力します

      ```terraform
      $ terraform output password
      ```

1. 各IAMユーザーのパスワード情報が、ダブルクォーテーションで区切られて出力します。ダブルクォーテーションの中身を`password.txt`として保存します

1. `password.txt`をbase64でデコードします

      ```bat
      $ certutil -f -decode pass.txt pass_r.txt
      ```

1. Kleopatraを起動し、「復号/検証」ボタンをクリックし、「pass_r.txt」を選択します
1. 復号された平文のパスワードが表示されるため、IAMユーザーの初期パスワードとして利用します

## IAMロールの権限追加について

[モジュールの理解に向けて](#モジュールの理解に向けて)に記載している通り、Eponaでは各IAMロールに必要と思われるIAMポリシーをアタッチしています。
本patternで作成するIAMロールにデフォルトで付与するIAMポリシーの詳細は[こちら](https://gitlab.com/eponas/epona/-/tree/master/modules/aws/components/iam/role)を確認ください。
しかし、プロジェクトによってはIAMロールに追加のIAMポリシーを与えたい場合も考えられます。
そのため、`additional_XXX_role_policy`（XXXは各ロール名）パラメータを用意しています。
追加したいIAMポリシーのARNをリスト形式で指定するとIAMロールに追加できます。

## パスワードの再発行について

稀に発行されたパスワードを復号すると以下のように**英数字以外**が含まれる文字列となります。

```txt
ZD%^cXGwhN==rOឝ)_
```

このパスワードを使ってもマネジメントコンソールにログインできません。
このようなパスワードが発行されてしまった場合は以下のワークアラウンドを実施しパスワードを設定してください。

- AdministratorAccess等、IAMを操作可能なIAMポリシーを持つIAMユーザーでマネジメントコンソールにログイン
- [IAM]->[ユーザー]を開き、対象のIAMユーザーを選択
- [認証情報]タブを開き、[サインイン認証情報]の[コンソールのパスワード]の右横にある`管理`を選択
- [パスワードの設定]で`カスタムパスワード`を選択し、入力欄に任意のパスワードを入力。[パスワードのリセットが必要]はどちらでも良い。入力したら`適用`

## サンプルコード

`users pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "users" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/users?ref=v0.2.6"
  users  = ["taro", "hanako", "yoshiki", "ayaka", "yoko", "ryota", "chiharu"]

  admins       = ["taro", "hanako"]
  approvers    = ["taro", "yoko"]
  costmanagers = ["taro", "ryota"]
  developers   = ["taro", "chiharu"]
  operators    = ["taro", "ayaka"]
  viewers      = ["taro", "yoshiki"]

  additional_developer_role_policies = [
    "arn:aws:iam::aws:policy/AWSCodePipelineApproverAccess"
  ]

  account_id       = "XXXXXXXXXX"
  environment_name = "delivery"
  system_name      = "hori"
  tags = {
    Owner       = "hori"
    Environment = "delivery"
    ManagedBy   = "epona"
  }
  pgp_key = "XXXXXXXXXXX"
}

output "password" {
  value = module.users.password
}


```

## 関連するpattern

`users pattern`に関連するpatternを、以下に記載します。

| pattern名                             | 説明                                                                                         |
| :------------------------------------ | :------------------------------------------------------------------------------------------- |
| [`bind_role pattern`](./bind_role.md) | このpatternで作成したIAMユーザーからIAMロール(Runtime環境)へスイッチするポリシーを作成します |
| [`roles pattern`](./roles.md)         | このpatternで作成したIAMユーザーからスイッチするIAMロール(Runtime環境)を作成します           |

## 入出力リファレンス

${TF_DOC}
