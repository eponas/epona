# datadog_log_trigger pattern

## 概要

`datadog_log_trigger pattern`モジュールでは、`datadog_fowarder pattern`でデプロイしたAWS Lambda関数を、AWS内のログリソースに紐付けます。

この紐付けを行うことで、AWS環境内のログをDatadogへ転送することが実現できます。

## 想定する適用対象環境

`datadog_log_trigger pattern`は、Delivery環境およびRuntime環境のいずれでも使用できますが、主としてRuntime環境で利用することになるでしょう。

## 依存するpattern

`datadog_log_trigger pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                                       | 利用する情報                                             |
| :---------------------------------------------- | :------------------------------------------------------- |
| [datadog_forwarder pattern](./datadog_forwarder.md) | Datadogが提供するAWS Lambda関数のARN |

また、収集対象となるログを出力するpatternも適用されている必要があります。

`datadog_log_trigger pattern`では、Datadog Forwarderと呼ばれるDatadogが提供するAWS Lambda関数を使用してログ転送を行います。

## AWS上にログを出力するpattern

Eponaが提供するpatternでAWS上にログを保存しうるものは、以下があります。

| pattern名 | ログの内容 | ログの保存先 |
|:----------|:-----------|:-------------|
| [network](network.md) | VPCフローログ | Amazon CloudWatch LogsまたはAmazon S3 |
| [cacheable_frontend](cacheable_frontend.md) | アクセスログ（Amazon CloudFront） | Amazon S3 |
| [cacheable_frontend](cacheable_frontend.md) | サーバーアクセスログ（Amazon S3） | Amazon S3 |
| [public_traffic_container_service](public_traffic_container_service.md)  | コンテナログ | Amazon CloudWatch Logs |
| [public_traffic_container_service](public_traffic_container_service.md)  | アクセスログ（ALB） | Amazon S3 |
| [database](database.md)  | ログ（エンジンごとに異なる） | Amazon CloudWatch Logs |
| [database](database.md)  | 拡張モニタリング | Amazon CloudWatch Logs |
| [webacl](webacl.md) | WebACLトラフィックログ | Amazon S3 |
| [audit](audit.md) | 証跡ログ | Amazon S3 |
| [rule_violation](rule_violation.md) | AWS Lambda関数の実行ログ | Amazon CloudWatch Logs |
| [threat_detection](threat_detection.md) | AWS Lambda関数の実行ログ | Amazon CloudWatch Logs |
| [cd_pipeline_frontend](cd_pipeline_frontend.md) | CodeBuildの実行ログ | Amazon CloudWatch Logs |

:warning: `datadog_forwarder pattern`によりデプロイされるAWS Lambda関数のログは紐付けの対象外とします（転送がループするため）。

各patternのドキュメントのOutputに、ログの出力先の情報が記録されます。  
`datadog_log_trigger pattern`では、これをRemote Stateで参照します。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                         | 説明                                                        |
| :----------------------------------------------------------------- | :---------------------------------------------------------- |
| [Amazon CloudWatch Logs サブスクリプションフィルタ](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/SubscriptionFilters.html) | Datadog Forwarderを呼び出すための、サブスクリプションフィルタが設定されます |
| [Amazon S3 通知](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/dev/NotificationHowTo.html) | バケットにイベントが発生した際に、Datadog Forwarderを呼び出すように通知設定を行います |

いずれも、Datadog ForwarderによるAWS Lambda関数を呼び出すためのイベントトリガーが設定されます。

## モジュールの理解に向けて

### patternのコンセプト

`datadog_log_trigger pattern`は、ログをDatadogに転送するAWS Lambda関数とログ保存リソースを紐付けるだけの、シンプルなモジュールです。

各patternにおいて、利用するマネージドサービスやコンテナ等、ログを出力することが自明なものにはあらかじめOutputに出力先が設定されています。  
`datadog_log_trigger pattern`では、`datadog_forwarder pattern`がデプロイしたAWS Lambad関数のARNと、ログ出力先の情報を紐付けます。

![datadog_log_trigger patternの全体像](./../../resources/datadog_log_trigger_overview.png)

ログが保存されるリソースは、利用するpatternやシステムの構成内容により、変化します。  
`datadog_forwarder pattern`の実行時にすべて揃っているとは限らず、その後に増減することもあるでしょう。  
このため、`datadog_forwarder pattern`で紐付けの設定までは行わず、2つのpatternはライフサイクルとして分離しています。

紐付けには、ログ出力先がAmazon CloudWatch Logsの場合はロググループ名、Amazon S3バケットの場合はバケット名を使用します。

各patternのログ有無、またログの保存先リソースについては、[AWS上にログを出力するpattern](#aws上にログを出力するpattern)および各patternの入出力リファレンスを参照してください。

### Datadog Forwarderとログリソースの紐付け

実際の紐付けイメージを記載します。

`datadog_forwarder pattern`によってデプロイされたAWS Lambda関数のARNは、Remote Stateで参照できます。  
これを`datadog_log_trigger pattern`の引数として設定します。

```terraform
  datadog_forwarder_lambda_arn = data.terraform_remote_state.production_datadog_forwarder.outputs.datadog_forwarder.datadog_forwarder_lambda_function_arn
```

紐付け先のリソースに関しては、Amazon CloudWatch Logsに対しては`cloudwatch_log_subscription_filters`で指定します。

```terraform
  cloudwatch_log_subscription_filters = concat(
    [
      {
        log_group_name = data.terraform_remote_state.production_network.outputs.network.flow_log_cloudwatch_log_group_name
      }
    ],

    ## ログリソースの分だけ記載
  )
```

:information_source: `concat`関数を使用しているのは、`database pattern`のようにロググループ数が可変（`list`）になるものがあるためです。

Amazon S3の場合は`logging_s3_bucket_notifications`で指定します。

```terraform
  logging_s3_bucket_notifications = [
    {
      bucket = data.terraform_remote_state.production_public_traffic_container_service.outputs.public_traffic_container_service.load_balancer_access_logs_bucket
    },

    ## ログが保存されるS3バケット分だけ記載
  ]
```

いずれの変数とも、`map`を要素とした`list`を設定します。  
この`map`の要素としては、少なくともAmazon CloudWatch Logsではロググループ名、Amazon S3ではバケット名を指定します。  
AWS Lambda関数とのARNの紐付けは、`datadog_log_trigger pattern`内で行います。

:information_source: 転送するログを絞りたい場合は、[ログフィルター](#ログフィルターについて)を定義できます。

### ログフィルターについて

Datadogでは、サービスに関連するすべてのログをDatadogに収集したうえで、必要に応じて検索・可視化することが想定されています
([参照](https://www.datadoghq.com/ja/product/log-management/))。  
よって、Eponaでも`datadog_log_trigger pattern`を使って可能な限りDatadogにログを送信することを推奨します。  
とはいえ、Datadogのログ収集についての課金額は
[インデックス化されたログイベントの量によって決まる](https://docs.datadoghq.com/ja/account_management/billing/log_management/)
ことから、ログの課金額を抑えたいケースもあるでしょう。  
その場合、Datadog側で[除外フィルター](https://docs.datadoghq.com/ja/logs/indexes/#%E9%99%A4%E5%A4%96%E3%83%95%E3%82%A3%E3%83%AB%E3%82%BF%E3%83%BC)を設定することで、Datadogにログを送信しつつ課金額を抑えるという方法があります。

Datadogに送信されたログイベントで、除外フィルターの条件に合致するものはインデックス化されません。  
インデックス化されないログイベントは、[検索、パターン、分析、ログ監視](https://docs.datadoghq.com/ja/logs/indexes/)に
利用できなくなります。  
しかし、ログ情報自体はDatadogに送られているため[メトリクス](https://docs.datadoghq.com/ja/logs/logs_to_metrics/)の収集や、
メトリクスを使った可視化は可能です。

:information_source: 除外したログを対象に
[パイプラインを使った非構造ログの可視化](../../how_to/datadog_log_analysis.md#ログフィルターが設定されている場合の注意点)や
[ダッシュボード作成](../../how_to/datadog_dashboard.md#ログフィルターが設定されている場合の注意点)をする場合、
特殊な対応が必要となります。詳細は各how toの`ログフィルターが設定されている場合の注意点`を参照してください。

ログの出力量が多い一方、ログ本文を参照する可能性が低いもの、傾向だけわかればよいものは除外フィルターの候補になるでしょう。  
たとえば、[VPCフローログ](https://docs.aws.amazon.com/ja_jp/vpc/latest/userguide/flow-logs.html)などがこれにあたると考えられます。

#### 設定方法

除外フィルターは
[インデックスの設定画面](https://app.datadoghq.com/logs/pipelines/indexes)[^1]
で設定します。  
たとえば、`epona-staging-network-flow-log`というVPCフローログを除外する場合は、Datadog側で以下のように設定します。

  1. [`INDEXES`]の`main`の`>`をクリックします
  1. `+ Add an Exclusion Filter`をクリックし、以下の条件を入力します  
  ![log_filterの設定内容](../../resources/datadog_forwarder_log_filter.png)

除外フィルターが不要になった場合も、[インデックスの設定画面](https://app.datadoghq.com/logs/pipelines/indexes)[^1]でオン・オフの切り替えが可能です。

[^1]: Datadogへのログインが必要です。

### 複数のリージョンへの対応

AWS環境内でログが複数のリージョンに保存される場合、使用しているリージョンの数だけ`datadog_forwarder pattern`を適用します。  
これに伴い、ログの転送設定を行う`datadog_log_trigger pattern`についても各リージョンに対して行う必要があります。

![複数リージョンへの対応](./../../resources/datadog_log_trigger_multi_region.png)

`cacheable_frontend`等、使用するリージョンが決められているpatternを使っている場合は注意してください。

## サンプルコード

`datadog_log_trigger pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "datadog_log_trigger" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/datadog_log_trigger?ref=v0.2.6"

  datadog_forwarder_lambda_arn = data.terraform_remote_state.production_datadog_forwarder.outputs.datadog_forwarder.datadog_forwarder_lambda_function_arn

  cloudwatch_log_subscription_filters = concat(
    [
      {
        log_group_name = data.terraform_remote_state.production_network.outputs.network.flow_log_cloudwatch_log_group_name
      }
    ],
    [for name in data.terraform_remote_state.production_database.outputs.database.instance_log_group_names : {
      log_group_name = name
    }],
    [
      {
        log_group_name = data.terraform_remote_state.production_database.outputs.database.monitoring_log_group_name
      }
    ],

    ## ログが保存されるCloudWatch Logsロググループ分、リストアップする
  )

  logging_s3_bucket_notifications = [
    {
      bucket = data.terraform_remote_state.production_public_traffic_container_service.outputs.public_traffic_container_service.load_balancer_access_logs_bucket
    },

    ## ログが保存されるS3バケット分、リストアップする
  ]
}
```

`cacheable_frontend pattern`を使用する等、異なるリージョンのログも収集する場合は、デフォルトのリージョン以外をProviderに指定してください。

```terraform
provider "aws" {
  assume_role {
    role_arn = "..."
  }

  region = "us-east-1"
}

module "datadog_log_trigger" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/datadog_log_trigger?ref=v0.2.6"

  ...
}
```

この場合、複数の`datadog_log_trigger pattern`インスタンスを使用します。

## 関連するpattern

`datadog_log_trigger pattern`は、AWS環境からログ転送を行う終端のpatternとなります。

Datadogに転送されたログからのメトリクス抽出等については、ドキュメントを参照してください。

- [Datadogを用いたログからのメトリクス抽出、可視化ガイド](../../how_to/datadog_log_analysis.md)

## 入出力リファレンス

${TF_DOC}
