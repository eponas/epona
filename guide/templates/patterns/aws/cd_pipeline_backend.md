# cd_pipeline_backend pattern

## 概要

`cd_pipeline_backend pattern`は、Runtime環境上の[Amazon Elastic Container Service (ECS)](https://aws.amazon.com/jp/ecs/)へデプロイを行うためのpatternです。

## 想定する適用対象環境

`cd_pipeline_backend pattern`は、Runtime環境での使用を想定しています。

## 依存するpattern

`cd_pipeline_backend pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                                                                         | 利用する情報                                                                                                                                                           |
| :-------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [ci_pipeline pattern](./ci_pipeline.md)                                           | Dockerコンテナレジストリ                                                                                                                                               |
| [cd_pipeline_backend_trigger pattern](./cd_pipeline_backend_trigger.md)           | デプロイのトリガとなるイベント、デプロイ設定を格納するAmazon S3バケット、Delivery環境のAmazon Elastic Container Registry(ECR)やAmazon S3バケットにアクセス可能なロール |
| [public_traffic_container_service pattern](./public_traffic_container_service.md) | デプロイ対象となるECSクラスター名、ECSサービス名、およびALBのリスナー                                                                                                  |

なお、`cd_pipeline_backend_trigger pattern`に関しては、本patternの適用後に得られる情報を使ってリソースを更新する必要があります。相互に依存関係があるので注意してください。

本patternが依存するリソースを他の構築手段で代替する場合は、依存するpatternと[入出力リファレンス](#入出力リファレンス)の内容を参考に、本patternが必要とするリソースを構築してください。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                               | 説明                                                                                                             |
| :----------------------------------------------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------------------------------------- |
| [AWS CodeDeploy (CodeDeploy)](https://aws.amazon.com/jp/codedeploy/)                                                     | コンテナイメージとデプロイ設定を元にしてECS上の[AWS Fargate](https://aws.amazon.com/jp/fargate/)にデプロイを行う |
| [AWS CodePipeline (CodePipeline)](https://aws.amazon.com/jp/codepipeline/)                                               | CodeDeployを含めたデプロイメントパイプラインを構成する                                                           |
| [Amazon EventBridge](https://aws.amazon.com/jp/eventbridge/)                                                             | 異なるAWSアカウント間でイベントを伝搬させるための経路(イベントバス)を構築します                                  |
| [Amazon CloudWatch Events](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/events/WhatIsCloudWatchEvents.html) | デプロイ実行のトリガとなるイベントを受け、デプロイメントパイプラインを起動するルールを構成します                 |
| [AWS Key Management Service (KMS)](https://aws.amazon.com/jp/kms/)                                                       | CodePipelineが利用するArtifact Store(S3バケット)を暗号化するための鍵を構築します                                 |

## モジュールの理解に向けて

`cd_pipeline_backend pattern`でサポートされるデプロイは、[Blue/Green Deployment](https://docs.aws.amazon.com/ja_jp/codedeploy/latest/userguide/welcome.html#welcome-deployment-overview-blue-green)と呼ばれる方法です。
本モジュールではこのデプロイをCodeDeployを使って実現します。

Blue/Green Deploymentでは、稼働中の環境とは別の新しい環境を構成し、新環境側にデプロイを行います。
デプロイが成功した後でユーザートラフィックを新環境に振り向ける(ルーティングする)ことで、スムーズに新環境への移行が可能です。また、デプロイが失敗した場合はルーティングを実施しないため、ユーザー影響がありません。

これらCodeDeployで実現されるBlue/Green Deploymentの詳細については、[こちら](https://docs.aws.amazon.com/ja_jp/codedeploy/latest/userguide/welcome.html#welcome-deployment-overview-blue-green)をご参照ください。

### 前提事項

`cd_pipeline_backend pattern`は、以下の前提事項のもとで利用されることを想定しています。

- アプリケーションがAWS Fargate上のECSで稼働すること
  - アプリケーションは、ECSサービスを利用した常駐形(Webアプリ、APIサーバー等)であること
- バックエンドシステムを構成するアプリケーションはコンテナ化され、そのイメージがECRに保存されていること
- Delivery環境のAmazon S3バケットにデプロイ用途で利用する以下の2つのファイルがzip形式で格納されていること
  - ECSの[タスク定義](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/developerguide/task_definitions.html)
  - CodePipelineおよびCodeDeployが参照する[アプリケーション仕様ファイル](https://docs.aws.amazon.com/ja_jp/codedeploy/latest/userguide/application-specification-files.html)

----

:information_source:

[`cd_pipeline_backend_trigger pattern`](./cd_pipeline_backend_trigger.md)では、デプロイ用のイベントを発火させるために、
CloudTrailのデータイベントログ記録機能に依拠しています。
一方で、本patternについては当該機能は有効化せずとも動作します。コンプライアンスや課金の観点を鑑み、有効・無効をご判断ください。

----

### 環境ごとのデプロイ戦略

本モジュールでは、デプロイを開始するトリガとして以下の2つを想定しています。

1. [`cd_pipeline_backend_trigger pattern`](./cd_pipeline_backend_trigger.md)から連携されるデプロイ開始イベントの伝搬
2. 手動でのデプロイ開始

トリガは異なるものの、デプロイ処理のシーケンスはほぼ同じです。下図は、前者の`cd_pipeline_backend_trigger pattern`を組み合わせた場合のデプロイ処理のアーキテクチャおよびシーケンスを示したものです。

![アーキテクチャ](../../resources/cd_pipeline_backend.png)

このケースでは、イベントバス経由でDelivery環境からデプロイ開始イベントが伝搬することでデプロイが開始されます。

デプロイ処理はCodePipelineが制御し、大きく分けて以下のシーケンスで行われます。

- CodePipeline: Delivery環境のECRからのコンテナイメージの読み取り
- CodePipeline: Delivery環境のAmazon S3バケット上のデプロイ設定ファイルの読み取り
- CodePipeline: (オプション)承認権限者へのデプロイ承認要求
- CodeDeploy: ECSへのアプリケーションのデプロイ

2種類のトリガを想定しているのは、デプロイ対象のRuntime環境によってデプロイフローが異なると考えているためです。

例えばサービスチームのみが利用する開発環境に対しては、修正したアプリケーションを迅速にデプロイし動作確認をすべきです。
これを実現するためにはソース修正からコンテナイメージのビルドとプッシュ、そしてデプロイという一連の流れをシームレスに連携させる方が望ましいでしょう。
これは、本patternと`cd_pipeline_backend_trigger pattern`と連携させることで実現できます。
また、この場合には承認権限者へのデプロイ承認を不要とすると良いでしょう。

開発環境へのデプロイを承認不要とする場合は、以下のように`deployment_require_approval`を`false`（承認不要）に設定します。

`runtimes/staging/cd_pipeline_backend/main.tf`

```terraform
module "cd_pipeline_backend" {

  ...

  deployment_require_approval = false  # 承認不要
}
```

一方でProduction環境においては、デプロイタイミングもサービス関係者の協議の元で決定されるため、デプロイはマニュアル操作で開始する方が多いであろうと想定しています。

Production環境へのデプロイには承認必須とする場合は、以下のように`deployment_require_approval`を`true`（承認必須）に設定します。

`runtimes/production/cd_pipeline_backend/main.tf`

```terraform
module "cd_pipeline_backend" {

  ...

  deployment_require_approval = true  # 承認必須
}
```

承認必須とした場合は、CodePipeline上に構成されるデプロイメントパイプラインを手動で実行してください。
例えば、CodePipelineのマネジメントコンソール上で「変更をリリースする」ボタンを押下することで、パイプラインを起動できます。

![パイプラインの起動](../../resources/cd_pipeline_backend_kick_pipeline.png)

### ブランチ戦略とイメージタグ

Eponaでは、ブランチ戦略に[GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)を利用することを想定しています。したがって、デプロイ先となるRuntime環境ごとにリリース用のbranchが存在します。

これらの内容については、[ci_pipeline pattern](./ci_pipeline.md#runtime環境とブランチ戦略)を参照してください。

## サンプルコード

`cd_pipeline_backend pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "cd_pipeline_backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend?ref=v0.2.6"
  
  delivery_account_id = "[デリバリ環境のAWSアカウントID]"
  ecr_repositories = {
    myrepo = {
      tag            = "staging" # リリース対象タグ
      artifact_name  = "myartifact"
      container_name = "IMAGE1_NAME"
    }
  }

  # Delivery環境のECRにCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_backend_trigger の output として出力される
  cross_account_codepipeline_access_role_arn = "arn:aws:iam::[デリバリ環境のAWSアカウントID]:role/[ロール名]"
  # cd_pipeline_backend_trigger の bucket_name と同じ名前を指定する
  source_bucket_name = "my-backend-pipeline-source"

  cluster_name = data.terraform_remote_state.staging_public_traffic_container_service.outputs.public_traffic_container_service.ecs_cluster_name
  service_name = data.terraform_remote_state.staging_public_traffic_container_service.outputs.public_traffic_container_service.ecs_service_name

  pipeline_name                       = "my-backend-cd-pipeline"
  artifact_store_bucket_name          = "my-artifact"
  artifact_store_bucket_force_destroy = false

  prod_listener_arns = [data.terraform_remote_state.staging_public_traffic_container_service.outputs.public_traffic_container_service.load_balancer_prod_listener_arn]
  target_group_names = data.terraform_remote_state.staging_public_traffic_container_service.outputs.public_traffic_container_service.load_balancer_target_group_names

  codedeploy_app_name         = "my-app"
  deployment_group_name_ecs   = "my-deployment-group"
  deployment_require_approval = true
}
```

### Runtime環境からECRへのアクセス許可

Runtime環境上のECSはもちろん、`cd_pipeline_backend pattern`で構成されるCodePipelineも
Delivery環境のECRにアクセスします。
このため当該ECRの[リポジトリポリシー](https://docs.aws.amazon.com/ja_jp/AmazonECR/latest/userguide/repository-policies.html)で、Runtime環境からのアクセスを許可する必要があります。

このECRは[`ci_pipeline pattern`](./ci_pipeline.md)で構成されるため、
設定方法は[`ci_pipeline pattern`のガイド](./ci_pipeline.md)をご参照ください。

## 関連するpattern

`cd_backend_pipeline pattern`に関連するpatternを、以下に記載します。

| pattern名                                                               | 説明                                                                                                                    |
| :---------------------------------------------------------------------- | :---------------------------------------------------------------------------------------------------------------------- |
| [cd_pipeline_backend_trigger pattern](./cd_pipeline_backend_trigger.md) | デプロイのトリガとなるイベントを生成します。本patternの適用後に得られる情報を使用して、リソースを更新する必要があります |
| [public_traffic_container_service pattern](./public_traffic_container_service.md) | `public_traffic_container_service pattern`によって初回デプロイされるコンテナはダミーです。本patternが構築するAWS CodeDeployのデプロイによって、本来のコンテナに置き換えられます |

## 入出力リファレンス

${TF_DOC}
