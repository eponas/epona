# redis pattern

## 概要

`redis pattern`モジュールでは、インメモリデータストアであるRedisを構築します。

構築したRedisは、例えばキャッシュやWebアプリケーションのセッション等で利用できます。

## 想定する適用対象環境

`redis pattern`は、Runtime環境での使用を想定しています。

## 依存するpattern

`redis pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                                     | 利用する情報           |
| :-------------------------------------------- | :--------------------- |
| [network pattern](./network.md)               | プライベートサブネット |
| [encryption_key pattern](./encryption_key.md) | CMKのエイリアス        |

本patternが依存するリソースを他の構築手段で代替する場合は、依存するpatternと[入出力リファレンス](#入出力リファレンス)の内容を参考に、本patternが必要とするリソースを構築してください。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                   | 説明                               |
| :--------------------------------------------------------------------------- | :--------------------------------- |
| [Amazon ElastiCache for Redis](https://aws.amazon.com/jp/elasticache/redis/) | インメモリデータストアを構築します |

## モジュールの理解に向けて

`redis pattern`では、Amazon ElastiCache for Redisを構築するためのTerraformモジュールを提供します。

Redisは、インメモリのデータストアです。キャッシュやWebアプリケーションのセッション保存などのユースケースで、多く用いられます。

Amazon ElastiCache for Redisは、以下の3つのいずれかの構成をとります。

* 単一ノードRedisクラスター
* 単一シャードであり、レプリケーションをサポートするRedisクラスター（クラスターモードが無効）
* シャードが1〜90であり、レプリケーションをサポートするRedisクラスター（クラスターモードが有効）

各構成の詳細については、次のドキュメントを参照してください。

[Redis 用 ElastiCache の用語](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/WhatIs.Terms.html)

いずれの構成も、Amazon ElastiCache for Redisでは「Redisクラスター」と呼ばれます。

どの構成を選択するかは、Redisに求める可用性、使用するデータ量に依存します。

> マルチAZに関する注意事項
>
> 現時点のEponaでは、自動フェイルオーバー構成のみ有効にできます。  
> もしもマルチAZ構成が必要となる場合は、Terraform外（AWS CLIやマネジメントコンソール）での変更作業が必要になります。

## 可用性とスケーラビリティの向上

単一ノードのRedisクラスターの場合、ノードが停止するとデータが失われます。これを避ける場合は、レプリケーション（クラスターモード無効）を行い他のノードにデータをコピーすることで、可用性を高めることができます。

[レプリケーショングループを使用する高可用性](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/Replication.html)

また、単一ノードのRedisクラスターの障害を軽減するには、以下のドキュメントを参照してください。

[ノードの障害の軽減](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/FaultTolerance.html#FaultTolerance.Redis.Cluster)

### クラスターモードが無効な場合

レプリケーション（クラスターモード無効）を使用していてレプリカノードを持つ場合は、プライマリノードの更新がレプリカノードに伝播されています。  
このため、プライマリノードに障害があった場合の復旧にレプリカを活用できます。

<!-- マルチAZが有効にできれば、直後の文章と入れ替え --

ただし、マルチAZの有効、無効で書き込み可能になる（プライマリノードが復帰する）までのフローが異なります。

[障害の軽減 Redisレプリケーション・グループ](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/FaultTolerance.html#FaultTolerance.Redis.Cluster.Replication)

マルチAZを有効にすると障害からの復帰は早くなりますが、マルチAZとすることによるコスト等の増加がトレードオフとなります。
-->

<!-- 入れ替え対象の文書 --ここから -->

プライマリノードが復帰するまでのフローについては、以下のドキュメントを参照してください。

[障害の軽減 Redisレプリケーション・グループ](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/FaultTolerance.html#FaultTolerance.Redis.Cluster.Replication)

<!-- 入れ替え対象の文書 --ここまで -->

また、読み込み操作に関してはレプリカを活用した負荷分散も可能です。  
レプリケーション（クラスターモード無効）では読み込みの負荷分散を行うことができますが、書き込みに対するオペレーションおよび保持できるデータ量は単一ノードに制限されます。

### クラスターモードが有効な場合

書き込み操作の負荷分散や、単一ノードあたりのデータ量を分散する必要がある場合は、レプリケーション（クラスターモード有効）の利用を検討します。

[レプリケーション: Redis (クラスターモードが無効) と Redis (クラスターモードが有効) の比較](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/Replication.Redis-RedisCluster.html)

<!-- マルチAZが有効にできれば、直後の文章と入れ替え --

:information_source: クラスターモードを有効にする場合、マルチAZが必須となります。

クラスターモードを有効にすることで、Redisクラスターの可用性およびスケーラビリティは高まります。  
ただし、それに合わせて使用するノードやAZも増えていくことになるためコストが増大します。
-->

<!-- 入れ替え対象の文書 --ここから -->

クラスターモードを有効にすることで、Redisクラスターの可用性およびスケーラビリティは高まります。  
ただし、それに合わせて使用するノードも増えていくことになるためコストが増大します。

<!-- 入れ替え対象の文書 --ここまで -->

要件に合わせて、適切なクラスター構成、インスタンスサイズを選択してください。

### エンドポイントを把握する

構築されたAmazon ElastiCache for Redisクラスターに接続するためには、エンドポイントに関する情報が必要になります。このエンドポイントの情報は、Redisクラスターの構築時に決まります。

またRedisクラスターのエンドポイントには複数の種類があり、どのエンドポイントを利用するのかはRedisクラスターの構成ごとに異なります。以下に、Redisクラスターの構成と、利用するエンドポイントの対応を記載します。

| 構成                                    | 書き込みエンドポイント | 読み込みエンドポイント |
| :-------------------------------------- | :--------------------- | :--------------------- |
| 単一ノードRedisクラスター               | ノードのエンドポイント | ノードのエンドポイント | ト |
| Redisクラスター（クラスターモード無効） | PrimaryEndpoint        | ReaderEndpoint         |
| Redisクラスター（クラスターモード有効） | ConfigurationEndpoint  | ConfigurationEndPoint  |

`redis pattern`の適用結果として取得できるのは、PrimaryEndpointとConfigurationEndpointの2つです。

:information_source: これは、Terraform AWS Providerの制限です。

その他のエンドポイントの取得方法に関しては、Amazon ElastiCache for Redisのドキュメントを参考にして取得してください。

[接続エンドポイントの検索](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/Endpoints.html)

取得したエンドポイントは、[parameter_store pattern](./parameter_store.md)を使用した秘匿情報として格納することになるでしょう。

### RedisをAUTHトークンで保護する

Redisクラスターは、デフォルトでは認証なしでアクセスできてしまいます。

`redis pattern`ではセキュリティを高めるため、AUTHトークンを設定で求めるように実装しており、構築されたRedisクラスターへのアクセスにはAUTHトークンが必要になります。

[Redis AUTH コマンドによるユーザーの認証](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/auth.html)

:information_source: AUTHトークンを設定するには、[Redis 用 ElastiCache 伝送中の暗号化 (TLS)](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/in-transit-encryption.html)の有効化が要件となります。

:information_source: AUTHトークンを設定したRedisクラスターへの接続へは、SSL/TLS通信が要求されることに注意してください。

また、構築の際にAUTHトークンを`redis pattern`に指定するのですが、`.tf`ファイルに記載すると秘匿情報が平文でハードコードされることになります。

この状態はセキュリティの面からは好ましくないので、`redis pattern`適用後にAWS CLIでAUTHトークンを更新することを推奨します。

[Redis AUTH コマンドによるユーザーの認証 / AUTH トークンの更新](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/auth.html#auth-modifyng-token)

Redisクラスター構築後、AUTHトークンを変更して、再設定したAUTHトークンを秘匿情報として管理してください。

:warning: `redis pattern`では、引数として指定するAUTHトークンは初期値として扱い、更新はサポートしていません。

:warning: `.tf`ファイル上のAUTHトークンを更新して再度`terraform apply`しても、その差分は無視されます。

### モニタリング

Amazon ElastiCache for Redisでは、Amazon CloudWatchメトリクスを使用したモニタリングが可能です。

[CloudWatch メトリクスを使用したモニタリング](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/CacheMetrics.html)

収集したメトリクスを使い、モニタリングおよび監視を行えます。

さらにDatadogが使用可能でありAWSとのインテグレーションを実施している場合は、Amazon CloudWatchメトリクスもDatadogで確認できます。

### イベント通知

Amazon ElastiCache for Redisクラスター上で発生する重要なイベントは、Amazon SNSにより通知されます。

[ElastiCache イベントのモニタリング](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/ECEvents.html)

この通知をモニタリングすることで、クラスターの状態等を知ることができます。

:information_source: 現時点のEponaでは、通知先のAmazon SNSを指定することのみをサポートしています。

### メンテナンス

Amazon ElastiCache for Redisでは、Redisクラスターのメンテナンスが実施されます。メンテナンスでは、変更の適用が実施されます。

* [メンテナンスの管理](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/maintenance-window.html)
* [Amazon ElastiCache 管理メンテナンスとサービス更新のヘルプページ](https://aws.amazon.com/jp/elasticache/elasticache-maintenance/)

`redis pattern`では、メンテナンスウィンドウの指定を必須としています。

* `maintenance_window`　：　メンテナンスウィンドウ（`ddd:hh24:mi-ddd:hh24:mi`：24時間表記、UTC指定）

サービスの内容に応じてメンテナンスウィンドウと変更の適用タイミングの方針を決定し、モジュールの設定に反映してください。

:information_source: 変更を即座に反映するか、メンテナンスウィンドウで反映するかは`apply_immediately`で制御します。

### バックアップ

Amazon ElastiCache for Redisでは、自動スナップショットを取得することでバックアップを実現できます。

[Redis 用 ElastiCache でのバックアップと復元](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/backups.html)

`redis pattern`では、以下の変数を設定することでRedisクラスターの自動スナップショット取得を有効とします。

* `snapshot_retention_limit`　：　バックアップ保管世代(最大35)
* `snapshot_window` ：　バックアップウィンドウ(HH:MM-HH:MM表記)

これらは必須項目としています。自動スナップショットを無効にしたい場合は両方に`null`を指定、または`snapshot_retention_limit`に`0`を指定してください。

:warning:  
自動スナップショットには保持期間があり、期限が過ぎたスナップショットは削除されます。  
`redis pattern`モジュールを使用して、自動で取得したスナップショットから[リストア](#リストア)を行う場合、スナップショットをコピーして使用することを**強く推奨します**。

ただし、現在の`redis pattern`では、スナップショットからのリストアのみがサポートされています。  
スナップショットのコピーは、AWS CLIで行う必要があります。

詳細は、[リストア](#リストア)を参照してください。

### リストア

`redis pattern`ではスナップショットからのリストアが可能です。リストアは、Redisクラスターを再作成することで行います。

:warning:  
Amazon ElastiCache for Redisでは、スナップショットおよびAmazon S3からのリストアが可能です。  
ただし、現在の`redis pattern`ではスナップショットからのリストアのみがサポートされています。

リストア手順は以下のとおりです。

1. Redisクラスターをデプロイした`terraform`コードに、`snapshot_name`を追加指定します。変数の値は、リストア先のスナップショット名を入力します

1. `terraform`を実行します

Redisクラスターが新規作成されることになるため、Redisのエンドポイント（DNS名）が変更される点にご注意ください。
必要に応じ、Redisを利用するアプリケーションのRedisへの接続設定を修正してください。

アプリケーションがDNSの参照結果をキャッシュする場合は、TTLを適切に設定してください。  
Javaの場合の参考情報は、[database pattern](./database.md#リストア)にも記載しています。

---

:warning: リストア後の注意点について。

リストアを行った後は、別のスナップショットからリストアする場合を除いて`snapshot_name`に指定した値を変更しないでください。  
`snapshot_name`を指定してリストア後、この値を変更してTerraformを実行した場合、以下の挙動になります。

* `snapshot_name`の値を削除 → Redisクラスターの再作成（データがない状態で再作成）
* `snapshot_name`に別のスナップショットを指定 → 指定のスナップショットからリストア

また`snapshot_name`に指定したスナップショットが存在しない場合、 **Redisクラスターが削除されリストアは失敗します**。  
スナップショットが期限切れなどで削除されたりすると、その後のTerraform実行時に意図せぬRedisクラスター削除となる恐れがあるため注意してください。

:information_source: リストア後に`snapshot_name`を変更せずにTerraformを実行した場合、リストアされるのは初回だけです。

このため自動バックアップで取得したスナップショットは直接使わずに、事前に手動でコピーして使用してください。

ただし、現在の`redis pattern`では、スナップショットからのリストアのみがサポートされています。  
スナップショットのコピーは、AWS CLIで行う必要があります。

```shell
$ aws elasticache copy-snapshot \
    --source-snapshot-name [コピー元スナップショット] \
    --target-snapshot-name [コピー先スナップショット]
```

[バックアップ (AWS CLI) のコピー](https://docs.aws.amazon.com/ja_jp/AmazonElastiCache/latest/red-ug/backups-copying.html#backups-copying-CLI)

---

## サンプルコード

`redis pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "redis" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/redis?ref=v0.2.6"

  replication_group_id          = "example-redis-cluster"
  replication_group_description = "Example Redis Cluster"

  vpc_id = data.terraform_remote_state.production_network.outputs.network.vpc_id

  cluster_mode_enabled = false

  number_cache_clusters = 1

  auth_token = "[your-redis-auth-token]"  # AUTHトークン

  kms_key_id = data.terraform_remote_state.production_encryption_key.outputs.encryption_key.keys["alias/my-encryption-key"].key_arn

  engine_version             = "5.0.6"
  node_type                  = "cache.t3.medium"
  automatic_failover_enabled = false
  
  maintenance_window         = "wed:16:00-wed:17:00"

  apply_immediately = true

  family = "redis5.0"

  subnets = data.terraform_remote_state.production_network.outputs.network.private_subnets

  snapshot_retention_limit = 35
  snapshot_window          = "18:00-19:00"

  # parameter groupの設定
  parameters = [
    {
      name  = ...
      value = ...
    },
    {
      name  = ...
      value = ...
    }
  ]

  tags = {
    # 任意のタグ
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }
}
```

## 関連するpattern

`redis pattern`に関連するpatternを、以下に記載します。

| pattern名                                                                           | 説明                                                                                                                  |
| :---------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------------------------------------- |
| [`parameter_store pattern`](./parameter_store.md)                                   | このpatternを使用して構築されたAmazon ElastiCache for Redisのエンドポイントや認証トークンを、秘匿情報として格納します |
| [`public_traffic_container_service pattern`](./public_traffic_container_service.md) | コンテナ環境で動作するアプリケーションから、インメモリデータストアとしてAmazon ElastiCache for Redisを利用します      |

## 入出力リファレンス

${TF_DOC}
