# cd_pipeline_frontend pattern

## 概要

`cd_pipeline_frontend pattern`は、Runtime環境上のAmazon S3へフロントエンドシステムをデプロイするためのpatternです。

## 想定する適用対象環境

`cd_pipeline_frontend pattern`は、Runtime環境で使用することを想定しています。

## 依存するpattern

`cd_pipeline_frontend pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名 | 利用する情報 |
|:----------|:-----------------|
| [cd_pipeline_frontend_trigger pattern](./cd_pipeline_frontend_trigger.md) | デプロイのトリガとなるイベント、Delievry環境のデプロイ元であるAmazon S3バケットへアクセスするためのロール |
| [cacheable_frontend pattern](./cacheable_frontend.md) | デプロイ先となるAmazon S3バケット |

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名 | 説明 |
| :--- | :--- |
| [AWS CodePipeline (CodePipeline)](https://aws.amazon.com/jp/codepipeline/)| デプロイメントパイプラインを構成します |
| [AWS CodeBuild (CodeBuild)](https://aws.amazon.com/jp/codebuild/)| S3バケットへデプロイするCodeBuildを構成します |
| [Amazon EventBridge](https://aws.amazon.com/jp/eventbridge/) | 異なるAWSアカウント間でイベントを伝搬させるための経路(イベントバス)を構築します |
| [Amazon CloudWatch Events](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/events/WhatIsCloudWatchEvents.html) | デプロイ実行のトリガとなるイベントを受信するためのイベントを構成します |
| [AWS Key Management Service (KMS)](https://aws.amazon.com/jp/kms/) | CodePipelineが利用するArtifact Store(S3バケット)を暗号化するための鍵を構築します |

## モジュールの理解に向けて

本モジュールはDelivery環境にアップロードされたフロントエンドシステムの資材を、Runtime環境にデプロイするためのモジュールです。  
デプロイされたコンテンツは、[cacheable_frontend pattern](./cacheable_frontend.md)で構築されるAmazon CloudFront(以降、CloudFront)を通して配信されることを想定しています。

### 前提事項

`cd_pipeline_frontend pattern`は、以下の前提事項のもとで利用されることを想定しています。

- アプリケーションが静的コンテンツであること
  - アプリケーションが静的なWebページ群、もしくはSPA(Single-Page Application)であること
- Delivery環境のAmazon S3(以降、S3バケット)にデプロイ用途で利用するアプリケーション（依存ライブラリを含む）がzip形式で格納されること

----

:information_source:

[`cd_pipeline_frontend_trigger pattern`](./cd_pipeline_frontend_trigger.md)では、デプロイ用のイベントを発火させるために、
CloudTrailのデータイベントログ記録機能に依拠しています。
一方で、本patternについては当該機能は有効化せずとも動作します。コンプライアンスや課金の観点を鑑み、有効・無効をご判断ください。

----

### 環境毎のデプロイ戦略

本モジュールでは、デプロイを開始するトリガとして以下の2つを想定しています。

1. [`cd_pipeline_frontend_trigger pattern`](./cd_pipeline_frontend_trigger.md)から連携されるデプロイ開始イベントの伝搬
2. 手動でのデプロイ開始

トリガは異なるものの、デプロイ処理のシーケンスはほぼ同じです。
下図は、前者の`cd_pipeline_frontend_trigger pattern`を組み合わせた場合のデプロイ処理のアーキテクチャおよびシーケンスを示したものです。

![アーキテクチャ](../../resources/cd_pipeline_frontend_overview.png)

図のケースでは、イベントバス経由でDelivery環境からデプロイ開始イベントが伝搬することでデプロイが開始されます。  
デプロイ処理はCodePipelineが制御し、大きく分けて以下のシーケンスで行われます。

- CodePipeline: Delivery環境のS3バケット上のzip圧縮されたアプリケーションの読み取り
- CodePipeline: (オプション)承認権限者へのデプロイ承認要求
- CodeBuild: S3バケットへのデプロイ
- CodeBuild: (オプション) CloudFrontのキャッシュ更新

2種類のトリガを想定しているのは、デプロイ対象のRuntime環境によってデプロイフローが異なると考えているためです。

例えばサービスチームのみが利用する開発環境に対しては、修正したアプリケーションを迅速にデプロイし動作確認をすべきです。
これを実現するためにはソース修正からアプリケーションのビルドとプッシュ、そしてデプロイという一連の流れをシームレスに連携させる方が望ましいでしょう。
これは、本patternと`cd_pipeline_frontend_trigger pattern`と連携させることで実現できます。
また、この場合には承認権限者へのデプロイ承認を不要とすると良いでしょう。

開発環境へのデプロイを承認不要とする場合は、以下の`deployment_require_approval`を`false`（承認不要）に設定します。

`runtimes/staging/cd_pipeline_frontend/main.tf`

```terraform
module "cd_pipeline_frontend" {

  ...

  deployment_require_approval = false  # 承認不要
}
```

一方でProduction環境においては、デプロイタイミングもサービス関係者の協議の元で決定されるため、デプロイはマニュアル操作で開始する方が多いであろうと想定しています。

Production環境へのデプロイには承認必須とする場合は、以下のように`deployment_require_approval`を`true`（承認必須）に設定します。

`runtimes/production/cd_pipeline_frontend/main.tf`

```terraform
module "cd_pipeline_frontend" {

  ...

  deployment_require_approval = true  # 承認必須
}
```

承認必須とした場合は、CodePipeline上に構成されるデプロイメントパイプラインを手動で実行してください。
例えば、CodePipelineのマネジメントコンソール上で「変更をリリースする」ボタンを押下することで、パイプラインを起動できます。

![パイプラインの起動](../../resources/cd_pipeline_backend_kick_pipeline.png)

### CloudFrontのキャッシュ制御

本パターンを適用してデプロイされたフロントエンドシステムは、S3バケットからCloudFrontを介してユーザーに配信されることを想定しています。  
CloudFrontを利用することで、リクエストに対して遅延の少ないエッジロケーションのキャッシュからコンテンツが配信されます。

CloudFrontにおけるキャッシュ制御は、[cacheable_frontend pattern](./cacheable_frontend.md)で設定可能です。

しかし、コンテンツに変更があった場合、キャッシュ上の旧コンテンツではなく、新コンテンツを速やかに配信したいケースもあります。

そのため、デプロイ後にCloudFrontのキャッシュされたファイルを無効化し、デプロイ毎に最新のコンテンツを配信するオプションを用意しています。
オプションを有効にするためには、本パターン内で以下のように`cache_invalidation_config`を設定してください。

```terraform
module "cd_pipeline_frontend" {

  ...

  
  cache_invalidation_config = {
    enable                     = true              # デプロイ後にキャッシュを無効化する
    cloudfront_distribution_id = "XXXXXXXXXXXXXX"  # 配信するためのCloudFrontのDistribution IDを指定する
  }
}
```

:information_source:
キャッシュの無効化の詳細については、以下のドキュメントを参照してください。  
[ファイルの無効化 - Amazon CloudFront](https://docs.aws.amazon.com/ja_jp/AmazonCloudFront/latest/DeveloperGuide/Invalidation.html)

:warning:  
本パターンで提供しているキャッシュの無効化はCloudFrontのキャッシュ「無効リクエスト」を利用しています。  
当該キャッシュ「無効リクエスト」は課金対象となっているため、本オプションを有効にするとデプロイ毎にコストが発生します。  
[cacheable_frontend pattern](./cacheable_frontend.md)でのキャッシュ制御も検討してください。  
`cloudfront_default_cache_behavior`内の`default_ttl`等の値でキャッシュが制御できます。  
（詳しくは、[CloudFrontの開発者ガイド](https://docs.aws.amazon.com/ja_jp/AmazonCloudFront/latest/DeveloperGuide/distribution-web-values-specify.html#DownloadDistValuesMinTTL)を参照してください）

## サンプルコード

`cd_pipeline_frontend pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "cd_pipeline_frontend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_frontend?ref=v0.2.6"

  delivery_account_id = "[Delivery環境のアカウントID]"

  pipeline_name = "my-frontend-cd-pipeline"

  artifact_store_bucket_name = "my-frontend-artifacts"

  # Delivery環境のS3にCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_frontend_trigger の output として出力される
  cross_account_codepipeline_access_role_arn = "arn:aws:iam::[Delivery環境のAWSアカウントID]:role/[ロール名]"

  # ci_pipeline の static_resource_buckets と同じ名前を指定する
  source_bucket_name = "my-frontend-source-bucket"
  source_object_key  = "source.zip"

  deployment_bucket_name = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.frontend_bucket_name
  deployment_object_path = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.frontend_bucket_origin_path

  cache_invalidation_config = {
    enable                     = true
    cloudfront_distribution_id = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.cloudfront_id
  }

  deployment_require_approval = true
}
```

## 関連するpattern

`cd_pipeline_frontend pattern`に関連するpatternを、以下に記載します。

| pattern名 | 説明 |
| :-------- | :--- |
| [cd_pipeline_frontend_trigger pattern](./cd_pipeline_frontend_trigger.md) | デプロイのトリガとなるイベントやDelievry環境のリソースアクセスに必要なロールなどを構築します。 |
| [cacheable_frontend pattern](./cacheable_frontend.md) | アプリケーションをデプロイするためのAmazon S3バケットと、コンテンツの配信環境を構築します。 |

## 入出力リファレンス

${TF_DOC}
