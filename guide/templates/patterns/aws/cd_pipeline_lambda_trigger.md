# cd_pipeline_lambda_trigger pattern

## 概要

`cd_pipeline_lambda_trigger pattern`モジュールでは、Lambda関数をRuntime環境にデプロイするためのトリガとなるリソースを構築します。

本パターンと`cd_pipeline_lambda pattern`とを連携させることで、Runtime環境に対してLambda関数を継続的にデプロイする環境が構築されます。

本パターンで構築される構成は以下のようになっています。

![アーキテクチャ](../../resources/cd_pipeline_lambda_trigger.png)

## 想定する適用対象環境

`cd_pipeline_lambda_trigger pattern`は、Delivery環境で使用することを想定しています。

## 依存するpattern

`cd_pipeline_lambda_trigger pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                               | 利用する情報                                                                  |
| :-------------------------------------- | :---------------------------------------------------------------------------- |
| [ci_pipeline pattern](./ci_pipeline.md) | Lambda関数のソース(zip)およびデプロイのための設定ファイルが圧縮されたzipファイルを格納するS3バケット情報 |
| [audit pattern](./audit.md) | S3バケットへのオブジェクト配置のイベント検知 |

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                               | 説明                                                                            |
| :----------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------ |
| [Amazon CloudWatch Events](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/events/WhatIsCloudWatchEvents.html) | デプロイ実行のトリガとなるイベントを発生させるルールを定義します                |
| [Amazon EventBridge](https://aws.amazon.com/jp/eventbridge/)                                                             | 異なるAWSアカウント間でイベントを伝搬させるための経路(イベントバス)を構築します |

## モジュールの理解に向けて

### `cd_pipeline_lambda_trigger pattern`が実現するフロー

本パターンでは`cd_pipeline_lambda pattern`で構築されるAWS CodePipelineに対して、デプロイをトリガするためのイベント[^1]を構築します。

[^1]: Amazon CloudWatch Events

デプロイに利用されるイベントとしては、以下のようなイベントがあります。

- Delivery環境のLambda関数デプロイのための資材を格納するS3バケットへ対象のオブジェクトをアップロード

:warning: Lambda関数デプロイのための資材は、zipファイルとしてまとめ、S3バケットにアップロードしてください。

このイベントは、Amazon CloudWatchのEventBusを通じてRuntimeアカウントに送信され、Runtime環境で定義されたデプロイのためのパイプライン実行をトリガします。

----

:information_source:

上述のデプロイに利用されるイベントを捕捉する際、CloudTrailの
[S3のデータイベントログ記録](https://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/logging-data-events-with-cloudtrail.html)機能を利用しています。

`audit pattern`を用いてCloudTrailを構成している場合、当該機能を有効化する方法については[audit patternのドキュメント](./audit.md)を参照してください。
Eponaを用いずにCloudTrailを構成している場合は、[証跡データイベントのログ記録](https://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/logging-data-events-with-cloudtrail.html)を
ご参照ください。

----

### デリバリシステムとの分離

上図の通りEponaでは、デリバリシステムをRuntime環境に配置し、S3バケットを配置するDelivery環境とは環境が分離されています。  
この理由は、以下の2点です。

1. 実質的なデプロイ処理[^2]をRuntime環境で完結できる
1. テスト済の同一のLambda関数を全てのRuntime環境上にデプロイすることを保証できる

[^2]: CodePipelineによるデプロイ処理を指しています

アプリケーションのデプロイはエンドユーザーにも大きな影響を及ぼし得るため、適切な権限管理の元で実行する必要があります。  
実質的なデプロイ処理をRuntime環境で完結させることができれば権限管理をシンプルにでき、意図しない、あるいは不適切なデプロイを防止できます。  
このためデプロイ処理を担うCodePipelineをS3バケットと同じDelivery環境ではなく、Runtime環境に配置するアーキテクチャとしました。

一方でデプロイされるLambda関数については、テスト済のファイルを各Runtine環境にデプロイできることが安全です。  
このため、Lambda関数のソースファイルやデプロイのための設定ファイルを格納するS3バケットはDelivery環境に配置し、バケットでファイルを一元管理するようにしています。

### よりスムーズなデプロイメントパイプラインの実現に向けて

本パターンの前提である`ci_pipeline pattern`では、CIパイプラインの他にデプロイ対象のファイルを格納するS3バケットを構築しています。
本パターンを適用することで、上記のS3バケットに格納されたLambda関数を、Runtime環境へデプロイする処理が開始されるようになります。

`ci_pipeline pattern`で構築されるCIパイプラインに対して以下の仕組みを組み込めば、ソース・設定ファイルを修正するだけでRuntime環境にデプロイできる仕組みを構築できます。CodeBuild、CodeDeployで使用する設定ファイルについては[cd_pipeline_lambda_pattern](./cd_pipeline_lambda.md)で説明しています。

- ソースが修正されたら、実行に必要となる資材とともにzip化して、S3バケットにアップロードする

これらの仕組みは [`ci_pipeline pattern`](./ci_pipeline.md) で記載の通り、`.gitlab-ci.yml` に記述できます。

## サンプルコード

`cd_pipeline_lambda_trigger pattern`を使用したサンプルコードを、以下に記載します。

```terraform
locals {
  runtime_account_id = "[Runtime環境のアカウントID]"
}

data "aws_region" "current" {}

module "cd_pipeline_lambda_trigger" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_lambda_trigger?ref=v0.2.6"

  pipeline_name      = "my-lambda-cd-pipeline"
  source_bucket_arn  = "arn:aws:s3:::lambda-static-resource"
  runtime_account_id = local.runtime_account_id

  # Runtime環境に構築するアーティファクトストアのバケット名
  artifact_store_bucket_arn = "arn:aws:s3:::yoshiki-artifacts"
  target_event_bus_arn      = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  # 以下は Runtime環境上で cd_pipeline_lambda pattern を動かしてから設定する
  artifact_store_bucket_encryption_key_arn = "arn:aws:kms:${data.aws_region.current.name}:${local.runtime_account_id}:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
```

## 関連するpattern

`cd_pipeline_lambda_trigger pattern`に関連するpatternを、以下に記載します。

| pattern名                                             | 説明                                                         |
| :---------------------------------------------------- | :----------------------------------------------------------- |
| [ci_pipeline pattern](./ci_pipeline.md)               | Lambda関数をデプロイするための資材をまとめたzipファイルを格納するS3バケットを生成します。 |
| [cd_pipeline_lambda pattern](./cd_pipeline_lambda.md) | 自動デプロイをするためのパイプラインを構築します。           |

## 入出力リファレンス

${TF_DOC}
