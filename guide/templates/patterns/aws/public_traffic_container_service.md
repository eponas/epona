# public_traffic_container_service pattern

## 概要

`public_traffic_container_service pattern`モジュールでは、インターネットからのトラフィックを受ける想定のロードバランサーおよびコンテナサービス環境を構築します。また、サービスを公開するにあたってのSSL/TLS証明書やDNSレコードの設定も行います。

このpatternにより構築されるコンテナサービスでは、バックエンドのサーバーアプリケーションがコンテナとして動作します。このアプリケーションは、ロードバランサーを介してインターネットに公開され、利用者に対してサービスや機能を提供します。

## 想定する適用対象環境

`public_traffic_container_service pattern`は、Runtime環境での使用を想定しています。

## 依存するpattern

`public_traffic_container_service pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                       | 利用する情報                                 |
| :------------------------------ | :------------------------------------------- |
| [network pattern](./network.md) | パブリックサブネット、プライベートサブネット |

またコンテナサービスで使用するコンテナイメージおよび動作するコンテナが、次のようなリソース、情報を必要とする場合は依存するpatternが追加されます。

* Amazon ECRに格納されたDockerイメージを使用する
* コンテナ内からデータストアを使用する
* 環境変数（秘匿情報）を参照する

| pattern名                                       | 利用する情報                                             |
| :---------------------------------------------- | :------------------------------------------------------- |
| [ci_pipeline pattern](./ci_pipeline.md)         | Delivery環境に構築されたAmazon ECR上のDockerイメージ     |
| [parameter_store pattern](./parameter_store.md) | AWS Systems Manager パラメータストアに格納された秘匿情報 |
| [database pattern](./database.md)               | RDBMS                                                    |
| [redis pattern](./redis.md)                     | Redis                                                    |

本patternが依存するリソースを他の構築手段で代替する場合は、依存するpatternと[入出力リファレンス](#入出力リファレンス)の内容を参考に、本patternが必要とするリソースを構築してください。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                         | 説明                                                                 |
| :----------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------- |
| [Elastic Load Balancing（Application Load Balancer）](https://aws.amazon.com/jp/elasticloadbalancing/)             | インターネットからのトラフィックを受けるロードバランサーを構築します |
| [AWS Fargate](https://aws.amazon.com/jp/fargate/)                                                                  | アプリケーションコンテナを動作させるサービスを構築します             |
| [AWS Certificate Manager](https://aws.amazon.com/jp/certificate-manager/)                                          | SSL/TLS証明書を作成します                                            |
| [Amazon Route 53](https://aws.amazon.com/jp/route53/)                                                              | DNSレコードを作成します                                              |
| [Amazon S3](https://aws.amazon.com/jp/s3/) | ALBのアクセスログ格納先                                                 |
 [Amazon CloudWatch Logs](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html) | コンテナログの格納先                                                 |

:warning: `public_traffic_container_service pattern`では、ドメインの取得は行いません。事前にドメインの取得が必要です。

## モジュールの理解に向けて

一般にサーバーアプリケーションをインターネットに公開する際には、以下のような環境を構築する必要があります。

* 負荷分散や可用性を考慮した、ロードバランサーや複数インスタンスで動作するサーバー環境
* インターネット公開に必要な、DNSレコードやSSL/TLS証明書の設定

`public_traffic_container_service pattern`では、これらを実現するリソースを構築します。

![public_traffic_container_serviceの全体像](./../../resources/public_traffic_container_service.png)

Eponaでは、アプリケーションがコンテナ化されていることを前提としています。  
`public_traffic_container_service pattern`を適用することにより、以下の環境が得られます。

* コンテナの稼働数を柔軟に増減（スケールイン・アウト）できる、AWS Fargateクラスター
  * コンテナを稼働させるインスタンスは、利用者は管理不要（AWS管理となる）
* インターネットからのトラフィックを受け付け、AWS Fargateクラスターへ転送するApplication Load Balancer

:information_source: Application Load Balancerは、AWS CodeDeployと連携することを想定しています。  
:information_source: 具体的には、Blue/Greenデプロイが行われることを想定し、ターゲットグループが2つ作成されます。  
:information_source: 詳しくは、[`cd_pipeline_backend pattern`](./cd_pipeline_backend.md)も参照してください。

また、今日のインターネットに公開されるWebサービス、Webサイトには固有のドメイン名でアクセスできることは当然として、HTTPSでのアクセスが求められるようになっている状況です。

このため、インターネット公開を前提として以下の環境を構成します。

* Amazon Route 53を使用した、サービス公開用のDNSレコードの取得
* サービス公開用のドメイン名に合わせた、AWS Certificate ManagerによるSSL/TLS証明書の取得

ここで作成するDNSレコードおよびSSL/TLS証明書は、同時に作成されるApplication Load Balancerへ紐付けられます。

### ドメイン取得について

`public_traffic_container_service pattern`では、ドメインの登録自体は範囲外となっています。

事前に、AWSのマネジメントコンソールで必要なドメインを取得してください。

[Amazon Route 53 を使用したドメイン名の登録](https://docs.aws.amazon.com/ja_jp/Route53/latest/DeveloperGuide/registrar.html)

ドメイン取得が`public_traffic_container_service pattern`の範囲外になっているのは、Terraformが対応していないことがひとつの理由です。加えて、ドメイン取得に関してはメール確認といった人手による作業が必要になるため、完全な自動化はできません。このため、Eponaの範囲外でドメインを取得する前提としています。

`public_traffic_container_service pattern`は、事前のドメイン取得およびドメイン取得に伴うホストゾーンの作成が完了した後に適用されることを想定しています。

:information_source: ホストゾーンの作成は、Amazon Route 53でドメインを取得した際に、自動的に作成されます。

`public_traffic_container_service pattern`では、作成済みのホストゾーンに対して、DNSレコードを作成します。

### ログの保存について

AWS Fargateのようなコンテナを管理するプラットフォーム上で動作するアプリケーションは、ログを標準出力として書き出すことで記録します。

ただ、AWS Fargateはコンテナインスタンスを実行している仮想マシンへは直接アクセスできないため、デフォルトではコンテナのログを確認できません。

このため、実際の利用にはログをクラスター外に保存することが必須となるでしょう。

`public_traffic_container_service pattern`では、ログはAmazon CloudWatch Logsへの保存を想定しています。

ロググループは`container_log_group_names`に、ロググループ名を指定して作成します。タスク内のコンテナ数分、ロググループを作成しましょう。

```terraform
  container_log_group_names             = ["ロググループ名1", "ロググループ名2", "ロググループ名3", ...]
```  

ここで指定したロググループ名は、タスクの`logConfiguration`に設定してコンテナのログを保存していきます。

Amazon CloudWatch Logsへのログ出力は、AWS FireLensを利用して行うことをおすすめします。詳しくは、以下のドキュメントを参照してください。

[Amazon ECS上のコンテナログを、AWS FireLensを使ってAmazon CloudWatch Logsに送信する](../../how_to/aws/ecs_log_send_to_cloudwatch_logs_using_firelens.md)

さらにDatadogが利用可能な場合は、ログを集約できます。詳しくは[こちら](#ログの集約)を参照してください。

### タスク定義で使用するコンテナについて

本patternで設定するタスク定義のコンテナには、実際のアプリケーションのものではなく、ダミーのものを設定するようにしてください。

本patternで構築されるFargate環境の更新は、AWS CodeDeployから行う前提となっています。
つまり、本patternに実際のアプリケーションのタスク定義を書いた場合、AWS CodeDeploy用のタスク定義と二重管理になってしまいます。
また、本patternは環境を構築するための最初の1回のみしか適用しないため、タスク定義の管理にコストをかけることは好ましくありません。

したがって、本patternでデプロイする初回のコンテナは、ダミーのコンテナを使用することを推奨します。

具体的なダミーのコンテナとしては、HashiCorp社が提供している[http-echo](https://hub.docker.com/r/hashicorp/http-echo/)などがあります。
次節のサンプルコードでは、`http-echo`をコンテナイメージに設定した例を記載しています。

なお、本pattern適用時にFargateへデプロイされるコンテナイメージこそダミーですが、コンテナ定義以外のリソースはすべて本来利用すべきものを構築します。
したがって、DNSレコードやクラスター名、ロググループの定義等は実際の利用で必要なものを作成してコンテナのみを差し替えます。

たとえば、ロググループを例にして説明します。
`cd_pipeline_backend pattern`によりデプロイするコンテナが以下の３つだとします。

* アプリケーションコンテナ
* Fluent Bitコンテナ
* Datadog Agentコンテナ

この場合、`public_traffic_container_service pattern`適用時にデプロイするコンテナと作成するロググループは、次のようになります。

* デプロイするコンテナ
  * `http-echo`のみ
* 作成するロググループ
  * アプリケーションコンテナ用ロググループ
  * Fluent Bitコンテナ用ロググループ
  * Datadog Agentコンテナ用ロググループ

### ロードバランサーのバックエンドのアクセス制御について

本パターンではALBのリスナーにルールを追加することで、特定のトラフィックのみバックエンドのコンテナサービスへ転送する等の柔軟なルーティングを実現できます。  
ルーティングに関する設定をしない場合は、ALBはすべてのトラフィックをバックエンドのコンテナサービスへと転送します。  
詳しくは
[ALBのリスナールール](https://docs.aws.amazon.com/ja_jp/elasticloadbalancing/latest/application/load-balancer-listeners.html#listener-rules)
を参照してください。

`public_traffic_container_service pattern`では以下のアクションタイプをサポートしています。

* 転送(forward)
* 固定レスポンス(fixed-response)
* リダイレクト(redirect)

リスナールール毎に、リクエストURLのパスパターンに基づいた条件を設定することでアクションを実行するかどうかを制御します。
ルールを複数設定した場合は、ルール毎に優先度を付けることでルールを評価する順番を制御できます。

#### 転送(forward)アクション

トラフィックをバックエンドのコンテナサービスへ転送します。
詳しくは
[転送アクション](https://docs.aws.amazon.com/ja_jp/elasticloadbalancing/latest/application/load-balancer-listeners.html#forward-actions)
を参照してください。  
転送先のターゲットグループは、デフォルトアクションの転送先をもとにEponaが自動的に設定します。  
:warning:
このため、本パターンでは、ALBリスナーのデフォルトアクションはECSへの転送アクションで固定としており、使用できません。

#### 固定レスポンス(fixed-response)アクション

HTTPステータスコードとオプションのメッセージを含む固定のHTTPレスポンスを返却します。
ALBがサポートしているレスポンスコードとコンテントタイプは以下になります。

* HTTPステータスコード
  * 2XX
  * 4XX
  * 5XX

* コンテントタイプ
  * `text/plain`
  * `text/css`
  * `text/html`
  * `application/javascript`
  * `application/json`

詳しくは
[固定レスポンスアクション](https://docs.aws.amazon.com/ja_jp/elasticloadbalancing/latest/application/load-balancer-listeners.html#fixed-response-actions)
を参照してください。

#### リダイレクト(redirect)アクション

インターネットからのトラフィックを別のURLにリダイレクトします。  
HTTPステータスコードとして一時的 (HTTP 302) または恒久的 (HTTP 301)が設定できます。  
リダイレクト先として以下の項目が設定できます。設定をしなかった項目は元の値が保持されます。

* host
* port
* path
* protocol
* query

詳しくは
[リダイレクトアクション](https://docs.aws.amazon.com/ja_jp/elasticloadbalancing/latest/application/load-balancer-listeners.html#redirect-actions)
を参照してください。

#### 転送アクションのデフォルト設定について

転送アクションに関する設定をしない場合、すべてのトラフィックをバックエンドに転送するリスナールールをEponaが内部的に作成します。  
このリスナールールは最優先で適用されます。
そのため、転送アクションを設定しない状態で、「/*」に対する固定レスポンスやリダイレクトアクションだけを設定した場合、
転送アクションが優先されてしまい、意図した挙動にならない場合があります。
固定レスポンスやリダイレクトのアクションだけ設定する場合でも、適切な優先度を設定した転送アクションをあわせて追加するようにしてください。

## サンプルコード

`public_traffic_container_service pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "public_traffic_container_service" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/public_traffic_container_service?ref=v0.2.6"

  name = "[リソースに共通的に付与する名前]"

  vpc_id                             = data.terraform_remote_state.production_network.outputs.network.vpc_id
  public_subnets                     = data.terraform_remote_state.production_network.outputs.network.public_subnets
  public_traffic_protocol            = "HTTPS"
  public_traffic_port                = 443
  public_traffic_inbound_cidr_blocks = ["0.0.0.0/0"]

  # リダイレクトのルールの追加例
  # `/redirect/*` のパスにアクセスしたときに`host-to-redirect`にリダイレクトします
  # ホスト以外のURLは元のURLを引き継ぎます
  alb_redirect_listener_rules = [
    {
      priority     = "10"
      host         = "host-to-redirect"
      status_code  = "HTTP_301"
      path_pattern = "/redirect/*"
    }
  ]

  # アクセス制御の例
  # `/api/*`のパスだけバックエンドのECSに転送し、それ以外のパスへのアクセスは403エラーを返却します
  alb_forward_listener_rules = [
    {
      priority     = "90"
      path_pattern = "/api/*"
    }
  ]
  alb_fixed_response_listener_rules = [
    {
      priority     = "100"
      content_type = "text/plain"
      message_body = "403 Forbidden"
      status_code  = "403"
      path_pattern = "/*"
    }
  ]

  dns = {
    zone_name   = "example.com"
    record_name = "chat-example-backend.example.com"
  }

  container_subnets  = data.terraform_remote_state.production_network.outputs.network.private_subnets
  container_protocol = "HTTP"
  container_port     = 8080

  container_health_check_path           = "/"
  container_cluster_name                = "chat-example"
  container_traffic_inbound_cidr_blocks = ["AWS Fargateを配置するVPCのCIDRブロック"]
  container_service_desired_count       = 3
  container_service_platform_version    = "1.4.0"
  container_task_cpu                    = 512
  container_task_memory                 = "1024"


  default_ecs_task_iam_role_name             = "ChatExampleContainerServiceTaskRole"
  default_ecs_task_iam_policy_name           = "ChatExampleContainerServiceTaskRolePolicy"
  default_ecs_task_execution_iam_policy_name = "ChatExampleContainerServiceTaskExecution"
  default_ecs_task_execution_iam_role_name   = "ChatExampleContainerServiceTaskExecutionRole"

  container_log_group_names             = ["fargate-log-group/chat-example"]

  container_definitions = <<-JSON
  [
    {
      "name": "[タスク名]",
      "image": "hashicorp/http-echo:0.2.3",
      "essential": true,
      "portMappings": [
        {
          "protocol": "tcp",
          "containerPort": 8080
        }
      ],
      "logConfiguration": {
        # ログ設定
      },
      "command": [
          "-listen",
          ":8080",
          "-text",
          "echo"
      ]
    }
  ]
  JSON

  tags = {
    # 任意のタグ
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }
}
```

## 関連するpattern

`public_traffic_container_service pattern`に関連するpatternを、以下に記載します。

| pattern名                                                 | 説明                                                                                                 |
| :-------------------------------------------------------- | :--------------------------------------------------------------------------------------------------- |
| [`ci_pipeline pattern`](./ci_pipeline.md)         | アプリケーションをコンテナイメージとしてビルドし、Amazon ECRへPushできるようになります               |
| [`cd_pipeline_backend_trigger pattern`](./cd_pipeline_backend_trigger.md) | Amazon ECRへのコンテナイメージPushをトリガーに、デプロイメントパイプラインを起動できるようになります |
| [`cd_pipeline_backend pattern`](./cd_pipeline_backend.md)                 | デプロイメントパイプラインを構築して、AWS FaragateクラスターへのBlue/Greenデプロイが可能になります   |

## ログの集約

`public_traffic_container_service pattern`では、ALBのログをAmazon S3に出力します。  
また、コンテナのログはAmazon CloudWatch Logsに出力します。

Amazon CloudWatch Logsへのログ出力は、AWS FireLensを利用して行うことをおすすめします。

[Amazon ECS上のコンテナログを、AWS FireLensを使ってAmazon CloudWatch Logsに送信する](../../how_to/aws/ecs_log_send_to_cloudwatch_logs_using_firelens.md)

また、Datadogが使用できる場合は[datadog_log_trigger pattern](datadog_log_trigger.md)を使ったログ集約を行いましょう。

## システムメトリクスの収集

`public_traffic_container_service pattern`では、コンテナのシステムメトリクス（CPU使用率やメモリ使用量など）を収集する仕組みは提供されません。

下記ガイドで、コンテナのメトリクスをDatadogへ送信する方法を解説しているので、そちらを参照してください。

* [ECS Fargate上のコンテナのメトリクスをDatadogへ送信する](../../how_to/aws/send_ecs_fargate_metrics_to_datadog.md)

## 入出力リファレンス

${TF_DOC}
