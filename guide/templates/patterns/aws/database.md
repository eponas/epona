# database pattern

## 概要

`database pattern`モジュールでは、Runtime環境で実行するアプリケーションから利用するデータベースを構築します。

## 想定する適用対象環境

`database pattern`は、Runtime環境で使用することを想定しています。

## 依存するpattern

`database pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                                     | 利用する情報           |
| :-------------------------------------------- | :--------------------- |
| [network pattern](./network.md)               | プライベートサブネット |
| [encryption_key pattern](./encryption_key.md) | CMKのエイリアス        |

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                         | 説明                                                               |
| :----------------------------------------------------------------------------------------------------------------- | :----------------------------------------------------------------- |
| [Amazon Relational Database Service (RDS)](https://aws.amazon.com/jp/rds/)                                         | 入力変数で指定された設定で、リレーショナルデータベースを構築します |
| [Amazon CloudWatch Logs](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html) | データベースログの格納先を構築します                               |

## モジュールの理解に向けて

サーバーアプリケーションでは、永続的にデータを格納するためのデータベースを必要とすることが多くあります。
Amazon Relational Database Service（以下、Amazon RDS）は、データの永続化をするためのリレーショナルデータベースをフルマネージドで提供するサービスです。

Amazon RDSが提供するデータベースシステムとしては、以下があります。

- Amazon Aurora
- PostgresSQL
- MySQL
- MariaDB
- Oracleデータベース
- SQLServer

:warning: Eponaの開発では、PostgreSQLを使って動作確認を行っています。

:warning: Amazon Auroraのデータベースエンジンは`database pattern`では構築できません。

ここでは、`database pattern`にて構築されるデータベースの運用観点と、その対応について記載しています。

- セキュリティ
- ログ
- モニタリング
- 監視
- メンテナンス
- 高可用性
- リードレプリカ
- バックアップ

patternモジュールそのものだけではなく、データベースを利用するアプリケーションに必要な情報や環境を管理する際に知っておくべき知識もあるので、各々把握するようにしてください。

## セキュリティ

### ネットワーク配置

一般的にAmazon RDSをインターネットからアクセス可能な環境に配置することは、悪意ある第三者による不正アクセスのリスクがあります。

Eponaでは、Amazon RDSインスタンスを`network pattern`で構築されたプライベートサブネットに配置し、同サブネット内のリソースからのアクセスに限定することを推奨します。

### 接続情報の管理

データベースへの接続情報は、秘匿すべき情報として管理されるべきです。

Eponaでは、アプリケーションは[public_traffic_container_service](./public_traffic_container_service.md)のようなコンテナサービスを使用して構築される想定です。

アプリケーションとデータベースとの接続情報は別途 [`parameter_store pattern`](./parameter_store.md)で構築される、パラメータストアで管理することを推奨します。

### 初期マスタパスワードの変更について

`database pattern`を適用後に構築されるデータベースシステムのマスタパスワードは、リソース構築用のterraformスクリプトに平文で記述する必要があります。

この状況は、コードを参照可能なユーザーによる不正なデータベースアクセスや、悪意のある第三者による不正アクセスのリスクを増大させます。

そのため、`database pattern`を適用した後には必ずマスタパスワードを変更するようにしてください。

変更手順は以下のようになります。

1. 構築されたRDSのマスタパスワードを変更する

    構築したRDSインスタンスに対して `rds:ModifyDBInstance` 権限を持つユーザーで以下のコマンドを実行する。

      ```sh
      $ aws rds modify-db-instance --db-instance-identifier [データベース名] --master-user-password [password]
      ```

:information_source: 通常、Terraformは構成定義ファイルの変更を検出して環境に反映しますが、`database pattern`でのパスワードは変更検出の対象外としています。

### データの暗号化

[encryption_key pattern](./encryption_key.md)で作成したAWS KMS CMKを用いて、ストレージや後述するパフォーマンスインサイトに保存するデータの暗号化を行うことができます。

### SSL/TLS

Amazon RDSでは、データベースインスタンスへの接続をSSL/TLS化できます。要件に応じ、SSL/TLS通信を使用したデータベースへの接続も可能です。

[SSL/TLS を使用した DB インスタンスへの接続の暗号化](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/UsingWithRDS.SSL.html)

構築されたデータベースインスタンス自体は、SSL/TLS通信が有効化されています。

ただし、クライアントプログラムからサーバー証明をしつつSSL/TLSで通信するためには、クライアントの環境にAmazon RDSが提供する証明書を組み込む必要がある点に注意してください。

詳細については、[Amazon RDSのSSL/TLSに関するドキュメント](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/UsingWithRDS.SSL.html)を確認してください。

また、クライアントプログラムからのSSL/TLS通信を強制したい場合は、以下のようにパラメーターグループを設定します。

```terraform
  parameters = [
    { name = "rds.force_ssl", value = "1", apply_method = "pending-reboot" }

    ## その他のパラメーター
    ...
  ]
```

:warning: このパラメーターが有効なデータベースエンジンは、PostgreSQLとSQL Serverに限られます。

- [PostgreSQL DB インスタンスへの SSL 接続を必須にする](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/CHAP_PostgreSQL.html#PostgreSQL.Concepts.General.SSL.Requiring)
- [SQL Server / DB インスタンスへの接続に SSL を使用させる](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/SQLServer.Concepts.General.SSL.Using.html#SQLServer.Concepts.General.SSL.Forcing)

## ログ

Amazon RDSでは、データベースインスタンスが出力するログをAmazon CloudWatch Logsへ保存できます。

[Amazon RDS データベースログファイル](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_LogAccess.html)

ただし、データベースに関するすべてのログをAmazon CloudWatch Logsに出力できるわけではありません。

データベースエンジンとAmazon CloudWatch Logsに出力なログの組み合わせは、[Amazon RDSのドキュメント](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_LogAccess.html#USER_LogAccess.Procedural.UploadtoCloudWatch)を確認してください。

`database pattern`は、各データベースエンジンがAmazon CloudWatch Logsに出力可能なログについては、デフォルトですべて有効化します。

Amazon CloudWatch Logsに出力する対象を絞りたい、もしくは出力自体をやめたい場合は`enabled_cloudwatch_logs_exports`変数を設定してください。

また、Amazon CloudWatch Logsのロググループは自動で作成します。

Datadogが利用可能な場合は、[ログの集約](#ログの集約)を参照してください。Datadogへのログの集約が可能です。

## モニタリング

`database pattern`では、デフォルトで以下の3つのモニタリングが可能なように構成されます。

- Amazon CloudWatchメトリクス
- [拡張モニタリング](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_Monitoring.OS.html)
- [パフォーマンスインサイト](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_PerfInsights.html)

Amazon RDSのモニタリングの概要については、以下のページを参照してください。

[モニタリングの概要](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/MonitoringOverview.html)

収集したメトリクスを使い、モニタリングおよび監視を行えます。

拡張モニタリング、パフォーマンスインサイトに関しては有効なままとすることを推奨しますが、patternモジュールの設定で無効にできます。

また、拡張モニタリングのメトリクスの収集間隔（詳細度）はデフォルトで60秒に、パフォーマンスインサイトのデータの保持期間はデフォルトで7日に設定しています。

`database pattern`の変数で設定可能ですので、必要に応じて変更してください。

拡張モニタリングの保存先はAmazon CloudWatch Logsとなるため、Datadogが利用可能な場合はログ集約が可能です。  
[ログの集約](#ログの集約)を参照してください。

## イベント通知

Amazon RDSで発生するイベントは、Amazon SNSにより通知されます。

[Amazon RDS イベント通知の使用](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_Events.html)

この通知をモニタリングすることで、Amazon RDS上で発生するイベントを把握できます。

:information_source: 現時点のEponaでは、このテーマをサポートしていません。

## メンテナンス

Amazon RDSでは、データベースインスタンスのメンテナンスが実施されます。

- [DB インスタンスのメンテナンス](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_UpgradeDBInstance.Maintenance.html)
- [DB インスタンス のエンジンバージョンのアップグレード](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_UpgradeDBInstance.Upgrading.html)

`database pattern`では、デフォルトでデータベースエンジンのメジャーアップグレードを無効に、マイナーバージョンの自動アップグレードの有無とメンテナンスウィンドウの指定を必須にしています。

- `allow_major_version_upgrade`　：　メジャーバージョンのアップグレードの許可
- `auto_minor_version_upgrade`　：　マイナーバージョンの自動アップグレードの有無
- `maintenance_window`　：　メンテナンスウィンドウ（`ddd:hh24:mi-ddd:hh24:mi`：24時間表記、UTC指定）

サービスの内容に応じて、メンテナンスウィンドウとエンジンのアップグレード方針を決定し、モジュールの設定に反映してください。

## 高可用性

Amazon RDSでは、インスタンスをマルチAZ配置にすることで、高可用構成を実現できます。

[Amazon RDS での高可用性 (マルチ AZ)](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/Concepts.MultiAZ.html)

ただし、インスタンス構成がマルチAZとなっても、インスタンスのフェイルオーバーの可能性を考慮したアプリケーションの対応が必要となる点に注意してください。

Eponaでは`database pattern`において、`multi_az`変数を`true`とすることでAmazon RDSインスタンスをマルチAZ構成とできます。この変数はデフォルトでは`false`となっており、シングルAZで構築されます。

## リードレプリカ

現在の`database pattern`では、リードレプリカを含めた構成はサポートしていません。

## バックアップ

Amazon RDSでは、自動スナップショットを取得することで、バックアップを実現できます。

[Amazon RDS DB インスタンスのバックアップと復元](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/CHAP_CommonTasks.BackupRestore.html)

Eponaでは、`database pattern`において、以下変数を入力することでAmazon RDSインスタンスの自動スナップショット取得を有効とします。これら変数はデフォルトでは`null`となっており、自動スナップショットは無効化されます。

- `backup_retention_period`　：　バックアップ保管世代(最大35)
- `backup_window` ：　バックアップウィンドウ(HH:MM-HH:MM表記)

:warning:  
自動スナップショットには保持期間があり、期限が過ぎたスナップショットは削除されます。  
`database pattern`モジュールを使用して、自動で取得したスナップショットから[リストア](#リストア)を行う場合、スナップショットをコピーして使用することを**強く推奨します**。

[DB スナップショットのコピー](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_CopySnapshot.html#USER_CopyDBSnapshot)

詳細は、[リストア](#リストア)を参照してください。

## リストア

Amazon RDSはスナップショットからのリストア可能です。`database pattern`では、リストアは指定のスナップショットからDBインスタンスを作り直すことで行います。

:warning:  
自動で取得したスナップショットからリストアする場合、スナップショットのコピーを行い、コピーからリストアすることを**強く推奨します**。  
また、Terraformでは、ポイントインタイムリカバリをサポートしていません。

リストア手順は以下のとおりです。

1. RDSインスタンスをデプロイした`terraform`コードに、`snapshot_identifier`を追加指定します。変数の値は、リストア先のSnapshot名を入力します
1. `terraform`を実行します

リストアを行うと、`snapshot_identifier`に指定したスナップショットを使ってDBインスタンスが再作成されます。  
RDSのエンドポイント（DNS名）は変わりませんが、インスタンスのIPアドレスは変更されることに注意してください。

RDSを使用するアプリケーションがDNSの参照結果をキャッシュする場合は、TTLを適切に指定してください。

Javaの場合は、以下を参考にします。

- [DNS 名参照用の JVM TTL の設定](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/Concepts.MultiAZ.html#Concepts.MultiAZ.Failover.Java-DNS)
  - ドキュメントとしてはマルチAZのものですが、DNSのTTLに対する考え方としては同じです
- [DNS 名参照用の JVM TTL の設定](https://docs.aws.amazon.com/ja_jp/sdk-for-java/v1/developer-guide/java-dg-jvm-ttl.html)

---

:warning: リストア後の注意点について。

リストアを行った後は、別のスナップショットからリストアする場合を除いて`snapshot_identifier`に指定した値を変更しないでください。  
`snapshot_identifier`を指定してリストア後、この値を変更してTerraformを実行した場合、以下の挙動になります。

- `snapshot_identifier`の値を削除 → インスタンスの再作成（データがない状態で再作成）
- `snapshot_identifier`に別のスナップショットIDを指定 → 指定のスナップショットからリストア

また`snapshot_identifier`に指定したスナップショットが存在しない場合、 **インスタンスが削除されリストアは失敗します**。  
スナップショットが期限切れなどで削除されたりすると、その後のTerraform実行時に意図せぬインスタンス削除となる恐れがあるため注意してください。

:information_source: リストア後に`snapshot_identifier`を変更せずにTerraformを実行した場合、リストアされるのは初回だけです。

このため自動バックアップで取得したスナップショットは直接使わずに、事前に手動でコピーして使用してください。

[DB スナップショットのコピー](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_CopySnapshot.html#USER_CopyDBSnapshot)

---

## 削除保護

`database pattern`で構築したRDSインスタンスは、デフォルトで削除保護が有効になっています。

[削除保護](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_DeleteInstance.html#USER_DeleteInstance.DeletionProtection)

これは`terraform destroy`を誤って実行してしまいデータを失うことを避けるため、セーフティネットを意図して有効化しています。

テスト目的などでRDSインスタンスの破棄できるようにするには、`deletion_protection`を`false`に設定します。  
削除保護が無効になり、`terraform destroy`が可能になります。

:information_source: 削除保護が有効なRDSを削除する場合は、先に`deletion_protection`を`false`に設定変更してください。

## Amazon QuickSightとの連携

RDSは[Amazon QuickSight](https://aws.amazon.com/jp/quicksight/)のデータソースとして使用できます。
本パターンで構築したRDSをQuickSightのデータソースとして利用する場合、`source_security_group_ids`の設定が必要です。
この場合、`source_security_group_ids`に[`quicksight_vpc_inbound_source pattern`](./quicksight_vpc_inbound_source.md)で出力されたセキュリティグループのIDを設定してください。

`quicksight_vpc_inbound_source pattern`の適用については、[`quicksight_vpc_inbound_source pattern`](./quicksight_vpc_inbound_source.md)のドキュメントを参照してください。

## サンプルコード

`database pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "database" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/database?ref=v0.2.6"

  name = "my-database-instance"
  tags = {
    # 任意のタグ
    Name               = "my-database"
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }

  identifier            = "my-rds-instance"
  engine                = "postgres"
  engine_version        = "12.4"
  port                  = 5432
  instance_class        = "db.t3.medium"
  allocated_storage     = 5
  max_allocated_storage = 10
  
  auto_minor_version_upgrade = true
  maintenance_window         = "Tue:18:00-Tue:18:30"

  username = "user"
  password = "password"

  kms_key_id = data.terraform_remote_state.encryption_key.outputs.encryption_key.keys["alias/my-encryption-key"].key_arn  # encryption_key patternで作成したモジュールから値を設定

  # network patternで作成したモジュールから値を設定
  vpc_id = data.terraform_remote_state.network.outputs.network.vpc_id
  availability_zone = "ap-northeast-1a"

  db_subnets = data.terraform_remote_state.staging_network.outputs.network.private_subnets
  db_subnet_group_name = "my-rds-subnet-group"

  parameter_group_name   = "my-rds-parameter-group"
  parameter_group_family = "postgres12"

  option_group_name                 = "my-rds-option-group"
  option_group_engine_name          = "postgres"
  option_group_major_engine_version = "12"

  backup_retention_period           = "35"
  backup_window                     = "00:00-01:00"

  performance_insights_kms_key_id = data.terraform_remote_state.encryption_key.outputs.encryption_key.keys["alias/my-encryption-key"].key_arn  # encryption_key patternで作成したモジュールから値を設定

  parameters = [
    # DBパラメータを記載
  ]

  # QuickSightから本パターンで構築したRDSを参照する場合、quicksight_vpc_inbound_source pattern適用後、再実行してください
  # source_security_group_ids = [
    # "sg-00000000000000000"  # quicksight_vpc_inbound_source patternで出力されるセキュリティグループID
  # ]
}
```

## 関連するpattern

`database pattern`に関連するpatternを、以下に記載します。

| pattern名                                                                         | 説明                                            |
| :-------------------------------------------------------------------------------- | :---------------------------------------------- |
| [public_traffic_container_service pattern](./public_traffic_container_service.md) | DBを利用するアプリケーションがデプロイされるECS |

## ログの集約

`database pattern`では、データベースに関連するログおよび拡張モニタリングをAmazon CloudWatch Logsに出力します。

これらのログは、[datadog_log_trigger pattern](datadog_log_trigger.md)を使用することでDatadogに集約できます。

## 入出力リファレンス

${TF_DOC}
