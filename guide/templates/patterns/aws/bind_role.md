# bind_role pattern

## 概要

`bind_role pattern`モジュールでは、Delivery環境のIAMユーザーからRuntime環境のIAMロールへスイッチロールするためのIAMポリシーを作成します。

本patternモジュールにより、Staging環境ではdeveloperのRoleを持たせるがRuntime環境ではviewerのRoleしか持たせないといった、柔軟なRoleの紐付けを実現します。

## 想定する適用対象環境

`bind_role pattern`は、Delivery環境で使用されることを想定しています。

## 依存するpattern

`bind_role pattern`モジュールは`users pattern`モジュールと`roles pattern`モジュールとセットで実行します。各モジュールの役割、実行順序は以下のとおりです。

1. `users pattern`：Delivery環境にIAMユーザーを作成します
1. `bind_role pattern`：Runtime環境のIAMロールへスイッチロールするIAMポリシーを作成し、Delivery環境のIAMユーザーへアタッチします
1. `roles pattern`：Runtime環境のIAMロールを作成します

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                 | 説明                                                                                                         |
| :----------------------------------------------------------------------------------------- | :----------------------------------------------------------------------------------------------------------- |
| [IAMポリシー](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/access_policies.html) | Delivery環境のIAMユーザーへ付与する、runtime環境のIAMロールへスイッチロールするためのIAMポリシーを作成します |

## モジュールの理解に向けて

`bind_role pattern`ではIAMユーザーに付与するIAMポリシーを作成します。

`bind_role pattern`では、Delivery環境上のIAMユーザー[^1]がRuntime環境上の以下のIAM Role[^2]にスイッチロールできるようなIAMポリシーを作成します。
[^1]: `users pattern` で作成します。
[^2]: `roles` patternで作成します。

なおロール名の`%s`は、本patternの入力パラメータである`system_name`の値をCamelCaseとして変換した文字列に置き換えてください。例えば`system_name`に`"epona"`を指定した場合、管理者ロールの名前は`EponaAdminRole`となります。

| ロール名            | 説明                                                                           |
| :------------------ | :----------------------------------------------------------------------------- |
| `%sAdminRole`       | 管理者ロール(全ての作業を行う権限をもちます)                                   |
| `%sViewerRole`      | 閲覧者ロール(全リソースの閲覧のみできる権限をもちます)                         |
| `%sOperatorRole`    | 作業者ロール(読み取り宣言+運用で必要な権限をもちます)                          |
| `%sDeveloperRole`   | 開発者ロール(NWなどインフラ系サービスの操作や秘匿パラメータの参照を除いたリソースを操作する権限をもちます) |
| `%sCostManagerRole` | コスト管理者ロール(課金情報の操作のみできる権限をもちます)                     |
| `%sApproverRole`    | 承認者ロール(CI/CDの承認のみ可能な権限をもちます)                              |

IAMユーザーは各権限のIAMロールへスイッチロールすることで、必要な権限を取得します。

`bind_role pattern`はDelivery環境からRuntime環境へスイッチロールする環境を構築します。

![IAMユーザ関連のpattern](../../resources/users.png)

## サンプルコード

`bind_role pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "bind_roles" {
  source           = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/bind_role?ref=v0.2.6"
  admins           = ["taro", "hanako"]
  approvers        = ["taro", "yoko"]
  costmanagers     = ["taro", "ryota"]
  developers       = ["hanako", "chiharu"]
  operators        = ["hanako", "ayaka"]
  viewers          = ["hanako", "yoshiki"]
  account_id       = "XXXXXXXXXX"
  environment_name = "production"
  system_name      = "hori"
}

```

## 関連するpattern

`bind_role pattern`に関連するpatternを、以下に記載します。

| pattern名                     | 説明                                                                               |
| :---------------------------- | :--------------------------------------------------------------------------------- |
| [`users pattern`](./users.md) | Delivery環境のIAMユーザーを作成します                                              |
| [`roles pattern`](./roles.md) | このpatternで作成したIAMユーザーからスイッチするIAMロール(Runtime環境)を作成します |

## 入出力リファレンス

${TF_DOC}
