# api_gateway pattern

## 概要

`api_gateway pattern`は、APIを作成・公開するための[Amazon API Gateway](https://aws.amazon.com/jp/api-gateway/)を作成するモジュールです。

## 想定する適用対象環境

`api_gateway pattern`は、Runtime環境で使用されることを想定しています。

## 依存するpattern

`api_gateway pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                                                                         | 利用する情報                                                                                                                                                           |
| :-------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [lambda pattern](./api_gateway.md)                                           | `Amazon API Gateway`と組み合わせて使うLambda関数および実行に必要なロール |

なお、必要に応じて以下のAWSリソースを用意してください。

- 独自ドメインの取得および`Amazon Route 53`でドメイン名のホストゾーン作成

  カスタムドメインを使用する場合は、事前にドメインを取得し、`Amazon Route 53`でホストゾーンを作成していただく必要があります

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                           | 説明                                                                             |
| :----------------------------------------------------------------------------------- | :------------------------------------------------------------------------------- |
| [Amazon API Gateway](https://aws.amazon.com/jp/api-gateway/)                                      | `AWS Lambda`などのバックエンドサービスにアクセスするための「正面玄関」となるAPI                         |
| [Amazon Route 53](https://aws.amazon.com/jp/route53/) | カスタムドメイン利用時に、指定されたドメイン名を`Amazon API Gateway`にルーティングするよう設定する                                           |
| [Amazon Route 53 レコード](https://docs.aws.amazon.com/ja_jp/Route53/latest/DeveloperGuide/rrsets-working-with.html)                                                | `Amazon API Gateway`にカスタムドメインでアクセスできるようにするためのDNSレコードを作成する               |
| [AWS Certificate Manager](https://aws.amazon.com/jp/certificate-manager/)                                                                                   | `Amazon API Gateway`にカスタムドメインでHTTPSアクセスする際に使用するSSL/TLS証明書を発行するサ                     |
| [Amazon CloudWatch Logs](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html)                                          | `Amazon API Gateway`のログの出力先となるロググループを作成する                                               |

## モジュールの理解に向けて

### Amazon API Gatewayについて

`Amazon API Gateway`とは、その名の通りAPIの「入り口」となるサービスです。  
APIを外部に公開するにあたっては、トラフィック管理・認証・アクセス管理・モニタリング等の諸条件を考慮し、適切に設定する必要があります。
しかし、これらの仕組みをAPI開発者が自前で構築するのは煩雑です。  
`Amazon API Gateway`はAPIの作成・公開に伴うこれらのタスクをフルマネージドで提供するサービスです。

Eponaの`api_gateway pattern`では、`AWS Lambda`をREST APIで公開することを想定し、以下のような構成を実現します。

![概要図](./../../resources/api_gateway.png)

なお、`Amazon API Gateway`に関する概念については[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-basic-concept.html)もあわせて参照してください。

### Amazon API Gatewayの設定について

`Amazon API Gateway`において、各APIは`ステージ`、`リソースパス`、`HTTPメソッド`ごとに設定できます。

![概要図](./../../resources/api_gateway_pattern.png)

たとえば、このような階層構造のAPIを作成するとします。

- GET /api: 固定レスポンス
- POST /api: 固定レスポンス
- GET /api/child_api: 読み取りアクセスを行うLambda関数を呼び出す
- POST /api/child_api: 書き込みアクセスを行うLambda関数を呼び出す

この場合、以下のような形でパラメータを設定します。

``` terraform
module "api_gateway_rest_api" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/api_gateway?ref=v0.2.6"
  api_gateway_settings = {
    ...
    api_resources = {
      "/api" = {
        get = {
          lambda     = "mock"
        }
        post = {
          lambda     = "mock"
        }
      }
      "/api/child_api" = {
        get = {
          lambda     = {読み取り用Lambda関数の名称}
        }
        post = {
          lambda     = {書き込み用Lambda関数の名称}
        }
      }
    }
  }
  stages = {
    "default" = {
      ...
    },
  }
  ...
}
```

個々の設定項目の内容については[入出力リファレンス](#入出力リファレンス)を参照してください。

<!-- textlint-disable -->
:information_source: 設定値の誤りなどにより`terraform apply`がエラー終了した場合、
`Imported on ～`という名称の`Amazon API Gateway`が残ってしまうことがあります。  
その場合は、マネジメントコンソールまたは`AWS CLI`により削除するようにしてください。
<!-- textlint-enable -->

### API Gatewayの配置について

[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-api-endpoint-types.html)
では、`Amazon API Gateway`のエンドポイントの配置方法として以下の３つが紹介されています。

- エッジ最適化
- リージョン
- プライベート

このうち、`エッジ最適化`と`リージョン`はインターネットに公開されるエンドポイントです。  
`api_gateway pattern`適用後に表示されるURLもしくは[カスタムドメイン](#カスタムドメインについて)によりアクセス可能です。  
この2つの違いはエンドポイントを地理的に分散するか（`Amazon CloudFront`を経由するか）にあります。  
`エッジ最適化`を選択することでレイテンシの軽減が見込まれますが、動作検証時など、簡易なAPIのみ動かすようなケースでは`リージョン`を選択しても良いでしょう。

`プライベート`で構成する場合は、`api_gateway pattern`適用後に表示されるURLによりアクセス可能です
（`プライベート`ではカスタムドメインは使用できません）。  
事前に`VPC Endpoint`を構成する必要があります。
これは、[`network pattern`](./network.md)で作成できますので、作成された`VPC EndPoint ID`を
控えておいてください。  
（入出力リファレンスの`api_gateway_endpoint_enabled`を参照してください）

エンドポイントの記載例は[サンプルコード](#サンプルコード)にありますので、適宜ご参照ください。

:warning: エンドポイントを`プライベート`にしただけでは、アクセス元の制限は行われません。
アクセス元の制限は`リソースポリシー`により設定してください。  
リソースポリシーについては[認証、アクセス制限に関する設定について](#認証、アクセス制限に関する設定について)をご参照ください。  
あわせて、[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/apigateway-private-apis.html)の記載も参照してください。

### カスタムドメインについて

`Amazon API Gateway`上のAPIには、デフォルトでは、以下の書式のURLが割り当てられます。

- `https://{restapi_id}.execute-api.{region}.amazonaws.com/{stage_name}/`

たとえば、`default`ステージにデプロイされている`/api/child_api`には、以下のURLでアクセスすることになります。

- `https://{restapi_id}.execute-api.{region}.amazonaws.com/default/api/child_api`

:information_source: 以降、本ページの記述ではステージ名を`default`として記載します。

URLをよりわかりやすいものとするため、代替URLをマッピングできます。これは、[入出力リファレンス](#入出力リファレンス)の`custom_domains`にて指定できます。

``` terraform
  custom_domains = {
    "custom_domain.example.com" = {
      custom_zone_name = "example.com"
      base_path_mapping = {
        "/" = {
          stage_name = "default"
        }
      }
    }
  }
```

この場合、`https://custom_domain.examle.com/api/child_api`で`/default/api/child_api`に
アクセスできます。

:information_source: カスタムドメインの証明書については、`AWS Certificate Manager`経由での証明書のみサポートしています。

`api_gateway pattern`では、カスタムドメインが必要な場合は`AWS Certificate Manager`の証明書を作成します。  
なお、[エンドポイント](#API-Gatewayの配置について)を`エッジ最適化`とした場合には`us-east-1`リージョン
(米国東部 (バージニア北部))に証明書を作成する必要があります。  
[サンプルコード](#サンプルコード)を参考に、適切にリージョンを設定してください。

:information_source: カスタムドメインを設定した状態で`AWS API Gateway`の
[エンドポイント](#API-Gatewayの配置について)を変更した場合、`terraform apply`がエラーとなることがあります。  
再実行することで解消するため、もし発生した場合は`terraform apply`を再実行してください。

### 認証、アクセス制限に関する設定について

Eponaの`api_gateway pattern`では、アクセス制限に関して以下のような設定が可能です。

- リソースポリシーによるIPアドレス制限  
  リソースポリシーにより、アクセス元のIPアドレスを限定できます。
  設定方法については[入出力リファレンス](#入出力リファレンス)の`api_policy_path`を参照してください

- APIキーによるアクセス制限  
  APIキーを必須にすることにより、APIキー無しでのアクセスを排除できます。
  設定方法については[入出力リファレンス](#入出力リファレンス)の`api_key_required`を参照してください。  
  [APIキーについて](#APIキーについて)の記載もあわせて参照してください

  :warning: APIキーを唯一の認証の手段として利用しないでください。  
  詳細は
  [APIキーと使用量プランのベストプラクティス](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-api-usage-plans.html#apigateway-usage-plans-best-practices)
  をご参照ください。

- Lambdaオーソライザーによる認証・認可  
  権限チェック処理を`AWS Lambda`で作成する（これを`Lambdaオーソライザー`と呼びます）ことで、
  認証・認可の機能を組み込むことができます。  
  [Lambdaオーソライザーについて](#Lambdaオーソライザーについて)の記載もあわせて参照してください

### Lambdaオーソライザーについて

Lambdaオーソライザーの概要、実装例については[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/apigateway-use-lambda-authorizer.html)を参照してください。

:warning: Eponaは[Cognitoオーソライザー](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/apigateway-integrate-with-cognito.html)をサポートしていません。

`api_gateway pattern`では、リソースパス・メソッドごとにLambdaオーソライザーのオン・オフを設定できます。

``` terraform
module "api_gateway_rest_api" {
  ...
  api_resources = {
    "/pets" = {
      get = {
        lambda     = local.lambda_function_name_get
        authorizer = "test_authorizer" # api_security_schemes内のキー名と一致させてください
      }
      post = {
        lambda     = local.lambda_function_name_post
      }
    }
  }
  api_security_schemes = {
    test_authorizer = { # この名前をapi_resources内のauthorizerに設定してください
      authorizer_parameter_name = "authorizer_token"
      authorizer_parameter_in   = "header"
      request_or_token_type     = "token"
      request_identity_source   = null
      authorizer_function_name  = (Lambdaオーソライザーとして使用するLambda関数の名前)
      authorizer_credentials    = (Lambdaオーソライザーを実行するためのロールのARN)
      authorizer_ttl            = "300"
    }
  }
  ...
}
```

このような設定をした場合、`/pets`のGETリクエスト時のみ、オーソライザーによる認証処理が行われます。

また、上記の設定の場合、Lambdaオーソライザーはリクエストヘッダーの`authorizer_token`の値を`event.authorizationToken`として受け取ります。

ヘッダーではなく、クエリ文字列から値を受け取ることも可能です。その場合は以下のような設定になります。

``` terraform
  api_security_schemes = {
    test_authorizer = { # この名前をapi_resources内のauthorizerに設定してください
      authorizer_parameter_name = null
      authorizer_parameter_in   = null
      request_or_token_type     = "request"
      request_identity_source   = "method.request.querystring.authorizer_token"
      authorizer_function_name  = (Lambdaオーソライザーとして使用するLambda関数の名前)
      authorizer_credentials    = (Lambdaオーソライザーを実行するためのロールのARN)
      authorizer_ttl            = "300"
    }
  }
}
```

設定項目の詳細については、[入出力リファレンス](#入出力リファレンス)を参照してください。

### WAFの設定について

`webacl pattern`をあわせて適用することで、`Amazon API Gateway`に`AWS WAF`
（ウェブアプリケーションファイアウォール）を設定できます。  
詳細は[`webacl pattern`のガイド](./webacl.md)を参照してください。

なお、`Amazon API Gateway`と`AWS WAF`の関連付けはステージ単位で行います。
そのため、`webacl pattern`の`resource_arn`には、`Amazon API Gateway`のステージのARNを指定してください。  
詳細は[Amazon API Gatewayの公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/apigateway-control-access-aws-waf.html)を参照してください。

### CORS有効化について

`Amazon API Gateway`のリソースが、シンプルでないクロスオリジンのHTTPリクエストを受け取る場合、CORSを有効にする必要があります。  
詳細は[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/how-to-cors.html)を参照してください。

`api_gateway pattern`でCORSを有効にする場合は、以下のように設定してください。

- プリフライトリクエストの対応（`OPTIONS`）

  `enable_cors`パラメータを設定することで、以下の`OPTIONS`メソッドを作成できます。

  - ステータスコード`200`のレスポンスを返却する
  - ステータスコード`200`を返す際に以下のレスポンスヘッダーを付与する
    - Access-Control-Allow-Methods : `allow_methods`に指定されたHTTPメソッド
    - Access-Control-Allow-Headers : `allow_headers`に指定されたヘッダー
    - Access-Control-Allow-Origin : `allow_origin`に指定されたオリジン

  `/pets`リソースで、プリフライトリクエストに対応する例を示します。

``` terraform
"/pets" = {
  enable_cors = {
    allow_methods = "DELETE,GET,OPTIONS,PATCH"
    allow_headers = "Content-Type,X-Amz-Date,Authorization,X-Api-Key,  X-Amz-Security-Token"
    allow_origin = "*"
  }
  get = {
    ...
  }
```

- モック統合のCORSサポート

  [公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/how-to-cors.html#apigateway-enable-cors-mock)
  に記載の通り、`Access-Control-Allow-Origin`を設定する必要があります。  
  これは`api_resources`ブロック内の`allow_origin`により設定可能です。詳細は[入出力リファレンス](#入出力リファレンス)を参照してください。

- Lambda統合のCORSサポート

  Lambda関数からヘッダーを返す必要があります。詳細は
  [公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/how-to-cors.html#apigateway-enable-cors-proxy)
  を参照してください。

### ログ

`Amazon API Gateway`実行ログとアクセスログの2種類があります。

- 実行ログ  
  `Amazon API Gateway`により、CloudWatchロググループが自動作成されます。
  名称は`API-Gateway-Execution-Logs_{rest-api-id}/{stage_name}`という形式になります。  
  出力項目も`Amazon API Gateway`での所定の内容が出力されますが、ログレベルの設定によりログに出力される内容を変更できます

  - INFO  
    すべてのリクエストの実行ログが生成されます
  - ERROR  
    エラーになったリクエストの実行ログのみを生成します
  - OFF  
    実行ログを生成しません

  :information_source: このログは`Amazon API Gateway`により自動的に作成されるため、
  `terraform destroy`をしても削除できません。また、保持期間も無期限となります。  
  `terraform import`によりインポートすることで、ログをTerraformの管理下に置くことができます。  
  これにより、`terraform destroy`による削除や、`retention_in_days`による保持期間の設定が可能です。  
  詳細は
  [Terraformのドキュメント](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_stage#managing-the-api-logging-cloudwatch-log-group)
  を参照してください。

- アクセスログ  
  上記の実行ログの他に、APIへのアクセスに関するログを別のロググループに出力できます。  
  ログの書式はCommon Log Formatで指定する必要があります。
  詳細は[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/set-up-logging.html)を参照してください

### APIキーについて

APIキーとは、APIへのアクセス元を識別する文字列です。これを[使用量プラン](#スロットリング、使用量プラン)と組み合わせることで、アクセス数に上限を設けるなどの制御ができます。

`Amazon API Gateway`は`HEADER`もしくは`AUTHORIZER`のいずれかからAPIキーを受け取ることができます。  
これは[入出力リファレンス](#入出力リファレンス)の`api_key_source`により指定可能です。  
`HEADER`, `AUTHORIZER`の中で、どのような項目名でセットするかは
[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-api-key-source.html)
を参照してください。

### スロットリング、使用量プラン

`Amazon API Gateway`では、過剰なリクエストがバックエンドに流れないように、APIへのリクエストを
調整する仕組み（スロットリング）があります。  
スロットリングはいくつかのレベルで設定でき、以下の優先順で適用されます。

1. APIキー（使用量プラン）で設定された、メソッドあたりのスロットリング制限
1. APIキー（使用量プラン）で設定された、ステージ全体のスロットリング制限
1. ステージごとのスロットリング制限
1. リージョンごとのアカウントレベルのスロットリング

詳細は
[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-request-throttling.html)
を参照してください。

:warning:「APIキー（使用量プラン）で設定された、メソッドあたりのスロットリング制限」については、
`api_gateway pattern`では設定できません。これは、Terraform Providerが未対応のためです。

`api_gateway pattern`でスロットリングを設定するには、以下のように設定します。

1. APIキー（使用量プラン）で設定された、ステージ全体のスロットリング制限は、`api_key_usage_plan_settings`にて設定します
1. ステージごとのスロットリング制限は、`api_stage_method_settings`にて設定します

たとえば、以下のようなパラメータを指定したとします。

``` terraform
module "api_gateway_rest_api" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/i_gateway?ref=v0.2.6"
  ...

  api_gateway_settings = {
    api_resources = {
      "/" = {
        get = {
          lambda    = "mock"
        }
      }
      "/pets" = {
        get = {
          lambda     = {Lambda関数名}
        }
        post = {
          lambda     = {Lambda関数名}
        }
      }
      "/pets/{petID}" = {
        get = {
          lambda     = {Lambda関数名}
        }
      }
    }
  }
  stages = {
    "default" = {
      xray_tracing_enabled = true
      # defaultステージのスロットリング設定
      log_throttling = {
        "/*/*" = {
          throttling_burst_limit = 5
          throttling_rate_limit  = 5
        }
        "/pets/get" = {
          throttling_burst_limit = 15
          throttling_rate_limit  = 15
        }
      }
    },
  }
  api_keys = {
    "example_key" = {
      api_key_enabled = true
      api_usage_plan = {
        quota = {
          limit  = 10
          offset = 2
          period = "WEEK"
        }
        # APIキーのスロットリング設定
        throttling = {
          burst_limit = 20
          rate_limit  = 20
        }
        # defaultステージに適用
        api_usage_plan_apply_stages = [
          "default"
        ]
      }
    }
  }
}
```

この場合、以下のようにスロットリング設定が適用されます。

| リクエストにAPIキーを含むか | リソースパス      | HTTPメソッド | 適用される設定 | 例     |
|-------------------------|-----------------|-------------|-------------|--------|
| 含む                     |/default/pets    | GET         |APIキーに紐づく使用量プランで設定された、ステージ全体のスロットリング制限 | burst_limit = 20<BR>rate_limit  = 20
| 含まない                  |/default/pets    | GET         |ステージごとのスロットリング制限                                  | burst_limit = 15<BR>rate_limit  = 15
| 含む                     |/default/pets    | POST        |APIキーに紐づく使用量プランで設定された、ステージ全体のスロットリング制限 | burst_limit = 20<BR>rate_limit  = 20
| 含まない                  |/default/pets    | POST        |ステージごとのスロットリング制限                                  | burst_limit = 5<BR>rate_limit  = 5

なお、該当する設定がなかった場合は、デフォルトのスロットリング設定が適用されます。  
詳細な値は[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/limits.html)を参照してください。

### `terraform apply`時の注意事項

一部リソースについては、`terraform apply`だけでは設定が反映されず、ステージの
[再デプロイ](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/how-to-deploy-api.html)
が必要となります。対象のリソースは
[こちら](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/updating-api.html)
を参照してください。

`terraform apply`でステージの再デプロイまで実施する場合は[入出力リファレンス](#入出力リファレンス)
の`stage`内にある`force_redeployment_string`を設定してください。

:information_source: 都度、値を変更するのが煩雑であれば、日時やファイルのハッシュ値などを設定してください。

:information_source: `force_redeployment_string`を設定した場合であっても、
[リソースポリシー](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/apigateway-resource-policies.html)
の変更においては再デプロイが必要となります。  
これは、現在のEponaのモジュール上、リソースポリシーの修正が`force_redeployment_string`による
ステージの再デプロイの後となってしまうためです。  
[手動で再デプロイ](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/set-up-deployments.html)
していただくか、`force_redeployment_string`を再度変更して`terraform apply`を実施していただく必要があります。

### Lambdaのデプロイメントパイプラインとの統合について

EponaでのLambdaのデプロイメントパイプラインでは、`AWS Lambda`の
[`エイリアス`](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-aliases.html)
機能を使用しています。

![Lambda_Alias](./../../resources/api_gateway_alias.png)

デプロイメントパイプラインで管理されているLambda関数を`api_gateway pattern`に統合するには、以下のように設定する必要があります。

- Lambda関数名に`${stageVariables.<ステージ変数名>}`を付与する

  - エイリアス名を`Amazon API Gateway`の
[ステージ変数](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/stage-variables.html)
経由で受け渡すため、このように記載します。Terraform上、文字列として処理させるため、`$${`とエスケープする必要があります

- `${stageVariables.<ステージ変数名>}`に当てはめる、実際のエイリアス名をステージ変数に記載する

以下に設定例を示します。

``` terraform
module "api_gateway_rest_api" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/i_gateway?ref=v0.2.6"
  ...

  api_gateway_settings = {
    api_resources = {
      "/pets" = {
        get = {
          lambda     = "<Lambda関数名>:$${stageVariables.alias}"  # 「alias」部分は任意に設定可能
          lambda_aliases = ["<Lambdaエイリアス名>"]
          authorizer = "<オーソライザー名>"
        }
      }
    }
    api_security_schemes = {
      <オーソライザー名> = {
        ...
        authorizer_function_name  = "<Lambda関数名>:$${stageVariables.alias}"
        authorizer_aliases = ["<Lambdaエイリアス名>"]
        ...
      }
    }
  }
  stages = {
    "default" = {
      stage_variables = {
        alias = "<Lambdaエイリアス名>"  # Lambdaデプロイメントパイプラインで指定されているエイリアス名
      }
    },
  }
  ...
}
```

:information_source: API内のLambdaのエイリアス名と、オーソライザーとして使用するLambdaのエイリアス名は同一である必要があります。

### サンプルコード

以下に、Eponaを使ったサンプルコードを示します。

``` terraform
provider "aws" {
  alias = "default_provider"
}

# CloudFront関連リソースを配置するリージョンを定義したAWS Provider（regionにはus-east-1を指定してください）
provider "aws" {
  alias  = "cloudfront_provider"
  region = "us-east-1"
}

module "api_gateway_rest_api" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/api_gateway?ref=v0.2.6"

  providers = {
    aws.default_provider    = aws.default_provider
    aws.cloudfront_provider = aws.cloudfront_provider
  }

  api_gateway_settings = {
    api_name        = "sample-api",
    api_description = "Managed by Terraform"
    api_endpoint_config = {
      endpoint_types   = ["EDGE"],
      vpc_endpoint_ids = null
    }
    # api_option_configには、設定が必要な項目のみセットしてください
    api_option_config = {
      api_policy_path = "${path.module}/api_gateway_policy.json" # このサンプルコードと同じディレクトリにファイルを配置して実行してください。ファイルの記載例は[こちら](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/apigateway-resource-policies-examples.html)を参照してください
    }
    api_resources = {
      "/" = {
        get = {
          lambda    = "mock"
          status_code = 200
          content_type = "text/html"
          response_template = "<html>\n<body>\n<h1>Hello World!</h1>\n</body>\n</html>"
        }
      }
      "/pets" = {
        enable_cors = {
          allow_methods = "DELETE,GET,OPTIONS,PATCH"
          allow_headers = "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token"
          allow_origin = "*"
        }
        get = {
          lambda           = "pets_get_lambda_function:$${stageVariables.alias}" # Lambda関数はこのサンプルコードを実行する前に作成しておいてください
          lambda_aliases   = ["default"]                                         # エイリアスでの管理が不要の場合は削除可
          authorizer       = "authorize_lambda_function"
          api_key_required = true
        }
        post = {
          lambda           = "pets_post_lambda_function:$${stageVariables.alias}"
          lambda_aliases   = ["default"]
          authorizer       = null
        }
      }
      "/pets/{petID}" = {
        get = {
          lambda           = "pets_petid_get_lambda_function:$${stageVariables.alias}"
          lambda_aliases   = ["default"]
          authorizer       = null
        }
      }
    }
    api_security_schemes = {
      authorize_lambda_function = {
        authorizer_parameter_name = "Authorization"
        authorizer_parameter_in   = "header"
        request_or_token_type     = "token"
        request_identity_source   = null
        authorizer_function_name  = "authorize_lambda_function:$${stageVariables.alias}" # Lambda関数はこのサンプルコードを実行する前に作成しておいてください
        authorizer_aliases        = ["default"]                                          # エイリアスでの管理が不要の場合は削除可
        authorizer_credentials    = (authorize_lambda_functionの実行を許可するロール)
        authorizer_ttl            = "300"
      }
    }
  }
  stages = {
    "default" = {
      stage_variables               = { alias = "default" }            # エイリアスでの管理が不要の場合は削除可
      xray_tracing_enabled = true
      custom_access_log_group_name  = "default-log"
      custom_access_log_format_path = "${path.module}/logformat.json"
      force_redeployment_string     = filesha1("${path.module}/main.tf") # 自身のハッシュ値を設定することで、変更のたびに再デプロイされるようにする
      log_throttling = {
        "/*/*" = {
          enable_metrics         = true
          logging_level          = "INFO"
          throttling_rate_limit  = 5
          throttling_burst_limit = 5
        }
        "/pets/get" = {
          enable_metrics         = false
          logging_level          = "INFO"
          throttling_rate_limit  = 15
          throttling_burst_limit = 15
        }
      }
    },
  }
  custom_domains = {
    "custom_domain.example.com" = {
      custom_zone_name = "example.com"
      base_path_mapping = {
        "/" = {
          stage_name = "default"
        }
      }
    }
  }
  api_keys = {
    "sample_api_key" = {
      api_key_enabled = true
      api_usage_plan = {
        quota = {
          limit  = 10
          offset = 2
          period = "WEEK"
        }
        throttling = {
          burst_limit = 5
          rate_limit  = 5
        }
        api_usage_plan_apply_stages = [
          "default"
        ]
      }
    }
  }
  api_responses = {
    "UNAUTHORIZED" = {
      status_code        = "401"
      response_templates = {
        "application/json" = "{\"message\":$context.error.messageString}"
      }
      response_parameters = {
        "gatewayresponse.header.Authorization" = "'Basic'"
      }
    }
    "AUTHORIZER_FAILURE" = {
      status_code        = "500"
      response_templates = {
        "application/json" = "{\"message\":$context.error.messageString}"
      }
      response_parameters = {
        "gatewayresponse.header.Authorization" = "'Basic'"
      }
    }
  }
}
```

## 関連するpattern

`api_gateway pattern`に関連するpatternを以下に記載します。

| pattern名                 | 説明                                                                |
| ------------------------- | ------------------------------------------------------------------ |
| [network](./network.md)   | `Amazon API Gateway`を`PRIVATE`で作成する場合のVPCエンドポイントを作成する |
| [lambda](./lambda.md)     | `Amazon API Gateway`の統合先となる`AWS Lambda`関数を作成する            |
| [webacl](./webacl.md)     | `Amazon API Gateway`のアクセス制限を行う`AWS WAF`を作成する             |

## ログの集約

`api_gateway pattern`では、AWS Lambda関数のログをAmazon CloudWatch Logsに出力します。

このログは、[datadog_log_trigger pattern](datadog_log_trigger.md)を使用することでDatadogに集約できます。

## 入出力リファレンス

${TF_DOC}
