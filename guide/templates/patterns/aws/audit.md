# audit pattern

## 概要

`audit pattern`モジュールでは、AWSアカウントのアクティビティや運用監査を行うためのログを保存します。

このpatternで保存されたアクティビティログは、監査のための証跡として使用します。

## 想定する適用対象環境

`audit pattern`は、Delivery環境およびRuntime環境のいずれでも使用されることを想定しています。

## 依存するpattern

`audit pattern`は、他のpatternの実行結果に依存しません。
独立して実行できます。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                         | 説明                                                                                            |
| :----------------------------------------------------------------- | :---------------------------------------------------------------------------------------------- |
| [AWS CloudTrail](https://aws.amazon.com/jp/cloudtrail/)            | 入力変数で指定された設定で、AWSアカウントのアクティビティや監査ログを保存します                 |
| [Amazon S3](https://aws.amazon.com/jp/s3/)                         | 入力変数で指定したbucketに対して、CloudTrailが収集したログを保存します                          |
| [AWS Key Management Service (KMS)](https://aws.amazon.com/jp/kms/) | 入力変数で指定したCloudTrailのログを暗号化するための、カスタマーマスターキー（CMK）を作成します |

## モジュールの理解に向けて

AWS CloudTrail(以降、CloudTrail)は、AWSアカウントで構築されたマネージドサービスのアクティビティログを収集し、保存します。  
CloudTrailが記録するAWSサービスとイベントは、以下のドキュメントを参照してください。

[CloudTrail サポートされるサービスと統合 - AWS CloudTrail](https://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/cloudtrail-aws-service-specific-topics.html)

Eponaでは、`audit pattern`のインスタンスは各環境のリージョン内に1つ作成し、対象環境のAWSアカウントにおけるアクティビティログを保存する運用を想定しています。

:information_source: `audit pattern`を適用することで、証跡が1つ作成されます。

`audit pattern`を用いることで、以下のメリットが生まれることを想定しています。

* セキュリティ監査に用いるためのログ収集
* AWSアカウントに対する不審な操作の記録

CloudTrailで収集したログは、データに対して高いセキュリティ機能を設定できるAmazon S3(以降、S3)に格納しています。

また、`audit pattern`で構築したCloudTrailでは、デフォルトで収集したログをAWS KMSで管理するカスタマーマスターキー(CMK)を用いて暗号化しています。
`audit pattern`が構築されたアカウントのIAMエンティティがS3への参照権限を持っている場合、復号化されたログを参照できます。

その他のCloudTrailを用いたセキュリティのプラクティスは以下を参照してください。

* [AWS CloudTrail におけるセキュリティのベストプラクティス](https://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/best-practices-security.html)

## CloudTrailで記録されるイベントを理解する

CloudTrailで記録するイベントは3種類あり、`autit pattern`と課金の関係は以下のようになっています。

| CloudTrailイベント名 | 概要                              | `audit pattern`のデフォルト設定 | 課金                         |
| --------------- | ------------------------------- | ----------------------- | -------------------------- |
| 管理イベント          | AWSアカウントのリソースで実行される管理オペレーション    | 全て記録する                  | 2つ目の証跡から、記録される管理イベントに応じて課金 |
| データイベント         | リソース上またはリソース内部で実行されるリソースオペレーション | 記録されない                  | 記録されたイベント数に応じて課金           |
| インサイトイベント       | AWSアカウント内の異常なアクティビティを記録         | 記録されない                  | 記録されたイベント数に応じて課金           |

それぞれのイベントの詳細は、[CloudTrailの概念 - AWS CloudTrail](https://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/cloudtrail-concepts.html#cloudtrail-concepts-events)のドキュメントを参照してください。

これらのイベントのうち、特にデータイベントはS3へのデータ操作やLambda関数の呼び出しなどが対象となります。  
よって、データイベントの記録を有効にすると大量のイベントが記録される傾向にあります。

:information_source: データイベントの記録できる対象は、Amazon S3、AWS Lambda、Amazon DynamoDBです。

データイベントはこの性質のため思わぬ課金の原因となる可能性があり、`audit pattern`ではデフォルトで記録しない設定としています。

一方で以下のpatternでは、CloudTrailでS3へのデータイベントが記録されることを前提としています。

* [cd_pipeline_frontend_trigger pattern](./cd_pipeline_frontend_trigger.md)
* [cd_pipeline_backend_trigger pattern](./cd_pipeline_backend_trigger.md)

これらCDパイプラインのpatternと組み合わせる場合は、S3のデータイベントを記録するように設定する必要があります。

データイベントを記録する場合、入力パラメータ`event_data_resources`を設定します。  
設定方法については、[入出力リファレンス](./#入出力リファレンス)を参照してください。

また、AWSリソースではAWS Key Management Service(以降、KMS)を使用した暗号化がよく行われます。  
CloudTrailではKMSの暗号化、複合化も記録するため、特にS3をKMSを使用して暗号化を行った場合などに多数の管理イベントが発生します。

CloudTrailの証跡は1つのリージョンに1つ作成すれば十分ですが、なんらかの理由で複数作成する場合はKMSの管理イベントの課金が問題になることがあります。

KMSによる暗号化と複合化のログを除外する場合は、マネジメントコンソールから`AWS KMSイベントの除外`のチェックボックスを有効にしてください。

[証跡からの AWS KMS イベントの除外](https://docs.aws.amazon.com/ja_jp/kms/latest/developerguide/logging-using-cloudtrail.html#filtering-kms-events)

:information_source:  
現時点のTerraform AWS Provider（3.15.0）では、KMSの管理イベントを除外できません。  
マネジメントコンソールからの操作が必要になります。

## 多要素認証によるS3バケットのバージョン保護を有効にする

`audit pattern`で構築されたCloudTrailでは、悪意のある第三者による不正なアクセスや
オペレーションミス等によるログの変更/削除に対して整合性が検証できるようになっています。

> :information_source: ログに対する変更、削除の整合性検証は、AWS CLIやAWS SDKを用いたカスタム実装により検証できます。
>
> * [AWS CLI による CloudTrail ログファイルの整合性の検証](https://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/cloudtrail-log-file-validation-cli.html)
> * [CloudTrail ログファイルの整合性検証のカスタム実装](https://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/cloudtrail-log-file-custom-validation.html)

それに加えて、予防的な対策としてAmazon S3に記録しているログ（オブジェクト）のバージョンに対して多要素認証(MFA)を用いた保護をかけることがセキュリティ観点で推奨されています。

MFAを用いたS3バケットの保護を設定することで、S3バケットのバージョンやオブジェクトのバージョンを削除する際に、rootユーザーによる追加の認証を求めることができます。
S3バケットのMFAによるバージョン保護はTerraformやAWSマネジメントコンソールで設定ができないため、設定を有効にする場合は以下の手順を実施してください。
:information_source: この設定を有効にする場合、S3のログをアーカイブできません。

:warning: **ここで示す手順はrootユーザーのcredential情報(アクセスキー)を用います。注意して実行してください。**

設定に必要なrootユーザーのセキュリティ設定情報は以下です。

* 多要素認証(MFA)の設定と、設定後に表示されるシリアル番号(ARN)
* AWS CLIを実行するためのアクセスキー(アクセスキーIDとシークレットアクセスキー)

------

1. `audit pattern`をTerraformで構築する

    適切に入力変数を設定して、`audit pattern`を実行してください。

    この設定を有効にする場合は`bucket_transitions`を設定しないでください。
    `audit pattern`で構築されたS3にライフサイクル設定がされている場合、以下のコマンドで削除してください。

      ```bash
      $ aws s3api delete-bucket-lifecycle --bucket [audit patternで設定したS3バケット名]
      ```

1. rootユーザーで必要なセキュリティ設定情報を取得する

    [AWS Management Console](https://console.aws.amazon.com/console/home)にアクセスし、rootユーザーでコンソールにログインをしてください。

    [アカウント認証情報]のページにアクセスし、MFAデバイスが登録されていない場合は登録してください。  
    登録完了後、シリアルナンバーの項目に表示される`ARN`を記録しておきます。

    [アカウント認証情報]のページでAWS CLIを利用するためのアクセスキーを発行してください。  
    :warning: **rootユーザーのアクセスキーを発行することは、一般的にはバッドプラクティスと言われています。設定が完了したら、即座にアクセスキーを"削除"もしくは"無効化"してください。**

1. AWS CLIを用いて、s3バケットのバージョニングに対してのMFA保護を有効化する

    rootユーザーで発行したアクセスキーを利用して、以下のコマンドを実行してください。

      ```bash
      $ aws s3api put-bucket-versioning --bucket [audit patternで設定したS3バケット名] \
      --versioning-configuration Status=Enabled,MFADelete=Enabled \
      --mfa '[MFAデバイスのARN] [MFAデバイスで発行されたトークン]'  # ARNとトークンの間は半角スペースが必要
      ```

1. S3バケットの設定を確認する

    :information_source: **以下のコマンドは、rootユーザーので実施する必要はありません。**

    S3へアクセスできる認証情報を用いて、以下のコマンドを実行してください。

      ```bash
      $ aws s3api get-bucket-versioning --bucket [audit patternで設定したS3バケット名]
      # 出力結果
      {
      "Status": "Enabled",
      "MFADelete": "Enabled"
      }
      ```

    上記のMFADeleteの項目が `"Enabled"` となっていた場合、正しく設定できていますので**即座に次の手順を参考にrootユーザーのアクセスキーを削除してください。**

1. rootユーザーのアクセスキーを削除する

    :warning: **アクセスキーが悪意のある第三者へ漏洩し不正に利用された場合、対象のAWSアカウントに甚大な被害をもたらす恐れがあります。**

    **特にrootユーザーのアクセスキーは非常に強い権限を持っています。rootユーザーのアクセスキーは必要になった場合のみ発行し、不要になったら、即座に削除してください。**

    [AWS Management Console](https://console.aws.amazon.com/console/home)にアクセスし、rootユーザーでコンソールにログインをしてください。

    [アカウント認証情報]のページで、2の手順で発行したアクセスキーの"削除"をしてください。  

## 多要素認証によるS3バケットのバージョン保護を無効にする

S3バケットのバージョン保護を無効化する場合は、以下の手順を実施してください。

1. rootユーザーで必要なセキュリティ設定情報を取得する

    有効化手順の"rootユーザーで必要なセキュリティ設定情報を取得する"の手順を参考にrootユーザーのアクセスキーを発行する。

1. AWS CLIを用いて、s3バケットのバージョニングに対してのMFA保護を無効にする

      ```bash
      $ aws s3api put-bucket-versioning --bucket [audit patternで設定したS3バケット名] \
      --versioning-configuration Status=Enabled,MFADelete=Disabled \
      --mfa '[MFAデバイスのARN] [MFAデバイスで発行されたトークン]'  # ARNとトークンの間は半角スペースが必要
      ```

1. rootユーザーのアクセスキーを削除する

    :warning: **アクセスキーが悪意のある第三者へ漏洩し不正に利用された場合、対象のAWSアカウントに甚大な被害をもたらす恐れがあります。**

    **特にrootユーザーのアクセスキーは非常に強い権限を持っています。rootユーザーのアクセスキーは必要になった場合のみ発行し、不要になったら、即座に削除してください。**

    有効化手順の"rootユーザーのアクセスキーを削除する"の手順を参考にrootユーザーのアクセスキーを削除する。

## サンプルコード

`audit pattern`を使用したサンプルコードを、以下に記載します。

<!-- markdownlint-disable MD046 -->
<!--
    リスト内のコードブロックをバッククォート３つのfencedスタイルを使用した場合、mkdocsでの出力結果が上手くいかない。
    このため、リスト内のコードブロックはインデントを利用したindentedスタイルを使用している。
    しかし、その結果コードブロックのスタイルが混在してしまい、markdownlintがエラーになる。
    リスト内でfencedスタイルが使用できないので、この部分のmarkdownlintを無効化している。
-->

```terraform
module "audit" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/audit?ref=v0.2.6"

  trail_name = "my-trail-name"
  trail_tags = {
    # 任意のタグ
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }

  bucket_name          = "my-trail-bucket"
  bucket_mfa_delete    = false

  kms_key_management_arns = [
    # CMKを参照/変更/削除する権限が必要なIAMエンティティのARN
    "arn:aws:iam::[AWS Account ID]:role/TerraformExecutionRole",
  ]
}
```

<!-- markdownlint-enable MD046 -->

## 関連するpattern

`audit pattern`に関連するpatternを、以下に記載します。

| pattern名                                                                  | 説明                                                    |
| ------------------------------------------------------------------------- | ----------------------------------------------------- |
| [cd_pipeline_frontend_trigger pattern](./cd_pipeline_frontend_trigger.md) | パイプラインの起動のためには、`autit pattern`でS3のデータイベントを記録する必要があります |
| [cd_pipeline_backend_trigger pattern](./cd_pipeline_backend_trigger.md)   | パイプラインの起動のためには、`autit pattern`でS3のデータイベントを記録する必要があります |

## ログの集約

`audit pattern`では、AWS Lambda関数のログをAmazon CloudWatch Logsに出力します。

このログは、[datadog_log_trigger pattern](datadog_log_trigger.md)を使用することでDatadogに集約できます。

## 入出力リファレンス

${TF_DOC}
