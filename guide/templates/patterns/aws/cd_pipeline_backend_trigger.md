# cd_pipeline_backend_trigger pattern

## 概要

`cd_pipeline_backend_trigger pattern`モジュールは、バックエンドシステムをRuntime環境にデプロイするトリガとなるイベント[^1]を生成します。

[^1]: CloudWatchイベント

対となる`cd_pipeline_backend pattern`と本patternを連携させることにより、Runtime環境への自動的なデプロイ[^2]を実現可能です。両patternを適用したときに構成されるアーキテクチャを図示すると以下のようになります。

![アーキテクチャ](../../resources/cd_pipeline_backend_trigger.png)

[^2]: [Blue/Green Deployment](https://docs.aws.amazon.com/ja_jp/codedeploy/latest/userguide/welcome.html#welcome-deployment-overview-blue-green)

## 想定する適用対象環境

`cd_pipeline_backend_trigger pattern`は、Delivery環境での使用を想定しています。

## 依存するpattern

`cd_pipeline_backend_trigger pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                               | 利用する情報             |
| :-------------------------------------- | :----------------------- |
| [ci_pipeline pattern](./ci_pipeline.md) | Dockerコンテナレジストリ |

本patternが依存するリソースを他の構築手段で代替する場合は、依存するpatternと[入出力リファレンス](#入出力リファレンス)の内容を参考に、本patternが必要とするリソースを構築してください。

また、本patternは既述の通り`cd_pipeline_backend pattern`との連携を意図しています。
この連携の際は`cd_pipeline_backend pattern`が構築するアーティファクトストアへアクセスすることにるるため、アーティファクトストア、およびその暗号化鍵が必要になります。

| pattern名                                               | 利用する情報                             |
| :------------------------------------------------------ | :--------------------------------------- |
| [cd_pipeline_backend pattern](./cd_pipeline_backend.md) | アーティファクトストア、およびその暗号鍵 |
| [audit pattern](./audit.md) | S3バケットへのオブジェクト配置のイベント検知 |

:warning: アーティファクトストア、およびその暗号化鍵は`cd_pipeline_backend pattern`で構築されるものであるため、patternの適用順序に制約が生まれます。こちらについては[patternの適用順序](#patternの適用順序)を参照してください。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                               | 説明                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| :----------------------------------------------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Amazon S3](https://aws.amazon.com/jp/s3/)(以下、S3)                                                                     | デプロイに必要な設定ファイル群(zip)を配置するバケットです。設定ファイルとしては、[Amazon Elastic Container Service](https://aws.amazon.com/jp/ecs/)(以下、ECS)の[タスク定義](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/developerguide/task_definitions.html)、[AWS CodePipeline](https://aws.amazon.com/jp/codepipeline/)(以下、CodePipeline)および[AWS CodeDeploy](https://aws.amazon.com/jp/codedeploy/)(以下、CodeDeploy)が参照する[アプリケーション仕様ファイル](https://docs.aws.amazon.com/ja_jp/codedeploy/latest/userguide/application-specification-files.html)を想定しています。 |
| [Amazon CloudWatch Events](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/events/WhatIsCloudWatchEvents.html) | デプロイ実行のトリガとなるイベントを発生させるルールを定義します                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| [Amazon EventBridge](https://aws.amazon.com/jp/eventbridge/)                                                             | 異なるAWSアカウント間でイベントを伝搬させるための経路(イベントバス)を構築します                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |

## モジュールの理解に向けて

ここでは、`cd_pipeline_backend_trigger pattern`が想定している環境や使い方、その背景について記載します。

### `cd_backend_pipeline_trigger pattern`が実現するフロー

`cd_backend_pipeline_trigger pattern`を適用することで、Runtime環境へのデプロイを開始するトリガイベント[^1]を生成できるようになります。当該のイベントが生成されるのは以下の2つのケースです。

- デプロイ設定用のS3バケットに、デプロイに関する設定をまとめたzipファイルがアップロードされる
- バックエンドシステムを構成するDockerイメージが[Amazon ECR](https://aws.amazon.com/jp/ecr/)(以下、ECR)にプッシュされる

このイベントはイベントバスを経由してデプロイ対象のRuntime環境へと伝搬し、[`cd_pipeline_backend pattern`](./cd_pipeline_backend.md)によって捕捉され、デプロイが実行されます。

つまり本patternと`cd_pipeline_backend pattern`を連携させることにより、デプロイ設定の変更、あるいはコンテナイメージの更新をトリガにした継続的デプロイを実現できます。

### 前提事項

`cd_pipeline_backend_trigger pattern`は、以下の前提事項のもとで利用されることを想定しています。

- アプリケーションがAWS Fargate環境で動作すること
  - アプリケーションは、ECSサービスを利用した常駐形(Webアプリ、APIサーバー等)であること
- バックエンドシステムを構成するアプリケーションはコンテナ化され、そのイメージがECRに保存されていること
- CloudTrailでECSサービスの設定ファイルを格納するS3バケットのデータイベントが記録されるようになっていること

----

:information_source:

デプロイ開始のイベントを捕捉する際、CloudTrailの
[データイベントログ記録](https://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/logging-data-events-with-cloudtrail.html)機能を利用しています。

`audit pattern`を用いてCloudTrailを構成している場合、当該機能を有効化する方法については[audit patternのドキュメント](./audit.md)を参照してください。
Eponaを用いずにCloudTrailを構成している場合は、[証跡データイベントのログ記録](https://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/logging-data-events-with-cloudtrail.html)を
ご参照ください。

----

### デリバリシステムとの分離

上図の通り、EponaではデリバリシステムをRuntime環境に配置し、コンテナリポジトリを配置するDelivery環境とは環境を分離することにしました。この理由は、以下の2点になります。

1. 実質的なデプロイ処理[^3]をRuntime環境で完結できる
1. テスト済のコンテナイメージと同一のイメージをRuntime環境上にデプロイすることを保証できる

[^3]: CodePipeline、CodeDeployによるデプロイ処理を指しています

アプリケーションのデプロイはエンドユーザーにも大きな影響を及ぼし得るため、
適切な権限管理の元で実行する必要があります。
実質的なデプロイ処理をRuntime環境で完結させることができれば権限管理をシンプルにでき、意図しない、あるいは不適切なデプロイを防止できます。このためCodePipeline、CodeDeployをECRと同じDelivery環境ではなく、Runtime環境に配置するアーキテクチャとしました。

一方でデプロイされるコンテナイメージについては、テスト済イメージを各Runtine環境で使い回す方が安全です。このため、ECRはDelivery環境に配置し、当該リポジトリでコンテイメージを一元管理するようにしています。

### patternの適用順序

本patternは既述の通り`cd_pipeline_backend pattern`との連携を意図しています。
この連携を実現しようとすると、以下の順でpatternを適用する必要がある点にご注意ください。

1. Delivery環境に対する`cd_pipeline_backend_trigger pattern`の適用
2. Runtime環境に対する`cd_pipeline_backend pattern`の適用
3. 2.で作成したリソースのARNをパラメータに設定し、Delivery環境へ`cd_pipeline_backend_trigger pattern`を再適用

このような煩雑な手順を実行する理由を以下に説明します。

`cd_pipeline_backend pattern`との連携のため、本patternではクロスアカウントアクセス用のIAM Role[^4]を作成します。
そしてこのIAM Roleは、下図の通りRuntime環境、Delivery環境の必要なリソースへのアクセス権限を持ちます。

![クロスアカウントアクセス](./../../resources/cd_pipeline_backend_trigger_cross_account.png)

このリソースへのアクセス権限を当該のIAM Roleへ与えるためには、Runtime環境上に構築されたリソースの[ARN](https://docs.aws.amazon.com/ja_jp/general/latest/gr/aws-arns-and-namespaces.html)が必要です。
`cd_pipeline_backend pattern`をRuntime環境に適用後、再度本patternをDelivery環境に再適用する必要があるのはこのためです。

また、必要なARNはRuntime環境およびRuntime環境用Stateに保存されています。
しかし、Delivery環境からRuntime環境のStateを参照することはEponaのセキュリティ方針として許容しません（逆方向は許容しています）。
このため、ARNはRemoteStateの参照ではなく`.tf`ファイルへ直接記述することとなります。

実際のイメージは、[サンプルコード](#サンプルコード)を参照してください。

[^4]:クロスアカウントアクセスについては、例えば[チュートリアル: AWS アカウント間の IAM ロールを使用したアクセスの委任](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/tutorial_cross-account-with-roles.html)をご参照ください。

## よりスムーズなデプロイメントパイプラインの実現に向けて

本パターンを適用することにより、S3バケットへの設定ファイルのアップロードあるいはECRに対するコンテナイメージのプッシュによって、デプロイが開始されるようになります。
`ci_pipeline pattern`で構築されるCIパイプラインに対して以下の仕組みを組み込めば、ソース・設定ファイルを修正するだけでRuntime環境にデプロイできる仕組みを構築できます。

- 設定ファイルが修正されたら、S3バケットに当該設定ファイルをアップロードする
- ソースが修正されたらコンテナをビルドしECRリポジトリにPUSHする

これらの仕組みは [`ci_pipeline pattern`](./ci_pipeline.md) で記載の通り、`gitlab-ci.yml` に記述できます。
どのような内容になるかは各サービスごとに異なりますが、具体的な記述例としては[example-cahtリポジトリの`.gitlab-ci.yml`](https://github.com/Fintan-contents/example-chat/blob/master/.gitlab-ci.yml)を参照してください。

## サンプルコード

`cd_pipeline_backend_trigger pattern`を使用したサンプルコードを、以下に記載します。

```terraform
locals {
  runtime_account_id = "[Runtime環境のアカウントID]"
}

data "aws_region" "current" {}

module "cd_pipeline_backend_trigger" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend_trigger?ref=v0.2.6"

  name                 = "my-backend-cd-pipeline"
  bucket_name          = "my-backend-pipeline-source"
  bucket_force_destroy = false
  runtime_account_id   = local.runtime_account_id

  # リポジトリ名と、その ARN およびデプロイ対象タグのマップ
  ecr_repositories = {
    for name, arn in data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns :
    name => {
      arn  = arn
      tags = ["staging"]
    }
  }

  target_event_bus_arn = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"
  # CodePipeline が用いる artifact store のバケット ARN
  # Runtime環境上に作成するバケット名をbucket_nameとすると、arn:aws:s3:::bucket_name の形式で記載してください
  # see: https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/dev/s3-arn-format.html
  artifact_store_bucket_arn = "arn:aws:s3:::my-artifact"

  # 以下は Runtime環境上で cd_pipeline_backend pattern を動かしてから設定する
  artifact_store_bucket_encryption_key_arn = "arn:aws:kms:ap-northeast-1:${local.runtime_account_id}:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
```

## 関連するpattern

`cd_backend_pipeline_trigger pattern`に関連するpatternを、以下に記載します。

| pattern名                                                 | 説明                                                                                                                                                                                                  |
| :-------------------------------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [`ci_pipeline pattern`](./ci_pipeline.md)                 | `ci_pipeline pattern`により、GitLabへの変更のPUSHをトリガにして、Amazon ECRへのコンテナイメージのPush、S3バケットへの設定ファイルをPUSHできます。これによりデプロイを開始するイベントを生成できます。 |
| [`cd_pipeline_backend pattern`](./cd_pipeline_backend.md) | `cd_pipeline_backend_trigger pattern`によるイベントをもとに、AWS Fargateクラスターへのデプロイを行うパイプラインを起動します                                                                          |

## 入出力リファレンス

${TF_DOC}
