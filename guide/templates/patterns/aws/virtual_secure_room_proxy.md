# virtual_secure_room_proxy pattern

## 概要

`virtual_secure_room_proxy pattern`モジュールは、VPC内から外部へのアクセスに制限をかけるProxyを作成します。  
本patternで作成されるProxyは本番環境からの情報漏洩防止を目的としており、仮想セキュアルーム内で利用することを想定しています。  
仮想セキュアルームについては以下のガイドをご参照ください。

* [virtual_secure_room_directory_service pattern](./virtual_secure_room_directory_service.md)
* [virtual_secure_room_workspaces pattern](./virtual_secure_room_workspaces.md)

[![運用想定図](../../resources/virtual_secure_room_proxy_howto.drawio.svg)](../../resources/virtual_secure_room_proxy_howto.drawio.svg)

## 想定する適用対象環境

`virtual_secure_room_proxy pattern`は、Runtime環境で使用されることを想定しています。

## 依存するpattern

`virtual_secure_room_proxy pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                               | 利用する情報                           |
|:----------------------------------------|:---------------------------------------|
| [network pattern](./network.md)         | プライベートサブネット                 |
| [ci_pipeline pattern](./ci_pipeline.md) | Dockerコンテナレジストリ（Amazon ECR） |

:warning:  
本patern実行前に、`ci_pipeline pattern`で作成されたECRにProxy用のコンテナイメージをPushしておく必要があります。  
コンテナイメージの存在しない状態で本patternを適用すると、コンテナのデプロイと失敗を延々と繰り返す影響により意図しない課金を発生させる可能性があります。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                    | 説明                                                           |
|:--------------------------------------------------------------------------------------------------------------|:---------------------------------------------------------------|
| [AWS Fargate](https://aws.amazon.com/jp/fargate/)                                                             | アプリケーションコンテナを動作させるサービスを構築します       |
| [Service Discovery](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/developerguide/service-discovery.html) | コンテナへのエンドポイントを提供するRoute 53の機能を構築します |

## モジュールの理解に向けて

* 本patternで作成されるProxyは、[Amazon ECS](https://aws.amazon.com/jp/ecs/)（以下、ECSとする）を利用してコンテナでデプロイしています
* Proxyのコンテナイメージを配置するコンテナレジストリには、Delivery環境に`ci_pipeline pattern`で作成したECRを使うことを想定しています
* コンテナへのアクセスはLBやDNSを利用したユニークな名前を使用することが一般的であり、本patternではECSの機能であるService Discoveryを利用しています

[![リソース構成図](../../resources/virtual_secure_room_proxy.drawio.svg)](../../resources/virtual_secure_room_proxy.drawio.svg)

### コンテナイメージのビルドについて

Eponaでは許可リスト形式でアクセス許可を行うことを想定しており、このための設定を適用してある
[コンテナイメージ](https://gitlab.com/eponas/epona/container_registry/1493395)
を提供しています。  
ただし、アクセスを許可したいドメインはユーザーや環境ごとに異なるため、基本的にはユーザー側でコンテナイメージをビルドする必要があります。  
以下の例を参考に、Eponaで用意しているコンテナイメージに許可リスト（`allowlist`）を埋め込んだコンテナイメージをビルドしてください。

:information_source:  
コンテナイメージのビルド方法については、[Dockerの公式ページ](https://docs.docker.jp/engine/reference/commandline/build.html)等をご参照ください。

`allowlist`（※アクセスを許可したいドメインを羅列して記載）

:warning:  
デフォルト設定として、AWS Management Consoleへのサインインに必須である以下のリストを記載しています。  
ただし、AWSの利用に必要なドメイン情報は公式から公開されていないため、独自に検証して確認したリストになります。

```text
.aws.amazon.com
.cloudfront.net
.aws.a2z.com
.amazonaws.com
.media-amazon.com
.awsstatic.com
```

`Dockerfile`

```Dockerfile
# Eponaで提供しているProxyのコンテナイメージをFROMに指定
FROM registry.gitlab.com/eponas/epona/proxy:v1.0.0
COPY ./allowlist /etc/squid/allowlist
```

なお、Eponaで提供しているコンテナイメージの
構成ファイルは[こちら](https://gitlab.com/eponas/epona/-/blob/master/container_images/virtual_secure_room_proxy/src/squid.conf)に置いてあります。
詳細を確認したい場合や、ユーザー側で自由にカスタマイズしたイメージを作成したい場合等にご参照ください。

:warning:  
Proxyのデフォルト設定では、
[RFC 1918に沿ったCIDRのプライベートアドレス](https://www.nic.ad.jp/ja/translation/rfc/1918.html)からのみ
Proxyを使用できるようになっています。  
そのため、仮想セキュアルームを配置するPrivate Subnetに上記CIDR以外のCIDRを設定する場合は
設定ファイル（`squid.conf`）変更とコンテナイメージビルドが必要になります。

### コンテナイメージのPushとコンテナデプロイについて

#### 手動で行う方法

本patternでは、コンテナイメージのPushやコンテナイメージ変更によるコンテナの再デプロイは手動で行うことを想定しています。
具体的には、ECRにアクセス可能な環境からコンテナイメージをタグ付きでPushし、
その後に本patternの`proxy_image_tag`変数を書き換えてApplyすることで行います。  
Applyが成功すると`proxy_image_tag`で指定したタグのコンテナイメージからコンテナのデプロイが行われ、自動的にアップデートされます。  
なお、なんらかの理由で新しいコンテナイメージからコンテナのデプロイに失敗した場合、デプロイ処理が延々と行われてしまいます。  
この状態を長期間続かせてしまうと意図しない課金を発生させる可能性がありますので、
デプロイ後は必ずクラスターのタスクがRUNNING状態になっているか確認することを推奨します。

##### コンテナイメージbuildとPushのコマンド例

```bash
AWS_DEFAULT_REGION=<リージョン名>
REPOSITORY_URL=<リポジトリURL>
IMAGE_TAG=<リポジトリに配置するイメージに付けるタグ>
# コンテナイメージにタグを付与してビルド
docker build . --tag ${REPOSITORY_URL}:${IMAGE_TAG}
# コンテナレジストリへログイン
aws ecr get-login-password --region ${AWS_DEFAULT_REGION} | docker login --username AWS --password-stdin ${REPOSITORY_URL}
# ECRへコンテナイメージをPush
docker push ${REPOSITORY_URL}
```

:information_source:  
ECRのリポジトリURLは、`ci_pipeline pattern`実行後に以下の名前で出力されます。  
`container_image_repository_urls[リポジトリ名]`

#### GitLab CI/CDを利用する方法

[GitLab CI/CDによるアプリケーションのビルド・デプロイガイド](../../how_to/gitlab_aplication_build_deploy.md)をご確認ください。

### Service Discoveryを利用したコンテナへのアクセスについて

通常コンテナはデプロイするたびにIPが変更されるため、クラスター外からコンテナへアクセスする際は都度IPを設定し直す必要が出てきてしまいます。  
この問題に対し、本patternではService Discoveryを利用する解決策を採用しています。
Service Discoveryについては以下の記事をご参照ください。  
[Amazon ECS サービスディスカバリ](https://aws.amazon.com/jp/blogs/news/amazon-ecs-service-discovery/)

:warning:  
仮想セキュアルームは`Microsoft AD`を利用しています。
ただし、当該ADのDNSサーバーはVPC-provided DNSへ自動的にリクエストをフォワードしません。  
このためService Discoveryを利用する際は、`Microsoft AD`のDNSサーバーへDNSフォワーダー設定が必要になります。
詳細や手順については[こちら](./virtual_secure_room_directory_service.md)をご参照ください。

### Proxyの設定方法について

Terraform適用後に出力されるProxyのエンドポイント（`internal_container_endpoint`）を
WorkSpacesごとに手動で設定します。  
設定方法は通常のWindowsでのProxy設定と同様なため、以下ページなどをご参照ください。

[Internet Explorer と共にプロキシサーバーを使用する](https://docs.microsoft.com/ja-jp/troubleshoot/browsers/use-proxy-servers-with-ie)

:warning:  
現時点では特にProxy利用必須とする仕組みの検討をしていないため、Proxyを設定しなくてもWorkSpacesの利用が可能になっています。  
ただし、本番運用中の環境でProxyを利用せずにWorkSpacesを利用することはセキュリティの観点から非推奨な利用方法となります。  
やむを得ず利用する場合は、リスクを理解した上でご利用ください。

## サンプルコード

`virtual_secure_room_proxy pattern`を使用したサンプルコードを、以下に記載します。

```terraform
locals {
  # ci_pipeline patternで作成された
  ecr_repository = "[ecr-repository-url]
}

module "virtual_secure_room_proxy" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/virtual_secure_room_proxy?ref=v0.2.6"

  name_prefix       = "test"
  vpc_id            = data.terraform_remote_state.network.outputs.network.vpc_id
  container_subnets = data.terraform_remote_state.network.outputs.network.private_subnets

  service_discovery_namespace_name = "epona.internal"
  service_discovery_service_name   = "proxy"

  # FIXME: repository_urlには、Proxy用のコンテナイメージをPushした`ci_pipeline pattern`で作成されたECRのurlを指定
  repository_url        = "xxxxxxxxxxxxxxxx..dkr.ecr.ap-northeast-1.amazonaws.com/epona-internal-proxy"
  proxy_image_tag       = "v1.0"
  container_task_cpu    = "256"
  container_task_memory = "512"

  default_ecs_task_execution_iam_policy_name = "DefaultContainerServiceTaskExecution"
  default_ecs_task_execution_iam_role_name   = "DefaultContainerServiceTaskExecutionRole"

  tags = {
    Owner              = "john"
    Environment        = "runtime"
    RuntimeEnvironment = "development"
    ManagedBy          = "epona"
  }
}
```

### 作成されるリソース構成

## 関連するpattern

`virtual_secure_room_proxy pattern`に関連するpatternを、以下に記載します。

| pattern名                                                                                   | 説明                                                                   |
|:--------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------|
| [virtual_secure_room_directory_service pattern](./virtual_secure_room_directory_service.md) | WorkSpacesに接続するユーザーおよびパスワードを管理するDirectoryService |
| [virtual_secure_room_workspaces](./virtual_secure_room_workspaces.md)                       | Workspacesをデプロイするパターン                                       |

## 入出力リファレンス

${TF_DOC}
