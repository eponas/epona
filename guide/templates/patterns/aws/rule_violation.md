# rule_violation pattern

## 概要

`rule_violation pattern`は、AWS環境に対する構成変更を監視し、本patternで定義したルールに違反する変更を検知した場合にはチャットツールへの通知を実施するモジュールです。

## 想定する適用対象環境

`rule_violation pattern`は、Delivery環境およびRuntime環境のいずれでも使用されることを想定しています。

## 依存するpattern

`rule_violation pattern`には、事前に実行が必要なpatternはありません。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                           | 説明                                                                             |
| :----------------------------------------------------------------------------------- | :------------------------------------------------------------------------------- |
| [AWS Config](https://aws.amazon.com/jp/config/)                                      | 事前に設定したルールをもとに、構成変更を監視するサービス                         |
| [S3バケット](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/dev/UsingBucket.html) | 構成変更に関するログを保存するバケット                                           |
| [AWS Lambda](https://aws.amazon.com/jp/lambda/)                                      | ルールに違反する構成変更があった場合に、チャットツールへ通知するための関数       |
| [AWS SNS](https://aws.amazon.com/jp/sns/)                                            | AWS Configからイベントを受け取り、チャットツールに通知する関数を呼び出すトピック |
| [AWS IAM](https://aws.amazon.com/jp/iam/)                                            | AWS ConfigやAWS Lambdaが使用するIAMポリシー、ロール                              |

## モジュールの理解に向けて

継続的にサービス開発を行い、AWS環境の構成の変更が続いていく中で、誤設定によるセキュリティリスクや予期しない構成変更を人手で監視し続けるのは困難です。  
AWS Configを利用することで、構成変更を監視し、ルールに違反する変更を自動的に検知することが可能になります。  
本モジュールではAWS Configサービス本体の立ち上げに加え、デフォルトの構成ルールの作成と、ルール違反を利用者に通知する仕組みが提供されます。

![イメージ](../../resources/rule_violation.png)

## デフォルトで設定される内容について

### AWS Config

リージョンごとにAWS Configを有効化します。複数リージョンにAWS Configの設定が必要となる場合は、リージョンごとに本モジュールを適用してください。

AWS Configは設定レコーダー(Configuration Recorder)と配信チャネル(Delivery Channel)というリソースを使用します。これらは
[1リージョンに1つのみ](https://docs.aws.amazon.com/ja_jp/config/latest/developerguide/gs-cli-subscribe.html)
作成できます。  
このため、すでにAWS Configが設定されているリージョンに本モジュールを適用する場合は、設定レコーダーや配信チャネルの作成をスキップする
必要があります。  
これは`create_config_recorder_delivery_channel`を`false`に設定することで実現が可能です。

本モジュールで作成したAWS Configでは、そのリージョンで監視可能な全リソースを監視対象とします。  
監視対象のリソースを限定する場合は`all_supported`を`false`に設定し、`recording_resource_types`に監視対象のリソースを指定してください。
[terraformのドキュメント](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/config_configuration_recorder)もあわせて参照してください。

なお、複数リージョンに対して本モジュールを適用する場合、IAMなどのグローバルリソースについては全リージョンで同じ情報が（重複して）記録されます。  
グローバルリソースを監視する必要のないリージョンについては、`include_grobal_resouces`を`false`とすることで監視しないよう設定できます。

6時間ごとに設定のスナップショットを取得しS3バケットに配信するように構成しています。配信間隔を変更する場合はパラメータの`delivery_frequency`に任意の値を設定してください。

### マネージドルール

以下のルールが設定されます。

| ルール名                                   | 説明                                                                                                                                                                          |
| :----------------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ACCESS_KEYS_ROTATED                        | maxAccessKeyAgeで指定された日数以内にアクティブなアクセスキーがローテーションされるかどうかを確認します。                                                                     |
| ACM_CERTIFICATE_EXPIRATION_CHECK           | アカウントのACM証明書に、指定された日数内に有効期限がマークされているかどうかを確認します。                                                                                   |
| CLOUD_TRAIL_ENCRYPTION_ENABLED             | AWS CloudTrailがサーバー側の暗号化（SSE）AWS Key Management Service（AWS KMS）カスタマーマスターキー（CMK）暗号化を使用するように構成されているかどうかを確認します。         |
| CLOUD_TRAIL_ENABLED                        | AWSアカウントでAWS CloudTrailが有効になっているかどうかを確認します。                                                                                                         |
| CLOUD_TRAIL_LOG_FILE_VALIDATION_ENABLED    | AWS CloudTrailがログ付きの署名済みダイジェストファイルを作成するかどうかを確認します。                                                                                        |
| DB_INSTANCE_BACKUP_ENABLED                 | RDS DBインスタンスでバックアップが有効になっているかどうかを確認します。                                                                                                      |
| EC2_INSTANCE_NO_PUBLIC_IP                  | Amazon Elastic Compute Cloud（Amazon EC2）インスタンスにパブリックIPアソシエーションがあるかどうかを確認します。                                                              |
| INSTANCES_IN_VPC                           | EC2インスタンスが仮想プライベートクラウド（VPC）に属しているかどうかを確認します。                                                                                            |
| ELB_LOGGING_ENABLED                        | Application Load BalancerとClassic Load Balancerのログが有効になっているかどうかを確認します。                                                                                |
| IAM_PASSWORD_POLICY                        | IAMユーザーのアカウントパスワードポリシーが指定された要件を満たしているかどうかを確認します。                                                                                 |
| IAM_POLICY_NO_STATEMENTS_WITH_ADMIN_ACCESS | 全リソースに対する全アクションをAllowするIAMポリシーを作成していないかを確認します。                                                                                          |
| IAM_ROOT_ACCESS_KEY_CHECK                  | rootユーザーアクセスキーが使用可能かどうかを確認します。                                                                                                                      |
| MFA_ENABLED_FOR_IAM_CONSOLE_ACCESS         | コンソールパスワードを使用するすべてのAWS Identity and Access Management（IAM）ユーザーに対してAWS Multi-Factor Authentication（MFA）が有効になっているかどうかを確認します。 |
| MULTI_REGION_CLOUD_TRAIL_ENABLED           | 少なくとも1つのマルチリージョンAWS CloudTrailがあることを確認します。                                                                                                         |
| RDS_LOGGING_ENABLED                        | Amazon Relational Database Service（Amazon RDS）の各ログが有効になっていることを確認します。                                                                                  |
| RDS_INSTANCE_PUBLIC_ACCESS_CHECK           | Amazon Relational Database Serviceインスタンスがパブリックにアクセスできないかどうかを確認します。                                                                            |
| RDS_SNAPSHOTS_PUBLIC_PROHIBITED            | Amazon Relational Database Service（Amazon RDS）スナップショットが公開されているかどうかを確認します。                                                                        |
| INCOMING_SSH_DISABLED                      | セキュリティグループの受信SSHトラフィックにアクセスできるかどうかを確認します。                                                                                               |
| ROOT_ACCOUNT_HARDWARE_MFA_ENABLED          | AWSアカウントがマルチ要素認証（MFA）ハードウェアデバイスを使用してルート認証情報でサインインできるかどうかを確認します。                                                      |
| S3_BUCKET_PUBLIC_READ_PROHIBITED           | Amazon S3バケットがパブリック読み取りアクセスを許可していないことを確認します。                                                                                               |
| S3_BUCKET_PUBLIC_WRITE_PROHIBITED          | Amazon S3バケットがパブリック書き込みアクセスを許可していないことを確認します。                                                                                               |
| WAFV2_LOGGING_ENABLED                      | AWSウェブアプリケーションファイアウォール（WAFV2）のリージョンおよびグローバルウェブアクセスコントロールリスト（ACL）でロギングが有効になっているかどうかを確認します。       |

上記ルールに設定できるパラメータについては、[入出力リファレンス](#入出力リファレンス)の`configrule_`というプレフィックスがついているパラメータを参照してください。

利用者側で任意のルールを設定したい場合は、[マネージドルールのリスト](https://docs.aws.amazon.com/ja_jp/config/latest/developerguide/managed-rules-by-aws-config.html)をもとに、以下のようなリソースを追記してください。

```terraform
resource "aws_config_config_rule" "my_config_rule" {
  name        = "${var.configrule_prefix}-my_config_rule"
  description = "ルールの説明"
  source {
    owner             = "AWS"
    source_identifier = "(適用するルールの識別子)"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
  input_parameters = jsonencode(
    {
      (パラメータが必要なルールの場合はここに記入してください)
    }
  )
}
```

デフォルトルールが不要となる場合は、パラメータ`enable_default_rule`を`false`にしてください。

### ルール違反時の通知先

本モジュールでは、検知した情報をチャットツールに連携します。  
マネージドルールに違反した場合だけでなく、構成変更が発生した際毎回通知を送ることも可能です。

- ルール違反した場合だけ通知を受け取りたい場合

```terraform
  notification_channel = {
    changeDetectionNotice = ""
    ruleViolationNotice   = "/services/XXXXXXXXX/XXXXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX"
  }
```

- 構成変更のたびに通知を受け取りたい場合（かつ、通知先を別にしたい場合）

```terraform
  notification_channel = {
    changeDetectionNotice = "/services/YYYYYYYYY/YYYYYYYYYYY/YYYYYYYYYYYYYYYYYYYYYYYY"
    ruleViolationNotice   = "/services/XXXXXXXXX/XXXXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX"
  }
```

通知先には、以下のようなチャットツールが利用可能です。

- [Slack](https://slack.com/intl/ja-jp/)  
- [Microsoft Teams](https://www.microsoft.com/ja-jp/microsoft-365/microsoft-teams/group-chat-software)

:warning: チャットツールへの通知には、webhookを用いたTLS通信を利用しています。  
通知先となるサービスのIncoming Webhookの設定は、Epona利用者で事前に実施いただく必要があります。

チャットツールに連携する内容は以下の項目となっています。

- ルール違反に関する通知

| 通知項目          | 説明                                                    |
| :---------------- | :------------------------------------------------------ |
| subject           | 通知の分類(ComplianceChangeNotification)                |
| awsAccountId      | 違反が発生したAWS Account ID                            |
| awsRegion         | 違反が発生したAWSリージョン                             |
| resourceType      | 違反が発生したリソースのタイプ                          |
| resourceId        | 違反が発生したリソースID                                |
| configRuleName    | 関係するマネージドルール名                              |
| configRuleArn     | 関係するマネージドルールのARN                           |
| newComplianceType | ルールの状態 (準拠：COMPLIANT / 非準拠 : NON_COMPLIANT) |
  
- 設定変更に関する通知

| 通知項目     | 説明                                            |
| :----------- | :---------------------------------------------- |
| subject      | 通知の分類(ConfigurationItemChangeNotification) |
| awsAccountId | 変更が発生したAWS Account ID                    |
| awsRegion    | 変更が発生したAWSリージョン                     |
| resourceType | 変更が発生したリソースのタイプ                  |
| resourceId   | 変更が発生したリソースID                        |
| changeType   | 変更の内容 (CREATE / UPDATE / DELETE)           |

### S3バケットのライフサイクル

AWS Configが変更情報を記録するS3バケットは、以下の設定となっています。

- プライベートバケット
- バージョニング有効
- サーバーサイド暗号化有効
- ライフサイクルルールあり
  - 30日経過後、Gracierに移行

バージョニングの有無、ライフサイクルルールの日数やストレージクラスについてはパラメータで変更可能です。  
詳細は[入出力リファレンス](#入出力リファレンス)をご参照ください。

## サンプルコード

`rule_violation pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "sample_aws_config" {
  source                  = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/rule_violation?ref=v0.2.6"
  s3_bucket_name          = "rule-violation-bucket"
  tags                    = { "Name" = "rule-violation" }
  function_name           = "ruleViolationFunction"
  aws_config_role_name    = "ruleViolation"
  notification_topic_name = "rule-violation-topic"
  notification_type       = "slack"
  notification_channel = {
    changeDetectionNotice = ""
    ruleViolationNotice   = "/services/XXXXXXXXX/XXXXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX"
  }
  create_config_recorder_delivery_channel = true
}
```

## 関連するpattern

`rule_violation pattern`に関連するpatternはありません。

## ログの集約

`rule_violation pattern`では、AWS Lambda関数のログをAmazon CloudWatch Logsに出力します。

このログは、[datadog_log_trigger pattern](datadog_log_trigger.md)を使用することでDatadogに集約できます。

## 入出力リファレンス

${TF_DOC}
