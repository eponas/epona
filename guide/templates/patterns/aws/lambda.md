# lambda pattern

## 概要

`lambda pattern`は、[AWS Lambda](https://aws.amazon.com/jp/lambda/)の関数を作成するpatternです。
Lambda関数だけでなく、当該関数を実行するトリガーや、Lambda関数の実行終了後の通知も同時に構築できます。

## 想定する適用対象環境

`lambda pattern`はRuntime環境で使用されることを想定しています。

## 依存するpattern

`lambda pattern`には、事前に実行が必要なパターンはありません。

ただし、以下の場合には事前の準備が必要となりますのでご注意ください。

- [VPC内にLambda関数を配置](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-vpc.html)
  する場合  
  [`network pattern`](./network.md)により事前にVPCを作成しておいてください
- [Lambda関数が実行完了した際のSNS通知](#Lambda関数が実行完了した際のSNS通知)を行う場合  
  通知先となる[Amazon SNS](https://docs.aws.amazon.com/ja_jp/sns/latest/dg/welcome.html)トピックを事前に作成しておいてください

## 構築されるリソース

| リソース名 | 説明 |
| -----| ----- |
| [AWS Lambda](https://aws.amazon.com/jp/lambda/)              | Lambda関数を作成します。                                     |
| [AWS IAM](https://aws.amazon.com/jp/iam) | Lambda関数の実行ロールを作成します。条件によってはSNS topicへのpublishを行うポリシーやVPC内のリソースにアクセスするためのポリシー等を作成します。 |
| [Amazon CloudWatch](https://aws.amazon.com/jp/cloudwatch)    | Lambda関数のログを配置するロググループを作成します。             |
| [Amazon EventBridge](https://aws.amazon.com/jp/eventbridge)  | Lambda関数のスケジュール実行する場合、ルールおよび、ターゲットを作成します。 |
| [Amazon S3](https://aws.amazon.com/jp/s3)(S3)                    | Lambda関数の発火イベントにAmazon S3イベント通知を利用する場合は、S3イベント通知設定を作成します。 |

## モジュールの理解に向けて

[AWS Lambda](https://aws.amazon.com/jp/lambda/)は、インフラのプロビジョニングなしに、コードを実行できるサービスです。
これにより、開発者はサーバー等を気にすることなく実行するアプリケーション(Lambda関数)へ集中できますし、
過剰なインフラにコストを支払う必要もありません。

一方でLambda関数は単体では存在し得ず、その実行のトリガーを必要とします。
また、Lambda関数がVPC内のリソースへのアクセスを必要とする場合はそれ専用の設定が必要となりますし、実行時にエラーが発生した場合に通知を受け取りたいこともあるでしょう。

本patternは、そういったLambda関数で煩雑となり得る設定を一元的に設定可能とするものです。

![構成図](../../resources/lambda.png)

### Lambda関数のデプロイ

#### Terraformでデプロイを行う

本patternを用いてLambda関数をデプロイする場合、Lambda関数のソースファイルが配置されたディレクトリ名を
`source_dir`に相対パスで指定します。
指定したパス以下のリソースがzip形式に圧縮・アップロードされ、Lambda関数として登録されます。

```hcl
module "lambda" {
  ...
  source_dir = "./lambda_function"
}
```

ディレクトリの中にはLambda関数とは無関係のファイルが含まれている場合もあるでしょう（設定ファイルやテストコード等）。
このような場合、本patternでは`source_dir_exclude_files`に指定することでLambda関数の実行に不要なファイルを除外できます。

----

:warning: ディレクトリ指定や、ワイルドカード(`*`)を使った指定はできない点にご注意ください。

----

```hcl
module "lambda" {
  ...
  source_dir               = "./lambda_function"
  source_dir_exclude_files = [ # Lambda関数実行に不要なファイルをリストする
    "package.json",
    "package-lock.json",
  ]
}
```

Lambda関数のデプロイ方法については[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/gettingstarted-package.html#gettingstarted-package-zip)も参照してください。

:information_source: `lambda pattern`では指定されたディレクトリのzip圧縮とデプロイのみ実施し、依存関係のあるパッケージのダウンロード等は行いません。同梱するパッケージがある場合は、公式ドキュメントの記載を参照していただき、事前にディレクトリ内へ配置しておいてください。

Lambda関数の[環境変数](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-envvars.html)を設定する場合は、[入出力リファレンス](#入出力リファレンス)の`environment`で設定してください。

:information_source: Lambdaに値を渡すには、環境変数の他に
[AWS Systems Manager パラメータストア](https://docs.aws.amazon.com/ja_jp/systems-manager/latest/userguide/systems-manager-parameter-store.html)
も利用可能です。  
詳細は
[こちら](https://aws.amazon.com/jp/premiumsupport/knowledge-center/lambda-common-environment-variables/)
を参照してください。  
Eponaの[parameter_store pattern](./parameter_store.md)もあわせてご参照ください。

#### CI/CDパイプラインを利用する

Eponaでは、AWS CodePipelineなどを利用したCI/CDパイプラインにも対応しています。概要については`cd_pipeline_lambda pattern`の[ガイド](./cd_pipeline_lambda.md)を参照してください。

### Lambda関数へのIAMポリシーの付与

Lambda関数が`Amazon DynamoDB`等、他のAWSリソースへアクセスする場合、Lambda関数にはそれらのリソースへのアクセス権限をIAMポリシーとして付与しなければなりません。

本patternではLambda関数に割り当てるIAMロールを作成し、そのARNを`role_arn`という名称で[`Output Values`](https://www.terraform.io/docs/language/values/outputs.html)に出力しています。
Lambda関数が必要とするIAMポリシーは、この`role_arn`に割り当ててください。

以下に、`Amazon DynamoDB`ストリームからレコードを読み出すためのアクセス許可をLambda関数に与える例を示します。

```hcl
module "lambda" {
  source = git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/lambda?ref=v0.2.6"
  ...
}
# このmoduleにより、Lambda関数用のロールが作成され、そのロールをrole_arnにより参照できるようになります

data "aws_iam_policy" "dynamodb" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaDynamoDBExecutionRole"
}

resource "aws_iam_role_policy_attachment" "iam" {
  # moduleのrole_arnを指定することで、必要なロールをアタッチできます
  role       = module.lambda.role_name
  policy_arn = data.aws_iam_policy.dynamodb
}
```

### Lambda関数の実行トリガー

Lambda関数はデプロイしただけでは実行されず、関数を「どのようなときに実行するか」というトリガーを指定する必要があります。
本patternでは代表的な実行トリガーとして、以下をサポートしています。

- `Amazon EventBridge`を使ったスケジュール実行
- `Amazon S3`バケット上のオブジェクト操作をトリガーとする実行

<!-- TODO: 現在の実装では、スケジュール実行以外でCloudWatch Event Ruleを使った柔軟な指定がサポートされていない -->

なお、トリガーは複数種類同時に指定が可能です。

:information_source: 複数のトリガーを設定し、同時に条件を満たした場合、Lambda関数は同時に並列で起動します。

- [参考](https://d1.awsstatic.com/whitepapers/ja_JP/serverless-architectures-with-aws-lambda.pdf)

また、まれにですが
[1つのトリガーにより複数回実行される](https://docs.aws.amazon.com/ja_jp/eventbridge/latest/userguide/eb-troubleshooting.html#eb-rule-triggered-more-than-once)
ことがあります。

トリガーに応じて厳密に1回の実行（exactly-once）を担保する必要がある場合は、[こちら](https://aws.amazon.com/jp/premiumsupport/knowledge-center/lambda-function-idempotent/)の記載を参照していただき、必要な措置を検討してください。

:information_source: Lambda関数をイベントにより呼び出す（`非同期呼び出し`）場合、`AWS Lambda`はイベントをキューに入れて管理し、エラー発生時には自動的に再試行します。イベントによりLambda関数を起動する場合は、再試行を考慮した実装となるようご留意ください。詳細は[こちら](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/invocation-async.html)をご参照ください。

#### Amazon EventBridgeを使ったスケジュール実行

`Amazon EventBridge`を使用し、毎時実行や日次実行といった定期実行や、特定時刻での実行といったスケジュール実行が可能です。

スケジュール実行する場合、`schedule_expression`にスケジュール式を指定してください。
記述方法については、[Rate または Cron を使用したスケジュール式](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/services-cloudwatchevents-expressions.html)を参照してください。

なお、スケジュール実行を一時的に無効化できます。この場合は、`is_enabled_schedule`に`false`を指定してください。

```hcl
module "lambda" {
  ...
  # Lambda関数を毎時実行したい場合
  schedule_expression = "rate(1 hour)"
}
```

#### Amazon S3バケット上のオブジェクト操作をトリガーとする実行

時刻ではなく、S3バケット上で何かオブジェクトが作成されたタイミングや更新・削除されたタイミングでもLambda関数を実行できます。

- [参考](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/with-s3-example.html)

この場合は、どのバケットのどのようなオブジェクトを対象にするのか、そしてどのような操作を対象とするのかを指定してください。

なお、どのような操作を対象とするかを指定する際、本patternでは[Amazon S3 イベント通知](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/NotificationHowTo.html#notification-how-to-event-types-and-destinations)を利用しています。
このため、指定方法については当該ドキュメントを参照してください。

```hcl
module "lambda" {
  ...
  # どのような操作(event)をトリガーとするのか
  invoke_object_events = [
    "s3:ObjectCreated:*", # オブジェクトが作成されたときに実行
  ]

  # どのバケットやオブジェクトを対象にトリガーとするのか。prefix/suffixを両方指定した場合、AND条件として判断します
  invoke_bucket_name          = "your_bucket_name"
  invoke_bucket_arn           = "arn:aws:s3:::your_bucket_name"
  invoke_object_filter_prefix = "image_"
  invoke_object_filter_suffix = ".png"
}
```

### Lambda関数が実行完了した際のSNS通知

Lambdaを[Amazon API Gateway](https://aws.amazon.com/jp/api-gateway/)などから同期的に呼び出す場合、成功・失敗は即座に判別できます。

一方、スケジュールやイベントなど、ユーザーが介在しない状態で非同期的にLambdaが実行される場合、その実行結果を受け取るには別途仕組みを用意する必要があります。

`lambda pattern`では、Lambda関数の実行完了時に、`Amazon SNS`(以下、SNS)へ通知できます。
これにより、例えばLambda関数の実行が完了した場合にメールを送る、といったことを実現できます。

なお、SNS Topicへ通知するにあたり、本patternで[SNS topic](https://docs.aws.amazon.com/ja_jp/sns/latest/dg/sns-create-topic.html)自体は作成しません。ユーザー側での準備が必要となる点にご注意ください。
これはSNSの使用用途が多岐に渡り、事前の想定が困難であるためです。

SNSへの通知機能を利用する際は、Lambda関数の実行成功時、失敗時でそれぞれSNS topicのARNを最大1つ指定します。
なお、成功時・失敗時用に同一のSNS topicを指定しても問題ありません。

```hcl
module "lambda" {
  ...
  notify_sns_topic_on_success = "arn:aws:sns:ap-northeast-1:000000000000:lambda-notification-success"
  notify_sns_topic_on_failure = "arn:aws:sns:ap-northeast-1:000000000000:lambda-notification-failure"
```

参考として、Lambda関数の成功時、失敗時のSNS通知のサンプルを以下に示します。

#### 成功時サンプル

```json
{
  "version": "1.0",
  "timestamp": "2021-07-12T10:31:45.866Z",
  "requestContext": {
    "requestId": "64329d74-bbde-49c3-9105-557a24a8aaaa",
    "functionArn": "arn:aws:lambda:ap-northeast-1:000000000000:function:YourLambdaFunction:$LATEST",
    "condition": "Success",
    "approximateInvokeCount": 1
  },
  "requestPayload": {},
  "responseContext": {
    "statusCode": 200,
    "executedVersion": "$LATEST"
  },
  "responsePayload": "2021/07/12/[$LATEST]b219eebba4894af59b50ccbb8a37aaaa"
}
```

#### 失敗時サンプル

```json
{
  "version": "1.0",
  "timestamp": "2021-07-12T10:26:29.290Z",
  "requestContext": {
    "requestId": "b9a868fb-96ef-4101-b4df-f12ce53eaaaa",
    "functionArn": "arn:aws:lambda:ap-northeast-1:000000000000:function:YourLambdaFunction:$LATEST",
    "condition": "RetriesExhausted",
    "approximateInvokeCount": 3
  },
  "requestPayload": {},
  "responseContext": {
    "statusCode": 200,
    "executedVersion": "$LATEST",
    "functionError": "Unhandled"
  },
  "responsePayload": {
    "errorType": "ReferenceError",
    "errorMessage": "function is not defined",
    "trace": [
      "ReferenceError: sz is not defined",
      "    at Runtime.exports.handler (/var/task/index.js:2:1)",
      "    at Runtime.handleOnce (/var/runtime/Runtime.js:66:25)"
    ]
  }
}
```

:information_source: SNSに通知された内容は、Lambdaあるいは
[AWS Chatbot](https://aws.amazon.com/jp/chatbot/)
を介して
[Slack](https://slack.com/intl/ja-jp/)
や
[Microsoft Teams](https://www.microsoft.com/ja-jp/microsoft-teams/group-chat-software)
等のツールにも通知できます。  
以下のリンク先もあわせてご参照ください。

- [SNSトピックの設定](https://docs.aws.amazon.com/ja_jp/sns/latest/dg/sns-configuring.html)
- [ウェブフックを使用して Amazon SNS メッセージを Amazon Chime、Slack、または Microsoft Teams に発行する方法](https://aws.amazon.com/jp/premiumsupport/knowledge-center/sns-lambda-webhooks-chime-slack-teams/)
- [AWS Chatbot](https://docs.aws.amazon.com/ja_jp/chatbot/latest/adminguide/what-is.html)

### Lambda関数のVPCへの配置

Lambda関数は、通常、利用者の`Amazon VPC`（以下、VPC）の外に置かれます（[参考](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/troubleshooting-networking.html)）。

一方で、`Amazon RDS`等、利用者のVPC内のリソースに対してLambda関数からアクセスしたいケースがあります。
このような場合、Lambda関数をVPC内に配置しますが、この配置についても本patternではサポートしています。

なお、本patternではVPCは作成しません。VPCの作成に関しては、必要に応じて[`network pattern`](./network.md)をご利用ください。
以下に`network pattern`で作成したVPC内にLambda関数を作成する例を示します。

```hcl
data "terraform_remote_state" "production_network" {
  backend = "s3"

  config = {
    bucket         = "myservice-production-terraform-tfstate"
    key            = "network/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "myservice_terraform_tfstate_lock"
    role_arn       = "arn:aws:iam::[Production環境のアカウントID]:role/RuntimeTerraformBackendAccessRole"
  }
}

# Lambda関数用にセキュリティグループを作成する
resource "aws_security_group" "lambda" {
  vpc_id = data.terraform_remote_state.production_network.outputs.network.vpc_id
  ...
}

module "lambda" {
  source                        = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/lambda?ref=v0.2.6"
  source_dir                    = "./lambda_function"
  function_name                 = "YourLambdaFuncionName"
  runtime                       = "nodejs14.x"
  handler                       = "index.handler"
  environment                   = { hoge = "moge" }
  vpc_config_subnet_ids         = data.terraform_remote_state.production_network.outputs.network.private_subnets
  vpc_config_security_group_ids = [aws_security_group.lambda.id]
}
```

なお、Eponaでの`terraform_remote_state`の利用方法は[こちら](https://eponas.gitlab.io/epona/guide/patterns/aws/#data-source%E3%81%A8%E3%81%97%E3%81%A6%E3%81%AEstate)をご覧ください。

:information_source: Lambda関数をVPC内に置くことで、Lambdaからのインターネットアクセスに制約が生じたり、VPC側に新たな設定が必要となったりすることがあります。設定に際しては以下の参照先もあわせてご参照ください。

- [VPC 内のリソースにアクセスするための Lambda 関数の設定](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-vpc.html)
- [Lambdaネットワークに関する問題のトラブルシューティング](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/troubleshooting-networking.html)

### Lambda関数の同時実行数

Lambda関数には同時実行数の制約を課すことができます。これにより、Lambda関数がアクセスするリソースの
スケーラビリティの制約以上に同時実行数を増やさないようにできます。

詳細は[Lambda 関数の同時実行数の管理](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-concurrency.html)を参照ください。

本patternでは、上記ページの「リザーブド同時実行」をLambda関数に設定できます。
設定するためには、`reserved_concurrent_executions`に数値を指定します。`0`を指定すると関数は実行されません。
`-1`を指定すると同時実行数を制限しません。

```hcl
module "lambda" {
  ...
  reserved_concurrent_executions = 0
}
```

### エイリアス

Lambda関数では1つ以上のエイリアスを作成できます。

Lambda関数はバージョンをサポートしており、バージョンに対してエイリアスを作成できます。
 `lambda pattern`では、デフォルトではLambda関数の最新バージョン(初期構築時のバージョンは1)に対して
 エイリアスを作成します。

Eponaでは、Lambda関数の更新およびデプロイは`cd_pipeline_lambda pattern`を使用して実施することを
想定しています。詳細は[`cd_pipeline_lambda pattern`](./cd_pipeline_lambda.md)のガイドをご参照ください。

:information_source: 初期構築後、本patternによりLambda関数を更新しても、エイリアスに紐づく
バージョンは変更されません。  
エイリアスに紐づくバージョンの変更には、`cd_pipeline_lambda pattern`で構築したパイプラインの実行が
必要です。

```terraform
module "lambda" {
  ...
  aliases = [
    "default"
  ]
}
```

詳細は公式ドキュメントを参照してください。

- [Lambda関数のバージョン](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-versions.html)
- [Lambda関数のエイリアス](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-aliases.html)

## 制限

2021/09現在、`lambda pattern`には以下の制限があります。

- Lambda関数のコードの変更はローカルにあるディレクトリから、あるいは`cd_pipeline_lambda pattern`を利用したデプロイが前提になっています
  - Amazon S3上に配置されたソースコードからのデプロイや、Amazon ECR上のコンテナのデプロイには未対応です
- [Lambdaレイヤー](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-layers.html)には対応しておりません
- [Amazon EFS](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/services-efs.html)には対応しておりません

## サンプル

```hcl
module "lambda" {
  source = "../../../../../modules/aws/patterns/lambda/"

  function_name = "my-function"
  source_dir    = "./src"
  source_dir_exclude_files = [
    "package-lock.json",
    "package.json",
  ]
  ignore_change_source = false
  runtime              = "nodejs14.x"
  description          = "サンプル"
  handler              = "index.handler"
  environment = {
    VAR = "VALUE"
  }

  memory_size = 128
  timeout     = 60 # 60秒

  tracing_mode                   = "Active"
  reserved_concurrent_executions = 1

  log_group_retention_in_days = 7

  schedule_expression = "cron(0 0 * * ? *)" # 毎日 09:00 (JST)に起動
  is_enabled_schedule = true
}
```

## 関連するpattern

`lambda pattern`に関連するpatternを以下に記載します。

| pattern名                                                  | 説明                                                         |
| ---------------------------------------------------------- | ------------------------------------------------------------ |
| [network](./network.md)                                    | VPC内に`AWS Lambda`を配置する場合のVPCを作成する             |
| [step_functions](./step_functions.md)                      | `AWS Lambda`などのAWSサービスを連携したワークフローを構築する |
| [api_gateway](./api_gateway.md)                            | APIを作成・公開するための`Amazon API Gateway`を作成する      |
| [cd_pipeline_lambda](./cd_pipeline_lambda.md)              | Lambda関数のデプロイメントパイプラインを構築する             |
| [cd_pipeline_lambda_trigger](./cd_pipeline_lambda_trigger.md) | Lambda関数のデプロイのトリガとなるイベントやDelivery環境のリソースアクセスに必要なロールなどを構築する |

## 入出力リファレンス

${TF_DOC}
