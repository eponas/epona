provider "aws" {
}

# Runtime環境上でのロール
module "terraformer_execution" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/terraform_execution?ref=v0.2.6"

  principals = ["Delivery環境上に定義された、本Runtime環境用Terraform実行ユーザのARN"]
}

module "backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/backend?ref=v0.2.6"

  name          = "myservice"   # FIXME:
  environment   = "Development" # FIXME: 
  force_destroy = false
}
