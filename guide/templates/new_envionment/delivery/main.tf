provider "aws" {
}

module "delivery" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/delivery?ref=v0.2.6"

  name          = "myservice" # FIXME: 
  force_destroy = false       #
  runtime_accounts = {
    "[Runtime環境1の名前]" = "[Runtime環境1のアカウントID]" # FIXME:
    "[Runtime環境2の名前]" = "[Runtime環境2のアカウントID]" # FIXME:
  }

  # Terraform 実行ユーザーをDelivery環境の任意のIAMグループに追加したい場合、そのIAMグループ名を指定してください。
  user_groups = [
    "[任意のユーザグループのname]", # FIXME
    "[任意のユーザグループのname]", # FIXME
  ]
}

module "cross_account_assume_role" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/cross_account_assume_policy?ref=v0.2.6"

  execution_role_map = {
    # "[Runtime環境1の名前]" = "[Runtime環境1で定義されたTerraform実行ロールのARN]" # FIXME:
    # "[Runtime環境2の名前]" = "[Runtime環境2で定義されたTerraform実行ロールのARN]" # FIXME:
  }
  terraformer_users = module.delivery.terraformer_users
}
