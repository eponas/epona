# GitLab CI/CDによるアプリケーションのビルド・デプロイガイド

## 概要

GitLabにはGitLab CI/CDという機能が組み込まれており、リポジトリへコードをPushするタイミング等で、
特定のコマンド列を自動実行させることができます。
これを利用することで、GitLab上で以下のようなことが可能になります。

- ソースコードのPushをトリガとしてコードからコンテナイメージをBuildし、任意のコンテナレジストリへPushする
- アーティファクト（jarファイルなど）をBuildし、S3へアップロードする

本ガイドではそれぞれの実現例も記載しますが、GitLab CI/CDの詳細に関しては以下の公式ページもあわせてご確認ください。

- [GitLabの継続的インテグレーション(CI)と継続的デリバリー(CD)](https://www.gitlab.jp/stages-devops-lifecycle/continuous-integration/)
- [GitLab CI/CDのドキュメント](https://docs.gitlab.com/ee/ci/)

## GitLab CI/CDの使い方について

GitLab CI/CDはリポジトリのトップに`.gitlab-ci.yml`というYamlファイルを配置することで利用できます。  
YamlファイルにはCIのジョブ定義を記載します。

CIは基本的にShared Runnerと呼ばれるGitLab共有の環境で実行されますが、
EponaではSpecific Runnerと呼ばれるサービス専用Runner上で実行します。  
Specific Runnerは[ci_pipeline pattern](../patterns/aws/ci_pipeline.md)を実行することで
AWS上にEC2インスタンスとして作成されます。

利用方法については[ci_pipeline pattern](../patterns/aws/ci_pipeline.md)をご確認ください。

## コンテナイメージのビルド・デプロイについて

本章では、コンテナイメージのビルド・デプロイをGitLab CI/CDで自動化する方法について記載します。  
そのために、GitLab Runner上でコンテナイメージをビルドし、作成したコンテナイメージを[Amazon ECR](https://aws.amazon.com/jp/ecr/)にプッシュする流れを`.gitlab-ci.yml`に定義します。

### コンテナイメージのビルド

GitLab CI/CDは指定したコンテナイメージ上で実行可能です。
このため、[Dockerのコンテナイメージ](https://hub.docker.com/_/docker)を使用した
`DinD（Docker in Docker）`を利用することでコンテナイメージのBuildを行えます。
これを実現する`.gitlab-ci.yaml`の記述については後述します。

### コンテナイメージのデプロイ

Eponaでは[Amazon ECR](https://aws.amazon.com/jp/ecr/)にコンテナイメージを配置することを想定しています。  
Amazon ECRのリポジトリは[ci_pipeline pattern](../patterns/aws/ci_pipeline.md)実行時に
作成されます。

Amazon ECRへのコンテナイメージのデプロイは以下のようにすることで行えます。

```shell
$ aws ecr get-login-password | docker login --username AWS --password-stdin [ECRのrepositoryUri]
$ docker image push [コンテナ名]:[タグ]
```

:information_source:  
上記のコマンドを実行するためには、通常は権限のあるIAMユーザーのアクセスキーの設定が必要になります。
ただし、Eponaでは上述のようにAWS上に作成したSpecific Runnerを利用するため、この設定は不要になります。  
これはSpecific RunnerのEC2インスタンスに適切なインスタンスプロファイルを設定しているためです。

:warning:  
Amazon ECRへのデプロイにはAWS CLIのコマンドを使用します。  
このため、`docker image push`を実行するイメージにはAWS CLIのインストールをしておく必要があります。

### コンテナイメージをビルド・デプロイする.gitlab-ci.ymlファイルの記載例

上記を考慮した`.gitlab-ci.yml`ファイルの記載例になります。  
このまま利用しても問題ありませんが、ビルド方法やブランチ戦略などを鑑み、適宜カスタマイズして利用してください。

----

:warning:  
GitLab CI/CDでは変数の値を事前にGitLab上で設定しておくことが可能です。リポジトリに含めたくない場合はこちらを利用することを推奨します。  
詳細は[GitLab CI/CD environment variables](https://docs.gitlab.com/ee/ci/variables/)をご確認ください。

以下の`.gitlab-ci.yml`では、以下4つの変数をGitLab上で設定している想定で記載しています。

- `MY_AWS_ACCOUNT_ID`
- `MY_AWS_DEFAULT_REGION`
- `MY_BUILD_DIRECTORY`
- `MY_ECR_REPOSITORY_NAME`

----

```yaml
variables:
  AWS_ACCOUNT_ID: ${MY_AWS_ACCOUNT_ID}
  AWS_DEFAULT_REGION: ${MY_AWS_DEFAULT_REGION}
  BUILD_DIRECTORY: ${MY_BUILD_DIRECTORY}
  ECR_REPOSITORY_NAME: ${MY_ECR_REPOSITORY_NAME}

stages:
  - preparation
  - build_and_deploy

# AWSへデプロイするためのベースとなるジョブ定義
.base_job:
  stage: preparation
  image: docker:20.10.1
  services:
    - docker:20.10.1-dind
  only:
    refs:
      - master
  before_script:
    - export ECR_BASE_URI=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com
    - export IMAGE_TAG=${CI_COMMIT_SHORT_SHA}  # コミットハッシュをコンテナイメージのタグに使う設定
    - export ECR_REPOSITORY=${ECR_BASE_URI}/${ECR_REPOSITORY_NAME}:${IMAGE_TAG}
    - ./install-awscliv2-on-alpine.sh

# コンテナイメージのビルドとECRへのデプロイを行うジョブ定義
build_and_deploy_application:
  stage: build_and_deploy
  extends: .base_job
  script:
    - docker image build ${BUILD_DIRECTORY} -t "${ECR_REPOSITORY}"
    - aws ecr get-login-password | docker login --username AWS --password-stdin ${ECR_REPOSITORY}
    - docker image push ${ECR_REPOSITORY}
```

## アーティファクトのビルド・デプロイについて

GitLab CI/CDで行えるのは、コンテナのビルド・デプロイに限りません。jarファイル等アーティファクトのビルドとデプロイ、あるいは実行可能プログラムのビルド等も可能です。
本章では一例として、Mavenを使ってjarファイルをビルドし、S3バケットに配置する流れをGitLab CI/CDで自動化する方法を記載します。そのためには、コンテナイメージのビルド・デプロイと同様に`.gitlab-ci.yml`の定義が必要です。

:information_source:  
jarファイルはS3バケットに配置して終わりではなく、実行環境への配置やアプリケーションサーバーへのデプロイを行うことになるでしょう。
ただし、この場合のデプロイ先、デプロイ方法はサービスによって多種多様であるため、本ガイドではS3へのアップロードまでの設定方法を記載します。
以降のデプロイプロセスについては、利用者にてご検討をお願いいたします。

アーティファクトのデプロイ先のS3バケットは
[ci_pipeline pattern](../patterns/aws/ci_pipeline.md)で作成可能です。  
[ci_pipeline patternのS3バケットに関する記載](../patterns/aws/ci_pipeline.md#s3バケット)を
参考に、デプロイするアーティファクトの種類に応じた適切なパラメータを設定してください。

S3へのアップロードは以下のコマンドで行います。

```shell
$ aws s3 cp [アーティファクトのファイル名] s3://[配置先のS3バケット名]/[アーティファクトのファイル名]
```

:information_source:  
Eponaを利用して上記のコマンドを実行する際、[コンテナイメージのデプロイ](#コンテナイメージのデプロイ)と同様に、
IAMユーザーのアクセスキーの設定は不要です。  
これは、Specific RunnerのEC2インスタンスに適切なインスタンスプロファイルを設定しているためです。

### アーティファクトをビルド・デプロイする.gitlab-ci.ymlファイルの記載例

上記を考慮した`.gitlab-ci.yml`ファイルの記載例になります。  
このまま利用しても問題ありませんが、ビルド方法やブランチ戦略などを鑑み、適宜カスタマイズして利用してください。

----

:warning:  
GitLab CI/CDでは変数の値を事前にGitLab上で設定しておくことが可能です。値をリポジトリに含めたくない場合はこちらを利用することを推奨します。  
詳細は[GitLab CI/CD environment variables](https://docs.gitlab.com/ee/ci/variables/)をご確認ください。

以下の`.gitlab-ci.yml`では、以下2つの変数をGitLab上で設定している想定で記載しています。

- `MY_BUILD_DIRECTORY`
- `MY_ARTIFACT_NAME`
- `MY_ARTIFACT_PATH`
- `MY_PUSH_TO_S3_BUCKET_NAME`

----

```yaml
variables:
  BUILD_DIRECTORY: ${MY_BUILD_DIRECTORY}
  ARTIFACT_NAME: ${MY_ARTIFACT_NAME}
  ARTIFACT_PATH: ${MY_ARTIFACT_PATH}
  PUSH_TO_S3_BUCKET_NAME: ${MY_PUSH_TO_S3_BUCKET_NAME}

stages:
  - preparation
  - build_and_deploy

# AWSへデプロイするためのベースとなるジョブ定義
.base_job:
  stage: preparation
  image: maven:3.6.3-openjdk-11
  only:
    refs:
      - master
  before_script:
    - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    - unzip awscliv2.zip
    - aws/install

# アーティファクトのビルドとS3へのアップロードを行うジョブ定義
build_and_deploy_application:
  stage: build_and_deploy
  extends: .base_job
  script:
    - mvn -f ${BUILD_DIRECTORY}/pom.xml package
    - aws s3 cp ${ARTIFACT_PATH}/${ARTIFACT_NAME} s3://${PUSH_TO_S3_BUCKET_NAME}/${ARTIFACT_NAME}
```
