# 複数行で出力されるログをひとつのログに集約する

## はじめに

Eponaで稼働するコンテナサービスのログは、Amazon CloudWatch Logsに送信し、Datadogに転送できます。

しかし、Javaのスタックトレース等の改行を含むログの場合、改行ごとに別の行として転送されます。  
結果として、本来1つのログエントリであるはずのスタックトレース等がDatadog上で別のログエントリとして扱われてしまいます。
このようになってしまうと、ログからシステムの状況が読み取りづらくなります。  
このため、ログを調査する上では、こうした複数行のログは1行に集約されていることが望ましいでしょう。

このガイドでは、Fluentdのプラグインを活用して複数行のログを1行に集約するための方法について説明します。

## 前提事項、関連資料

本ガイドでは、以下のガイドの記載をベースに、どこを変更すれば複数行のログを集約できるかを説明します。  
以下のガイドをご一読いただいた上で読み進めていただくようお願いいたします。

- [Amazon ECS上のコンテナログを、AWS FireLensを使ってAmazon CloudWatch Logsに送信する](ecs_log_send_to_cloudwatch_logs_using_firelens.md)

## 概要

[AWS Samples](https://github.com/aws-samples)では、複数行のログを集約する方法として[Fluentdのプラグインを使う方法](https://github.com/aws-samples/amazon-ecs-firelens-examples/tree/b35bab51d850f6cc7b7a6bec0cf0023e31f1c0cd/examples/fluentd/multiline-logs)が紹介されています。

この例によってログを集約するためには、以下の手順を踏む必要があります。

1. AWS SamplesのDockerfileとFluentd設定をベースに、Fluentdのコンテナイメージを自作する
1. 作成したイメージをAmazon ECRにプッシュする
1. 作成したイメージを使用したタスク定義を記述する

以下、この手順にしたがって説明します。

## AWS SamplesのDockerfileとFluentd設定をベースに、Fluentdのコンテナイメージを自作する

[AWS Samples](https://github.com/aws-samples/amazon-ecs-firelens-examples/tree/b35bab51d850f6cc7b7a6bec0cf0023e31f1c0cd/examples/fluentd/multiline-logs)
の`Dockerfile`では、複数行のログをAmazon CloudWatch Logsへ送信するのに
必要なプラグインを含むFluentdのコンテナイメージを作成できます。  
また`extra.conf`は、複数行のログを送信するときのFireLens用サンプル設定となっています。

これらを任意のディレクトリにコピーします。

```shell
[任意のディレクトリ]
├── Dockerfile
└── extra.conf
```

### Dockerfile

以下の設定を確認・修正してください。

- `Fluentd`のバージョンを、利用者の環境に応じて修正してください
- `extra.conf`をコピーする設定を追加してください

以下に設定例を示します。

```Dockerfile
# Be sure to update the versions to the ones you require
FROM fluent/fluentd:v1.7.4-1.0

# Required if you're not storing this config on S3.
# In this case, you need to change the reference in the task     definition as well.
# ADD fluentd.conf /extra.conf

# Use root account to install apk
USER root

# Below RUN installs the concat and cloudwatch-logs plugin.
# You may want to adjust these to the plugins you require
RUN apk add --no-cache --update --virtual .build-deps \
        sudo build-base ruby-dev \
  && sudo gem install fluent-plugin-concat \
  && sudo gem install fluent-plugin-cloudwatch-logs \
  && sudo gem sources --clear-all \
  && apk del .build-deps \
  && rm -rf /tmp/* /var/tmp/* /usr/lib/ruby/gems/*/cache/*.gem

COPY extra.conf /fluentd/etc/

USER fluent
```

### extra.conf

改行を含むログを1行に集約するための設定を追記します。以下に例を示します。

```aconf
<filter **-firelens-**>
  @type concat
  key log
  stream_identity_key container_id
  multiline_start_regexp [ログの先頭行とマッチする正規表現]
</filter>
```

`<filter **-firelens--**>`の指定で、どのログに対して集約処理を適用するのかの条件を設定しています。  
AWS FireLensにより転送されたログには`<container name>-firelens-<task ID>`というタグが付与される（
[参照](https://aws.amazon.com/jp/blogs/containers/under-the-hood-firelens-for-amazon-ecs-tasks/)
）ため、それにあわせて設定します。

改行を含むログを1つのログエントリに集約するためには、「どこからが1つのログエントリ」なのかをFluentdに伝える必要があります。
それを行うパラメータが`multiline_start_regexp`です。  
アプリケーションのログに合わせて、複数行のログの先頭にマッチするような正規表現パターンを指定してください。

`multiline_start_regexp`以外のパラメータの詳細は[こちら](https://github.com/fluent-plugins-nursery/fluent-plugin-concat)を参照してください。

## 作成したイメージをAmazon ECRにプッシュする

上記の設定後、Dockerイメージを作成してAmazon ECRにpushします。リポジトリは事前に作成しておいてください。  
[ci_pipeline pattern](../../patterns/aws/ci_pipeline.md)で作成するAmazon ECRリポジトリに加えるとよいでしょう。

以下に例を示します（123456789012は、実際にお使いのAWSアカウントIDに読み替えてください）。

```shell
$ docker image build -t [イメージ名:タグ] .
$ docker image tag [イメージ名:タグ] 123456789012.dkr.ecr.region.amazonaws.com/[イメージ名:タグ]
$ docker image push 123456789012.dkr.ecr.region.amazonaws.com/[イメージ名:タグ]
```

## 作成したイメージを使用したタスク定義を記述する

[前提事項、関連資料](#前提事項、関連資料)の参照先では、
[Fluent Bitイメージを使ってログルーティング](ecs_log_send_to_cloudwatch_logs_using_firelens.md#タスク定義でAWS-FireLensを使い、Amazon-CloudWatch-Logsにログを送信する)
する手順を記載しています。  
Fluentdでログを処理するために、タスク定義を以下のように変更します。

### ログルーターの設定

Fluentdのイメージを使用するために、ログルーターの`image`の内容を
[作成したイメージをAmazon ECRにプッシュする](#作成したイメージをAmazon-ECRにプッシュする)でプッシュしたコンテナイメージのURIに変更します。

```json
   {
      "essential": true,
      "image": "[Amazon ECRにプッシュしたFluentdコンテナイメージのURI]",
      "name": "log_router",
      "firelensConfiguration": {
        "type": "fluentd",
        "options": {
          "config-file-type": "file",
          "config-file-value": "/fluentd/etc/extra.conf"
        }
      },
      ...
    },
```

### タスク内で動作するコンテナの設定

ログルーター以外のタスク内で動作するコンテナの`logConfiguration`は、以下のように設定します。

```json
      "logConfiguration": {
        "logDriver":"awsfirelens",
        "options": {
          "@type": "cloudwatch_logs",
          "region": "ap-northeast-1",
          "log_group_name": "[ロググループ名]",
          "auto_create_group": "false",
          "auto_create_stream": "true",
          "use_tag_as_stream": "true"
        }
      },
```

`options`は、Fluentdの`CloudWatch Logsプラグイン`のパラメータにあわせて設定します。  
パラメータの詳細は[こちら](https://github.com/fluent-plugins-nursery/fluent-plugin-cloudwatch-logs)を参照してください。

## Datadogへのログ転送

上記までの設定により、Amazon CloudWatch Logs上で複数行のログを1行に集約して出力できるようになります。  
このログをDatadogに転送することで、Datadogでも集約したログを参照できるようになります。詳細は以下のガイドを参照してください。

- [datadog_log_trigger pattern](../../patterns/aws/datadog_log_trigger.md)
- [datadog_forwarder pattern](../../patterns/aws/datadog_forwarder.md)
