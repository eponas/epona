# Datadogのアラート設定

## 概要

本ガイドでは監視およびモニタリングに関するSaaSである[Datadog](https://www.datadoghq.com/ja/)のアラートの設定方法を記載します。
アラートを設定することでシステムの異常を自動で検知でき、その異常を利用者に任意の方法で通知できます。

アラートの設定は以下2つの設定で構成されます。

- インテグレーション設定（通知先の設定）
- アラート設定（モニターおよび通知の設定）

また、アラート設定はサンプルとなるTerraformコードを案内します。

----

:information_source: Eponaがこれらの設定機能をpatternとして提供しません。これは以下の理由に依ります。

- インテグレーション設定がTerraformでサポートされていないため
- アラート設定はTerraformがサポートしているものの、その設定内容は運営するサービスに強く依存し、汎用的な形で提供ができないため

一方で、IaC (Infrastructure as Code)の恩恵に預かるためには、アラート設定はTerraform化すべきと考えています。
このため、アラート設定をどのように記述するかのサンプルとなるTerraformコードをあわせて案内します。

----

## 想定する適用対象環境

本ガイドはRuntime環境で使用することを想定しています。

## 設定方法

本ガイドでは大きく以下2つの手順を案内します。1->2の順で実施してください。

1. [インテグレーション設定](#インテグレーション設定)
2. [アラート設定](#アラート設定)

`インテグレーション設定`はDatadogが連携する通知先の設定です。

`アラート設定`では、どのようなメトリクス/ログを監視するかの`モニター`と、どのようにユーザーへ通知するかの`通知`を設定します。

### インテグレーション設定

通知先の設定を[インテグレーション](https://docs.datadoghq.com/ja/monitors/notifications/?tab=is_alert#%E3%82%A4%E3%83%B3%E3%83%86%E3%82%B0%E3%83%AC%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3)として設定します。

本ガイドでは通知先として`Microsoft Teams`(以下、Teams)、`Slack`、`Mail`を想定しています。
それぞれの設定方法については、以下のDatadogの公式ドキュメントを確認ください。

- [Teamsの追加方法](https://docs.datadoghq.com/ja/integrations/microsoft_teams/)
- [Slackの追加方法](https://docs.datadoghq.com/ja/integrations/slack)

インテグレーションを設定し、通知本文に`@<インテグレーション名>-<値>`を含めることで、当該インテグレーションを通知先にできます。
例えばSlackのインテグレーションを設定し、通知本文に`@slack-[channel]`と指定すれば、アラートをSlackの指定channelに通知できます。

なお、各インテグレーションで設定する`CHANNEL NAME`（※）はTeamsの場合`teams-`、Slackの場合`slack-`のプレフィックスが自動で付与されます。
そのため`CHANNEL NAME`には、純粋に通知を表示させたいチャンネル名を指定するのがおすすめです。

また、`Mail`での通知にはインテグレーションを構成する必要はありません。
アラート設定における通知先の設定（messageの`@`）にてメールアドレスを指定すれば、`alert@dtdg.co`よりメール通知が送られます。
例えば、`hoge@test.com`というメールアドレスに通知したい場合は`@hoge@test.com`という指定になります。

（※）CHANNEL NAMEは以下の手順で設定するポスト先のチャンネルの名前です。上記リンクから以下のようにページを辿ってください。

- [Teamsの追加方法]-[セットアップ]-[手順5]
- [Slackの追加方法]-[コンフィグレーション]-[手順8]

### アラート設定

アラートは`モニター`および`通知`で設定します。

`モニター`は監視設定です。どのようなメトリクスやログメッセージが検出されたときに異常と見なすか設定します。
`モニター`はいくつかのタイプがDatadogにより用意されています。
利用者はモニターのタイプを選択し、監視したい条件を設定します。
各タイプの説明などは[こちら](https://docs.datadoghq.com/ja/monitors/monitor_types/)を参照ください。
または、[モニタータイプについて](#モニタータイプについて)にも各タイプの簡単な説明を記載しています。

`通知`は通知設定です。モニターで検知した異常をどのようなメッセージでユーザーへ知らせるか、またどのような方法（サービス、メール等）で知らせるか設定します。
`通知`の設定は`モニター`の設定と同時に行います。Terraformだと`message`の部分で設定します。
`通知`について詳しくは[こちら](https://docs.datadoghq.com/ja/monitors/notifications/?tab=is_alert)の公式ドキュメントを参照ください。

アラートはWeb UIとTerraformで設定できます。
Eponaではインフラ設定の管理をIaCで行うことを推奨しています。
一方で、`モニター`の「どのような条件でアラートを発報するか」を指定する条件（Datadogでは`query`と呼びます）をゼロからコードで記述するのは困難です。
DatadogのWeb UIであれば様々な補完機能を利用してスムーズに`query`を記述できます。
そのため、Web UIで`query`などのモニターに関する設定情報を生成し、その後Terraformで設定する以下方法を案内します。

- DatadogのWeb UIに接続します。[Monitor]-[New Monitor]を表示し、任意のモニタータイプを選択します
- モニターの設定をします。設定方法は[こちら](https://docs.datadoghq.com/ja/monitors/monitor_types/)のDatadogの公式ドキュメントを参照ください
- 入力が完了したら`Export Monitor`を選択し、設定内容のJSONをコピーしてください
- JSONをコピーしたらブラウザバックなどで設定をキャンセルします（最後の`Save`はしない）
- Terraformのコードを記述します。コードはサンプルコードを参考にしてください。また、とくに以下の点に注意ください
  - `query`は先ほどコピーしたJSON内の`query`部分から値をコピーして貼り付ける
  - `message`の最後に@で通知先（`インテグレーション名-CHANNEL NAME`や`メールアドレス`）を指定する
- Terraformを実行します。実行後、DatadogのWeb UIに接続し、[Monitor]-[Manage Monitors]を表示し設定できていることを確認ください
- 通知をテストするには以下で行います
  - [Monitor]-[Manage Monitors]でモニターを表示
  - モニターを選択し[edit]で編集画面を表示し、ページ下部の`Test Notifications`でインテグレーション宛に通知を送れる

## サンプルコード

アラートをTerraformで設定するサンプルコードを以下に記載します。
なお、`datadog provider`の実行には`api_key`と`app_key`を使用します。あらかじめDatadogで各keyを発行してください。
また、各keyの値は慎重に管理してください。Terraformのコードにハードコードしてしまうと漏洩につながる恐れがあるため、環境変数やリポジトリ管理外のterraform.tfvarsなどで指定して実行しましょう。

### ログの通知

以下のコードは、Logsモニターでログを監視し任意のメッセージを検知した際に、`Teams`のチャネルおよび`hoge@test.com`のメールアドレスに通知するサンプルです。
あらかじめTeamsをインテグレーションに追加しており、チャンネル名は`runtime-monitor`としてある前提です。

サンプルのコードでは、[Amazon RDS](https://aws.amazon.com/jp/rds/)から出力された
`database system is shut down`を含むメッセージが1件以上発生すると、criticalレベルのアラートを発報します。
文字列は`@msg`で指定していますが、これはあらかじめログをパースし本文を`@msg`ファセットで検索できるようにしてあります。
ログをパースする方法について[こちら](./datadog_log_analysis.md)のガイドを参照ください。

```terraform
variable "api_key" {}
variable "app_key" {}

provider "datadog" {
  api_key = var.api_key
  app_key = var.app_key
}

resource "datadog_monitor" "rds_shutdown" {
  name    = "rds_shutdown"
  type    = "log alert"
  message = "`database system is shut down` has been detected @teams-runtime-monitor @hoge@test.com"
  query   = "logs(\"service:rds @message:\\\"  database system is shut down\\\"\").index(\"*\").rollup(\"count\").last(\"5m\") >= 1"

  thresholds = {
    warning  = 0.1
    critical = 1
  }

  enable_logs_sample = true

  lifecycle {
    ignore_changes = [silenced]
  }

  tags = ["terraform", "epona"]
}
```

なお、`silenced`をignore_changesにしている理由は[こちら](https://registry.terraform.io/providers/DataDog/datadog/latest/docs/resources/monitor#silencing-by-hand-and-by-downtimes)を参照ください。

上記Terraformでアラートを作成すると、Teamsおよびメールには以下のように通知されます。
なお、下記キャプチャはTest用の発報です。また、チャンネル名とメールアドレスは上記サンプルコードと若干異なります。

#### Teams

![Teams通知例](../resources/datadog_monitor_teams.png)

#### Mail

![Mail通知例](../resources/datadog_monitor_mail.png)

### メトリクスの通知

Metricモニターでメトリクスを監視し任意のメトリクスがしきい値を超えた際に`Slack`へ通知するサンプルです。
あらかじめSlackをインテグレーションに追加しており、チャンネル名は`runtime-monitor`としてある前提です。
サンプルのコードだと「指定したRoute 53ホストゾーン（ドメイン）のDNSクエリ総数」が「2回以上だとcritical」「1回だとwarning」を発報します。

```terraform
variable "api_key" {}
variable "app_key" {}

provider "datadog" {
  api_key = var.api_key
  app_key = var.app_key
}

resource "datadog_monitor" "route53_dnsquerys_teams" {
  name    = "route53_dnsquerys"
  type    = "metric alert"
  message = "`aws.route53.dnsqueries` was detected. this message setup by terraform : @slack-runtime-monitor"
  query   = "sum(last_5m):avg:aws.route53.dnsqueries{hostedzoneid:z10456113p4voj60j2cu7,region:us-east-1}.as_count() >= 2"

  thresholds = {
    warning  = 1
    critical = 2
  }

  notify_no_data    = false
  renotify_interval = 60

  notify_audit = false
  timeout_h    = 60
  include_tags = true

  lifecycle {
    ignore_changes = [silenced]
  }

  tags = ["terraform", "epona"]
}
```

`silenced`をignore_changesにしている理由は[こちら](https://registry.terraform.io/providers/DataDog/datadog/latest/docs/resources/monitor#silencing-by-hand-and-by-downtimes)を参照ください。

上記Terraformでアラートを作成するとSlackには以下のように通知されます。
なお、下記キャプチャはTest用の発報です。また、チャンネル名は上記サンプルコードと若干異なります。

#### Slack

![Slack通知例](../resources/datadog_monitor_slack.png)

## モニタータイプについて

Web UIで設定できるタイプとTerraformで設定できるタイプでは名前と数が異なります。
しかし、これはWeb UIでしか設定できないタイプがあるということではありません。
Web UIは見かけ上たくさんのタイプがありますが、実際にはTerraformで設定できるタイプのAPIを使用しています。
詳しくは[こちら](https://docs.datadoghq.com/ja/api/v1/monitors/)のDatadogドキュメントを参照ください。
まとめるとWeb UIとTerraformのタイプの紐付けは以下の通りとなります。

| Web UIタイプ名 | Terraformタイプ名                    | タイプの説明                                                                         |
| -------------- | ------------------------------------ | ------------------------------------------------------------------------------------ |
| anomaly        | query alert                          | メトリクスの異常動作を履歴データに基づいて検出します。                               |
| APM            | query alert or trace-analytics alert | APMメトリクスをユーザー定義のしきい値と比較します。                                  |
| composite      | composite                            | 複数のモニターを組み合わせた式に対してアラートします。                               |
| custom         | service check                        | 任意のカスタムチェックのステータスを監視します。                                     |
| event          | event alert                          | Datadogによって収集されたイベントを監視します。                                      |
| forecast       | query alert                          | メトリクスが将来しきい値を超えるかどうかを予測して、超えそうな場合にアラートします。 |
| host           | service check                        | Datadogに報告を行っているホストがあるかどうかをチェックします。                      |
| integration    | query alert or service check         | 特定のインテグレーションのメトリクス値または健全性ステータスを監視します。           |
| live process   | process alert                        | ホストで実行中のプロセスがあるかどうかをチェックします。                             |
| logs           | log alert                            | Datadogによって収集されたログを監視します。                                          |
| metric         | metric alert                         | メトリクスの値をユーザー定義のしきい値と比較します。                                 |
| network        | service check                        | TCP/HTTPエンドポイントのステータスをチェックします。                                 |
| outlier        | query alert                          | グループ内の他のメンバーと挙動が異なるメンバーについてアラートします。               |
| process        | service check                        | process.upサービスチェックで生成されたステータスを監視します。                       |
| rum            | rum alert                            | Datadogによって収集されたリアルなユーザーデータを監視します。                        |
| watchdog       | event alert                          | Watchdogが異常動作を検出した場合に通知を受け取ります。                               |
