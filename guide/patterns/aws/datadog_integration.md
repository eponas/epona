# datadog_integration pattern

## 概要

`datadog_integration pattern`モジュールでは、監視およびモニタリングに関するSaaSである[Datadog](https://www.datadoghq.com/ja/)とAWSのインテグレーションを実現します。

このpatternを使用すると、DatadogとAWSのインテグレーションが行われ、Amazon CloudWatchのメトリクスの取得やプリセットされたダッシュボードを利用できます。

この後、Datadogを利用してのサービス固有のダッシュボードの作成、監視設定の基礎となるpatternです。

## 想定する適用対象環境

`datadog_integration pattern`は、Runtime環境で使用することを想定しています。

## 依存するpattern

`datadog_integration pattern`は、他のpatternの実行結果に依存しません。独立して実行できます。

:warning: Datadog自体の利用手続きは、利用者自身で行っていることが前提となります。  
:warning: `datadog_integration pattern`は、Datadog利用手続きに関する機能は提供しません。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                         | 説明                                                             |
| :--------------------------------------------------------------------------------- | :--------------------------------------------------------------- |
| [AWS IAM](https://aws.amazon.com/jp/iam/)                                          | Datadogへ必要なアクセス許可を与えるための、IAMロールを構築します |
| [AWS Integration](https://docs.datadoghq.com/ja/integrations/amazon_web_services/) | DatadogとAWSのインテグレーションを行います                       |

## モジュールの理解に向けて

### TerraformでのDatadogの利用

Datadogは、Terraformから操作可能です。Datadog自身が、TerraformのProviderを提供しています。

[Datadog Provider](https://www.terraform.io/docs/providers/datadog/index.html)

以下のようなTerraformの定義を記述することで、Datadog Providerを利用可能です。

```terraform
terraform {
  ...

  required_providers {
    ...
    aws = {
      source  = "hashicorp/aws"
      version = "[AWS Providerのバージョン]"
    }

    datadog = {
      source  = "DataDog/datadog"
      version = "[Datadog Providerのバージョン]"
    }
    ...
  }

  ...
}

provider "datadog" {
}
```

Datadog Providerは[DatadogのAPI](https://docs.datadoghq.com/ja/api/)を使用するため、APIキーおよびアプリケーションキーが必要です。

以下のドキュメントを参照して、APIキーおよびアプリケーションキーを取得してください。

[API キーとアプリケーションキー](https://docs.datadoghq.com/ja/account_management/api-app-keys/)

取得したAPIキーおよびアプリケーションキーは、Terraformを実行する際に、環境変数として設定してください。Datadogのクレデンシャル情報となりますので、Terraformの定義ファイル等へのハードコードは避けてください。

#### Linuxの場合

```shell
$ export DD_API_KEY=[DatadogのAPIキー]
$ export DD_APP_KEY=[Datadogのアプリケーションキー]
```

#### Windowsの場合

```shell
> set DD_API_KEY=[DatadogのAPIキー]
> set DD_APP_KEY=[Datadogのアプリケーションキー]
```

### AWSインテグレーション時のモニタリング対象の制御

Datadogでは本patternの適用で実行されるAWSインテグレーションにより、デフォルトではすべてのEC2インスタンスがモニタリング対象となり、
課金が発生します。

[AWS インテグレーションの課金](https://docs.datadoghq.com/ja/account_management/billing/aws/)

一方で、コストを抑制するためにモニタリングする対象を限定したい[^2]という要望が発生し得ます。
この目的のため、Datadogではモニタリングの対象を除外する仕組みがあります。

[^2]: 例えば、特定のVPC内のEC2のみモニタリングを行いたいといったケースがあります。

[AWS リソースの除外](https://docs.datadoghq.com/ja/account_management/billing/aws/#aws-%E3%83%AA%E3%82%BD%E3%83%BC%E3%82%B9%E3%81%AE%E9%99%A4%E5%A4%96)

EC2に特定の[タグを付与](https://docs.aws.amazon.com/ja_jp/AWSEC2/latest/UserGuide/Using_Tags.html)することで、モニタリング対象にする・しないを制御できます。

どのようなタグで制御するかについては、本patternの引数`filter_tags`で定義できます。
記述方法については、以下のDatadogのマニュアルをご参照ください。

[タグの使用方法](https://docs.datadoghq.com/ja/getting_started/tagging/using_tags/?tab=assignment#%E3%82%A4%E3%83%B3%E3%83%86%E3%82%B0%E3%83%AC%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3)

例えば、本ページ下部のサンプルコードにあるように以下の設定をすると、`datadog:monitored`というタグを付与されたEC2のみがモニタリング対象となります。

```terraform
filter_tags = ["datadog:monitored"]
```

`filter_tags`で指定した内容は、DatadogのAWS Integrationページ内で反映されたことを確認できます。

[AWS Integration](https://app.datadoghq.com/account/settings#integrations/amazon-web-services)[^1]

上記ページの、`Optionally limit resource collection`の`to hosts with tag`を参照してください。

### IAMロールとDatadogのAWS Integration

`datadog_integration pattern`では、Terraformを使用したAWS Integrationのドキュメントに従い、IAMロールを作成します。

[The AWS Integration with Terraform](https://docs.datadoghq.com/ja/integrations/faq/aws-integration-with-terraform/)

これは、以下のページ内の「手動」セットアップの手順に相当します。

[AWS Integration](https://docs.datadoghq.com/ja/integrations/amazon_web_services/)

このIAMリソースを作成する際に、Datadogと連携するためのExternal IDが必要となります。これは、Datadogのページから取得できます。

`datadog_integration pattern`ではExternal IDは`datadog_integration_aws`リソースにて作成するため、通常、意識することはありません。

[datadog_integration_aws](https://www.terraform.io/docs/providers/datadog/r/integration_aws.html)

ただし、AWS IntegrationにおけるDatadogとの連携条件のひとつとなるため、存在については押さえておいてください。

また、本patternを使用したAWS Integrationがうまくいかない場合にも、使用することになります。

### AWS用にプリセットされたダッシュボード

DatadogによるAWS Integrationが成功すると、[ダッシュボードリスト](https://app.datadoghq.com/dashboard/lists)[^1]にプリセットされているダッシュボードが表示されます。

[^1]: Datadogへのログインが必要です。

プリセットされたダッシュボードには、Amazon ECSやAmazon RDSなどAWSのリソースに対するメトリクスが表示されるように設定されています。  
これらのダッシュボードは、モニタリングの第一歩としては非常に有用です。

ただ、実際のダッシュボードを見ていると、タイムラグがあるように感じられるでしょう。

DatadogはAmazon CloudWatchよりメトリクスを取得しますが、これには実行間隔があります。詳細は、以下のドキュメントを参照してください。

- [AWS Integration and CloudWatch FAQ](https://docs.datadoghq.com/ja/integrations/faq/aws-integration-and-cloudwatch-faq/)
- [Cloud Metric Delay](https://docs.datadoghq.com/ja/integrations/faq/cloud-metric-delay/)

### AWS Integrationの解除

DatadogとAWSとのIntegrationを解除する場合、本patternのインスタンスに対して、`terraform destroy`を行います。

Datadog上では、本patternで関連付けられたAWSアカウントとの紐付けが削除されます。

:information_source: `create_iam_role_only`を`true`にしている場合は、Datadog上でAWSアカウントの削除が必要です。

### AWS Integrationに失敗する場合

`datadog_integration pattern`を実行しても、AWS Integrationに失敗することがあります。

AWS Integratoinに失敗している場合、[Datadog上のAWS Integrationのページ](https://app.datadoghq.com/account/settings#integrations/amazon-web-services)[^1]を開くと次のように権限不足で`BROKEN`と表示されます。

![AWS Integration失敗](../../resources/datadog_integration_broken.png)

うまくいかない場合、`datadog_integration pattern`の実行後に`Update Configuration`ボタンを押して設定を更新してください。

![AWS Integration Update Configuration](../../resources/datadog_integration_update_configuration.png)

> `Update Configuration`ボタンは、[Datadog上のAWS Integrationのページ](https://app.datadoghq.com/account/settings#integrations/amazon-web-services)の下部にあります

やや時間がかかりますが、プリセットされたダッシュボードにメトリクスが表示されるようになります。

それでもうまく行かない場合は、`create_iam_role_only`を`true`とすることで、作成するリソースをIAMロールのみにとどめ、インテグレーション自体はDatadog側で行います。

`datadog_integration pattern`モジュールには、以下のように設定します。

```terraform
  create_iam_role_only = true
  aws_integration_external_id = "[External ID]"
```

External IDは、[Datadog上のAWS Integrationのページ](https://app.datadoghq.com/account/settings#integrations/amazon-web-services)にて取得できます。

![AWS Integration External ID](../../resources/datadog_integration_external_id.png)

## サンプルコード

`datadog_integration pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "datadog_integration" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/datadog_integration?ref=v0.2.6"

  integrate_aws_account_id = "232105380006"

  tags = {
    # 任意のタグ
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }

  filter_tags = ["datadog:monitored"]
}
```

## 関連するpattern

`datadog_integration pattern`の結果を利用するpatternはありません。

AWSとのインテグレーション後、Datadogに送信されてくるメトリクスを元にダッシュボードやアラートを設定してください。

- [Datadogのダッシュボード設定](../../how_to/datadog_dashboard.md)
- [Datadogのアラート設定](../../how_to/datadog_monitor.md)

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| datadog | >= 2.25.0, < 3.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| integrate\_aws\_account\_id | Datadogと紐付けるAWSアカウントID | `string` | n/a | yes |
| account\_specific\_namespace\_rules | このアカウント内の特定の名前空間に対して、メトリクス収集の有効、無効を設定する | `map(bool)` | `null` | no |
| allow\_actions | Datadogに実行許可するアクションを、リストで指定する。<br>  どのようなアクションが指定可能かは、[DatadogのAWSインテグレーション](https://docs.datadoghq.com/ja/integrations/amazon_web_services/?tab=cloudformation)を参照すること。<br><br>  デフォルトでは、[DatadogのAWSインテグレーション]に記載されているすべてのアクセスを権限を許可している。 | `list(string)` | <pre>[<br>  "apigateway:GET",<br>  "autoscaling:Describe*",<br>  "budgets:ViewBudget",<br>  "cloudfront:GetDistributionConfig",<br>  "cloudfront:ListDistributions",<br>  "cloudtrail:DescribeTrails",<br>  "cloudtrail:GetTrailStatus",<br>  "cloudtrail:LookupEvents",<br>  "cloudwatch:Describe*",<br>  "cloudwatch:Get*",<br>  "cloudwatch:List*",<br>  "codedeploy:List*",<br>  "codedeploy:BatchGet*",<br>  "directconnect:Describe*",<br>  "dynamodb:List*",<br>  "dynamodb:Describe*",<br>  "ec2:Describe*",<br>  "ecs:Describe*",<br>  "ecs:List*",<br>  "elasticache:Describe*",<br>  "elasticache:List*",<br>  "elasticfilesystem:DescribeFileSystems",<br>  "elasticfilesystem:DescribeTags",<br>  "elasticfilesystem:DescribeAccessPoints",<br>  "elasticloadbalancing:Describe*",<br>  "elasticmapreduce:List*",<br>  "elasticmapreduce:Describe*",<br>  "es:ListTags",<br>  "es:ListDomainNames",<br>  "es:DescribeElasticsearchDomains",<br>  "health:DescribeEvents",<br>  "health:DescribeEventDetails",<br>  "health:DescribeAffectedEntities",<br>  "kinesis:List*",<br>  "kinesis:Describe*",<br>  "lambda:AddPermission",<br>  "lambda:GetPolicy",<br>  "lambda:List*",<br>  "lambda:RemovePermission",<br>  "logs:DeleteSubscriptionFilter",<br>  "logs:DescribeLogGroups",<br>  "logs:DescribeLogStreams",<br>  "logs:DescribeSubscriptionFilters",<br>  "logs:FilterLogEvents",<br>  "logs:PutSubscriptionFilter",<br>  "logs:TestMetricFilter",<br>  "rds:Describe*",<br>  "rds:List*",<br>  "redshift:DescribeClusters",<br>  "redshift:DescribeLoggingStatus",<br>  "route53:List*",<br>  "s3:GetBucketLogging",<br>  "s3:GetBucketLocation",<br>  "s3:GetBucketNotification",<br>  "s3:GetBucketTagging",<br>  "s3:ListAllMyBuckets",<br>  "s3:PutBucketNotification",<br>  "ses:Get*",<br>  "sns:List*",<br>  "sns:Publish",<br>  "sqs:ListQueues",<br>  "states:ListStateMachines",<br>  "states:DescribeStateMachine",<br>  "support:*",<br>  "tag:GetResources",<br>  "tag:GetTagKeys",<br>  "tag:GetTagValues",<br>  "xray:BatchGetTraces",<br>  "xray:GetTraceSummaries"<br>]</pre> | no |
| aws\_integration\_external\_id | `create_iam_role_only`を`false`とした場合、必須。DatadogのAWS Integrationページより、External IDを取得して設定する | `string` | `null` | no |
| create\_iam\_role\_only | IAMロールの作成までにとどめ、DatadogとAWSのインテグレーションまでは行わない場合、`true`を指定する | `bool` | `false` | no |
| excluded\_regions | メトリクス収集から除外するリージョンを指定する | `list(string)` | `null` | no |
| filter\_tags | メトリクスの収集対象とのあるEC2を制御するためのタグを指定する。指定しない場合は、`integrate_aws_account_id`で指定するAWSアカウント配下の全EC2が収集対象となる。指定方法については[タグの使用方法](https://docs.datadoghq.com/ja/getting_started/tagging/using_tags/?tab=assignment#%E3%82%A4%E3%83%B3%E3%83%86%E3%82%B0%E3%83%AC%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3)を参照 | `list(string)` | `null` | no |
| host\_tags | このインテグレーションによりレポートされる、すべてのホストとメトリクスに追加するタグを指定する | `list(string)` | `null` | no |
| integration\_policy\_name | Datadog連携のために作成するポリシーの名前 | `string` | `"DatadogAWSIntegrationPolicy"` | no |
| integration\_role\_name | Datadog連携のために作成するIAMロール名 | `string` | `"DatadogAWSIntegrationRole"` | no |
| tags | Datadogと連携するIAMロール関連のリソースに、共通的に付与するタグ | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| datadog\_aws\_integration\_external\_id | AWS Integrationを行ったExternal ID |
