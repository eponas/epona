# rule_violation pattern

## 概要

`rule_violation pattern`は、AWS環境に対する構成変更を監視し、本patternで定義したルールに違反する変更を検知した場合にはチャットツールへの通知を実施するモジュールです。

## 想定する適用対象環境

`rule_violation pattern`は、Delivery環境およびRuntime環境のいずれでも使用されることを想定しています。

## 依存するpattern

`rule_violation pattern`には、事前に実行が必要なpatternはありません。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                           | 説明                                                                             |
| :----------------------------------------------------------------------------------- | :------------------------------------------------------------------------------- |
| [AWS Config](https://aws.amazon.com/jp/config/)                                      | 事前に設定したルールをもとに、構成変更を監視するサービス                         |
| [S3バケット](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/dev/UsingBucket.html) | 構成変更に関するログを保存するバケット                                           |
| [AWS Lambda](https://aws.amazon.com/jp/lambda/)                                      | ルールに違反する構成変更があった場合に、チャットツールへ通知するための関数       |
| [AWS SNS](https://aws.amazon.com/jp/sns/)                                            | AWS Configからイベントを受け取り、チャットツールに通知する関数を呼び出すトピック |
| [AWS IAM](https://aws.amazon.com/jp/iam/)                                            | AWS ConfigやAWS Lambdaが使用するIAMポリシー、ロール                              |

## モジュールの理解に向けて

継続的にサービス開発を行い、AWS環境の構成の変更が続いていく中で、誤設定によるセキュリティリスクや予期しない構成変更を人手で監視し続けるのは困難です。  
AWS Configを利用することで、構成変更を監視し、ルールに違反する変更を自動的に検知することが可能になります。  
本モジュールではAWS Configサービス本体の立ち上げに加え、デフォルトの構成ルールの作成と、ルール違反を利用者に通知する仕組みが提供されます。

![イメージ](../../resources/rule_violation.png)

## デフォルトで設定される内容について

### AWS Config

リージョンごとにAWS Configを有効化します。複数リージョンにAWS Configの設定が必要となる場合は、リージョンごとに本モジュールを適用してください。

AWS Configは設定レコーダー(Configuration Recorder)と配信チャネル(Delivery Channel)というリソースを使用します。これらは
[1リージョンに1つのみ](https://docs.aws.amazon.com/ja_jp/config/latest/developerguide/gs-cli-subscribe.html)
作成できます。  
このため、すでにAWS Configが設定されているリージョンに本モジュールを適用する場合は、設定レコーダーや配信チャネルの作成をスキップする
必要があります。  
これは`create_config_recorder_delivery_channel`を`false`に設定することで実現が可能です。

本モジュールで作成したAWS Configでは、そのリージョンで監視可能な全リソースを監視対象とします。  
監視対象のリソースを限定する場合は`all_supported`を`false`に設定し、`recording_resource_types`に監視対象のリソースを指定してください。
[terraformのドキュメント](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/config_configuration_recorder)もあわせて参照してください。

なお、複数リージョンに対して本モジュールを適用する場合、IAMなどのグローバルリソースについては全リージョンで同じ情報が（重複して）記録されます。  
グローバルリソースを監視する必要のないリージョンについては、`include_grobal_resouces`を`false`とすることで監視しないよう設定できます。

6時間ごとに設定のスナップショットを取得しS3バケットに配信するように構成しています。配信間隔を変更する場合はパラメータの`delivery_frequency`に任意の値を設定してください。

### マネージドルール

以下のルールが設定されます。

| ルール名                                   | 説明                                                                                                                                                                          |
| :----------------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ACCESS_KEYS_ROTATED                        | maxAccessKeyAgeで指定された日数以内にアクティブなアクセスキーがローテーションされるかどうかを確認します。                                                                     |
| ACM_CERTIFICATE_EXPIRATION_CHECK           | アカウントのACM証明書に、指定された日数内に有効期限がマークされているかどうかを確認します。                                                                                   |
| CLOUD_TRAIL_ENCRYPTION_ENABLED             | AWS CloudTrailがサーバー側の暗号化（SSE）AWS Key Management Service（AWS KMS）カスタマーマスターキー（CMK）暗号化を使用するように構成されているかどうかを確認します。         |
| CLOUD_TRAIL_ENABLED                        | AWSアカウントでAWS CloudTrailが有効になっているかどうかを確認します。                                                                                                         |
| CLOUD_TRAIL_LOG_FILE_VALIDATION_ENABLED    | AWS CloudTrailがログ付きの署名済みダイジェストファイルを作成するかどうかを確認します。                                                                                        |
| DB_INSTANCE_BACKUP_ENABLED                 | RDS DBインスタンスでバックアップが有効になっているかどうかを確認します。                                                                                                      |
| EC2_INSTANCE_NO_PUBLIC_IP                  | Amazon Elastic Compute Cloud（Amazon EC2）インスタンスにパブリックIPアソシエーションがあるかどうかを確認します。                                                              |
| INSTANCES_IN_VPC                           | EC2インスタンスが仮想プライベートクラウド（VPC）に属しているかどうかを確認します。                                                                                            |
| ELB_LOGGING_ENABLED                        | Application Load BalancerとClassic Load Balancerのログが有効になっているかどうかを確認します。                                                                                |
| IAM_PASSWORD_POLICY                        | IAMユーザーのアカウントパスワードポリシーが指定された要件を満たしているかどうかを確認します。                                                                                 |
| IAM_POLICY_NO_STATEMENTS_WITH_ADMIN_ACCESS | 全リソースに対する全アクションをAllowするIAMポリシーを作成していないかを確認します。                                                                                          |
| IAM_ROOT_ACCESS_KEY_CHECK                  | rootユーザーアクセスキーが使用可能かどうかを確認します。                                                                                                                      |
| MFA_ENABLED_FOR_IAM_CONSOLE_ACCESS         | コンソールパスワードを使用するすべてのAWS Identity and Access Management（IAM）ユーザーに対してAWS Multi-Factor Authentication（MFA）が有効になっているかどうかを確認します。 |
| MULTI_REGION_CLOUD_TRAIL_ENABLED           | 少なくとも1つのマルチリージョンAWS CloudTrailがあることを確認します。                                                                                                         |
| RDS_LOGGING_ENABLED                        | Amazon Relational Database Service（Amazon RDS）の各ログが有効になっていることを確認します。                                                                                  |
| RDS_INSTANCE_PUBLIC_ACCESS_CHECK           | Amazon Relational Database Serviceインスタンスがパブリックにアクセスできないかどうかを確認します。                                                                            |
| RDS_SNAPSHOTS_PUBLIC_PROHIBITED            | Amazon Relational Database Service（Amazon RDS）スナップショットが公開されているかどうかを確認します。                                                                        |
| INCOMING_SSH_DISABLED                      | セキュリティグループの受信SSHトラフィックにアクセスできるかどうかを確認します。                                                                                               |
| ROOT_ACCOUNT_HARDWARE_MFA_ENABLED          | AWSアカウントがマルチ要素認証（MFA）ハードウェアデバイスを使用してルート認証情報でサインインできるかどうかを確認します。                                                      |
| S3_BUCKET_PUBLIC_READ_PROHIBITED           | Amazon S3バケットがパブリック読み取りアクセスを許可していないことを確認します。                                                                                               |
| S3_BUCKET_PUBLIC_WRITE_PROHIBITED          | Amazon S3バケットがパブリック書き込みアクセスを許可していないことを確認します。                                                                                               |
| WAFV2_LOGGING_ENABLED                      | AWSウェブアプリケーションファイアウォール（WAFV2）のリージョンおよびグローバルウェブアクセスコントロールリスト（ACL）でロギングが有効になっているかどうかを確認します。       |

上記ルールに設定できるパラメータについては、[入出力リファレンス](#入出力リファレンス)の`configrule_`というプレフィックスがついているパラメータを参照してください。

利用者側で任意のルールを設定したい場合は、[マネージドルールのリスト](https://docs.aws.amazon.com/ja_jp/config/latest/developerguide/managed-rules-by-aws-config.html)をもとに、以下のようなリソースを追記してください。

```terraform
resource "aws_config_config_rule" "my_config_rule" {
  name        = "${var.configrule_prefix}-my_config_rule"
  description = "ルールの説明"
  source {
    owner             = "AWS"
    source_identifier = "(適用するルールの識別子)"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
  input_parameters = jsonencode(
    {
      (パラメータが必要なルールの場合はここに記入してください)
    }
  )
}
```

デフォルトルールが不要となる場合は、パラメータ`enable_default_rule`を`false`にしてください。

### ルール違反時の通知先

本モジュールでは、検知した情報をチャットツールに連携します。  
マネージドルールに違反した場合だけでなく、構成変更が発生した際毎回通知を送ることも可能です。

- ルール違反した場合だけ通知を受け取りたい場合

```terraform
  notification_channel = {
    changeDetectionNotice = ""
    ruleViolationNotice   = "/services/XXXXXXXXX/XXXXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX"
  }
```

- 構成変更のたびに通知を受け取りたい場合（かつ、通知先を別にしたい場合）

```terraform
  notification_channel = {
    changeDetectionNotice = "/services/YYYYYYYYY/YYYYYYYYYYY/YYYYYYYYYYYYYYYYYYYYYYYY"
    ruleViolationNotice   = "/services/XXXXXXXXX/XXXXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX"
  }
```

通知先には、以下のようなチャットツールが利用可能です。

- [Slack](https://slack.com/intl/ja-jp/)  
- [Microsoft Teams](https://www.microsoft.com/ja-jp/microsoft-365/microsoft-teams/group-chat-software)

:warning: チャットツールへの通知には、webhookを用いたTLS通信を利用しています。  
通知先となるサービスのIncoming Webhookの設定は、Epona利用者で事前に実施いただく必要があります。

チャットツールに連携する内容は以下の項目となっています。

- ルール違反に関する通知

| 通知項目          | 説明                                                    |
| :---------------- | :------------------------------------------------------ |
| subject           | 通知の分類(ComplianceChangeNotification)                |
| awsAccountId      | 違反が発生したAWS Account ID                            |
| awsRegion         | 違反が発生したAWSリージョン                             |
| resourceType      | 違反が発生したリソースのタイプ                          |
| resourceId        | 違反が発生したリソースID                                |
| configRuleName    | 関係するマネージドルール名                              |
| configRuleArn     | 関係するマネージドルールのARN                           |
| newComplianceType | ルールの状態 (準拠：COMPLIANT / 非準拠 : NON_COMPLIANT) |
  
- 設定変更に関する通知

| 通知項目     | 説明                                            |
| :----------- | :---------------------------------------------- |
| subject      | 通知の分類(ConfigurationItemChangeNotification) |
| awsAccountId | 変更が発生したAWS Account ID                    |
| awsRegion    | 変更が発生したAWSリージョン                     |
| resourceType | 変更が発生したリソースのタイプ                  |
| resourceId   | 変更が発生したリソースID                        |
| changeType   | 変更の内容 (CREATE / UPDATE / DELETE)           |

### S3バケットのライフサイクル

AWS Configが変更情報を記録するS3バケットは、以下の設定となっています。

- プライベートバケット
- バージョニング有効
- サーバーサイド暗号化有効
- ライフサイクルルールあり
  - 30日経過後、Gracierに移行

バージョニングの有無、ライフサイクルルールの日数やストレージクラスについてはパラメータで変更可能です。  
詳細は[入出力リファレンス](#入出力リファレンス)をご参照ください。

## サンプルコード

`rule_violation pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "sample_aws_config" {
  source                  = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/rule_violation?ref=v0.2.6"
  s3_bucket_name          = "rule-violation-bucket"
  tags                    = { "Name" = "rule-violation" }
  function_name           = "ruleViolationFunction"
  aws_config_role_name    = "ruleViolation"
  notification_topic_name = "rule-violation-topic"
  notification_type       = "slack"
  notification_channel = {
    changeDetectionNotice = ""
    ruleViolationNotice   = "/services/XXXXXXXXX/XXXXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX"
  }
  create_config_recorder_delivery_channel = true
}
```

## 関連するpattern

`rule_violation pattern`に関連するpatternはありません。

## ログの集約

`rule_violation pattern`では、AWS Lambda関数のログをAmazon CloudWatch Logsに出力します。

このログは、[datadog_log_trigger pattern](datadog_log_trigger.md)を使用することでDatadogに集約できます。

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.37.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| notification\_type | 結果の通知先となるチャットツールの種類（`slack`: Slack \| `teams`: Microsoft Teams）を設定する。 | `string` | n/a | yes |
| s3\_bucket\_name | AWS Configがリソースの設定変更の詳細を送信するS3バケット名を設定する。 | `string` | n/a | yes |
| all\_supported | リージョン内の全リソースを監視対象にするかどうかを設定する。`recording_resource_types`を設定する場合は`false`にする必要がある。 | `bool` | `true` | no |
| aws\_config\_channel\_name | AWS Configがリソースの状態を配信するための「配信チャネル」の名称を設定する。 | `string` | `"rule-violation-channel"` | no |
| aws\_config\_recorder\_name | AWS Configがリソースの設定変更を検出するための「設定レコーダー」の名称を設定する。 | `string` | `"rule-violation-recorder"` | no |
| aws\_config\_role\_name | AWS Configがリソースの情報を収集するために使用するIAMロールの名称を設定する。 | `string` | `"ruleViolation"` | no |
| bucket\_transitions | AWS Configが使用するS3バケットに対するポリシーを設定する。未設定の場合は30日後にAmazon S3 Glacierへ移行する。 | `list(map(string))` | <pre>[<br>  {<br>    "days": 30,<br>    "storage_class": "GLACIER"<br>  }<br>]</pre> | no |
| configrule\_access\_key\_frequency | ACCESS\_KEYS\_ROTATEDルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_access\_key\_max\_age | ACCESS\_KEYS\_ROTATEDルールのアクセスキーのローテーション期間を設定する。 | `string` | `"90"` | no |
| configrule\_acm\_certificate\_frequency | ACM\_CERTIFICATE\_EXPIRATION\_CHECKルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_acm\_days\_to\_expiration | ACM\_CERTIFICATE\_EXPIRATION\_CHECKルールのACM証明書の有効期限を設定する。 | `string` | `"30"` | no |
| configrule\_cloud\_trail\_enabled\_frequency | CLOUD\_TRAIL\_ENABLEDルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_cloud\_trail\_encryption\_frequency | CLOUD\_TRAIL\_ENCRYPTION\_ENABLEDルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_cloud\_trail\_multi\_region\_frequency | MULTI\_REGION\_CLOUD\_TRAIL\_ENABLEDルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_cloud\_trail\_validation\_frequency | CLOUD\_TRAIL\_LOG\_FILE\_VALIDATION\_ENABLEDルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_iam\_mfa\_enabled\_frequency | MFA\_ENABLED\_FOR\_IAM\_CONSOLE\_ACCESSルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_iam\_root\_access\_key\_frequency | IAM\_ROOT\_ACCESS\_KEY\_CHECKルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_password\_max\_age | IAM\_PASSWORD\_POLICYルールのチェック時、パスワードの有効期限を指定する場合に設定する。 | `string` | `"90"` | no |
| configrule\_password\_min\_length | IAM\_PASSWORD\_POLICYルールのチェック時、パスワードの最小の長さを指定する場合に設定する。 | `string` | `"14"` | no |
| configrule\_password\_policy\_frequency | IAM\_PASSWORD\_POLICYルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_password\_require\_lower\_case | IAM\_PASSWORD\_POLICYルールのチェック時、小文字の使用有無を指定する場合に設定する。 | `string` | `"true"` | no |
| configrule\_password\_require\_numbers | IAM\_PASSWORD\_POLICYルールのチェック時、数字の使用有無を指定する場合に設定する。 | `string` | `"true"` | no |
| configrule\_password\_require\_symbols | IAM\_PASSWORD\_POLICYルールのチェック時、記号の使用有無を指定する場合に設定する。 | `string` | `"true"` | no |
| configrule\_password\_require\_upper\_case | IAM\_PASSWORD\_POLICYルールのチェック時、大文字の使用有無を指定する場合に設定する。 | `string` | `"true"` | no |
| configrule\_password\_reuse\_prevention | IAM\_PASSWORD\_POLICYルールのチェック時、パスワードの再使用を禁止する世代数を指定する場合に設定する。 | `string` | `"24"` | no |
| configrule\_prefix | 既存ルールとの重複を避けるため、ルール名に付与するプレフィックスを設定する。 | `string` | `"rule-violation"` | no |
| configrule\_root\_mfa\_frequency | ROOT\_ACCOUNT\_HARDWARE\_MFA\_ENABLEDルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_s3\_public\_read\_frequency | S3\_BUCKET\_PUBLIC\_READ\_PROHIBITEDルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_s3\_public\_write\_frequency | S3\_BUCKET\_PUBLIC\_WRITE\_PROHIBITEDルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| configrule\_waf\_logging\_frequency | WAFV2\_LOGGING\_ENABLEDルールのチェック頻度を設定する。 | `string` | `"TwentyFour_Hours"` | no |
| create\_config\_recorder\_delivery\_channel | AWS Configのリソースのうち、Config Recorder, Deliverly Channelを作成するかどうかを設定する。すでにAWS Configを使用しているリージョンにパターンを適用する場合はfalseとする。 | `bool` | `true` | no |
| delivery\_frequency | AWSConfigが設定スナップショットを配信する頻度を設定する。 | `string` | `"Six_Hours"` | no |
| enable\_default\_rule | デフォルトルールを適用するかどうかを設定する。 | `bool` | `true` | no |
| function\_name | 設定変更またはルール違反に関する情報を通知先へ送信する関数の名前を設定する。 | `string` | `"ruleViolationFunction"` | no |
| include\_grobal\_resources | グローバルリソースを監視対象に含めるかどうかを設定する。 | `bool` | `true` | no |
| log\_group\_kms\_key\_id | CloudWatch Logsを暗号化するためのKMS CMKのARN | `string` | `null` | no |
| log\_group\_retention\_in\_days | ログをCloudWatch Logsに出力する場合の保持期間を設定する。<br><br>値は、次の範囲の値から選ぶこと： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.<br>[Resource: aws\_cloudwatch\_log\_group / retention\_in\_days](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group#retention_in_days) | `number` | `null` | no |
| notification\_channel | 結果の通知先となるSlack channelのIncoming Webhook URLを設定する。`changeDetectionNotice`キーに対する値としては、対象リソース変更時の通知先、`ruleViolationNotice`キーに対する値としては、ルール違反情報の通知先を設定すること | `map(string)` | <pre>{<br>  "changeDetectionNotice": null,<br>  "ruleViolationNotice": null<br>}</pre> | no |
| notification\_lambda\_handler | 設定変更またはルール違反に関する情報を通知先へ送信する関数の、エントリーポイントとなる関数名を設定する。 | `string` | `"index.handler"` | no |
| notification\_lambda\_runtime | 設定変更またはルール違反に関する情報を通知先へ送信する関数のランタイムを設定する。設定内容については[AWS Lambdaランタイム](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/lambda-runtimes.html)を参照してください。 | `string` | `"nodejs12.x"` | no |
| notification\_lambda\_source\_dir | 設定変更またはルール違反に関する情報を通知先へ送信する関数のソースコードが配置されたディレクトリのパスを設定する。 | `string` | `null` | no |
| notification\_lambda\_timeout | 設定変更またはルール違反に関する情報を通知先へ送信する関数のタイムアウト時間（秒）を設定する。デフォルトでは10秒。 | `number` | `10` | no |
| notification\_topic\_name | AWS Configがリソースの設定変更またはルール違反を通知するトピック名を設定する。 | `string` | `"rule-violation-topic"` | no |
| recorder\_status | AWS Configを有効にするかどうかを設定する。 | `bool` | `true` | no |
| recording\_resource\_types | 監視対象とするリソースを指定する。このパラメータを設定する場合は`all_supported`は`false`にする必要がある。 | `list(string)` | `[]` | no |
| s3\_bucket\_force\_destroy | AWS Configが使用するS3バケットを強制的に削除するかどうかを設定する。 | `bool` | `false` | no |
| tags | このモジュールで作成されるリソースに付与するタグを設定する。 | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| lambda\_function\_rule\_violation\_log\_group\_name | Lambda関数のログ出力先となる、CloudWatch Logsロググループ名 |
