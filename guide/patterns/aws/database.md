# database pattern

## 概要

`database pattern`モジュールでは、Runtime環境で実行するアプリケーションから利用するデータベースを構築します。

## 想定する適用対象環境

`database pattern`は、Runtime環境で使用することを想定しています。

## 依存するpattern

`database pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                                     | 利用する情報           |
| :-------------------------------------------- | :--------------------- |
| [network pattern](./network.md)               | プライベートサブネット |
| [encryption_key pattern](./encryption_key.md) | CMKのエイリアス        |

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                         | 説明                                                               |
| :----------------------------------------------------------------------------------------------------------------- | :----------------------------------------------------------------- |
| [Amazon Relational Database Service (RDS)](https://aws.amazon.com/jp/rds/)                                         | 入力変数で指定された設定で、リレーショナルデータベースを構築します |
| [Amazon CloudWatch Logs](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html) | データベースログの格納先を構築します                               |

## モジュールの理解に向けて

サーバーアプリケーションでは、永続的にデータを格納するためのデータベースを必要とすることが多くあります。
Amazon Relational Database Service（以下、Amazon RDS）は、データの永続化をするためのリレーショナルデータベースをフルマネージドで提供するサービスです。

Amazon RDSが提供するデータベースシステムとしては、以下があります。

- Amazon Aurora
- PostgresSQL
- MySQL
- MariaDB
- Oracleデータベース
- SQLServer

:warning: Eponaの開発では、PostgreSQLを使って動作確認を行っています。

:warning: Amazon Auroraのデータベースエンジンは`database pattern`では構築できません。

ここでは、`database pattern`にて構築されるデータベースの運用観点と、その対応について記載しています。

- セキュリティ
- ログ
- モニタリング
- 監視
- メンテナンス
- 高可用性
- リードレプリカ
- バックアップ

patternモジュールそのものだけではなく、データベースを利用するアプリケーションに必要な情報や環境を管理する際に知っておくべき知識もあるので、各々把握するようにしてください。

## セキュリティ

### ネットワーク配置

一般的にAmazon RDSをインターネットからアクセス可能な環境に配置することは、悪意ある第三者による不正アクセスのリスクがあります。

Eponaでは、Amazon RDSインスタンスを`network pattern`で構築されたプライベートサブネットに配置し、同サブネット内のリソースからのアクセスに限定することを推奨します。

### 接続情報の管理

データベースへの接続情報は、秘匿すべき情報として管理されるべきです。

Eponaでは、アプリケーションは[public_traffic_container_service](./public_traffic_container_service.md)のようなコンテナサービスを使用して構築される想定です。

アプリケーションとデータベースとの接続情報は別途 [`parameter_store pattern`](./parameter_store.md)で構築される、パラメータストアで管理することを推奨します。

### 初期マスタパスワードの変更について

`database pattern`を適用後に構築されるデータベースシステムのマスタパスワードは、リソース構築用のterraformスクリプトに平文で記述する必要があります。

この状況は、コードを参照可能なユーザーによる不正なデータベースアクセスや、悪意のある第三者による不正アクセスのリスクを増大させます。

そのため、`database pattern`を適用した後には必ずマスタパスワードを変更するようにしてください。

変更手順は以下のようになります。

1. 構築されたRDSのマスタパスワードを変更する

    構築したRDSインスタンスに対して `rds:ModifyDBInstance` 権限を持つユーザーで以下のコマンドを実行する。

      ```sh
      $ aws rds modify-db-instance --db-instance-identifier [データベース名] --master-user-password [password]
      ```

:information_source: 通常、Terraformは構成定義ファイルの変更を検出して環境に反映しますが、`database pattern`でのパスワードは変更検出の対象外としています。

### データの暗号化

[encryption_key pattern](./encryption_key.md)で作成したAWS KMS CMKを用いて、ストレージや後述するパフォーマンスインサイトに保存するデータの暗号化を行うことができます。

### SSL/TLS

Amazon RDSでは、データベースインスタンスへの接続をSSL/TLS化できます。要件に応じ、SSL/TLS通信を使用したデータベースへの接続も可能です。

[SSL/TLS を使用した DB インスタンスへの接続の暗号化](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/UsingWithRDS.SSL.html)

構築されたデータベースインスタンス自体は、SSL/TLS通信が有効化されています。

ただし、クライアントプログラムからサーバー証明をしつつSSL/TLSで通信するためには、クライアントの環境にAmazon RDSが提供する証明書を組み込む必要がある点に注意してください。

詳細については、[Amazon RDSのSSL/TLSに関するドキュメント](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/UsingWithRDS.SSL.html)を確認してください。

また、クライアントプログラムからのSSL/TLS通信を強制したい場合は、以下のようにパラメーターグループを設定します。

```terraform
  parameters = [
    { name = "rds.force_ssl", value = "1", apply_method = "pending-reboot" }

    ## その他のパラメーター
    ...
  ]
```

:warning: このパラメーターが有効なデータベースエンジンは、PostgreSQLとSQL Serverに限られます。

- [PostgreSQL DB インスタンスへの SSL 接続を必須にする](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/CHAP_PostgreSQL.html#PostgreSQL.Concepts.General.SSL.Requiring)
- [SQL Server / DB インスタンスへの接続に SSL を使用させる](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/SQLServer.Concepts.General.SSL.Using.html#SQLServer.Concepts.General.SSL.Forcing)

## ログ

Amazon RDSでは、データベースインスタンスが出力するログをAmazon CloudWatch Logsへ保存できます。

[Amazon RDS データベースログファイル](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_LogAccess.html)

ただし、データベースに関するすべてのログをAmazon CloudWatch Logsに出力できるわけではありません。

データベースエンジンとAmazon CloudWatch Logsに出力なログの組み合わせは、[Amazon RDSのドキュメント](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_LogAccess.html#USER_LogAccess.Procedural.UploadtoCloudWatch)を確認してください。

`database pattern`は、各データベースエンジンがAmazon CloudWatch Logsに出力可能なログについては、デフォルトですべて有効化します。

Amazon CloudWatch Logsに出力する対象を絞りたい、もしくは出力自体をやめたい場合は`enabled_cloudwatch_logs_exports`変数を設定してください。

また、Amazon CloudWatch Logsのロググループは自動で作成します。

Datadogが利用可能な場合は、[ログの集約](#ログの集約)を参照してください。Datadogへのログの集約が可能です。

## モニタリング

`database pattern`では、デフォルトで以下の3つのモニタリングが可能なように構成されます。

- Amazon CloudWatchメトリクス
- [拡張モニタリング](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_Monitoring.OS.html)
- [パフォーマンスインサイト](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_PerfInsights.html)

Amazon RDSのモニタリングの概要については、以下のページを参照してください。

[モニタリングの概要](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/MonitoringOverview.html)

収集したメトリクスを使い、モニタリングおよび監視を行えます。

拡張モニタリング、パフォーマンスインサイトに関しては有効なままとすることを推奨しますが、patternモジュールの設定で無効にできます。

また、拡張モニタリングのメトリクスの収集間隔（詳細度）はデフォルトで60秒に、パフォーマンスインサイトのデータの保持期間はデフォルトで7日に設定しています。

`database pattern`の変数で設定可能ですので、必要に応じて変更してください。

拡張モニタリングの保存先はAmazon CloudWatch Logsとなるため、Datadogが利用可能な場合はログ集約が可能です。  
[ログの集約](#ログの集約)を参照してください。

## イベント通知

Amazon RDSで発生するイベントは、Amazon SNSにより通知されます。

[Amazon RDS イベント通知の使用](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_Events.html)

この通知をモニタリングすることで、Amazon RDS上で発生するイベントを把握できます。

:information_source: 現時点のEponaでは、このテーマをサポートしていません。

## メンテナンス

Amazon RDSでは、データベースインスタンスのメンテナンスが実施されます。

- [DB インスタンスのメンテナンス](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_UpgradeDBInstance.Maintenance.html)
- [DB インスタンス のエンジンバージョンのアップグレード](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_UpgradeDBInstance.Upgrading.html)

`database pattern`では、デフォルトでデータベースエンジンのメジャーアップグレードを無効に、マイナーバージョンの自動アップグレードの有無とメンテナンスウィンドウの指定を必須にしています。

- `allow_major_version_upgrade`　：　メジャーバージョンのアップグレードの許可
- `auto_minor_version_upgrade`　：　マイナーバージョンの自動アップグレードの有無
- `maintenance_window`　：　メンテナンスウィンドウ（`ddd:hh24:mi-ddd:hh24:mi`：24時間表記、UTC指定）

サービスの内容に応じて、メンテナンスウィンドウとエンジンのアップグレード方針を決定し、モジュールの設定に反映してください。

## 高可用性

Amazon RDSでは、インスタンスをマルチAZ配置にすることで、高可用構成を実現できます。

[Amazon RDS での高可用性 (マルチ AZ)](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/Concepts.MultiAZ.html)

ただし、インスタンス構成がマルチAZとなっても、インスタンスのフェイルオーバーの可能性を考慮したアプリケーションの対応が必要となる点に注意してください。

Eponaでは`database pattern`において、`multi_az`変数を`true`とすることでAmazon RDSインスタンスをマルチAZ構成とできます。この変数はデフォルトでは`false`となっており、シングルAZで構築されます。

## リードレプリカ

現在の`database pattern`では、リードレプリカを含めた構成はサポートしていません。

## バックアップ

Amazon RDSでは、自動スナップショットを取得することで、バックアップを実現できます。

[Amazon RDS DB インスタンスのバックアップと復元](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/CHAP_CommonTasks.BackupRestore.html)

Eponaでは、`database pattern`において、以下変数を入力することでAmazon RDSインスタンスの自動スナップショット取得を有効とします。これら変数はデフォルトでは`null`となっており、自動スナップショットは無効化されます。

- `backup_retention_period`　：　バックアップ保管世代(最大35)
- `backup_window` ：　バックアップウィンドウ(HH:MM-HH:MM表記)

:warning:  
自動スナップショットには保持期間があり、期限が過ぎたスナップショットは削除されます。  
`database pattern`モジュールを使用して、自動で取得したスナップショットから[リストア](#リストア)を行う場合、スナップショットをコピーして使用することを**強く推奨します**。

[DB スナップショットのコピー](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_CopySnapshot.html#USER_CopyDBSnapshot)

詳細は、[リストア](#リストア)を参照してください。

## リストア

Amazon RDSはスナップショットからのリストア可能です。`database pattern`では、リストアは指定のスナップショットからDBインスタンスを作り直すことで行います。

:warning:  
自動で取得したスナップショットからリストアする場合、スナップショットのコピーを行い、コピーからリストアすることを**強く推奨します**。  
また、Terraformでは、ポイントインタイムリカバリをサポートしていません。

リストア手順は以下のとおりです。

1. RDSインスタンスをデプロイした`terraform`コードに、`snapshot_identifier`を追加指定します。変数の値は、リストア先のSnapshot名を入力します
1. `terraform`を実行します

リストアを行うと、`snapshot_identifier`に指定したスナップショットを使ってDBインスタンスが再作成されます。  
RDSのエンドポイント（DNS名）は変わりませんが、インスタンスのIPアドレスは変更されることに注意してください。

RDSを使用するアプリケーションがDNSの参照結果をキャッシュする場合は、TTLを適切に指定してください。

Javaの場合は、以下を参考にします。

- [DNS 名参照用の JVM TTL の設定](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/Concepts.MultiAZ.html#Concepts.MultiAZ.Failover.Java-DNS)
  - ドキュメントとしてはマルチAZのものですが、DNSのTTLに対する考え方としては同じです
- [DNS 名参照用の JVM TTL の設定](https://docs.aws.amazon.com/ja_jp/sdk-for-java/v1/developer-guide/java-dg-jvm-ttl.html)

---

:warning: リストア後の注意点について。

リストアを行った後は、別のスナップショットからリストアする場合を除いて`snapshot_identifier`に指定した値を変更しないでください。  
`snapshot_identifier`を指定してリストア後、この値を変更してTerraformを実行した場合、以下の挙動になります。

- `snapshot_identifier`の値を削除 → インスタンスの再作成（データがない状態で再作成）
- `snapshot_identifier`に別のスナップショットIDを指定 → 指定のスナップショットからリストア

また`snapshot_identifier`に指定したスナップショットが存在しない場合、 **インスタンスが削除されリストアは失敗します**。  
スナップショットが期限切れなどで削除されたりすると、その後のTerraform実行時に意図せぬインスタンス削除となる恐れがあるため注意してください。

:information_source: リストア後に`snapshot_identifier`を変更せずにTerraformを実行した場合、リストアされるのは初回だけです。

このため自動バックアップで取得したスナップショットは直接使わずに、事前に手動でコピーして使用してください。

[DB スナップショットのコピー](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_CopySnapshot.html#USER_CopyDBSnapshot)

---

## 削除保護

`database pattern`で構築したRDSインスタンスは、デフォルトで削除保護が有効になっています。

[削除保護](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_DeleteInstance.html#USER_DeleteInstance.DeletionProtection)

これは`terraform destroy`を誤って実行してしまいデータを失うことを避けるため、セーフティネットを意図して有効化しています。

テスト目的などでRDSインスタンスの破棄できるようにするには、`deletion_protection`を`false`に設定します。  
削除保護が無効になり、`terraform destroy`が可能になります。

:information_source: 削除保護が有効なRDSを削除する場合は、先に`deletion_protection`を`false`に設定変更してください。

## Amazon QuickSightとの連携

RDSは[Amazon QuickSight](https://aws.amazon.com/jp/quicksight/)のデータソースとして使用できます。
本パターンで構築したRDSをQuickSightのデータソースとして利用する場合、`source_security_group_ids`の設定が必要です。
この場合、`source_security_group_ids`に[`quicksight_vpc_inbound_source pattern`](./quicksight_vpc_inbound_source.md)で出力されたセキュリティグループのIDを設定してください。

`quicksight_vpc_inbound_source pattern`の適用については、[`quicksight_vpc_inbound_source pattern`](./quicksight_vpc_inbound_source.md)のドキュメントを参照してください。

## サンプルコード

`database pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "database" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/database?ref=v0.2.6"

  name = "my-database-instance"
  tags = {
    # 任意のタグ
    Name               = "my-database"
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }

  identifier            = "my-rds-instance"
  engine                = "postgres"
  engine_version        = "12.4"
  port                  = 5432
  instance_class        = "db.t3.medium"
  allocated_storage     = 5
  max_allocated_storage = 10
  
  auto_minor_version_upgrade = true
  maintenance_window         = "Tue:18:00-Tue:18:30"

  username = "user"
  password = "password"

  kms_key_id = data.terraform_remote_state.encryption_key.outputs.encryption_key.keys["alias/my-encryption-key"].key_arn  # encryption_key patternで作成したモジュールから値を設定

  # network patternで作成したモジュールから値を設定
  vpc_id = data.terraform_remote_state.network.outputs.network.vpc_id
  availability_zone = "ap-northeast-1a"

  db_subnets = data.terraform_remote_state.staging_network.outputs.network.private_subnets
  db_subnet_group_name = "my-rds-subnet-group"

  parameter_group_name   = "my-rds-parameter-group"
  parameter_group_family = "postgres12"

  option_group_name                 = "my-rds-option-group"
  option_group_engine_name          = "postgres"
  option_group_major_engine_version = "12"

  backup_retention_period           = "35"
  backup_window                     = "00:00-01:00"

  performance_insights_kms_key_id = data.terraform_remote_state.encryption_key.outputs.encryption_key.keys["alias/my-encryption-key"].key_arn  # encryption_key patternで作成したモジュールから値を設定

  parameters = [
    # DBパラメータを記載
  ]

  # QuickSightから本パターンで構築したRDSを参照する場合、quicksight_vpc_inbound_source pattern適用後、再実行してください
  # source_security_group_ids = [
    # "sg-00000000000000000"  # quicksight_vpc_inbound_source patternで出力されるセキュリティグループID
  # ]
}
```

## 関連するpattern

`database pattern`に関連するpatternを、以下に記載します。

| pattern名                                                                         | 説明                                            |
| :-------------------------------------------------------------------------------- | :---------------------------------------------- |
| [public_traffic_container_service pattern](./public_traffic_container_service.md) | DBを利用するアプリケーションがデプロイされるECS |

## ログの集約

`database pattern`では、データベースに関連するログおよび拡張モニタリングをAmazon CloudWatch Logsに出力します。

これらのログは、[datadog_log_trigger pattern](datadog_log_trigger.md)を使用することでDatadogに集約できます。

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.37.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allocated\_storage | RDSに割り当てるストレージ容量（GB単位） | `number` | n/a | yes |
| auto\_minor\_version\_upgrade | メンテナンスウィンドウ中に、RDSエンジンのマイナーバージョンアップグレードを許可する場合、true | `bool` | n/a | yes |
| backup\_retention\_period | バックアップ保管期間。0〜35(日)の範囲で指定すること。<br>なお、このパラメータを0から0以外の値、0以外の値から0に変更した場合、機能停止が発生する点に注意。詳細はドキュメントを参照のこと。<br>[バックアップの使用](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_WorkingWithAutomatedBackups.html) | `string` | n/a | yes |
| backup\_window | DBスナップショット(バックアップ)を作成する時間帯を09:46-10:16(UTC)という形式で指定する。<br>Single-AZ DBインスタンスの場合、スナップショット取得中はI/Oが短時間中断する点に注意。また、maintenance\_windowとは重複させないこと。 | `string` | n/a | yes |
| db\_subnet\_group\_name | DBサブネットグループ名 | `string` | n/a | yes |
| db\_subnets | DBサブネットに割り当てる、サブネットIDのリスト | `list(string)` | n/a | yes |
| engine | RDSのエンジンの種類 | `string` | n/a | yes |
| engine\_version | RDSのエンジンのバージョン | `string` | n/a | yes |
| identifier | RDSのインスタンス名 | `string` | n/a | yes |
| instance\_class | RDSのDBインスタンスクラス | `string` | n/a | yes |
| kms\_key\_id | 暗号化に使用するKMSキーを指定 | `string` | n/a | yes |
| maintenance\_window | メンテナンスを実行する時間帯を指定する <https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_UpgradeDBInstance.Maintenance.html#AdjustingTheMaintenanceWindow> | `string` | n/a | yes |
| name | RDSのデータベース名 | `string` | n/a | yes |
| option\_group\_engine\_name | DBオプショングループに関連付けるエンジンの名前 | `string` | n/a | yes |
| option\_group\_major\_engine\_version | DBオプショングループに関連付けるエンジンのメジャーバージョン | `string` | n/a | yes |
| option\_group\_name | DBオプショングループ名 | `string` | n/a | yes |
| parameter\_group\_family | DBパラメーターグループのファミリー | `string` | n/a | yes |
| parameter\_group\_name | DBパラメーターグループ名 | `string` | n/a | yes |
| password | RDSのマスタユーザーのパスワード | `string` | n/a | yes |
| performance\_insights\_kms\_key\_id | パフォーマンスインサイトに保存する際の、KMSキー | `string` | n/a | yes |
| port | RDSのポートを指定する | `number` | n/a | yes |
| username | RDSのマスタユーザー名 | `string` | n/a | yes |
| vpc\_id | VPC ID | `string` | n/a | yes |
| allow\_major\_version\_upgrade | RDSエンジンのメジャーアップグレードを許可する場合、true | `bool` | `false` | no |
| apply\_immediately | データベースの変更をすぐに反映する場合、trueを指定する。falseを指定した場合、次のメンテナンス期間に適用される | `bool` | `false` | no |
| availability\_zone | このRDSインスタンスを配置するAZ。マルチAZ構成とする場合は、nullを指定します | `string` | `null` | no |
| ca\_cert\_identifier | RDSインスタンスに適用するSSL/TLS証明書の識別子 | `string` | `"rds-ca-2019"` | no |
| character\_set\_name | OracleおよびMicrosoft SQL Server使用時の、エンコーディングに使用する文字集合名 | `string` | `null` | no |
| cloudwatch\_logs\_kms\_key\_id | CloudWatch Logsを暗号化するためのKMS CMKのARN | `string` | `null` | no |
| cloudwatch\_logs\_retention\_in\_days | ログをCloudWatch Logsに出力する場合の保持期間を設定する。<br><br>値は、次の範囲の値から選ぶこと： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.<br>[Resource: aws\_cloudwatch\_log\_group / retention\_in\_days](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group#retention_in_days) | `number` | `null` | no |
| copy\_tags\_to\_snapshot | 最終スナップショットに、インスタンスのタグをコピーして付与するか否か | `bool` | `true` | no |
| deletion\_protection | RDSインスタンスの削除保護を有効にする場合、trueを指定する。有効にすると、RDSインスタンスを削除できなくなる | `bool` | `true` | no |
| enabled\_cloudwatch\_logs\_exports | CloudWatch Logsに出力するログの種類をリストで指定する。空のリストを指定した場合、ログはCloudWatch Logsに出力されない。<br>なにも指定しない場合、RDSMSごとのデフォルトのログが出力される。<br><br>指定可能なログの種類はRDBMSごとに異なるので、[Amazon RDS データベースログファイル](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_LogAccess.html)、[Resource: aws\_db\_instance#enabled\_cloudwatch\_logs\_exports](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance#enabled_cloudwatch_logs_exports)を参照すること | `list(string)` | `null` | no |
| final\_snapshot\_identifier | 最終スナップショットに指定する識別子 | `string` | `null` | no |
| iops | プロビジョンドIOPS SSDストレージを使用した場合の、IOPSを指定する <https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/CHAP_Storage.html#USER_PIOPS> | `number` | `null` | no |
| license\_model | OracleまたはMicrosoft SQL Serverを使用する際のライセンスモデル | `string` | `null` | no |
| max\_allocated\_storage | この値が指定されている場合、RDSのストレージが不足した場合にこの値（GBで指定）まで自動的にスケーリングする | `number` | `null` | no |
| monitoring\_interval | 拡張モニタリングにおける詳細度（収集間隔）を秒単位で指定する。0, 1, 5, 10, 15, 30, 60のいずれかが指定可能 | `number` | `60` | no |
| monitoring\_role\_arn | 拡張モニタリング用のIAMロールのARNを指定する | `string` | `null` | no |
| mssql\_timezone | Microsoft SQL Server使用時のタイムゾーンを指定する | `string` | `null` | no |
| multi\_az | マルチAZを有効にする場合、true | `bool` | `false` | no |
| option\_group\_options | DBオプショングループに割り当てる、オプションのリスト | `list(map(any))` | `[]` | no |
| parameters | DBパラメーターグループに割り当てる、パラメーターのリストを指定する。各パラメーターには、name, value, apply\_methodの3つを指定する | `list(map(string))` | `[]` | no |
| performance\_insights\_enabled | パフォーマンスインサイトを有効にするか否か | `bool` | `true` | no |
| performance\_insights\_retention\_period | パフォーマンスインサイトのデータ保持期間を日数で指定する。`performance_insights_enabled`が`trueの時のみ設定可能で、7 または 731 （2年）のいずれかを指定する` | `number` | `7` | no |
| security\_groups | RDSインスタンスに割り当てるセキュリティグループのリスト | `list(string)` | `[]` | no |
| skip\_final\_snapshot | RDSのインスタンスを削除する際に、最後のスナップショットを作成しない場合はtrueを指定する | `bool` | `false` | no |
| snapshot\_identifier | Snapshotから復元する場合、Snapshot IDを入力 | `string` | `null` | no |
| source\_security\_group\_ids | RDSに設定するセキュリティグループのインバウンドに指定するSecurity Group IDリスト | `list(string)` | `[]` | no |
| storage\_encrypted | ストレージの暗号化を行う場合、true | `bool` | `true` | no |
| storage\_type | RDSのストレージタイプ | `string` | `null` | no |
| tags | このモジュールで作成するリソースに、共通的に付与するタグ | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| instance\_address | RDSインスタンスへアクセスするためのアドレス |
| instance\_endpoint | RDSインスタンスのエンドポイント |
| instance\_engine | RDSインスタンスのデータベースエンジン |
| instance\_engine\_version | RDSインスタンスのデータベースエンジンのバージョン |
| instance\_identifier | RDSインスタンスの識別子 |
| instance\_log\_group\_names | RDSインスタンスのロググループ名のリスト |
| instance\_name | データベース名 |
| instance\_port | RDSインスタンスへアクセスするためのポート |
| instance\_security\_group\_id | RDSインスタンスのセキュリティグループのID |
| instance\_username | ユーザー名 |
| monitoring\_log\_group\_name | 拡張モニタリングが有効な場合、拡張モニタリングメトリクスを含むCloudWatch Logsロググループ名を返却する。ただし、このロググループは、各RDSインスタンスで共有となる |
