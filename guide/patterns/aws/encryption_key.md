# encryption_key pattern

## 概要

`encryption_key pattern`モジュールでは、AWS環境内で使用する暗号化キーを構築します。

このpatternで構築された暗号化キーは、コンテナ等の環境変数として使用する秘匿情報の暗号化や、データベース等のストレージの暗号化などで使用します。

## 想定する適用対象環境

`encryption_key pattern`は、Delivery環境およびRuntime環境のいずれでも使用されることを想定しています。

## 依存するpattern

`encryption_key pattern`は、他のpatternの実行結果に依存しません。独立して実行できます。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                         | 説明                                                        |
| :----------------------------------------------------------------- | :---------------------------------------------------------- |
| [AWS Key Management Service (KMS)](https://aws.amazon.com/jp/kms/) | 入力変数で指定したカスタマーマスターキー（CMK）を作成します |

## モジュールの理解に向けて

AWSの暗号化キーサービスには、AWS KMSとAWS CloudHSMの2つが存在します。

Eponaでは、暗号化キーを使用する用途を以下の2つで想定しています。

* コンテナ等の環境変数で与える値の暗号化
* AWSのマネージドサービスで保存するデータの暗号化

AWSでこれらの用途で統合されているのはAWS KMSであり、`encryption_key pattern`ではAWS KMSをリソースとして作成します。

`encryption_key pattern`を適用すると、デフォルトでは[ローテーション](https://docs.aws.amazon.com/ja_jp/kms/latest/developerguide/rotate-keys.html)を有効化にしたカスタマーマスターキー（CMK）が作成されます。

CMKは複数作成可能です。ただし以下の理由により、CMKはひとつで十分だと考えます。

* AWSのマネージドサービス内でのみの利用を想定しており、CMKおよび派生するデータキーを直接操作しない
* 保存するデータの暗号化は物理的な盗難などへの対応であり、これはAWSのデータセンターに対するリスク対策となる
  * この対策を突き詰めて考えていくとマネージドサービス単位でCMKを作ることになるが、鍵の管理が煩雑となり、Eponaが想定するサービスレベルとしてはややオーバースペック

一方でこの範囲を逸脱する範囲や用途別にCMKの独立性を高めたい場合は、リスク範囲を鑑みて複数のCMKを作成することを検討してください。

たとえば、[AWS Encryption SDK](https://docs.aws.amazon.com/encryption-sdk/latest/developer-guide/introduction.html)を使用したアプリケーションによるデータの暗号化などで使用する場合などです。

## サンプルコード

`encryption_key pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "encryption_key" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/encryption_key?ref=v0.2.6"

  kms_keys = [
    {
      alias_name              = "alias/my-encryption-key"
      deletion_window_in_days = 30
    }
  ]

  tags = {
    # 任意のタグ
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }
}
```

### データソースの利用について

AWS KMSを使用するリソースには、CMKのエイリアスやARNを求めるものがあります。特にエイリアスを使用するものについては、単純にエイリアスを文字列リテラルとして指定可能です。ですが、Eponaではエイリアスであってもデータソースでの参照を推奨します。

```terraform
## Good
key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/my-key"].key_alias_name

## Bad
key_id = "alias/my-key-name"
```

このようにすることで、`encryption_key pattern`への依存がソースコード上でも明確になります。

## 関連するpattern

`encryption_key pattern`に関連するpatternを、以下に記載します。

| pattern名                                         | 説明                                                                                                   |
| :------------------------------------------------ | :----------------------------------------------------------------------------------------------------- |
| [`parameter_store pattern`](./parameter_store.md) | このpatternで作成したCMKを使用して、AWS System Manager Parameter Storeに格納する秘匿情報を暗号化します |
| [`database pattern`](./database.md)               | このpatternで作成したCMKを使用して、Amazon RDSに保存するデータを暗号化します                           |
| [`redis pattern`](./redis.md)                     | このpatternで作成したCMKを使用して、Amazon ElastiCache for Redisに保存するデータを暗号化します         |

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.37.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| kms\_keys | KMSで作成するキーに指定するパラメーター（alias\_name, description, key\_usage, customer\_master\_key\_spec, policy, deletion\_window\_in\_days, is\_enabled, enable\_key\_rotation | `list(map(any))` | n/a | yes |
| tags | このモジュールで作成されるリソースに付与するタグ | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| keys | n/a |
