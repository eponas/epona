# datadog_forwarder pattern

## 概要

`datadog_forwarder pattern`モジュールでは、AWS上に出力されるログやメトリクス等を、監視およびモニタリングに関するSaaSである[Datadog](https://www.datadoghq.com/ja/)へ転送する機能を実現します。

このpatternを使用すると、Datadogにより提供されるDatadog Forwarderと呼ばれるAWS Lambda関数を、指定のAWS環境にデプロイします。

[Datadog Forwarder](https://docs.datadoghq.com/ja/serverless/forwarder)

このAWS Lambda関数は、AWS環境内にログが保存されるリソースに紐付ける必要があります。
対象のリソースには、Amazon CloudWatch Logs、Amazon S3があります。  
これは、[datadog_log_trigger pattern](./datadog_log_trigger.md)を使用して実現します。

## 想定する適用対象環境

`datadog_forwarder pattern`は、Delivery環境およびRuntime環境のいずれでも使用できますが、主としてRuntime環境で利用することになるでしょう。

## 依存するpattern

`datadog_forwarder pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                                       | 利用する情報                                             |
| :---------------------------------------------- | :------------------------------------------------------- |
| [parameter_store pattern](./parameter_store.md) | AWS Systems Manager パラメータストアに格納された、DatadogのAPIキー |

`datadog_forwarder pattern`では、事前にDatadoogのAPIキーが格納されていることを前提にしており、これを実行時に利用します。

また、[datadog_integration pattern](./datadog_integration.md)と合わせて適用し、モニタリングも合わせて実施することを強く推奨します。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                         | 説明                                                        |
| :----------------------------------------------------------------- | :---------------------------------------------------------- |
| [AWS CloudFormation](https://aws.amazon.com/jp/cloudformation/) | Datadog Forwarderを使用するための、リソース一式をデプロイします |
| [AWS Lambda](https://aws.amazon.com/jp/lambda/) | Datadog Forwarderによりデプロイされる、Datadogへの転送処理本体です |
| [AWS Secrets Manager](https://aws.amazon.com/jp/secrets-manager/) | AWS Lambda関数から参照される、DatadogのAPIキーが格納されます |
| [AWS KMS](https://aws.amazon.com/jp/kms/) | AWS Secrets Managerの暗号化に使用される、AWS KMS CMKを作成します |

:information_source: AWS CloudFormationスタックに含まれる、すべてのリソースの説明は行いません。

:information_source: 内容に興味のある方はAWS CloudFormationスタックの定義、もしくはデプロイされた内容を直接ご確認ください。

[Datadog ForwarderのAWS CloudFormationスタック定義](https://datadog-cloudformation-template.s3.amazonaws.com/aws/forwarder/latest.yaml)

## モジュールの理解に向けて

### patternのコンセプト

`datadog_forwarder pattern`は、Datadogが提供する[Datadog Forwarder](https://docs.datadoghq.com/ja/serverless/forwarder)をEponaへ適用しやすい形にまとめたpatternモジュールです。

Datadogが提供するリソースを活用しつつ、Eponaが想定する環境やpatternへの適用が容易になるように設計しています。

### AWS上のログ収集

AWS上では、マネージドサービスやコンテナ上のアプリケーション等より様々なログが出力され、Amazon CloudWatch LogsやAmazon S3へと格納されます。

DatadogではAWSとのインテグレーションの一環として、ログ収集の方法が紹介されています。

[AWS / ログの収集](https://docs.datadoghq.com/ja/integrations/amazon_web_services/?tab=cloudformation#%E3%83%AD%E3%82%B0%E3%81%AE%E5%8F%8E%E9%9B%86)

Datadogのドキュメントで紹介されているのはAmazon Kinesis Data Firehoseを使用する方法、AWS CloudFormationを使用する方法の2つです。

Eponaでは、AWS CloudFormationを使用する方法を採用します。  
※その理由は後述します。

[Send AWS services logs with the Datadog Lambda function](https://docs.datadoghq.com/ja/logs/guide/send-aws-services-logs-with-the-datadog-lambda-function/?tab=awsconsole)

Datadogの提供するAWS CloudFormationテンプレートを使用すると、AWS Lambda関数がデプロイされます。  
Datadogの提供するAWS Lambda関数により、Amazon CloudWatch LogsやAmazon S3へ出力されたログをDatadogに転送することが可能になります。

`datadog_forwarder pattern`により構築されるリソースと、ログ収集に関するリソースの全体図を以下に記載します。

![datadog_forwarder patternの全体像](./../../resources/datadog_forwarder_overview.png)

### AWS CloudFormationの利用とログの保存

Eponaでは、IaCの要素としてTerraformを採用しています。  
この一方で、`datadog_forwarder pattern`ではAWS CloudFormationを内部的に使用します。  
これは、Datadog自身が提供するリソースであり、またTerraformを介して実行する方法もDatadogにより紹介されています。

[Datadog Forwarder / Terraform](https://docs.datadoghq.com/ja/serverless/forwarder#terraform)

内部的にとはいえ複数のIaC要素を利用することにはなりますが、以下の理由によりEpona自身が該当箇所を作り込んで提供するよりは理にかなった方法だと考えます。

* Datadogの機能をDatadog自身が提供するリソースで構築する方法であること
* Datadogのドキュメントに記載のある方法であること

また、Datadogのドキュメントではログを複数ヶ所に転送する場合はAmazon Kinesis Data Firehoseを推奨しています。

[AWS / ログの収集](https://docs.datadoghq.com/ja/integrations/amazon_web_services/?tab=cloudformation#%E3%83%AD%E3%82%B0%E3%81%AE%E5%8F%8E%E9%9B%86)

この利点を得るにはAmazon Kinesis Data Firehoseに加えて、Amazon Kinesis Data Streamsが必要となります。  
現時点のEponaではログを複数の出力先に転送することを想定しないため、これはオーバースペックな仕組みとなります。

[Datadog Kinesis Firehose Destination を使用して AWS サービスログを送信する](https://docs.datadoghq.com/ja/logs/guide/send-aws-services-logs-with-the-datadog-kinesis-firehose-destination/?tab=kinesisfirehosedelivery)

AWS上の各種ログは、Amazon CloudWatch LogsおよびAmazon S3からDatadogに送信します。

Datadogに送信されたログは、一定期間のみ保持されます（期間はDatadogとの契約で決定します）。

[Log Management](https://www.datadoghq.com/ja/pricing/#section-log)

このため、Datadogへのログ送信元となったAmazon CloudWatch LogsおよびAmazon S3ではより長期にログを保持し続ける必要があります。

:information_source: CloudWatch LogsからAmazon S3へログを転送するような機能は、現時点のEponaでは提供しません。

:information_source: Datadogでは、インデックス化されたログイベントをもとに課金額が計算されます。必要に応じてログフィルターの使用を検討してください。詳細は[ログフィルターについて](datadog_log_trigger.md#ログフィルターについて)を参照してください。

### AWS Systems Manager パラメータストアとAWS Secrets Managerの併用

Eponaでは、秘匿情報の管理は`parameter_store pattern`で構築する、AWS Systems Manager パラメータストアを利用する方針としています。

Datadog Forwarderで必要となるDatadogのAPIキーは、秘匿情報にあたります。  
しかしDatadog Forwarderの仕様上、DatadogのAPIキーはAWS Secrets Managerに格納する必要があります。  
開発対象のサービスがAWS Secrets Managerを使うのであればよいのですが、Datadog Forwarderを使うためだけに導入を強いるのはEpona利用者に不便です。

このため`datadog_forwarder pattern`内では、AWS Systems Manager パラメータストアとAWS Secrets Managerの両方を使用します。

* DatadogのAPIキーをAWS Systems Manager パラメータストアより取得する
* AWS Secrets Managerにコピーする

:information_source: この時AWS KMSを使用し、CMKも内部的に作成します。

![datadog_forwarder patternでのAPIキー管理](./../../resources/datadog_forwarder_api_key.png)

AWS Systems Managerに格納されたDatadogのAPIキーは、`datadog_forwarder pattern`の引数として設定します。

```terraform
  dd_api_key_ssm_parameter_name = data.terraform_remote_state.production_parameter_store.outputs.parameter_store.parameters["[Datadog APIキーを格納した、AWS Systems Managerパラメータストアのパラメータ名]"].name
```

:information_source: 見かけ上やや冗長に見えますが、`parameter_store pattern`への依存関係を明示した方が後々にわかりやすいでしょう。

### 複数のリージョンへの対応

AWS環境内で保存されるログをDatadog Forwarderで転送する場合、AWS Lambda関数がログの保存場所と同じリージョンにデプロイされている必要があります。

Eponaでは、Amazon CloudFrontやAWS WAF等をpatternとして提供しています。  
`cacheable_frontend pattern`、`webacl pattern`などが該当します。  
Amazon CloudFrontにAWS Certificate ManagerやAWS WAF等を適用すると、これらのリソースはバージニア北部リージョンに構築しなくてはなりません。

例えば主として東京リージョンを使用している場合でも、これらのサービスのログは必然的にバージニア北部リージョンに保存されます。

:warning: Amazon CloudFrontやAmazon S3をAWS Lambdaでサブスクライブする場合、これらを同じリージョンに構築する必要があります。

このため、EponaでDatadog Forwarderをデプロイしようとすると、バージニア北部リージョンへのリソース構築も必要になります。  
DatadogのAPIキーを格納するためだけに`encryption_key pattern`および`parameter_store pattern`が必要となり、準備が大掛かりになります。

`datadog_forwarder pattern`では、この負担を軽減します。

AWS Systems Manager パラメータストアを利用するAWS Providerと、Datadog Forwarderに関するAWS Providerを分離、別々に指定する方針としています。

![複数リージョンへの対応](./../../resources/datadog_forwarder_multi_region.png)

具体的には、以下の2つのProviderを定義しています。

* `ssm_provider` … AWS Systems Manager パラメータストア用
* `datadog_deployment` … Datadogに関するAWS CloudFormationを始めとする一式用

`parameter_store pattern`とDatadog Forwarderを同じリージョンにデプロイする場合は、両方に同じProviderを指定します。

```terraform
provider "aws" {
  assume_role {
    role_arn = "..."
  }
}

module "datadog_forwarder" {
  source = "..."

  providers = {
    aws.ssm_provider       = aws
    aws.datadog_deployment = aws
  }

  dd_api_key_ssm_parameter_name = data.terraform_remote_state.production_parameter_store.outputs.parameter_store.parameters["[Datadog APIキーを格納した、AWS Systems Managerパラメータストアのパラメータ名]"].name

  ...
}
```

`parameter_store pattern`の適用先と異なるリージョンにDatadog Forwarderをデプロイする場合、AWS Providerを使い分けます。

```terraform
provider "aws" {
  assume_role {
    role_arn = "..."
  }
}

provider "aws" {
  assume_role {
    role_arn = "..."
  }

  alias  = "us_east_region"
  region = "us-east-1"
}

module "datadog_forwarder_us_east_region" {
  source = "..."

  providers = {
    aws.ssm_provider       = aws
    aws.datadog_deployment = aws.us_east_region
  }

  dd_api_key_ssm_parameter_name = data.terraform_remote_state.production_parameter_store.outputs.parameter_store.parameters["[Datadog APIキーを格納した、AWS Systems Managerパラメータストアのパラメータ名]"].name

  ...
}
```

このようにすることで、Datadog APIキーは主として使うリージョンから参照つつ、Datadog Forwarderは米国北部バージニアにデプロイできます。

### ログ転送設定の自動セットアップと手動セットアップ

Datadog Forwarderをデプロイしただけでは、Datadogへのログ転送はできません。  
ログが保存されるリソースと、デプロイしたAWS Lambda関数の紐付けが必要です。

[Send AWS services logs with the Datadog Lambda function](https://docs.datadoghq.com/ja/logs/guide/send-aws-services-logs-with-the-datadog-lambda-function/?tab=awsconsole)

この方法には、Datadogのコンソールを使用した自動セットアップと、AWSコンソールを使用した手動セットアップの2つがあります。

[Automatically set up triggers](https://docs.datadoghq.com/ja/logs/guide/send-aws-services-logs-with-the-datadog-lambda-function/?tab=awsconsole#automatically-set-up-triggers)

[Manually set up triggers](https://docs.datadoghq.com/ja/logs/guide/send-aws-services-logs-with-the-datadog-lambda-function/?tab=awsconsole#manually-set-up-triggers)

Eponaでは、手動セットアップをTerraformで実現する方法を採用します。patternとしては、[datadog_log_trigger pattern](./datadog_log_trigger.md)を用います。

![Datadog Forwarderとログリソースの紐付け](./../../resources/datadog_forwarder_log_resource_binding.png)

理由は、以下となります。

* 自動セットアップで、すべてのログ保存リソースとAWS Lambda関数を紐付けられるわけではない
  * 手動セットアップとの併用が必要になる
* 手動セットアップが必要になるのであれば、Terraformで紐付けるようにした方がログリソースとの関連が明確になる

また、Datadog Forwarderのデプロイと紐づけを異なるpatternに分離したのは、以下の2つの変更頻度の差を考慮して実行単位を分離しています。

* AWS CloudFormationによりデプロイされるリソースと、AWS Secrets Manager
* patternインスタンスの増減、変更により発生する、Datadog Forwarderとログリソースの紐付け

:information_source: AWS Secrets Managerはすぐには削除されないことに注意してください。

[シークレットの削除とリストア](https://docs.aws.amazon.com/ja_jp/secretsmanager/latest/userguide/manage_delete-restore-secret.html)

### Datadog ForwarderのAWS CloudFormationパラメーター

Datadog Forwarderが提供するAWS CloudFormationテンプレートには、いくつかのパラメーターがあり、利用者によるカスタマイズができます。

[Datadog Forwarder CloudFormation パラメーター](https://docs.datadoghq.com/ja/serverless/forwarder/#cloudformation-%E3%83%91%E3%83%A9%E3%83%A1%E3%83%BC%E3%82%BF%E3%83%BC)

この指定には、引数`datadog_forwarder_parameters`を指定します。

```terraform
  datadog_forwarder_parameters = {
    "MemorySize" = 2048
  }
```

この中でも、特に利用するパラメーターについては、`datadog_forwarder pattern`で引数として切り出しているので、こちらを使用します。

特に、Datadogでのメトリクス、ログ収集に確認するにはタグ付けは非常に重要なので、必ず設定するようにしてください。

[タグの付け方](https://docs.datadoghq.com/ja/getting_started/tagging/assigning_tags/?tab=)

Datadog Forwarderにより提供されるAWS Lambda関数で転送するログにタグを付与するには、`datadog_forwarder_tags`を使用します。

```terraform
  datadog_forwarder_tags        = "environment:runtime,runtime_environment:production,collected_by:epona"
```

なお、利用するAWS CloudFormationテンプレートは、デフォルトでは以下を利用します。

* [latest.yaml](https://datadog-cloudformation-template.s3.amazonaws.com/aws/forwarder/latest.yaml)

新しいバージョンへのアップグレードや、他のバージョンを選択する際には`datadog_forwarder_cloudformation_template_url`でテンプレートを変更できます。

たとえば、以下の例ではバージョン`3.0.2`のテンプレートを使用します。

```terraform
  datadog_forwarder_cloudformation_template_url = "https://datadog-cloudformation-template.s3.amazonaws.com/aws/forwarder/3.0.2.yaml"
```

この詳細については、Datadogのドキュメントを参照してください。

[Datadog Forwarder / 新しいバージョンにアップグレードする](https://docs.datadoghq.com/ja/serverless/forwarder/#%E6%96%B0%E3%81%97%E3%81%84%E3%83%90%E3%83%BC%E3%82%B8%E3%83%A7%E3%83%B3%E3%81%AB%E3%82%A2%E3%83%83%E3%83%97%E3%82%B0%E3%83%AC%E3%83%BC%E3%83%89%E3%81%99%E3%82%8B)

## サンプルコード

`datadog_forwarder pattern`を使用したサンプルコードを、以下に記載します。

`parameter_store pattern`と`datadog_forwarder pattern`を同じリージョンにデプロイする場合。

```terraform
provider "aws" {
  assume_role {
    role_arn = "[TerraformExecutionRoleのARN]"
  }
}

module "datadog_forwarder" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/datadog_forwarder?ref=v0.2.6"

  providers = {
    aws.ssm_provider       = aws
    aws.datadog_deployment = aws
  }

  dd_api_key_ssm_parameter_name = data.terraform_remote_state.production_parameter_store.outputs.parameter_store.parameters["[Datadog APIキーを格納した、AWS Systems Managerパラメータストアのパラメータ名]"].name

  datadog_forwarder_tags        = "environment:runtime,runtime_environment:production,collected_by:epona"

  tags = {
    # 任意のタグ
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }
}
```

`parameter_store pattern`と`datadog_forwarder pattern`を異なるリージョンにデプロイする場合。

```terraform
provider "aws" {
  # デフォルトのリージョン
  assume_role {
    role_arn = "[TerraformExecutionRoleのARN]"
  }
}

provider "aws" {
  assume_role {
    role_arn = "[TerraformExecutionRoleのARN]"
  }

  alias  = "us_east_region"
  region = "us-east-1"
}

module "datadog_forwarder_us_east_region" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/datadog_forwarder?ref=v0.2.6"

  providers = {
    aws.ssm_provider       = aws
    aws.datadog_deployment = aws.us_east_region
  }

  dd_api_key_ssm_parameter_name = data.terraform_remote_state.production_parameter_store.outputs.parameter_store.parameters["[Datadog APIキーを格納した、AWS Systems Managerパラメータストアのパラメータ名]"].name

  datadog_forwarder_tags        = "environment:runtime,runtime_environment:production,collected_by:epona"

  tags = {
    # 任意のタグ
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }
}
```

## 関連するpattern

`datadog_forwarder pattern`に関連するpatternを、以下に記載します。

| pattern名                                                                         | 説明                                            |
| :-------------------------------------------------------------------------------- | :---------------------------------------------- |
| [datadog_log_trigger pattern](./datadog_log_trigger.md) | このパターンでデプロイされたAWS Lambda関数と、ログが蓄積されるリソースの紐づけを行います |

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.37.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| dd\_api\_key\_ssm\_parameter\_name | AWS Systems Manager パラメーターストアに格納した、DatadogのAPIキーのパラメーター名。Datadog Forwarderが提供するLambda関数に必要なため、このモジュールでリソース設定を行う。<br><br>  最終的には、AWS Secrets Managerに格納され、DdApiKeySecretArnとして使用される。<br><br>  Datadogにより提供されるLambda関数が参照するAPIキーは、本来はAWS Secrets Managerで登録する必要があるが、<br>  Eponaでは秘匿情報はAWS Systems Manager パラメーターストアを使用するため、本モジュールでAWS Systems Manager パラメーターストアに<br>  AWS Secrets Managerに登録し直している<br><br>  詳細は、[こちら](https://docs.datadoghq.com/ja/serverless/forwarder#cloudformation-%E3%83%91%E3%83%A9%E3%83%A1%E3%83%BC%E3%82%BF%E3%83%BC)を参照。 | `string` | n/a | yes |
| datadog\_forwarder\_cloudformation\_stack\_name | このモジュールでデプロイするCloudFormationのスタック名を指定する | `string` | `"datadog-forwarder"` | no |
| datadog\_forwarder\_cloudformation\_template\_url | このモジュールがデプロイする、CloudFormationのテンプレートURLを指定する | `string` | `"https://datadog-cloudformation-template.s3.amazonaws.com/aws/forwarder/latest.yaml"` | no |
| datadog\_forwarder\_lambda\_function\_name | このモジュールでデプロイされる、Lambdaの関数名を指定する（DatadogのCloudFormationパラメーター`FunctionName`に相当する）<br><br>  詳細は、[こちら](https://docs.datadoghq.com/ja/serverless/forwarder#cloudformation-%E3%83%91%E3%83%A9%E3%83%A1%E3%83%BC%E3%82%BF%E3%83%BC)を参照。 | `string` | `"datadog-forwarder"` | no |
| datadog\_forwarder\_parameters | このモジュールでデプロイする、CloudFormationスタックに引き渡すパラメーターを`map`形式で指定する。<br><br>  ただし、DdApiKeySecretArn、FunctionName、DdTagsについては他の変数を使用すればよく、DdApiKeyについては指定する必要がない。<br><br>  詳細は、[こちら](https://docs.datadoghq.com/ja/serverless/forwarder#cloudformation-%E3%83%91%E3%83%A9%E3%83%A1%E3%83%BC%E3%82%BF%E3%83%BC)を参照。 | `map(any)` | `{}` | no |
| datadog\_forwarder\_tags | このモジュールでデプロイされるLambda関数が付与する、タグを指定する（DatadogのCloudFormationパラメーター`DdTags`に相当する）<br><br>  詳細は、[こちら](https://docs.datadoghq.com/ja/serverless/forwarder#cloudformation-%E3%83%91%E3%83%A9%E3%83%A1%E3%83%BC%E3%82%BF%E3%83%BC)を参照。 | `string` | `""` | no |
| dd\_api\_key\_secretsmanager\_secret\_name | DatadogのAPIキーをAWS Secrets Managerに格納する際の名前 | `string` | `"datadog_api_key"` | no |
| secretsmanager\_kms\_key\_deletion\_window\_in\_days | このモジュールが、SecretsManager用に内部で作成するKMS CMKの削除猶予期間を指定する | `number` | `30` | no |
| secretsmanager\_recovery\_window\_in\_days | SecretsManager上に作成する、Datadog転送用パラメータの削除猶予期間。削除猶予期間を7〜30（単位：日）の間で設定する | `number` | `null` | no |
| tags | このモジュールで構築されるリソースに、共通的に付与するタグ | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| datadog\_forwarder\_lambda\_function\_arn | CloudFormationによりデプロイされた、Datadogのログ転送用のLambda関数のARN |
| dd\_api\_key\_secret\_arn | Datadog APIキーを格納した、Secrets ManagerのARN |
| kms\_keys | このモジュールが作成したKMS CMK |
