# cd_pipeline_lambda pattern

## 概要

`cd_pipeline_lambda pattern`は、Runtime環境上のLambda関数をデプロイするためのpatternです。

## 想定する適用対象環境

`cd_pipeline_lambda pattern`は、Runtime環境で使用することを想定しています。

## 依存するpattern

`cd_pipeline_lambda pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名 | 利用する情報 |
|:----------|:-----------------|
| [cd_pipeline_lambda_trigger pattern](./cd_pipeline_lambda_trigger.md) | デプロイのトリガとなるイベント、Delivery環境のデプロイ元であるAmazon S3バケットへアクセスするためのロール |

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名 | 説明 |
| :--- | :--- |
| [AWS CodePipeline (CodePipeline)](https://aws.amazon.com/jp/codepipeline/)| デプロイメントパイプラインを構成します |
| [AWS CodeBuild (CodeBuild)](https://aws.amazon.com/jp/codebuild/)| Lambda関数の[パブリッシュ](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/API_PublishVersion.html)を行います。 |
| [AWS CodeDeploy(CodeDeploy)](https://aws.amazon.com/jp/codedeploy/) | Lambda関数のエイリアスに対して、バージョンを紐づけます。 |
| [Amazon EventBridge](https://aws.amazon.com/jp/eventbridge/) | 異なるAWSアカウント間でイベントを伝搬させるための経路(イベントバス)を構築します |
| [Amazon CloudWatch Events](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/events/WhatIsCloudWatchEvents.html) | デプロイ実行のトリガとなるイベントを受信するためのイベントを構成します |
| [AWS Key Management Service (KMS)](https://aws.amazon.com/jp/kms/) | CodePipelineが利用するArtifact Store(S3バケット)を暗号化するための鍵を構築します |

## モジュールの理解に向けて

本モジュールはDelivery環境にアップロードされたLambda関数のソースを、Runtime環境にデプロイするためのモジュールです。

### 前提事項

`cd_pipeline_lambda pattern`は、以下の前提事項のもとで利用されることを想定しています。

- [`lambda pattern`](./lambda.md)で作成されたLambda関数のソースの更新

----

:information_source:

[`cd_pipeline_lambda_trigger pattern`](./cd_pipeline_lambda_trigger.md)では、デプロイ用のイベントを発火させるために、
CloudTrailのデータイベントログ記録機能に依拠しています。
一方で、本patternについては当該機能は有効化せずとも動作します。
コンプライアンスや課金の観点を鑑み、有効・無効をご判断ください。

Lambda関数のデプロイにあたっては、`AWS Lambda`の`バージョン`や`エイリアス`の機能を利用しています。

----

### 環境毎のデプロイ戦略

下図は、前者の`cd_pipeline_lambda_trigger pattern`を組み合わせた場合のデプロイ処理のアーキテクチャおよびシーケンスを示したものです。

![アーキテクチャ](../../resources/cd_pipeline_lambda.png)

図のケースでは、イベントバス経由でDelivery環境からデプロイ開始イベントが伝搬することでデプロイが開始されます。
デプロイ処理はCodePipelineが制御し、大きく分けて以下のシーケンスで行われます。

1. CodePipeline: Delivery環境のS3バケット上のzip圧縮されたアプリケーションの読み取り。アーティファクトストアの`source`に配置されます
   - Delivery環境のS3バケット上に配置されるzipファイルには以下のファイルを含めておきます
     - 必須
       - Lambda関数用のソース(zip)
       - `buildspec.yml`
     - オプション
       - `appspec.yaml`(パイプラインを最後まで実行するためには必要ですが、ここでは必要ありません)
2. CodePipeline: (オプション)承認権限者へのデプロイ承認要求
3. CodeBuild: Lambda関数の[パブリッシュ](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/API_PublishVersion.html)。アーティファクトストアの`source`からアプリケーションと設定ファイル`buildspec.yml`を読み取り。CodeDeployで使用する`appspec.yaml`ファイルを、アーティファクトストアの`build`に配置
   - `appspec.yaml`は、CodeBuild実行時に作成しても問題ありません。もちろん、Delivery環境のS3バケットへアップするzipに含めておくこともできます
4. CodePipeline: (オプション)承認権限者へのデプロイ承認要求
5. CodeDeploy: (オプション)Lambda関数のエイリアスのバージョン付け替え。設定ファイル`appspec.yaml`はアーティファクトストアの`build`より取得

2つの設定ファイル、`buildspec.yml`、`appspec.yaml`については[別途説明します](#デプロイをするための設定ファイル群)。

承認権限者への承認はオプションです。例えば、開発環境の場合はCodeBuild実行前とCodeDeploy実行前、両方とも承認不要にするといった対応が可能です。その場合、以下のように設定します。

`runtimes/staging/cd_pipeline_lambda/main.tf`

```terraform
module "cd_pipeline_lambda" {

  ...

  deployment_require_approval_before_build = false  # CodeBuild実行前の承認不要
  deployment_require_approval_before_deploy = false  # CodeDeploy実行前の承認不要
}
```

一方でProduction環境においては、デプロイタイミングもサービス関係者の協議の元で決定されるため、パイプラインの進行はマニュアル操作で開始する方が多いであろうと想定しています。

Production環境へのデプロイには承認必須とする場合は、以下のように設定します。

`runtimes/production/cd_pipeline_lambda/main.tf`

```terraform
module "cd_pipeline_lambda" {

  ...

  deployment_require_approval_before_build = true  # CodeBuild実行前の承認が必要
  deployment_require_approval_before_deploy = true  # CodeDeploy実行前の承認が必要
}
```

承認必須とした場合は、CodePipeline上に構成されるデプロイメントパイプラインを手動で進行再開させてください。

![承認](../../resources/cd_pipeline_lamba_approval.png)

:information_source: 設定しなかった場合、いずれもデフォルトは`true`（承認必須）となります。

:information_source:  承認が必要となった場合、[Amazon SNS](https://aws.amazon.com/jp/sns/)によって承認者宛に通知できます。詳細は[入出力リファレンス](#入出力リファレンス)を参照してください。

このパターンで作成されるCodeDeployステージの実行もオプションです。エイリアスの付け替えを行わない場合は、このステージを実行しないようにできます。

```terraform
module "cd_pipeline_lambda" {

  ...

  require_codedeploy = false  # CodeDeployのステージを実行しない
  deployment_require_approval_before_deploy = false # CodeDeploy実行前の承認が不要
}
```

## サンプルコード

`cd_pipeline_lambda pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "cd_pipeline_lambda" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_lambda?ref=v0.2.6"

  delivery_account_id = "[Delivery環境のアカウントID]"

  pipeline_name = "my-lambda-cd-pipeline"

  artifact_store_bucket_name = "my-lambda-artifacts"

  # Delivery環境のS3にCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_lambda_trigger の output として出力される
  cross_account_codepipeline_access_role_arn = "arn:aws:iam::[Delivery環境のAWSアカウントID]:role/[ロール名]"

  # ci_pipeline の static_resource_buckets と同じ名前を指定する
  source_bucket_name = "my-lambda-source-bucket"
  source_object_key  = "source.zip"

  # CodeBuild、CodeDeployを実行する前の承認を行うかどうか
  deployment_require_approval_before_build = true
  deployment_require_approval_before_deploy = true

  # CodeBuild、CodeDeployを実行する前の承認を行う場合、SNS topicに承認のためのURLを送る場合、
  # SNS topicのARNを指定する
  sns_topic_arn_approval_before_build  = "arn:aws:sns:ap-northeast-1:XXXXXXXXXXXX:sns-topic01"
  sns_topic_arn_approval_before_deploy = "arn:aws:sns:ap-northeast-1:XXXXXXXXXXXX:sns-topic02"

  # CodeDeplyの設定。デプロイメントグループと名称を指定。
  codedeploy_deployment_group_name = "deployment_group-lambda-01"
  codedeploy_name                  = "codedeploy-lambda-01"
}
```

## デプロイをするための設定ファイル群

Eponaとしては、Lambda関数のデプロイを行うケースとして以下の2種類のユースケースを想定しています。以下、それぞれのユースケースに対したサンプルを記載します。

1. 常にLambda関数の最新バージョンを使う
   - 主として開発環境等において、最新のLambda関数を利用して実行とコード修正を繰り返したい場合を想定しています
2. 利用するLambda関数のバージョンを明示的に指定する
   - 主としてProduction環境等において、十分にテストされたLambda関数を実行したい場合を想定しています

Eponaで提供するLambda関数のためのCI/CDパイプライン全体で必要になるファイル群の詳細なサンプルについては、[Getting Started](https://gitlab.com/eponas/epona_aws_getting_started)を参照してください。

### 常にLambda関数の最新バージョンを使う

- CodeBuild
  - AWS CLIを利用してエイリアスが向いている現在のバージョンを取得
  - AWS CLIを利用してLambda関数を[パブリッシュ](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/API_PublishVersion.html)
  - AWS CLIを利用してLambda関数の最新バージョン(`$LATEST`)を取得
  - エイリアスの現在のバージョンと、変更後のバージョンを記載した`appspec.yaml`を作成
    - `appspec.yaml`を作成するためのテンプレート`appspec.yaml.tmpl`を用意しておき、sedコマンドで置き換えています
  - `appspec.yaml`をアーティファクトのアウトプットとして設定
- CodeDeploy
  - エイリアスに紐づくバージョンを変更

```yaml
# buildspec.yaml
version: 0.2
phases:
  build:
    commands:
      - function_name="YOUR_FUNCTION_NAME"
      - alias_name="YOUR_ALIAS_NAME"
      - current=$(aws lambda get-alias --function-name ${function_name} --name ${alias_name} --query  FunctionVersion)
      - aws lambda update-function-code --publish --function-name ${function_name} --zip-file  fileb://source.zip
      - target=$(aws lambda publish-version --function-name ${function_name} --query Version)
      - sed -e "s/CURRENTVERSION/${current}/" -e "s/TARGETVERSION/${target}/" appspec.yaml.tmpl > appspec.yaml
artifacts:
  files:
    - appspec.yaml
  discard-paths: yes
```

```yaml
# appspec.yaml.tmpl
version: 0.0
Resources:
  - myLambdaFunction:
      Type: AWS::Lambda::Function
      Properties:
        Name: "YOUR_FUNCTION_NAME"
        Alias: "YOUR_ALIAS_NAME"
        CurrentVersion: CURRENTVERSION
        TargetVersion: TARGETVERSION
```

### 利用するLambda関数のバージョンを明示的に指定する

- CodeBuild
  - Lambda関数をパブリッシュ
- CodeDeploy
  - エイリアスに紐づくバージョンを変更

```yaml
# buildspec.yml
version: 0.2
phases:
  build:
    commands:
      - aws lambda update-function-code --publish --function-name Lambda関数名 --zip-file  fileb://source.zip
artifacts:
  files:
    - appspec.yaml
  discard-paths: yes
```

```yaml
# appspec.yaml
version: 0.0
Resources:
  - myLambdaFunction:
      Type: AWS::Lambda::Function
      Properties:
        Name: Lambda関数名
        Alias: エイリアス名
        CurrentVersion: 1 # 現在のバージョン
        TargetVersion: 2 # 変更後のバージョン 存在しないバージョンを指定するとCodeDeploy実行時にエラーになります。
```

### 注意

- CodeDeployで実行する内容の設定ファイル名は`appspec.yaml`です
  - 拡張子は`.yaml`です。`.yml`ではありませんので注意してください
- `appspec.yaml`の`CurrentVersion`や`TargetVersion`は数字を指定する必要があります
  - `$LATEST`は指定できません

- 以下のサイトも参考にしてください
  - [CodeBuild のビルド仕様に関するリファレンス - AWS CodeBuild](https://docs.aws.amazon.com/ja_jp/codebuild/latest/userguide/build-spec-ref.html)
  - [CodeDeploy AppSpec ファイルのリファレンス - AWS CodeDeploy](https://docs.aws.amazon.com/ja_jp/codedeploy/latest/userguide/reference-appspec-file.html)
  - [AppSpec ファイルの例 - AWS CodeDeploy](https://docs.aws.amazon.com/ja_jp/codedeploy/latest/userguide/reference-appspec-file-example.html#appspec-file-example-lambda)

## 関連するpattern

`cd_pipeline_lambda pattern`に関連するpatternを、以下に記載します。

| pattern名                                                    | 説明                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| [cd_pipeline_lambda_trigger pattern](./cd_pipeline_lambda_trigger.md) | デプロイのトリガとなるイベントやDelivery環境のリソースアクセスに必要なロールなどを構築します。 |
| [lambda pattern](./lambda.md)            | このパターンのデプロイ先となるLambda関数を作成できます。 |

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.37.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| codedeploy\_deployment\_group\_name | CodeDeployのデプロイメントグループ名 | `string` | n/a | yes |
| codedeploy\_name | CodeDeployのアプリケーション名 | `string` | n/a | yes |
| cross\_account\_codepipeline\_access\_role\_arn | Runtime環境のCodePipelineから利用するクロスアカウントアクセス用RoleのARN。 | `string` | n/a | yes |
| delivery\_account\_id | Delivery環境のアカウントID | `string` | n/a | yes |
| pipeline\_name | CodePipelineの名前 | `string` | n/a | yes |
| source\_bucket\_name | デプロイ用のzip化されたソースファイルを配置するバケット名 | `string` | n/a | yes |
| artifact\_store\_bucket\_force\_destroy | Artifact Storeとして使っているS3 bucketを強制的に削除可能にするか否か | `bool` | `false` | no |
| artifact\_store\_bucket\_name | CodePipelineの[アーティファクト](https://docs.aws.amazon.com/ja_jp/codepipeline/latest/userguide/concepts.html#concepts-artifacts)を管理するS3 bucket名<br>未設定の場合、 "[pipeline\_name]-artifact-store" でS3 bucketを作成する。 | `string` | `""` | no |
| artifact\_store\_bucket\_transitions | Artifact Storeの移行に対するポリシー設定。最新でなくなったファイルに対して適用される。未設定の場合は30日後にAmazon S3 Glacierへ移行する | `list(map(string))` | <pre>[<br>  {<br>    "days": 30,<br>    "storage_class": "GLACIER"<br>  }<br>]</pre> | no |
| codebuild\_compute\_type | デプロイ用CodeBuildのコンピューティングリソースタイプ。選択できる値については[Terraformドキュメントのcompute\_type](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codebuild_project#compute_type)を参照。 | `string` | `"BUILD_GENERAL1_SMALL"` | no |
| deployment\_cloudwatch\_logs\_kms\_key\_id | デプロイ用CodeBuildのログデータを暗号化するためのKMS CMKのARN | `string` | `null` | no |
| deployment\_cloudwatch\_logs\_retention\_in\_days | デプロイ用CodeBuildのログをCloudWatch Logsで保持する期間を設定する。<br><br>値は、次の範囲の値から選ぶこと： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.<br>[Resource: aws\_cloudwatch\_log\_group / retention\_in\_days](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group#retention_in_days) | `number` | `null` | no |
| deployment\_require\_approval\_before\_build | CodeBuildの実行前に承認を必要とするか | `bool` | `true` | no |
| deployment\_require\_approval\_before\_deploy | CodeDeployの実行前に承認を必要とするか | `bool` | `true` | no |
| require\_codedeploy | CodeDeployをパイプラインに含めるかどうか。デフォルトは含める(`true`) | `bool` | `true` | no |
| sns\_topic\_arn\_approval\_before\_build | ビルド前の承認リクエストの送信先となるSNSトピックのARNを指定する。 | `string` | `null` | no |
| sns\_topic\_arn\_approval\_before\_deploy | デプロイ前の承認リクエストの送信先となるSNSトピックのARNを指定する。 | `string` | `null` | no |
| source\_object\_key | デプロイ用のzip化されたソースファイルのObject Key | `string` | `"source.zip"` | no |
| tags | CodePipelineに付与するタグ | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| artifact\_store\_arn | Artifact store用bucketのARN |
| codebuild | デプロイプロパイダとして用いるCodeBuildの情報 |
| kms\_keys | 作成されたKMSの鍵 |
