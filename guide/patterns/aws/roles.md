# roles pattern

## 概要

`roles pattern`モジュールでは、Runtime環境上にDevOpsを実践するための役割を定義するIAM Roleを作成します。

## 想定する適用対象環境

`roles pattern`は、Runtime環境で使用されることを想定しています。

## 依存するpattern

`roles pattern`モジュールは`users pattern`モジュールと`bind_role pattern`モジュールとセットで実行します。各モジュールの役割、実行順序は以下のとおりです。

1. `users pattern`：Delivery環境にIAMユーザーを作成します
1. `bind_role pattern`：Runtime環境のIAMロールへスイッチロールするIAMポリシーを作成し、Delivery環境のIAMユーザーへアタッチします
1. `roles pattern`：Runtime環境のIAMロールを作成します

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                 | 説明                                       |
| :----------------------------------------------------------------------------------------- | :----------------------------------------- |
| [IAMポリシー](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/access_policies.html) | IAMロールへ付与するIAMポリシーを作成します |
| [IAMロール](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_roles.html)          | Runtime環境のIAMロールを作成します         |

## モジュールの理解に向けて

`roles pattern`ではIAMロール、IAMロールに付与するIAMポリシーを作成します。

`roles pattern`では、以下の役割を定義するIAM Roleを作成します。

なおロール名の`%s`は、本patternの入力パラメータである`system_name`の値をCamelCaseとして変換した文字列に置き換えてください。例えば`system_name`に`"epona"`を指定した場合、管理者ロールの名前は`EponaAdminRole`となります。

| ロール名            | 説明                                                                           |
| :------------------ | :----------------------------------------------------------------------------- |
| `%sAdminRole`       | 管理者ロール(全ての作業を行う権限をもちます)                                   |
| `%sViewerRole`      | 閲覧者ロール(全リソースの閲覧のみできる権限をもちます)                         |
| `%sOperatorRole`    | 作業者ロール(読み取り宣言+運用で必要な権限をもちます)                          |
| `%sDeveloperRole`   | 開発者ロール(NWなどインフラ系サービスの操作や秘匿パラメータの参照を除いたリソースを操作する権限をもちます) |
| `%sCostManagerRole` | コスト管理者ロール(課金情報の操作のみできる権限をもちます)                     |
| `%sApproverRole`    | 承認者ロール(CI/CDの承認のみ可能な権限をもちます)                              |

![IAMユーザ関連のpattern](../../resources/users.png)

## IAMロールの権限追加について

[モジュールの理解に向けて](#モジュールの理解に向けて)に記載している通り、Eponaでは各IAMロールに必要と思われるIAMポリシーをアタッチしています。
本patternで作成するIAMロールにデフォルトで付与するIAMポリシーの詳細は[こちら](https://gitlab.com/eponas/epona/-/tree/master/modules/aws/components/iam/role)を確認ください。
しかし、プロジェクトによってはIAMロールに追加のIAMポリシーを与えたい場合も考えられます。
そのため、`additional_XXX_role_policy`（XXXは各ロール名）パラメータを用意しています。
追加したいIAMポリシーのARNをリスト形式で指定するとIAMロールに追加できます。

## サンプルコード

`roles pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "roles" {
  source       = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/roles?ref=v0.2.6"
  admins       = ["taro", "hanako"]
  approvers    = ["taro", "yoko"]
  costmanagers = ["taro", "ryota"]
  developers   = ["hanako", "chiharu"]
  operators    = ["hanako", "ayaka"]
  viewers      = ["hanako", "yoshiki"]

  additional_developer_role_policies = [
    "arn:aws:iam::aws:policy/AWSCodePipelineApproverAccess"
  ]

  account_id  = "XXXXXXXXXX"
  system_name = "hori"
  tags = {
    Owner              = "hori"
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }
}

```

## 関連するpattern

`roles pattern`に関連するpatternを、以下に記載します。

| pattern名                             | 説明                                                                                  |
| :------------------------------------ | :------------------------------------------------------------------------------------ |
| [`users pattern`](./users.md)         | Delivery環境のIAMユーザーを作成します                                                 |
| [`bind_role pattern`](./bind_role.md) | Delivery環境のIAMユーザーからIAMロール(Runtime環境)へスイッチするポリシーを作成します |

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.37.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| account\_id | 信頼関係を作成するユーザーのAWSアカウントID | `string` | n/a | yes |
| admins | adminへスイッチロール可能なユーザー一覧 | `list(string)` | n/a | yes |
| approvers | approverへスイッチロール可能なユーザー一覧 | `list(string)` | n/a | yes |
| costmanagers | costmanagerへスイッチロール可能なユーザー一覧 | `list(string)` | n/a | yes |
| developers | developerへスイッチロール可能なユーザー一覧 | `list(string)` | n/a | yes |
| operators | operatorへスイッチロール可能なユーザー一覧 | `list(string)` | n/a | yes |
| system\_name | システム名 | `string` | n/a | yes |
| viewers | viewerへスイッチロール可能なユーザー一覧 | `list(string)` | n/a | yes |
| additional\_admin\_role\_policies | adminロールに追加するIAMポリシーのARNリスト | `list(string)` | `null` | no |
| additional\_approver\_role\_policies | approverロールに追加するIAMポリシーのARNリスト | `list(string)` | `null` | no |
| additional\_costmanager\_role\_policies | costmanagerロールに追加するIAMポリシーのARNリスト | `list(string)` | `null` | no |
| additional\_developer\_role\_policies | developerロールに追加するIAMポリシーのARNリスト | `list(string)` | `null` | no |
| additional\_operator\_role\_policies | operatorロールに追加するIAMポリシーのARNリスト | `list(string)` | `null` | no |
| additional\_viewer\_role\_policies | viewerロールに追加するIAMポリシーのARNリスト | `list(string)` | `null` | no |
| tags | このモジュールで作成されるリソースに付与するタグ | `map(string)` | `{}` | no |

## Outputs

No output.
