# step_functions pattern

## 概要

`step_functions pattern`は、
[`AWS Step Functions`](https://aws.amazon.com/jp/step-functions/)
により、`AWS Lambda`などのAWSサービスを連携したワークフローを構築するモジュールです。

## 想定する適用対象環境

`step_functions`は、Runtime環境で使用することを想定しています。

## 依存するpattern

`step_functions pattern`は他のpattarnの実行結果に依存しません。独立して実行できます。

ただし、ワークフローに含めるAWSリソース（`AWS Lambda`関数など）は、事前に準備して頂く必要があります。

Lambda関数を作成する場合は、Eponaでは[`lambda pattern`](./lambda.md)が用意されていますのでご参照ください。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名 | 説明 |
|:--|:--|
| [AWS Step Functions](https://aws.amazon.com/jp/step-functions/) |ステートマシンを作成します。|
| [AWS Identity and Access Management](https://aws.amazon.com/jp/iam) (IAM) |ステートマシンを実行するために必要なロール、ポリシーを作成します。|
| [Amazon CloudWatch](https://aws.amazon.com/jp/cloudwatch/) |ステートマシンのログを配置するロググループを作成します。|
| [Amazon EventBridge](https://aws.amazon.com/jp/eventbridge/) |ステートマシンを起動するトリガーとなるイベントや、実行状態を通知するためのイベントルールやターゲットを作成します。|

![構成図](../../resources/sfn.png)

## `AWS Step Functions`について

`AWS Step Functions`に関する概念や用語については[公式ドキュメント](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/welcome.html)の記載を参照してください。

`AWS Step Functions`では、1つの作業単位（たとえばLambda関数の実行など）を`ステート`と呼び、これをつなげることでできる一連の流れ（ワークフロー）を`ステートマシン`と呼びます。

`ステート`にはLambda関数の実行などの`タスク`の他に、待ち状態や分岐などの種類があり、これらを組み合わせることで様々なワークフローを構築できます。

`ステート`の種類については[こちら](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/concepts-states.html)を参照してください。

また、`AWS Step Functions`で実現できるワークフローの概要については以下のドキュメントを参照してください。

- [ユースケース](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/use-cases.html)
- [チュートリアル](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/tutorials.html)

## ステートマシンの作成

### ステートマシン定義

`step_functions pattern`は、事前に作成していただいたステートマシン定義（json）を元にステートマシンを作成します。

ステートマシン定義は[Amazonステートメント言語](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/concepts-amazon-states-language.html)に従って記述する必要があります。

言語仕様に従ってテキストベースでの作成もできますが、
[AWS Step FunctionsWorkflow Studio](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/workflow-studio.html)
によりグラフィカルに作成した結果をエクスポートすることでも作成できます。

ステートマシンの定義にあたっては[ベストプラクティス](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/sfn-best-practices.html)もあわせて参照してください。

:information_source: `step_functions pattern`では`標準ワークフロー`の作成のみサポートしています。ワークフローの種類については[こちら](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/concepts-standard-vs-express.html)を参照してください。

### ステートマシンが各タスクを実行する権限

ステートマシンがタスク内で他のAWSサービスを呼び出すためには、ステートマシンに適切な権限が与えられていることが前提となります。

AWSサービスは多岐にわたるためすべてを網羅はできませんが、`step_functions pattern`では、一部のタスクについては必要なロールを自動作成できます。

これは、[入出力リファレンス](#入出力リファレンス)にある`execution_resource_arns`というパラメータを設定していただくことで可能となります。

下記のサンプルコードのように、タスク内で使用しているAWSリソースのARNを設定してください。

```hcl
  execution_resource_arns = [
    "arn:aws:ecs:ap-northeast-1:000000000000:YourECSTask",
    "arn:aws:lambda:ap-northeast-1:000000000000:function:YourLambdaFunction"
  ]
```

対応しているAWSリソースは以下の通りです。

- [AWS Lambda](https://aws.amazon.com/jp/lambda/)関数
- [Amazon Elastic Container Service](https://aws.amazon.com/jp/ecs/)(ECS)タスク
- [Amazon Simple Notification Service](https://aws.amazon.com/jp/sns/)(SNS)トピック
- [Amazon Simple Queue Service](https://aws.amazon.com/jp/sqs/)(SQS)キュー

----

:information_source: 例えばAmazon SQSキューのARNを指定した場合、
`sqs:SendMessage`を可能とするIAMポリシーがIAM実行ロールに付与されます。

----

これら以外のAWSリソースを使用する場合は、利用者側で必要な権限を持ったIAMポリシーを作成し、ステートマシンの実行ロールにアタッチしていただく必要があります。

たとえば、以下のようになります。

```hcl
# ポリシードキュメント
data "aws_iam_policy_document" "document" {
  statement {
    ...
  }
}

# IAMポリシー
resource "aws_iam_policy" "policy" {
  policy = data.aws_iam_policy_document.document.json
  name   = "..."
}

# ポリシーをロールにアタッチ
resource "aws_iam_role_policy_attachment" "attachment" {
  role       = "..." # step_functions patternで作成されるロールの名称
  policy_arn = aws_iam_policy.policy.arn
}

```

なお、`step_functions pattern`で作成されるロールは`role_arn`、`role_name`にて参照可能です。詳細は[入出力リファレンス](#入出力リファレンス)を参照してください。

AWSサービスごとに必要となる権限については[こちら](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/service-integration-iam-templates.html)をご覧ください。

### Lambdaのデプロイメントパイプラインとの統合について

EponaでのLambdaのデプロイメントパイプラインでは、`AWS Lambda`の
[`エイリアス`](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-aliases.html)
機能を使用しています。

デプロイメントパイプラインで管理されているLambda関数を`step_functions pattern`に統合するには、Lambda関数のARNを指定する箇所に、エイリアスを含める必要があります。

ARNにエイリアス名を含める場合の書式は`<Lambda関数のARN>:＜Lambdaエイリアス名＞`となります。
また、ステートマシンに対して、エイリアス付きのLambda関数を実行する権限の付与が必要です。

ここでは、エイリアス名を`default`とした場合の、ステートマシン定義およびステートマシンへの権限付与の例を記載します。

- ステートマシン定義で、Lambda関数のエイリアスを指定する

```json
{  
 "StartAt":"CallFunction",
 "States":{  
    "CallFunction": {  
       "Type":"Task",
       "Resource":"arn:aws:lambda:ap-northeast-1:123456789012:function:SampleFunction:default",
       "End": true
    }
  }
}
```

- エイリアス付きのLambda関数を実行する権限を、ステートマシンに付与する

```hcl
module "step_functions" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/step_functions?ref=v0.2.6"

  sfn_name        = "state-machine-lambda-alias"
  definition_json = file("sample-lambda-alias.json") # Lambdaエイリアスが指定されたステートマシン定義
  execution_resource_arns = [
    "arn:aws:lambda:ap-northeast-1:123456789012:function:SampleFunction:default"
  ]
  ...
}
```

## ステートマシンの実行

### ステートマシンの実行トリガー

ステートマシンはマネジメントコンソールや`AWS CLI`により手動で起動できますが、スケジュールやイベントなどに基づいて、自動的に起動するようにも設定できます。

実行方法については[こちら](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/concepts-state-machine-executions.html)を参照してください。

`step_functions pattern`では、`Amazon EventBridge`を使ったスケジュール実行と`Amazon S3`イベントでのトリガーをサポートしています。

なお、トリガーは複数種類同時に指定できます。

:information_source: トリガー条件を同時に複数満たした場合、ステートマシンは同時並行して起動します。ただし、同時実行数には制限があります。詳細は[こちら](https://docs.aws.amazon.com/step-functions/latest/dg/limits-overview.html#service-limits-api-action-throttling-general)を参照してください。

### Amazon EventBridgeを使ったスケジュール実行

`schedule_expression`にスケジュール式を指定することで、`Amazon EventBridge`を使用したスケジュール実行ができます。

`cron()`式あるいは`rate()`式で指定できます。

記述方法については、[こちら](https://docs.aws.amazon.com/ja_jp/eventbridge/latest/userguide/eb-create-rule-schedule.html#eb-cron-expressions)を参照してください。

以下に設定例を示します。

```hcl
module "step_functions" {
  ...
  schedule_configs = [
    {
      schedule_expression = "rate(5 minutes)"
      is_enabled = true
    },
    {
      schedule_expression = "cron(0/5 * * ? *)"
      is_enabled = false
    }
  ]
}
```

なお、スケジュール実行は`is_enabled`によって有効・無効を切り替えられます。デフォルトは`false`（スケジュール実行しない）ですので、有効にしたい場合は`is_enabled`に`true`を指定してください。

設定値については、[入出力リファレンス](#入出力リファレンス)もあわせて参照してください。

### Amazon EventBridgeを使ったイベントトリガー

#### Amazon S3

指定した`Amazon S3`バケット上のオブジェクトに変更があったことをトリガーにステートマシンを起動できます。

イベントは`AWS CloudTrail`の記録イベントで指定できます。たとえば、`PutObject`や`GetObject`などです。

指定できる値の詳細については、[こちら](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/cloudtrail-logging-s3-info.html#cloudtrail-object-level-tracking)をご覧ください。

また、[こちら](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/tutorial-cloudwatch-events-s3.html)のチュートリアルもあわせてご参照ください。

:information_source: `Amazon S3`バケット上のオブジェクトのイベントは`AWS CloudTrail`経由で検知します。
したがって、あらかじめ`AWS CloudTrail`を有効にしておく必要があります。以下のリンク先の記述もあわせてご参照ください。

- [AWS CloudTrail 公式ドキュメント](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/enable-cloudtrail-logging-for-s3.html)
- [Epona audit pattern](./audit.md#CloudTrailで記録されるイベントを理解する)

以下に設定例を示します。

```hcl
module "step_functions" {
  ...
  s3_event_configs = [
    {
      bucket_name   = aws_s3_bucket.this.bucket
      events        = ["PutObject"]
      filter_prefix = null
    },
    {
      bucket_name   = aws_s3_bucket.this.bucket
      events        = ["PutObject"]
      filter_prefix = null
    }
  ]
}
```

`filter_prefix`を設定することで、イベントの検知対象とするオブジェクトの絞り込みが可能です。
[こちら](https://docs.aws.amazon.com/ja_jp/eventbridge/latest/userguide/eb-event-patterns-content-based-filtering.html#eb-filtering-prefix-matching)の記載もあわせて参照してください。

#### その他のイベントトリガー

上記以外のイベントによりステートマシンを起動したい場合は、イベントルールを個別に作成していただく必要があります。

以下に設定例を示します。

```hcl
resource "aws_cloudwatch_event_rule" "rule" {
  name        = "..."
  description = "..."

  event_pattern = <<EOF
{
  <Step Functionsのトリガーとしたいイベントパターン>
}
EOF
}

resource "aws_cloudwatch_event_target" "step_functions" {
  rule      = aws_cloudwatch_event_rule.rule.name
  target_id = "SendToSNS" # 一意である必要があります。参考：https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_target
  arn       = <Step FunctionsのARN>
}
```

イベントパターンの設定方法については以下のドキュメントを参照してください。

- [Amazon EventBridge イベントパターン](https://docs.aws.amazon.com/ja_jp/eventbridge/latest/userguide/eb-event-patterns.html)
- [CloudWatch イベントルールのカスタムイベントパターンを作成する](https://aws.amazon.com/jp/premiumsupport/knowledge-center/cloudwatch-create-custom-event-pattern/)

## ステートマシンの監視・通知

### ステートマシンのステータスについて

ステートマシンは、[こちら](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/cw-events.html#cw-events-events)にあるようなステータスで管理されています。

ステータスはマネジメントコンソールや`AWS CLI`から手動で確認できますが、ステータスの変化時に`Amazon EventBridge`イベントを発行し、通知を送るような設定も可能です。

`step_functions pattern`では、[Slack](https://slack.com/intl/ja-jp/)、[Microsoft Teams](https://www.microsoft.com/ja-jp/microsoft-teams/group-chat-software)への通知を設定可能です。

以下のドキュメントの記載もあわせて参照してください。

- [Step Functions 実行ステータスの変更の EventBridge (CloudWatch イベント)](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/cw-events.html)
- [AWS Step Functions で状態 (実行イベント) が変更されたときに呼び出す Lambda 関数を設定する](https://aws.amazon.com/jp/premiumsupport/knowledge-center/lambda-state-change-step-functions/)

### 設定方法

#### 通知先、通知タイミングについて

事前に、Slack、Microsoft TeamsのWebhook URLを取得しておいてください。

- Slack/Microsoft Teamsのどちらかだけにも通知できますし、両方へも通知できます
- ステータスに応じて、チャンネルをわけて通知できます

デフォルトでは、ステータスが変更された際はすべて通知を送るようになっています。どのステータスで通知を送るかは`event_pattern_status`で指定可能です。

:information_source: 通知有無は`slack_notification_configs`、`teams_notification_configs`の
設定有無により切替可能です。  
通知を送りたくない場合は、これらの項目を設定しないようにしてください。

#### 通知内容について

通知内容は[入出力リファレンス](#入出力リファレンス)の`input_transformer_input_paths`と`input_transformer_input_template`で編集できます。

イベントが送られてくると、以下の順で動作します。

1. `input_transformer_input_paths`を使って情報を取り出す
2. `input_transformer_input_template`のテンプレートに送られる
3. 送信されるデータが作成される

設定内容については以下のリンク先を参照してください。

- [入出力リファレンス](#入出力リファレンス)
- [Amazon EventBridge ターゲット入力の変換](https://docs.aws.amazon.com/ja_jp/eventbridge/latest/userguide/eb-transform-target-input.html)
- [InputTransformer - Amazon EventBridge](https://docs.aws.amazon.com/eventbridge/latest/APIReference/API_InputTransformer.html)

なお、`null`を渡すと、Epona側で用意したプリセットが使用されます。

#### 通知の設定例

```hcl
slack_notification_configs =
[
  {
    webhook_uri                      = var.webhook_uri01
    input_transformer_input_paths    = null
    input_transformer_input_template = templatefile(
      "template.json", { color = "#00FF99" }
      )
    event_bus                        = "default"
    event_pattern_status             = ["SUCCEEDED"]
  },
  {
    webhook_uri                      = var.webhook_uri02
    input_transformer_input_paths    = null
    input_transformer_input_template = templatefile(
      "template.json", { color = "#0099FF" }
      )
    event_bus                        = "default"
    event_pattern_status             = ["RUNNING"]
  }
]
```

#### 各ツールへの通知のサンプル

##### Slack

![Slack](../../resources/sfn_slack_notify.png)

##### Microsoft Teams

![Microsoft Teams](../../resources/sfn_teams_notify.png)

## ステートマシン内の個々のステート（タスク）の監視・通知

複数のステートをつなげたステートマシンにおいて、どこのステートを実行していてもステートマシン全体としては`RUNNING`であり、「どこまで進んだか」といった状態は判断できません。

ステートマシン内の各ステート（タスク）の開始・完了を検知したい場合は、以下のような対応が必要です。

- CloudWatch Logsからメトリクスを作る
- 各タスク内でEventBridgeなどを利用する
- EventBridgeやSNSを利用したタスクを挟む
- 等

## ステートマシンのエラーハンドリング

処理中にエラーが発生した場合は、デフォルトではステートマシンの実行が失敗し停止します。  
`Catch`ステートや、`Task`ステート内の`Retry`を設定することでエラーハンドリングが可能です。

詳細は[こちら](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/concepts-error-handling.html)を参照してください。

なお、実行が停止したステートマシンの、途中からの再実行はできません（新しく実行を開始すると先頭のステートから実行されます）。  
ステートマシンの定義にあたっては、冪等性を意識した設計（たとえば、エラー終了時に途中状態をクリアするなど）となるようご留意ください。

:information_source: [こちら](https://aws.amazon.com/jp/blogs/compute/resume-aws-step-functions-from-any-state/)
のブログ記事では、ステートマシン定義をコピーし、`GoToState`ステートにより任意のステートから実行する方法が紹介されています。必要に応じてご参照ください。

## ステートマシンのログ

`AWS Step Functions`の実行ログは[Amazon CloudWatch Logsに保存](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/cw-logs.html)されます。

ログに記録する対象をログレベルにより調整できます。これは[入出力リファレンス](#入出力リファレンス)の`log_level`で設定できます。
ログレベルには`OFF`、`ALL`、`ERROR`、`FATAL`があり、このうち1つを指定します。デフォルトは`ALL`です。

:information_source: ログレベルを`OFF`にしても、ステートマシン用のロググループ自体は作成されます。

詳細については[こちら](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/cloudwatch-log-level.html)を参照してください。

```hcl
module "step_functions" {
  ..
  log_level                   = "ERROR"
}
```

## サンプルコード

``` hcl
# スケジュールにより起動するステートマシンの例
module "step_functions" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/step_functions?ref=v0.2.6"

  sfn_name        = "YourStateMachineName"
  definition_json = file("your-definition.json")
  execution_resource_arns = [
    "arn:aws:lambda:ap-northeast-1:123456789012:function:SampleFunction:default" # Lambdaの場合の例。エイリアスでの管理が不要の場合は、エイリアス削除可
  ]

  enabled_xray                = true

  log_group_name              = "your-statemachine-log"
  log_level                   = "ALL"
  log_group_retention_in_days = 14

  schedule_configs = [
    {
      schedule_expression = "cron(0/5 * * ? *)"
      is_enabled          = true
    }
  ]
}
```

## 関連するpattern

`step_functions_pattern`に関連するpatternを以下に記載します。

| pattern名                | 説明                 |
| ------------------------ | ------------------- |
| [lambda](./lambda.md)    | Lambda関数を作成する  |

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.44.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| definition\_json | Step Functionsの実行対象となる、ステートマシンのJSON定義を指定する。 | `string` | n/a | yes |
| log\_group\_name | ロググループの名前を指定する。 | `string` | n/a | yes |
| sfn\_name | Step Functionsのステートマシンの名前を指定する。ロールなどのリソース名の編集にも使用している。文字数超過エラーが起きた場合は名称の長さを調整すること。 | `string` | n/a | yes |
| enabled\_xray | X-Rayを利用するか指定する。デフォルトでは利用しない。 | `bool` | `false` | no |
| execution\_resource\_arns | ステートマシンのタスクが連携(統合)するAWSリソースのARNをリストで指定する。それにより、ステートマシンのIAM実行ロールに必要なIAMポリシーがアタッチされる。<br><br>対応しているAWSリソースは以下の通り。<br><br>- [AWS Lambda](https://aws.amazon.com/jp/lambda/)関数<br>- [Amazon Elastic Container Service](https://aws.amazon.com/jp/ecs/)(ECS)タスク<br>- [Amazon Simple Notification Service](https://aws.amazon.com/jp/sns/)(SNS)トピック<br>- [Amazon Simple Queue Service](https://aws.amazon.com/jp/sqs/)(SQS)キュー<br><br>これら以外のAWSリソースを使用する場合は、利用者側で必要な権限を持ったIAMポリシーを作成し、ステートマシンの実行ロールにアタッチしていただく必要がある。 | `list(string)` | `[]` | no |
| log\_group\_retention\_in\_days | CloudWatch Logsにログを残す日数を指定する。デフォルトは14日間。 | `number` | `14` | no |
| log\_level | CloudWatch Logsに書き出すログのログレベルとして、`OFF`、`ALL`、`ERROR`、`FATAL`のどれかを指定可能。<br>詳細は[ログレベル](https://docs.aws.amazon.com/ja_jp/step-functions/latest/dg/cloudwatch-log-level.html)を参照のこと。デフォルトは`ALL`。 | `string` | `"ALL"` | no |
| region | Step Functionsのステートマシンを実行するリージョンを指定する。指定しなかった場合は、現在Terraformを実行中のリージョンが指定される。 | `string` | `null` | no |
| s3\_event\_configs | S3バケットのイベント経由でステートマシンを開始したい場合に指定する。<br><br>- `bucket_name`: ステートマシンを開始するトリガーとなるS3バケットの名前。(Required)<br>- `events`: ステートマシンを起動するS3イベント。イベントの種類は[AWS CloudTrailのログ記録によって追跡されるAmazon S3オブジェクトレベルのアクション](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/cloudtrail-logging-s3-info.html#cloudtrail-object-level-tracking)を参照。(Required)<br>- `filter_prefix`: S3バケット側のイベント発行の対象になるS3オブジェクトのプレフィックス。(Optional)<br><br>example:<pre>s3_event_configs = [<br>  {<br>      bucket_name   = YOUR_BUCKET_NAME<br>      events        = ["PutObject"]<br>      filter_prefix = "prefix"<br>  },<br>]</pre> | `list(any)` | `[]` | no |
| schedule\_configs | EeventBridgeのスケジュールを使ってステートマシンを開始したい場合に指定する。<br><br>`schedule_expression` にはスケジュールを`rate()`か`cron()`で指定する。<br>記載方法は[RateまたはCronを使用したスケジュール式](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/services-cloudwatchevents-expressions.html)を参照。<br><br>`is_enabled`が`true`の時はスケジューリングを有効にする。デフォルトは`false`。<br><br>example:<pre>schedule_configs = [<br>  {<br>    schedule_expression = "rate(5 minutes)"<br>    is_enabled = true<br>  },<br>  {<br>    schedule_expression = "cron(0/5 * * ? *)"<br>    is_enabled = false<br>  }<br><br>]</pre> | `list(any)` | `[]` | no |
| slack\_notification\_configs | 本設定があった場合はSlackに対して通知を行う。詳細は`step_functions pattern`の「通知」を参照。<br><br>example:<pre>slack_notification_configs = [<br>  {<br>    webhook_uri = "https://webhook.example.com/webhook"<br>    input_transformer_input_paths = null<br>    input_transformer_input_template = null<br>    event_bus = "default"<br>    event_pattern_status = ["SUCCEEDED"]<br>  },<br>    webhook_uri = "https://another-webhook.example.com/webhook"<br>    input_transformer_input_paths = null<br>    input_transformer_input_template = null<br>    event_bus = "default"<br>    event_pattern_status = ["RUNNING"]<br>  }<br>]</pre> | `list(any)` | `[]` | no |
| tags | タグを指定する。 | `map(string)` | `{}` | no |
| teams\_notification\_configs | 本設定があった場合はMicrosoft Teamsに対して通知を行う。詳細は`step_functions pattern`の「通知」を参照。<br><br>example:<pre>teams_notification_configs = [<br>  {<br>    webhook_uri = "https://webhook.example.com/webhook"<br>    input_transformer_input_paths = null<br>    input_transformer_input_template = null<br>    event_bus = "default"<br>    event_pattern_status = ["SUCCEEDED"]<br>  },<br>    webhook_uri = "https://another-webhook.example.com/webhook"<br>    input_transformer_input_paths = null<br>    input_transformer_input_template = null<br>    event_bus = "default"<br>    event_pattern_status = ["RUNNING"]<br>  }<br>]</pre> | `list(any)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| arn | Step FunctionsのステートマシンのARN。 |
| role\_arn | Step Functionsのステートマシンを実行するロールのARN。 |
| role\_name | Step Functionsのステートマシンを実行するロールの名前を返す。 |
