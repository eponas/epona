# quicksight_vpc_inbound_source pattern

## 概要

`quicksight_vpc_inbound_source pattern`では、Amazon QuickSightがVPC内に構築されたデータソースと接続するためのセキュリティグループを作成します。

本ドキュメントでは、Amazon QuickSight(以降、QuickSight)の設定方法などは解説しません。
Amazon QuickSightの利用方法については、[Amazon QuickSightを用いたBI指標可視化ガイド](./../../how_to/aws/quicksight_monitor.md)をご参照ください。

:warning:
VPC内のリソースに接続するためには、QuickSightは`Enterprise Edition`のサブスクリプションが必要となります。
QuickSightのサブスクリプションを確認の上、本patternを適用してください。

## 想定する適用対象環境

`quicksight_vpc_inbound_source pattern`は、QuickSightが構築されるRuntime環境で使用されることを想定しています。

## 依存するpattern

`quicksight_vpc_inbound_source pattern`は、QuickSightが接続するデータソース（RDSなど）を構築するpatternが先に適用されていることを想定しています。

想定するデータソースを構築するpatternは[関連するpattern](#関連するpattern)を確認してください。

:warning: 本patternで構築されるセキュリティグループは、データソースを構築するそれぞれのpatternでも設定が必要となります。
そのため、patternの適用順序に制約が生まれます。
こちらについては[patternの適用順序](#patternの適用順序)を参照してください。

## 連携可能なpattern

Eponaが提供するpatternで、`quicksight_vpc_inbound_source pattern`と連携を想定しているものは、以下があります。

| pattern名                         | データソースとして連携するAWSリソース         |
| :-------------------------------- | :-------------------------------------------- |
| [database pattern](./database.md) | Amazon Relational Database Service(以降、RDS) |

:information_source:
QuickSightが連携できるデータソースの一覧については、以下のドキュメントを参照してください。  
[サポートされているデータソース - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/supported-data-sources.html)

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                            | 説明                                                                                                       |
| :-------------------------------------------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------------------------------- |
| [Amazon EC2 セキュリティグループ](https://docs.aws.amazon.com/ja_jp/AWSEC2/latest/UserGuide/ec2-security-groups.html) | 指定されたセキュリティグループからのインバウンドを許可する、QuickSight用のセキュリティグループを作成します |

## モジュールの理解に向けて

DevOpsにおいて、サービスの現状を知り、どのように発展/課題解決をするのかを検討することは重要な要素です。

Eponaではビジネスモニタリングを行うための方法として、QuickSightの利用を想定しています。

:information_source:
[Amazon QuickSight](https://aws.amazon.com/jp/quicksight/)

QuickSightからRDSのように通常インターネットに公開されないAWSリソースと接続する場合、セキュリティグループの設定が必要になります。
本patternを適用することで、QuickSightからVPC内のAWSリソースに接続するためのセキュリティグループを構築します。

## patternの適用順序

本patternでは、QuickSightとVPC内に構築されたデータソースとの連携を前提としています。
この連携のためには、以下の順でpatternを適用する必要がある点にご注意ください。

1. VPC内のデータソースを構築するpattern(例として、`database pattern`)の適用
2. `quicksight_vpc_inbound_source pattern`の適用
3. 2.で作成したセキュリティグループをデータソースを構築するpatternのパラメータに設定し、再度VPC内のデータソースを構築するpatternを適用

このような手順をとる理由を以下に説明します。

QuickSightからVPC内のデータソースを参照するには、以下の双方向のアクセスを許可する必要があります。

- QuickSight -> データソースの接続の許可（行き）
- データソース -> QuickSightの接続の許可（戻り）

本モジュールで作成されるセキュリティグループは、QuickSightが接続するデータソースからの戻りの通信に対する受信ルールに利用されます。

通常、セキュリティグループはStatefulフィルタリングを行うため、応答のための設定は不要です。
しかし、QuickSightで生成されるElastic Network Interfaceに設定されたセキュリティグループはStatefulではありません。
このため、接続先となるデータソースからの戻りの通信を受信するためのセキュリティグループルールが必要となります。

:information_source:
別途セキュリティグループが必要になる詳細な理由は、以下のドキュメントを参照してください。

[VPCへの接続 Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/working-with-aws-vpc.html)

[Amazon QuickSightのプライベートVPC内のデータアクセスの設定方法について](https://aws.amazon.com/jp/blogs/news/amazon-quicksight-private-vpc-access/)

:information_source:

利用者が作成したセキュリティグループをデータソースを構築するpatternで利用する場合、本patternで作成されたセキュリティグループをインバウンドに設定してください。

## サンプルコード

`quicksight_vpc_inbound_source pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "quicksight_vpc_inbound_source" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/quicksight_vpc_inbound_source?ref=v0.2.6"

  name = "RDS-QuickSight-Integration"
  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }

  # QuickSightと接続を行うリソースのセキュリティグループと、セキュリティルールの説明を設定する
  integration_security_groups = [{
    description       = "Enable access postgresql from QuickSight"
    security_group_id = data.terraform_remote_state.production_database.outputs.database.instance_security_group_id
  }]

  # 接続対象となるVPCを指定
  vpc_id = data.terraform_remote_state.production_network.outputs.network.vpc_id
}
```

接続先となるAWSリソースのサンプルコードは[関連するpattern](#関連するpattern)の各patternのドキュメントを参照してください。

:warning:
本patternは1つのVPC内のデータソースへアクセスするためのセキュリティグループを構築します。
VPCが異なるデータソースへのアクセスが必要な場合は、VPCの数に応じて本patternを適用してください。

## 関連するpattern

`quicksight_vpc_inbound_source pattern`は、その他のVPC内にリソースを構築するpatternと組み合わせて利用されます。
Eponaが提供するpatternの内、本patternと組み合わせて利用することが想定されている一覧は、[連携可能なpattern](#連携が可能なpattern)を参照してください。

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.37.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| integration\_security\_groups | QuickSightが参照するAWSリソースに設定されているセキュリティグループ設定のリスト<br><br>VPC内にあるリソースをQuickSightから参照する際に必要になる設定<pre>integration_security_groups = [<br>  {<br>    description       = "RDS integration for QuickSight"   # 結合するセキュリティグループの説明<br>    security_group_id = "sg-000000"   # VPC内のリソースに設定されているセキュリティグループ<br>  }<br>]</pre> | `list(map(string))` | n/a | yes |
| security\_group\_name | このモジュールで作成されるSecurityGroupの名前 | `string` | n/a | yes |
| vpc\_id | QuickSightが参照するリソースが配置されているVPCのID | `string` | n/a | yes |
| tags | このモジュールで作成されるリソースに付与するタグ | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| security\_group\_id | 作成されたQuickSight用のSecurityGroupのID |
