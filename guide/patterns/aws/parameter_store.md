# parameter_store pattern

## 概要

`parameter_store pattern`モジュールでは、設定情報および秘匿情報を保存します。

このpatternで保存された設定情報および暗号化された秘匿情報は、コンテナ等の環境変数などで使用します。

## 想定する適用対象環境

`parameter_store pattern`は、主にRuntime環境での使用を想定しています。

## 依存するpattern

`parameter_store pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                                     | 利用する情報    |
| :-------------------------------------------- | :-------------- |
| [encryption_key pattern](./encryption_key.md) | CMKのエイリアス |

本patternが依存するリソースを他の構築手段で代替する場合は、依存するpatternと[入出力リファレンス](#入出力リファレンス)の内容を参考に、本patternが必要とするリソースを構築してください。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                                                      | 説明                                                     |
| :---------------------------------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------- |
| [AWS Systems Manager パラメータストア](https://docs.aws.amazon.com/ja_jp/systems-manager/latest/userguide/systems-manager-parameter-store.html) | 入力変数で指定された設定情報、または秘匿情報を保存します |

## モジュールの理解に向けて

AWS Systems Manager パラメータストア（以後、単に「パラメータストア」と記載）には、設定情報や暗号化した秘匿情報を格納できます。また、AWS Fargate等で動作するアプリケーションから環境変数として参照が可能です。

`parameter_store pattern`のインスタンスを使い、設定情報および秘匿情報を定義してパラメータストアに格納してください。暗号化にはAWS KMSで管理されるCMKを用います。

パラメータストアで扱う情報は、その値を参照するリソースよりも先に定義が必要なため、他のpatternの中には組み込まず独立したpatternとして作成しています。

ここで、`parameter_store pattern`のインスタンス数（設定情報や秘匿情報の数ではありません）をどのように考えるかは、検討の余地があります。

Eponaでは、`parameter_store pattern`のインスタンスをひとつ作成し、パラメータストアで管理すべき情報をひとまとめに定義する運用を想定しています。

パラメータストアはAWSアカウント内でひとつであり、`parameter_store pattern`のインスタンスを複数定義しても格納先は同じです。このため、一元管理した方が扱いやすいと考えています。

:information_source: `parameter_store pattern`を使用するインスタンス数に、Eponaとして制限を設けているわけではありません。

分割した方が管理しやすいと判断した場合は、`parameter_store pattern`のインスタンスを複数作成してください。

また、パラメータストアに格納する際の設定情報や秘匿情報の名前（パラメータ名）は、前述の通り格納先のパラメータストアは単一のため、ユニークである必要があります。このため、用途に応じてパラメータ名の命名規則で整理していく必要があります。この目的には、パラメータ名に`/`（スラッシュ）を含め、パラメータ階層を利用するとよいでしょう。

* [パラメータを階層に編成](https://docs.aws.amazon.com/ja_jp/systems-manager/latest/userguide/sysman-paramstore-su-organize.html)
* [Organize Parameters by Hierarchy, Tags, or Amazon CloudWatch Events with Amazon EC2 Systems Manager Parameter Store](https://aws.amazon.com/jp/blogs/mt/organize-parameters-by-hierarchy-tags-or-amazon-cloudwatch-events-with-amazon-ec2-systems-manager-parameter-store/)

## 秘匿情報の管理

`parameter_store pattern`で管理する情報には、秘匿情報を含むことが多いでしょう。

秘匿情報は`.tf`ファイルにハードコードするのではなく、入力変数ファイル（`.tfvars`ファイル）に定義して、`terraform`コマンドのオプションで指定してください。

```shell
$ terraform apply -var-file=[秘匿情報を含む入力変数ファイル（.tfvars）]
```

この入力変数ファイルには秘匿情報を含むため、本来は秘匿情報を参照できないはずのメンバーがアクセスできる可能な場所で管理しないでください。一般には、サービス開発チームで開発するTerraformの定義ファイルとは、異なる場所で管理することになると考えられます。

ソースコードにアクセス可能なメンバーであれば秘匿情報に対してアクセス権関係なく自由に閲覧可能な状態になっていた場合、秘匿情報の意味がないからです。

秘匿情報を参照可能な立場にある限られたメンバーのみアクセス可能な、プライベートリポジトリまたはファイルサーバー等で管理することを推奨します。

## サンプルコード

`parameter_store pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "parameter_store" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/parameter_store?ref=v0.2.6"

  parameters = [
    {
      name   = "/App/Config/nablarch_db_url"
      value  = var.nablarch_db_url
      type   = "SecureString"
      key_id = data.terraform_remote_state.production_encryption_key.outputs.encryption_key.keys["alias/my-encryption-key"].key_alias_name
    },
    {
      name   = "/App/Config/nablarch_db_user"
      value  = var.nablarch_db_user
      type   = "SecureString"
      key_id = data.terraform_remote_state.production_encryption_key.outputs.encryption_key.keys["alias/my-encryption-key"].key_alias_name
    },
    {
      name   = "/App/Config/nablarch_db_password"
      value  = var.nablarch_db_password
      type   = "SecureString"
      key_id = data.terraform_remote_state.production_encryption_key.outputs.encryption_key.keys["alias/my-encryption-key"].key_alias_name
    },
  ]

  tags = {
    # 任意のタグ
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }
}
```

## 関連するpattern

`parameter_store pattern`に関連するpatternを、以下に記載します。

| pattern名                                                                           | 説明                                                                                        |
| :---------------------------------------------------------------------------------- | :------------------------------------------------------------------------------------------ |
| [`public_traffic_container_service pattern`](./public_traffic_container_service.md) | このpatternを使用してパラメータストアに格納した設定情報、秘匿情報を環境変数として参照します |

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.37.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| parameters | パラメーターを構成する属性（name, type, value, description, tier, key\_id, overwrite, allowed\_pattern, tags）のリスト | `list(map(any))` | n/a | yes |
| tags | このモジュールで作られるリソースに共通的に付与するタグ | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| parameters | n/a |
