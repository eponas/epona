# vpc_peering pattern

## 概要

`vpc_peering pattern`モジュールでは、異なるVPC間を接続するVPCピアリングを設定します。

## 想定する適用対象環境

`vpc_peering pattern`は、Delivery環境およびRuntime環境のいずれでも使用されることを想定しています。

## 依存するpattern

`vpc_peering pattern`は、事前に以下のpatternが適用されていることを前提としています。また、network patternにより複数のVPCが作成されていることを前提とします。なお、接続するVPCは異なるCIDRである必要があります。

| pattern名                       | 利用する情報                           |
|:--------------------------------|:---------------------------------------|
| [network pattern](./network.md) | VPC ID |

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                     | 説明                                                                                                       |
| :------------------------------------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------------------------------- |
| [Peering connection](https://docs.aws.amazon.com/ja_jp/vpc/latest/peering/what-is-vpc-peering.html)                                                                   | 異なるVPCを相互に接続できます。|

## モジュールの理解に向けて

`vpc_peering pattern`では異なる2つのVPCを接続するVPCピアリングを構築します。
たとえば、サービス用のVPCと仮想セキュアルーム用のVPCを分ける場合にVPCピアリングを行います。
VPCピアリングの全体像は以下の通りです。

![全体像](./../../resources/vpc_peering_overview.png)

この内、`vpc_peering pattern`は以下図の通りVPCピアリングの部分のみを構築します。
VPCピアリングはピアリング要求をするVPCとピアリングを承諾するVPCの2つのVPC IDを指定して行います。
ピアリング要求をするVPCのことを`リクエスタVPC`、ピアリングを承諾するVPCのことを`アクセプタVPC`と呼びます。
リクエスタVPCとアクセプタVPCでとくに動作の違いはありません。
VPCピアリングは異なるAWSアカウント（クロスアカウント）や異なるリージョン（クロスリージョン）のVPC同士を繋げることもできます。
しかし、eponaで提供する`vpc_peering pattern`は同一アカウント、同一リージョンのみを想定した実装になっています。

![vpc_peering](./../../resources/vpc_peering_this.png)

### ルートテーブルの設定について

VPCピアリングを作成した後、以下図の通り各VPC内で接続先VPCへのルーティングを設定する必要があります。
この設定は[network pattern](./network.md)の`peering_id`と`peering_cidr`を指定して行います。
`peering_id`は`vpc_peering pattern`のoutputで出力されるpeering_idを指定します。
`peering_cidr`は接続先VPCのCIDRを指定します。
このルーティング設定はピア接続する相互のVPCに行ってください。

![network](./../../resources/vpc_peering_network.png)

つまり、`vpc_peering pattern`は前後にnetwork patternの実行が必要です。
以下の流れで設定します。

- network patternをapply（2つ以上のVPCを作成）
- vpc_peering patternをapply（outputされるpeering_idを確認）
- network patternを再apply（各VPCに`peering_id`および`peering_cidr`を設定）

### 3つ以上のVPCを接続する場合の注意

3つ以上のVPCを接続する場合は注意が必要です。
VPCピアリングの仕様として、ピアリングは直接する必要があります。
たとえばA、B、Cの3つのVPCを相互に接続したい場合、以下図の悪い例のようにA-BとB-Cのピア接続だけだとA-Cの接続はできません。
この場合は以下図の良い例のようにA-B、B-Cに加え、A-Cのピアリングも設定します。

:warning: 現在eponaで提供する`vpc_peering pattern`は1対1のピアリングにしか対応していません。今後の機能拡張で複数ピアにも対応予定です。

![triple](./../../resources/vpc_peering_triple.png)

### アクセス制限について

各VPC内のアクセス制限はセキュリティグループなどで設定してください。
たとえば、仮想セキュアルーム用VPC内のWorkSpacesからサービス用VPC内のRDSへ接続する場合を考えます。
この場合はサービス用VPCのRDSに適用しているセキュリティグループのインバウンドで仮想セキュアルーム用VPCのCIDR（または、プライベートサブネットのCIDR）を許可します。
このようなアクセス許可の設定は`vpc_peering pattern`ではなく、各patternモジュールで設定します。

## サンプルコード

`vpc_peering pattern`を使用したサンプルコードを、以下に記載します。
また、VPCピアリングを設定する場合の`network pattern`のサンプルコードも記載します。

### vpc_peering pattern

```terraform
module "vpc_peering" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/vpc_peering?ref=v0.2.6"

  tags = {
    env = "production"
  }

  name             = "secureroom-service-peering"
  requester_vpc_id = data.terraform_remote_state.network.outputs.secureroom.vpc_id
  accepter_vpc_id  = data.terraform_remote_state.network.outputs.service.vpc_id
}
```

### network pattern

以下サンプルコードでは`peering_id`をremote_stateから読み込む想定の指定です。
この場合、`instance_dependencies.tf`を作成することになります。
しかし、network patternの初回実行時は`instance_dependencies.tf`でremote_stateを定義しないでください。
network patternの初回実行時は`vpc_peering pattern`をまだ実行していないため、エラーで実行できなくなってしまいます。
`vpc_peering pattern`実行後に`instance_dependencies.tf`の定義と下記サンプルコードの様に`peering_id`と`peering_cidr`を指定ください。

```terraform
module "secureroom" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/network?ref=v0.2.6"

  name = "secureroom-runtime-production-network"

  cidr_block = "10.81.0.0/16"

  availability_zones = ["ap-northeast-1a", "ap-northeast-1c"]

  public_subnets             = ["10.81.10.0/24", "10.81.20.0/24"]
  private_subnets            = ["10.81.30.0/24", "10.81.40.0/24"]
  nat_gateway_deploy_subnets = ["10.81.10.0/24", "10.81.20.0/24"]

  # peering関連のパラメータはvpc_peering pattern実行後に設定する。
  # vpc_peering pattern実行前は以下パラメータをコメントアウトにする。
  peering_id   = data.terraform_remote_state.vpc_peering.outputs.vpc_peering.peering_id
  peering_cidr = "10.82.0.0/16"
}

module "service" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/network?ref=v0.2.6"

  name = "service-runtime-production-network"

  cidr_block = "10.82.0.0/16"

  availability_zones = ["ap-northeast-1a", "ap-northeast-1c"]

  public_subnets             = ["10.82.10.0/24", "10.82.20.0/24"]
  private_subnets            = ["10.82.30.0/24", "10.82.40.0/24"]
  nat_gateway_deploy_subnets = ["10.82.10.0/24", "10.82.20.0/24"]

  # peering関連のパラメータはvpc_peering pattern実行後に設定する。
  # vpc_peering pattern実行前は以下パラメータをコメントアウトにする。
  peering_id   = data.terraform_remote_state.vpc_peering.outputs.vpc_peering.peering_id
  peering_cidr = "10.81.0.0/16"
}
```

## 関連するpattern

`vpc_peering pattern`に関連するpatternを、以下に記載します。

| pattern名                                                             | 説明                                            |
|:----------------------------------------------------------------------|:------------------------------------------------|
| [network](./network.md) | ネットワークをデプロイするパターン                |

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.37.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| accepter\_vpc\_id | ピアリングを承諾するVPC（アクセプタ）のID | `string` | n/a | yes |
| name | VPCピアリングの名前 | `string` | n/a | yes |
| requester\_vpc\_id | ピアリングを要求するVPC（リクエスタ）のID | `string` | n/a | yes |
| tags | VPCピアリングに関するリソースに、共通的に付与するタグ | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| peering\_id | VPCピアリングのID |
