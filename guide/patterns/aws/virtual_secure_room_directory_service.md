# virtual_secure_room_directory_service pattern

## 概要

`virtual_secure_room_directory_service pattern`モジュールでは、仮想セキュアルームのユーザーを作成・登録するディレクトリサービスを構築します。

## 想定する適用対象環境

`virtual_secure_room_directory_service pattern`は、Runtime環境で使用することを想定しています。

## 依存するpattern

`virtual_secure_room_directory_service pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                       | 利用する情報                           |
|:--------------------------------|:---------------------------------------|
| [network pattern](./network.md) | パブリックおよびプライベートサブネット |

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                           | 説明                                                                            |
|:---------------------------------------------------------------------|:--------------------------------------------------------------------------------|
| [AWS Directory Service](https://aws.amazon.com/jp/directoryservice/) | 入力変数で指定された設定で、ディレクトリサービスを構築します。                  |
| [Amazon EC2](https://aws.amazon.com/jp/ec2/)                         | Directory Serviceを管理するAD Managerサーバーを構築します。                     |
| [AWS Systems Manager](https://aws.amazon.com/jp/systems-manager/)    | マネージャサーバーを自動でドメイン参加させるためのSSMドキュメントを作成します。 |

## モジュールの理解に向けて

仮想セキュアルームは``Directory Service``と``WorkSpaces``で実現します。以下に各サービスの概要を簡単に記載します。

![仮想セキュアルーム](../../resources/virtual_secure_room.png)

**Directory Service** :ディレクトリ(AD)サービス。WorkSpacesにログインするユーザーを管理する。  
**WorkSpaces** :VDIサービス。仮想セキュアルームの端末となる。

`virtual_secure_room_directory_service pattern`は以下の部分を作成するモジュールになります。

![DirectoryServiceパターン作成範囲](../../resources/virtual_secure_room_directory_service.png)

以下のリソースを作成します。

### Directory Service

- 仮想セキュアルームでは``MicrosoftAD``タイプのディレクトリを構築します
  - これはユーザーのパスワードのリセットや有効期限を設けたいためです
  - また、``MicrosoftAD``の場合は``Standard``か``Enterprise``のエディションを選択できます
  - エディションにより管理できるオブジェクト数の上限に違いがあり、``Standard``のオブジェクト上限は50,000となります
  - 仮想セキュアルームではそこまで多くのオブジェクトを扱わないため``Standard``をデフォルトのエディションとしています
- ドメイン名、短縮名、OUは指定した任意の値で設定します
- ドメイン管理者の``Admin``のパスワードを任意の値で設定します
- WorkDocsは使用しないのでSSOは設定しません

### AD Manager用 EC2インスタンス

- Directory Serviceへユーザー登録などをするためのEC2インスタンスです
- デフォルトでは最新のWindows2019日本語版のAMIで起動します。(任意に指定も可能)
- 管理に必要な``AD LDSツール``、``DNSサーバーツール``、``グループポリシーの管理``をUSERDATAでインストールします
- Directory Serviceへのドメイン参加も自動で行われます。これはSSMでコマンドを実行することで実現しています  
  SSMのコマンド実行後にサーバー再起動が行われ、それ以後はドメインのユーザーでログインできるようになります
- 上記SSM実行のためのインスタンスプロファイルを設定します。(ADManage用EC2インスタンスに付与するIAMロール参照)
- ローカルAdministratorのパスワードを任意の値で設定します
- EIPを付与し、インスタンスのパブリックIPを固定化しています。(ファイアウォール申請用)

### AD Manager用 EC2インスタンスに付与するIAMロール

- EC2を自動でドメイン参加させるためSSMを実行するための以下ポリシーをアタッチします
  - ``AmazonSSMManagedInstanceCore``
  - ``AmazonSSMDirectoryServiceAccess``

### AD Manager用 EC2インスタンスに付与するSG

- 特定の端末(作業スペースの固定端末など)からのみのアクセスを許可する

## AD Manager用 EC2インスタンスへの接続について

AD Manager用EC2にはpattern実行後に出力される`admanager_public_dns`または`admanager_public_ip`を指定してRDPで接続します。
接続はpattern実行時に許可したCIDRから接続します。
ドメインのAdminでAD Manager用EC2へ接続する際、以下メッセージで接続できず数分待っても改善しないことがあります。
その場合は一度EC2インスタンスを再起動してから再接続してみてください。

```txt
Unable connect. We couldn't connect to the remote PC. You might not have permission to sign in remotely. Contact your network Administrator for assistance.
```

## 初期マスタパスワードの変更について

本パターンを適用後に構築される以下ユーザーのパスワードは、リソース構築用のterraformスクリプトに平文で記述する必要があります。

- Directory ServiceのAdmin
- AD ManagerサーバーのAdministrator

この状況は、コードを参照可能なユーザーによる不正なアクセスや、悪意のある第三者による不正アクセスのリスクを増大させます。

そのため、本パターンを適用した後には必ずパスワードを変更するようにしてください。

変更手順は以下のようになります。

### Directory ServiceのAdmin

1. マネジメントコンソールにアクセスする
2. [サービス]-[Directory Service]でDirectory Serviceの画面を表示する
3. 本patternで作成したDirectory Serviceを選択し、[アクション]-[ユーザーパスワードのリセット]を選択する
4. `ユーザー名`に`Admin`、`新しいパスワード`および`パスワードを確認`に任意のパスワードを入力して[パスワードのリセット]
5. AD Manager用EC2にRDPでログインする。その際、ユーザーはドメインのAdmin、パスワードは4の手順で設定したパスワードを使いログインする

### AD ManagerサーバーのAdministrator

1. Runtime環境のAD Manager用EC2のEIPを確認し、許可した端末からRDPで接続する  
   この時、接続ユーザーはインスタンスの``Administrator``で接続する  

      ``` yaml
      user: Administrator
      password: P@ssw0rd1234
      ```

2. [スタート]-[Windwos PowerShell]を起動する
3. 以下コマンドでパスワードを変更する

      ```powershell
      $Password = ConvertTo-SecureString "変更後のパスワード" -AsPlainText -Force
      Get-LocalUser -Name "Administrator" | Set-LocalUser -Password $Password
      ```

4. [スタート]-[電源]-[切断]で一度ログアウトする
5. 変更したパスワードで``Administrator``にログインできることを確認する

## ディレクトリユーザーの追加について

以下の手順でユーザーを追加します。

1. Runtime環境のAD Manager用EC2のEIPを確認し、許可した端末からRDPで接続する  
   この時、接続ユーザーはドメインの``Admin``で接続する。サンプルコードで記載した例だと以下でアクセス可能  
   (ログイン出来ない場合EC2の初期化処理が終わっていない可能性がある。数分おいてからやり直す。)  

      ``` yaml
      user: Admin@TEST or TEST\Admin or TESTをtest.epona.comに変えてもOK
      password: P@ssw0rd1234
      ```

2. [スタート]-[Windows管理ツール]-[Active Directoryユーザーとコンピューター]を起動する
3. [ドメイン名]-[OU名]-[Users]を開く(サンプルコードの例だとtest.epona.com->TEST->Users)。Adminが表示さる  
   ここで画面上部の人間が一人のボタン[``現在のコンテナーに新しいユーザーを作成``]をクリック
4. ``姓``、``名``、``ユーザーログオン名``を入力する。例えば以下の通り。入力したら[次へ]をクリック

      ``` yaml
      姓: test
      名: a
      ユーザーログオン名: test.a
      ```

5. ``パスワード``と``パスワードの確認入力``を入力(!!!作業端末によっては英語キーボードと認識される可能性もあるので記号などは注意!!!)する。入力したら[次へ]
6. [完了]をクリック
7. 作成したユーザーをクリックしてpropertiesを表示する。[全般]タブの``電子メール``に使用可能なメールアドレスを設定する  
   WorkSpacesを作成するとこのメールアドレスにworkspaceへの招待メールが飛ぶ。入力したら[OK]をクリック

## AD Manager用EC2のインバウンド設定について

AD Manager用EC2への接続を許可する送信元IPアドレス(CIDR)は、リポジトリにコミットしたくない場合もあります。その場合は、.tfファイルにハードコードするのではなく、入力変数ファイル(.tfvarsファイル)に定義して、terraformコマンドのオプションで指定してください。管理方針については、合わせて[こちら](./parameter_store.md)もご参照ください。

## DNSフォワーダーの設定について

仮想セキュアルームから外部へのアクセスはProxy経由で行うことを想定しています。  
Proxyは[virtual_secure_room_proxy pattern](./virtual_secure_room_directory_service.md)で作成可能です。
当該patternを適用するとECSのコンテナとしてProxyが作成されます。  
ここで作成したProxyはVPC内DNSからのみ参照可能なService Discoveryの名前を指定して利用することを想定しています。  
ただし、本patternで作成する`Microsoft AD`のDNSサーバーはVPC-provided DNSへ自動的にリクエストをフォワードしません。  
このためService Discoveryを利用する際は、`Microsoft AD`のDNSサーバーへDNSフォワーダー設定が必要になります。  
設定方法は以下の通りです。

1. Runtime環境のAD Manager用EC2のEIPを確認し、許可した端末からRDPで接続する  
   この時、接続ユーザーはドメインの``Admin``で接続する。サンプルコードで記載した例だと以下でアクセス可能  
   (ログイン出来ない場合EC2の初期化処理が終わっていない可能性がある。数分おいてからやり直す。)  

      ``` yaml
      user: Admin@TEST or TEST\Admin or TESTをtest.epona.comに変えてもOK
      password: P@ssw0rd1234
      ```

2. ADに設定されているDNSサーバーのIPv4アドレスを確認する
   1. [スタート]-[Windows PowerShell]を起動する
   2. PowerShellで`Get-DnsClientServerAddress`コマンドを実行する  
      :information_source: 優先度順に2つのDNSサーバーのIPアドレスが表示されます
3. DNSマネージャーでDNSサーバーへ接続する
   1. [スタート]-[Windwos管理ツール]-[DNS]を起動する
   2. [次のコンピューター]を選択し、2-2で確認したDNSサーバーのIPv4アドレスを入力して[OK]をクリックする。  
      :information_source: どちらのDNSサーバーへ接続してもよいですが、後述の理由により優先度の高い方に接続することを推奨します
4. 条件付フォワーダーを設定する
   1. 3-2で接続したDNSマネージャーの画面でDNSサーバーのIPアドレスをクリックする
   2. [条件付フォワーダー]をダブルクリックする
   3. サイドメニューに表示された[条件付フォワーダー]を右クリックし、[新規条件付フォワーダー]をクリックする
   4. `DNSドメイン`にProxyの名前を入力する。  
      :information_source: `virtual_secure_room_proxy pattern`適用時に出る`internal_container_endpoint`から`:3128`を除いた値
   5. `マスターサーバーのIPアドレス`にVPC内部DNSのIPアドレスを入力する。  
      :information_source:
      VPCのCIDRブロックの第4オクテットを2にしたアドレス（`10.71.0.0/16`の場合は`10.71.0.2`）となります。  
      :warning: `このIPアドレスのサーバーは、必要なゾーンに対して権限がありません。`というエラーが表示されますが、そのまま設定して問題ありません
   6. [このActive Directory条件付きフォワーダーを保存し、次の方法でレプリケートする]にチェックを入れ、[OK]をクリックする。  
      このオプションを設定することで、もう一方のDNSサーバーにも数分後に条件付フォワーダーの設定が反映されます。  
      :warning: このため、優先度の低いDNSサーバーへ設定した場合は実際に設定が使われるようになるまで数分のタイムラグが発生します

## 各種共有を制限する設定について

仮想セキュアルームからの情報漏えいを防ぐため、仮想セキュアルームとクライアント端末との各種共有を制限できます。
本手順では、クリップボードとファイルのコピー、プリンターによる印刷を制限する手順を説明します。

これらの設定は、ドメインのグループポリシーに設定することでドメイン内のすべてのWorkSpacesに適用できます。

なお、以下手順はAWS公式の[こちら](https://docs.aws.amazon.com/ja_jp/workspaces/latest/adminguide/group_policy.html)の手順をベースにしています。

### ドメインに対するグループポリシーの設定手順

本手順では、ドメインを管理するAD Mangerに対してグループポリシーを設定する手順を説明します。

設定にはWorkSpacesとAD Manager用EC2を使用します。  
WorkSpaces内にあるAWSにより作成されたグループポリシーをテンプレートとしてAD Manager用EC2にコピーします。  
コピーしたテンプレートを使いAD Manager用EC2からドメインのグループポリシーとして設定します。  
設定方法は以下の通りです。

<!-- markdownlint-disable MD013 -->
<!-- textlint-disable -->
1. PCoIP のグループポリシー管理用テンプレートの準備
   1. [virtual_secure_room_workspaces pattern](./virtual_secure_room_workspaces.md)で作成したWorkSpacesに作成したドメインユーザーでログインする
   2. エクスプローラーから`C:\Program Files (x86)\Teradici\PCoIP Agent\configuration`ディレクトリを開く  
      :warning: Cドライブはデフォルトで非表示となっていますが、エクスプローラーのアドレスバーに直接パスを入力することでアクセス可能です
   3. `pcoip.adm`をnotepad等で開き、内容をコピーしてクライアント端末のテキスト等に貼り付けておく
2. PCoIP のグループポリシー管理用テンプレートのインストール
   1. AD Manager用EC2にドメインのAdminでログインする
   2. notepadを開き1-3でクライアント端末に貼り付けておいた`pcoip.adm`の内容を貼り付け任意の場所に`pcoip.adm`という名前で保存する  
      :warning: 拡張子が `txt` にならないように、保存ダイアログでファイル種別を`全てのファイル`に変更して保存してください
   3. [スタート]-[Windows管理ツール]-[グループポリシーの管理]を開く
   4. 左メニューの[フォレスト]-[ドメイン]-[ドメイン名]以下の`OU名`を右クリック、この`このドメインにGPOを作成し、コンテナーにリンクする`を選択
   5. 任意の名前で新しいGPO（グループポリシーオブジェクト）を作成する
   6. 作成したGPOを右クリックし`編集`を選択
   7. [コンピューターの構成]-[ポリシー]-[管理用テンプレート]を開き、メニューバーの`操作`から`テンプレートの追加と削除`を選択する
   8. `追加`を選択し5の手順で作成した`pcoip.adm`を選択し閉じる
<!-- textlint-enable -->
<!-- markdownlint-enable MD013 -->

これで、仮想セキュアルームで利用するドメインに対するグループポリシーの設定は完了です。

以降の手順では、作成したグループポリシーに対して各種共有の制限を設定します。  
グループポリシーに対して設定することで、ドメイン内の全WorkSpacesに対して一括で適用できます。

:warning:
グループポリシーの変更内容をWorkSpacesに反映させるには、**WorkSpacesの再起動が必要**となります。

### クリップボードの共有制限について

本手順では、仮想セキュアルームとクライアント端末間のクリップボード共有制限の設定手順について説明します。

この手順ではクリップボードリダイレクトの設定を通じて、仮想セキュアルームとクライアント端末間のクリップボードを経由したコピーを制限します。  
設定には[ドメインに対するグループポリシーの設定手順](#ドメインに対するグループポリシーの設定手順)で利用したEC2とグループポリシーを利用します。  
設定方法は以下の通りです。

<!-- markdownlint-disable MD013 -->
<!-- textlint-disable -->
1. PCoIP のクリップボードリダイレクトの設定
   1. AD Manager用EC2にドメインのAdminでログインする
   2. [スタート]-[Windows管理ツール]-[グループポリシーの管理]を開く
   3. 左メニューの[フォレスト]-[ドメイン]-[ドメイン名]-[OU名]以下にある2-5で作成したGPOを右クリックし`編集`を選択
   4. [コンピューターの構成]-[ポリシー]-[管理用テンプレート]-[従来の管理用テンプレート(ADM)]-[PCoIP Session Variables]-[Overridable Administrator Defaults]を開く
   5. `Configure clipboard redirection`を選択して`ポリシー設定`をクリック
   6. ラジオボタンで`有効`を選択し`オプション`から`Disabled in both directions(双方で無効)`を選択して`OK`
2. リモートデスクトップセッションによるクリップボードリダイレクトの制限設定
   1. [クリップボードの共有制限について](#クリップボードの共有制限について)の1.3の手順を参照し、グループポリシーエディターを表示する
   2. [コンピューターの構成]-[ポリシー]-[管理用テンプレート]-[Windowsコンポーネント]-[リモートデスクトップサービス]-[リモートデスクトップ セッションホスト]-[デバイスとリソースのリダイレクト]を開く
   3. `クリップボードのリダイレクトを許可しない`を選択して`ポリシー設定`をクリック
   4. ラジオボタンで`有効`を選択して`OK`
<!-- textlint-enable -->
<!-- markdownlint-enable MD013 -->

これで、仮想セキュアルームとクライアント端末とのクリップボード共有制限の設定は完了です。

### ファイルの共有制限について

本手順では、仮想セキュアルームとクライアント端末間のファイル共有制限の設定手順について説明します。

この手順ではドライブリダイレクトの設定を通じて、仮想セキュアルームとクライアント端末間のファイルコピーを制限します。  
設定には[ドメインに対するグループポリシーの設定手順](#ドメインに対するグループポリシーの設定手順)で利用したEC2とグループポリシーを利用します。  
設定方法は以下の通りです。

<!-- markdownlint-disable MD013 -->
<!-- textlint-disable -->
1. リモートデスクトップセッションによるドライブリダイレクトとプリンターリダイレクトの設定
   1. [クリップボードの共有制限について](#クリップボードの共有制限について)の1.3の手順を参照し、グループポリシーエディターを表示する
   2. [コンピューターの構成]-[ポリシー]-[管理用テンプレート]-[Windowsコンポーネント]-[リモートデスクトップサービス]-[リモートデスクトップ セッションホスト]-[デバイスとリソースのリダイレクト]を開く
   3. `ドライブのリダイレクトを許可しない`を選択して`ポリシー設定`をクリック
   4. ラジオボタンで`有効`を選択して`OK`
<!-- textlint-enable -->
<!-- markdownlint-enable MD013 -->

これで、仮想セキュアルームとクライアント端末とのファイル共有制限の設定は完了です。

### プリンターサポートの設定について

本手順では、仮想セキュアルームとクライアント端末間のプリンターサポートの設定手順について説明します。

この手順ではプリンターリダイレクトの設定を通じて、仮想セキュアルームとクライアント端末間のプリンター利用を制限します。  
設定には[ドメインに対するグループポリシーの設定手順](#ドメインに対するグループポリシーの設定手順)で利用したEC2とグループポリシーを利用します。  
設定方法は以下の通りです。

<!-- markdownlint-disable MD013 -->
<!-- textlint-disable -->
1. PCoIP のプリンターサポートの設定
   1. [クリップボードの共有制限について](#クリップボードの共有制限について)の1.3の手順を参照し、グループポリシーエディターを表示する
   2. [コンピューターの構成]-[ポリシー]-[管理用テンプレート]-[従来の管理用テンプレート(ADM)]-[PCoIP Session Variables]-[Overridable Administrator Defaults]を開く
   3. `Configure remote printing`を選択して`ポリシー設定`をクリック
   4. ラジオボタンで`有効`を選択し`オプション`から`printing disabled(クライアント端末への印刷機能を無効化)`を選択して`OK`
2. リモートデスクトップセッションによるプリンターリダイレクトの設定
   1. [クリップボードの共有制限について](#クリップボードの共有制限について)の1.3の手順を参照し、グループポリシーエディターを表示する
   2. [コンピューターの構成]-[ポリシー]-[管理用テンプレート]-[Windowsコンポーネント]-[リモートデスクトップサービス]-[リモートデスクトップ セッションホスト]-[プリンターのリダイレクト]を開く
   3. `クライアントプリンターのリダイレクトを許可しない`を選択して`ポリシー設定`をクリック
   4. ラジオボタンで`有効`を選択して`OK`
<!-- textlint-enable -->
<!-- markdownlint-enable MD013 -->

これで、仮想セキュアルームとクライアント端末とのプリンターサポート設定は完了です。

## サンプルコード

`virtual_secure_room_directory_service pattern`を使用したサンプルコードを、以下に記載します。

<!-- markdownlint-disable MD046 -->
<!--
    リスト内のコードブロックをバッククォート３つのfencedスタイルを使用した場合、mkdocsでの出力結果が上手くいかない。
    このため、リスト内のコードブロックはインデントを利用したindentedスタイルを使用している。
    しかし、その結果コードブロックのスタイルが混在してしまい、markdownlintがエラーになる。
    リスト内でfencedスタイルが使用できないので、この部分のmarkdownlintを無効化している。
-->

```terraform
module "directory_service" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/virtual_secure_room_directory_service?ref=v0.2.6"

  tags = {
    Owner              = "test"
    Environment        = "virtual-secure-room"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }

  directory_service_domain_name     = "test.epona.com"
  directory_service_domain_password = "P@ssw0rd1234"
  directory_service_short_name      = "TEST"
  directory_service_ou              = "OU=TEST,DC=test,DC=epona,DC=com"
  directory_service_vpc_id          = data.terraform_remote_state.network.outputs.network.vpc_id
  directory_service_subnet_ids      = data.terraform_remote_state.network.outputs.network.private_subnets

  ad_manager_name                   = "ad_manager"
  ad_manager_administrator_password = "P@ssw0rd1234"
  ad_manager_subnet_id              = data.terraform_remote_state.network.outputs.network.public_subnets[0]

  ad_manager_role_name             = "admanagerrole"
  ad_manager_instance_profile_name = "admanagerinstanceprofile"

  ad_manager_sg_name      = "ad_manager_sg"
  ad_manager_sg_ingresses = var.ingresses
}

```

<!-- markdownlint-enable MD046 -->

## 関連するpattern

`virtual_secure_room_directory_service pattern`に関連するpatternを、以下に記載します。

| pattern名                                                             | 説明                                            |
|:----------------------------------------------------------------------|:------------------------------------------------|
| [virtual_secure_room_workspaces](./virtual_secure_room_workspaces.md) | WorkSpacesをデプロイするパターン                |
| [virtual_secure_room_proxy](./virtual_secure_room_proxy.md)           | 仮想セキュアルーム用Proxyをデプロイするパターン |

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.37.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| ad\_manager\_administrator\_password | AD管理ツールを導入するEC2インスタンスのAdministratorのパスワード | `string` | n/a | yes |
| ad\_manager\_instance\_profile\_name | AD管理ツールを導入するEC2インスタンスに付与するインスタンスプロファイル名 | `string` | n/a | yes |
| ad\_manager\_name | AD管理ツールを導入するEC2インスタンスのNameタグの値 | `string` | n/a | yes |
| ad\_manager\_role\_name | AD管理ツールを導入するEC2インスタンスに付与するIAMロール名 | `string` | n/a | yes |
| ad\_manager\_sg\_ingresses | AD管理ツールを導入するEC2インスタンスへのアクセス許可。記述例は以下の通り。<pre>rules = [{<br>  port        = 3389<br>  protocol    = "TCP"<br>  cidr_blocks = ["192.0.2.0/24"]<br>  description = "AD Manager inbound SG Rule"<br>}]</pre> | <pre>list(object({<br>    port        = number<br>    protocol    = string<br>    cidr_blocks = list(string)<br>    description = string<br>  }))</pre> | n/a | yes |
| ad\_manager\_sg\_name | AD管理ツールを導入するEC2インスタンスに付与するSGの名前 | `string` | n/a | yes |
| ad\_manager\_subnet\_id | AD管理ツールを導入するEC2インスタンスを配置する、サブネットのID | `string` | n/a | yes |
| directory\_service\_domain\_name | ディレクトリのFQDN。例: corp.example.com | `string` | n/a | yes |
| directory\_service\_domain\_password | Directory Serviceのadminのパスワード | `string` | n/a | yes |
| directory\_service\_ou | ディレクトリ内の組織(OU=<短縮名>,DC=<第2レベルドメイン>,DC=<第1レベルドメイン>) | `string` | n/a | yes |
| directory\_service\_short\_name | ディレクトリの短縮ドメイン名 | `string` | n/a | yes |
| directory\_service\_subnet\_ids | ディレクトリをデプロイするサブネットIDのリスト。異なるAZにあるサブネットIDを指定すること。 | `list(string)` | n/a | yes |
| directory\_service\_vpc\_id | Directory ServiceをデプロイするVPCのID | `string` | n/a | yes |
| ad\_manager\_ami | AD管理ツールを導入するEC2インスタンスのAMI。指定なしの場合、最新のWindows2019Base日本語版のAMIを使用 | `string` | `null` | no |
| ad\_manager\_instance\_type | AD管理ツールを導入するEC2インスタンスのインスタンスタイプ | `string` | `"t2.micro"` | no |
| ad\_manager\_root\_block\_volume\_size | AD管理ツールを導入するEC2インスタンスの、ルートデバイスボリュームのサイズ | `string` | `"30"` | no |
| tags | 仮想セキュアルームに関するリソースに付与するタグ | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| admanager\_login\_password | ドメインのAdminに設定している初期パスワード |
| admanager\_login\_user | ドメインのAdminでAD Managerへログインする際のユーザー名 |
| admanager\_public\_dns | AD Managerのパブリックホスト名 |
| admanager\_public\_ip | AD ManagerのパブリックIPアドレス |
| directory\_id | Diretory ServiceのID |
