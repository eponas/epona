# Epona AWS patternモジュール

- [Epona AWS patternモジュール](#epona-aws-patternモジュール)
  - [patternモジュール一覧](#patternモジュール一覧)
  - [Terraformの実行を支えるリソース](#terraformの実行を支えるリソース)
  - [環境構築手順](#環境構築手順)
  - [インスタンスでのStateの利用](#インスタンスでのstateの利用)
    - [Terraform実行結果の格納先としてのState](#terraform実行結果の格納先としてのstate)
    - [Data SourceとしてのState](#data-sourceとしてのstate)

## patternモジュール一覧

AWS環境で利用可能なpatternモジュールの一覧です。

| pattern名                                                                          | 用途                                                              | 主な適用環境                           |
| --------------------------------------------------------------------------------- | --------------------------------------------------------------- | -------------------------------- |
| [audit](audit.md)                                                                 | 監査のための環境を構築する                                                   | （どの環境でも利用する）                     |
| [network](network.md)                                                             | 基本的なネットワーク環境を構築する                                               | （どの環境でも利用する）                     |
| [encryption_key](encryption_key.md)                                               | 暗号化キーを構築する                                                      | （どの環境でも利用する）                     |
| [ci_pipeline](ci_pipeline.md)                                                     | CIパイプラインを実行する環境を構築する                                            | Delivery環境                       |
| [cd_pipeline_backend_trigger](cd_pipeline_backend_trigger.md)                     | Runtime環境にバックエンドシステムをデプロイするためのトリガとなるイベントを生成する                   | Delivery環境                       |
| [cd_pipeline_backend](cd_pipeline_backend.md)                                     | バックエンドシステムをRuntime環境へデプロイする                                     | Runtime環境                        |
| [cd_pipeline_frontend_trigger](cd_pipeline_frontend_trigger.md)                   | Runtime環境にフロントエンドシステムをデプロイするためのトリガとなるイベントを生成する                  | Delivery環境                       |
| [cd_pipeline_frontend](cd_pipeline_frontend.md)                                   | フロントエンドシステムをRuntime環境へデプロイする                                    | Runtime環境                        |
| [cd_pipeline_lambda_trigger](cd_pipeline_lambda_trigger.md) | Runtime環境にLambda関数をデプロイするためのトリガとなるイベントを生成する | Delivery環境 |
| [cd_pipeline_lambda](cd_pipeline_lambda.md) | Lambda関数をRuntime環境へデプロイする | Runtime環境 |
| [parameter_store](parameter_store.md)                                             | 設定情報および秘匿情報を保存する                                                | Runtime環境                        |
| [cacheable_frontend](cacheable_frontend.md)                                       | CloudFront＋S3によるフロントエンド配信環境を構築する                                | Runtime環境                        |
| [public_traffic_container_service](public_traffic_container_service.md)           | インターネットからのトラフィックを受け付けるような、LB＋コンテナサービスを構築する                      | Runtime環境                        |
| [database](database.md)                                                           | RDBMSを構築する                                                      | Runtime環境                        |
| [redis](redis.md)                                                                 | Redisを構築する                                                      | Runtime環境                        |
| [users](users.md)                                                                 | Delivery環境に役割に応じたIAMロールとIAMユーザーを作成する                            | Delivery環境                       |
| [bind_role](bind_role.md)                                                         | Delivery環境からRuntime環境へスイッチロールするためのIAMポリシーを構築する                  | Delivery環境                       |
| [roles](roles.md)                                                                 | Runtime環境にIAMロールを構築する                                           | Runtime環境                        |
| [webacl](webacl.md)                                                               | インターネットからのトラフィックを制御するためのファイアフォールを構築する                           | Runtime環境                        |
| [vpc_peering](vpc_peering.md)                                                     | 異なるVPCを接続するVPCピアリングを構築する                                     | Runtime環境                        |
| [virtual_secure_room_workspaces](virtual_secure_room_workspaces.md)               | 仮想セキュアルームとなる仮想デスクトップ環境を構築する                                     | Runtime環境                        |
| [virtual_secure_room_directory_service](virtual_secure_room_directory_service.md) | 仮想セキュアルームのユーザー管理のためのディレクトリサービスを構築する                             | Runtime環境                        |
| [threat_detection](threat_detection.md)                                           | AWS環境に対する悪意あるアクティビティや不正な動作の脅威検出とチャットツールへの通知用リソースを構築する           | （どの環境でも利用する）                     |
| [rule_violation](rule_violation.md)                                               | ルールに基づいたAWS環境の構成変更を監視するリソースと、ルール違反となる構成変更をチャットツールへ通知するリソースを構築する | （どの環境でも利用する）                     |
| [datadog_integration](datadog_integration.md)                                     | DatadogとAWSのインテグレーションを行い、基本的なメトリクスの収集とダッシュボードのプリセットをセットアップする    | （どの環境でも利用する）                     |
| [datadog_forwarder](datadog_forwarder.md)                                         | AWSのログ等をDatadogに転送するためのリソースをデプロイする                              | （どの環境でも利用する）                     |
| [datadog_log_trigger](datadog_log_trigger.md)                                     | `datdog_forwarder pattern`でデプロイされたリソースと、各パターンが出力するログリソースの紐づけを行う | （どの環境でも利用する）                     |
| [quicksight_vpc_inbound_source](quicksight_vpc_inbound_source.md)                 | QuickSightがVPC内のAWSリソースに接続するためのリソースを構築する                        | Runtime環境(QuickSightが有効にされている環境) |
| [lambda](lambda.md)                                                               | Lambda関数を構築する                               | Runtime環境 |
| [api_gateway](api_gateway.md)                                                     | API Gatewayを構築する                               | Runtime環境 |
| [step_functions](step_functions.md)                                               | Lambda関数などを連携したワークフロー（ステートマシン）を構築する                          | Runtime環境 |

## Terraformの実行を支えるリソース

Eponaが提供するpatternモジュールを使用するには、Terraformを実行できる環境を構築する必要があります。  
その環境の全体像を、2つの断面で図示します。

ひとつは、Terraformを実行するためのユーザー、権限に関するものです。  
以下の図は、Delivery環境、Runtime環境それぞれに存在するAWS IAMのユーザー、グループ、ロール、ポリシーを示しています。

![Terraform実行用ユーザ・グループ・ロール](../../resources/terraform-accounts.png)

Terraformを実行する際に利用するIAMユーザーは、Delivery環境またはRuntime環境を問わず、**Delivery環境に作成します**。  
**環境を変更する権限が付与されたIAMロールはそれぞれの環境ごとに存在**し、**Terraform実行ユーザーはそのロールを引き受け**、権限を行使します。

もうひとつは、TerraformのState管理に関するものです。  
以下の図は、State管理に使用するAmazon S3バケットおよびAmazon DynamoDBテーブル、その配置先の環境を示しています。

![Backend](../../resources/backend.png)

Stateには、Terraformによって構築された環境の情報が保存されます。Terraformは、Stateの情報と構成ファイルを元に、環境を管理します。

図内のリソースは、わかりやすさのために具体的な名前を使用しています。  
これは、サービス名として`myservice`、Runtime環境名として`staging`、`production`の使用を指しています。  
これらの命名規則の詳細については[Terraform実行環境の構築（詳細版）](./terraform_backend/README.md)を参照してください。

Eponaが提供するpatternモジュールを実行し、環境を管理するために必要なリソースは以下となります。

<!-- markdownlint-disable MD033 -->
<!-- 表の中で箇条書きを使うため、markdownlintを無効化しています -->

| 用途 | カテゴリ | リソース | 作成先環境 | 図内の名称例 |
| ----- | -------- | ------- | ---------- | ------------ |
| TerraformのState保存先 | State管理 | Amazon S3 | <ul><li>Delivery環境</li></ul> | <ul><li>`myservice-delivery-terraform-tfstate`</li><li>`myservice-staging-terraform-tfstate`</li><li>`myservice-production-terraform-tfstate`</li></ul> |
| State更新時のロックの仕組み | State管理 | Amazon DynamoDB | <ul><li>Delivery環境</li><li>Runtime環境</li></ul> | <ul><li>`myservice_terraform_tfstate_lock`</li></ul> |
| Terraformを実行して環境を変更するユーザー | Terraform実行ユーザー | AWS IAMユーザー | <ul><li>Delivery環境</li></ul> | <ul><li>`DeliveryTerraformer`</li><li>`StagingTerraformer`</li><li>`ProductionTerraformer`</li></ul> |
| Terraformを実行するユーザーが所属するグループ| Terraform実行ユーザー | AWS IAMグループ | <ul><li>Delivery環境</li></ul> | <ul><li>`TerraformGroup`</li></ul> |
| State管理を行うリソースへアクセスする権限 | Stateアクセスロール | AWS IAMロール | <ul><li>Delivery環境</li></ul> | <ul><li>`DeliveryTerraformBackendAccessRole`</li><li>`StagingTerraformBackendAccessRole`</li><li>`ProductionTerraformBackendAccessRole`</li></ul> |
| 各環境を変更できる権限（`AdministratorAccess`権限） | Terraform実行ロール | AWS IAMロール | <ul><li>Delivery環境</li><li>Runtime環境</li></ul> | <ul><li>`TerraformExecutionRole`</li></ul> |

<!-- markdownlint-enable MD033 -->

:information_source: IAMロールに付与されているポリシーについては上記表から省略しています。

Terraformを実行するIAMユーザーはDelivery環境に集約され、環境の変更時には各環境のIAMロールを引き受けて行使します。

**patternモジュールを適用時に使用するアクセスキーは、このTerraform実行ユーザーのものを使用します。**

また、環境ごとのStateを管理するAmazon S3バケットは、Delivery環境に集約しています。

ここまで解説してきたTerraform実行ユーザーやState管理に関する内容もまた、AWS上に構築されたリソースです。  
Eponaでは、これらのリソースもTerraformで構築します。

<!-- markdownlint-disable MD033 -->
<!-- 表の中で箇条書きを使うため、markdownlintを無効化しています -->

| 用途 | カテゴリ | リソース | 作成先環境 | 図内の名称例 |
| ---- | -------- | -------- | ---------- | ------------ |
| TerraformのState保存先 | State管理 | Amazon S3 | <ul><li>Delivery環境</li><li>Runtime環境</li></ul> | <ul><li>`myservice-delivery-backend`</li><li>`myservice-staging-backend`</li><li>`myservice-production-backend`</li></ul> |

<!-- markdownlint-enable MD033 -->

State管理が2つの表にそれぞれ登場していますが、以下のように管理しているものが異なることに注意してください。

- patternモジュールを適用し、構築されたリソースを管理するためのState
- Terraform実行に必要なState管理、ユーザー、権限に関するリソースを構築、管理するためのState

## 環境構築手順

Eponaを使用するためには、[Terraformの実行を支えるリソース](#terraformの実行を支えるリソース)を作成する必要があります。

これは、[Epona AWS Starter CLI](https://gitlab.com/eponas/epona-aws-starter-cli)を使用することで簡単に作成できます。

:information_source: Epona AWS Starter CLIが行う環境構築の詳細について知りたい場合は、[Terraform実行環境の構築（詳細版）](./terraform_backend/README.md)を参照してください。

Epona AWS Starter CLIを利用するには、以下のAWSの環境準備およびソフトウェアのインストールが必要です。

- [AWS](https://aws.amazon.com/jp/)
  - 最低でも2つのAWSアカウント（Delivery環境 × 1、Runtime環境 × 1）が必要です
    - それぞれに、`AdministratorAccess`権限を持ったIAMユーザーを用意してください
    - ※ 使用するリージョンは、`init`コマンドが生成する設定ファイルを変更することで選択可能です
- ソフトウェア
  - AWS CLI v2
  - [Terraform 0.14.10](https://releases.hashicorp.com/terraform/0.14.10/)

この前提条件を満たしたうえで、以下の手順に沿って環境を構築してください。

1. [Releases](https://gitlab.com/eponas/epona-aws-starter-cli/-/releases)ページより、利用しているOSに応じたバイナリをダウンロード
1. Epona AWS Starter CLIの`config-gen`コマンドの実行
1. `config-gen`コマンドが出力する設定ファイルを書き換え、Epona AWS Starter CLIの`init`コマンドを実行

以下が、Epona AWS Starter CLIを使用した`config-gen`および`init`コマンドの実行例です。

```bash
## config-genコマンドの実行
### サービス名を my-service 、Runtime環境をstaging、productionの2つ利用する例です
$ ./epona-aws-starter-cli config-gen -service-name my-service -runtime-name staging -runtime-name production


## config-genコマンドの実行後、出力されるファイルを修正してください
## initコマンドの実行
$ ./epona-aws-starter-cli init
```

`config-gen`コマンドの実行後には、`init`コマンドに必要な設定ファイルが出力されます。  
出力された設定ファイル内に、`AdministratorAccess`権限を持ったIAMユーザーのアクセスキーを指定してください。  
記載する箇所は、設定ファイル内のコメントに記載してあります。

`init`コマンドが正常に終了すれば、作成されたTerraform実行ユーザーのアクセスキーが得られます。

:warning: Terraform実行ユーザーには非常に強力な権限を割り当てることになります。
アクセスキーの管理には十二分に注意を払ってください。
当該のアクセスキーは、誰が使ったのかを記録しておくとともに、不要になったらすぐに削除するのが望ましいでしょう。

Epona AWS Starter CLIの`init`コマンド実行後に表示される、アクセスキーのイメージを以下に記載します。  
※`init`コマンドが生成する設定ファイルにも、アクセスキーの情報が記録されます。

```bash
2021/03/18 15:15:38 [INFO] 作成したTerraformerのアクセスキーを表示します

Delivery環境用Terraform実行ユーザーのアクセスキー情報:
  ユーザー名： DeliveryTerraformer
  アクセスキーID： [アクセスキーID]
  シークレットアクセスキー： [シークレットアクセスキー]

Runtime / staging環境用Terraform実行ユーザーのアクセスキー情報:
  ユーザー名： StagingTerraformer
  アクセスキーID： [アクセスキーID]
  シークレットアクセスキー： [シークレットアクセスキー]

```

アクセスキー取得後は、以下のように環境変数を設定してください。  
アクセスキーID、シークレットアクセスキーは、変更したい環境のTerraform実行ユーザーのものを指定します。  
これで、Terraform実行ユーザーの持つ権限で`terraform plan`や`terraform apply`等の、Terraformのコマンドを実行できるようになります。

| 環境変数名              | 設定値                                   |
| ----------------------- | ---------------------------------------- |
| `AWS_ACCESS_KEY_ID`     | アクセスキーID                           |
| `AWS_SECRET_ACCESS_KEY` | シークレットアクセスキー                 |
| `AWS_DEFAULT_REGION`    | 利用するリージョン (`ap-northeast-1` 等) |

あとは、[Terraform](https://www.terraform.io/docs/configuration/terraform.html)および[S3のバックエンド](https://www.terraform.io/docs/backends/types/s3.html)に必要なロールを設定することで、各環境に対するTerraformでのリソース管理が可能になります。

実際に[`network` pattern](./network.md)を使ったときの
バックエンドとAWS Providerの設定例を示します。

```shell
terraform {
  required_version = "[利用するTerraformのバージョン]"

  required_providers {
    aws = "[利用するAWS providerのバージョン]"
  }

  backend "s3" {
    bucket         = "myservice-staging-terraform-tfstate" # Staging環境のstate管理用バケットを指定
    key            = "network/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "myservice_terraform_tfstate_lock"

    # Staging環境のバックエンドにアクセスするためのロールを設定
    role_arn = "arn:aws:iam::[Delivery環境のアカウントID]:role/StagingTerraformBackendAccessRole"
  }
}

provider "aws" {
  assume_role {
    # Staging環境のTerraform実行権限を定義したロールを設定
    role_arn = "arn:aws:iam::[Staging環境のアカウントID]:role/TerraformExecutionRole"
  }
}
```

## インスタンスでのStateの利用

環境構築で作成したS3バケットや、DynamoDBは、State管理の仕組みとして各インスタンスで利用します。

### Terraform実行結果の格納先としてのState

各インスタンスに対するTerraformのStateは、以下のように命名規則で設定します。

```go
terraform {
  backend "s3" {
    bucket          = "[バケット名]"
    key             = "[インスタンス名]/terraform.tfstate"
    encrypt         = true
    dynamodb_table  = "[任意の名称]_terraform_tfstate_lock"

    # バックエンドアクセス用ロール
    role_arn = "arn:aws:iam::[Delivery環境のアカウントID]:role/[対象環境名]TerraformBackendAccessRole"
  }
}
```

どの環境に対するStateなのかはバケットで、どのインスタンスのStateなのかはS3保存時のキーで区別します。

以下は、Delivery環境に適用する、`network`インスタンスでのState設定の例です。

```go
terraform {
  backend "s3" {
    bucket          = "myservice-delivery-terraform-tfstate"
    key             = "network/terraform.tfstate"
    encrypt         = true
    dynamodb_table  = "myservice_terraform_tfstate_lock"

    # バックエンドアクセス用ロール
    role_arn = "arn:aws:iam::[Delivery環境のアカウントID]:role/DeliveryTerraformBackendAccessRole"
  }
}
```

### Data SourceとしてのState

ここではData SourceとしてのStateの扱いについて、具体的なパターン名を伴いつつ記載します。

[Epona TerraformのState管理](../README.md#Data SourceとしてのState)

あるインスタンスから、別のインスタンスの実行結果を利用したい場合は、以下のようなStateをData Sourceとして利用するよう定義します。

```go
data "terraform_remote_state" "[環境名]_[データを参照したいインスタンス名]" {
  backend = "s3"

  config = {
    bucket         = "[バケット名]"
    key            = "[データを参照したいインスタンス名]/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "[任意の名称]_terraform_tfstate_lock"

    # バックエンドアクセス用ロール
    role_arn = "arn:aws:iam::[Delivery環境のアカウントID]:role/DeliveryTerraformBackendAccessRole"
  }
}
```

Data SourceとしてStateを利用する場合の例を記載します。Runtime環境（production）における、`redis`インスタンスが`network`インスタンスのStateを参照したい場合は、以下のような記述になります。

```go
data "terraform_remote_state" "production_network" {
  backend = "s3"

  config = {
    bucket         = "myservice-production-terraform-tfstate"
    key            = "network/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "myservice_terraform_tfstate_lock"

    # バックエンドアクセス用ロール
    role_arn = "arn:aws:iam::[Production環境のアカウントID]:role/RuntimeTerraformBackendAccessRole"
  }
}

module "redis" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/redis?ref=[バージョン]"
  ...

  # networkインスタンスのプライベートサブネットIDを参照
  subnets = data.terraform_remote_state.production_network.outputs.network.private_subnets

  ...
}
```

このように、依存する環境名とインスタンス名をData Sourceの名前として利用することで、他の環境や他のインスタンスへの依存が明快になります。

```go
data "terraform_remote_state" "production_network" {
  ...
}

module "redis" {
  # networkインスタンスへの依存を表現
  subnets = data.terraform_remote_state.production_network.outputs.network.private_subnets
  ...
}
```

他のインスタンスへの依存をわかりやすくするため、Data SourceとしてのStateの定義は独立した定義ファイルにしておくとよいでしょう。

:information_source: [Getting Started](https://gitlab.com/eponas/epona_aws_getting_started)では、`instance_dependencies.tf`というファイル名でまとめています。

また、このようにState経由でインスタンスの実行結果を利用するため、Outputの定義も必要になります。

```go
output "[インスタンス名]" {
  value = module.[インスタンス名]
}
```

たとえば、`network`インスタンスのOutputを保存するには、以下のように定義します。

```go
output "network" {
  value = module.network
}
```

この他、各インスタンス内で独自にリソース定義などを行った場合は、必要に応じてOutputを定義するようにしてください。

:information_source: 秘匿情報を含むモジュール、リソースを使用している場合は、`sensitive`の指定を行い、コンソールに秘匿情報が出力されないようにしましょう。
