# network pattern

## 概要

`network pattern`モジュールでは、AWS環境内で基本となるネットワーク環境（Amazon VPCやサブネット等）を構築します。

## 想定する適用対象環境

`network pattern`は、Delivery環境およびRuntime環境のいずれでも使用されることを想定しています。

## 依存するpattern

`network pattern`は、他のpatternの実行結果に依存しません。独立して実行できます。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                     | 説明                                                                                                       |
| :------------------------------------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------------------------------- |
| [Amazon VPC](https://aws.amazon.com/jp/vpc/)                                                                   | 他のAmazon VPC内で動作するリソースの基盤となる、独立した仮想ネットワークを構築します                       |
| [サブネット](https://docs.aws.amazon.com/ja_jp/vpc/latest/userguide/VPC_Subnets.html)                          | パブリックサブネットおよびプライベートサブネットを構築します                                               |
| [インターネットゲートウェイ](https://docs.aws.amazon.com/ja_jp/vpc/latest/userguide/VPC_Internet_Gateway.html) | インターネットとの通信を可能にするコンポーネントで、パブリックサブネットのルートテーブルに結び付けられます |
| [NAT ゲートウェイ](https://docs.aws.amazon.com/ja_jp/vpc/latest/userguide/vpc-nat-gateway.html)                | プライベートサブネットにあるリソースから、インターネットへの通信が可能になるコンポーネント                 |
| [Elastic IP アドレス](https://docs.aws.amazon.com/ja_jp/vpc/latest/userguide/vpc-eips.html)                    | 静的なパブリックIPアドレス。`network pattern`では、NAT ゲートウェイに結び付けられます                      |
| [ルートテーブル](https://docs.aws.amazon.com/ja_jp/vpc/latest/userguide/VPC_Route_Tables.html)                 | パブリックサブネット、プライベートサブネットそれぞれに作成され、ネットワーク経路を設定します               |
| [VPCエンドポイント](https://docs.aws.amazon.com/ja_jp/vpc/latest/userguide/vpc-endpoints.html)                 | 特定のサービス（ECR、S3、CloudWatch Logs）への通信経路をインターネットを経由しないAWSに閉じた経路にします               |

## モジュールの理解に向けて

Amazon VPCおよび、その関連するリソースを用いることで、AWS環境内に独自のネットワーク環境を構築できます。

`network pattern`ではサブネットの基本に従い、Amazon VPCおよび関連リソースを構築します。

:information_source: [VPC とサブネットの基本](https://docs.aws.amazon.com/ja_jp/vpc/latest/userguide/VPC_Subnets.html#vpc-subnet-basics)

`network pattern`ではリージョンおよびアベイラビリティゾーンを指定して、Amazon VPC、サブネットを作成します。  
サブネットには、パブリックサブネット、プライベートサブネットの2種類があります。  
これが、`network pattern`で構築されるリソースの基本的なセットになります。

各サブネットは、アベイラビリティゾーン内に含まれます。

![VPCとAZ](./../../resources/network_az.png)

`network pattern`では、アベイラビリティゾーンの指定とサブネットの指定はリストのインデックスを合わせて指定する必要があります。

たとえば、以下のコードを例に挙げます。

```terraform
  availability_zones = ["ap-northeast-1a", "ap-northeast-1c"]

  public_subnets             = ["10.51.1.0/24", "10.51.2.0/24"]
  private_subnets            = ["10.51.3.0/24", "10.51.4.0/24"]
```

この例では、アベイラビリティゾーンと含まれるサブネットの関係は以下のようになります。

* アベイラビリティゾーン`ap-northeast-1a`内に含まれるサブネット
  * パブリックサブネット`10.51.1.0/24`、プライベートサブネット`10.51.3.0/24`
* アベイラビリティゾーン`ap-northeast-1c`内に含まれるサブネット
  * パブリックサブネット`10.51.2.0/24`、プライベートサブネット`10.51.4.0/24`

ここで、パブリックサブネットおよびプライベートサブネットの定義は、以下のようになります。

* パブリックサブネット … トラフィックがインターネットゲートウェイにルーティングされるサブネット
* プライベートサブネット … インターネットゲートウェイへのルートがないサブネット

![パブリックサブネットおよびプライベートサブネット](./../../resources/network_subnets.png)

パブリックサブネットには、インターネットからのトラフィックを直接受けるリソースが配置されます。主たる例は、ロードバランサーです。

プライベートサブネットには、インターネットからのトラフィックを直接受けないリソースが配置されます。主にコンテナオーケストレーションサービスや、データベースなどが該当します。

サービスを構築する際に、Amazon VPC内に配置するリソースのうち、インターネットからのトラフィックを直接受ける必要があるものは非常に限られます。このため、リソースを作成する際には、まずプライベートサブネットに配置することを検討してください。

プライベートサブネットに配置されるリソースからのインターネットに向けたアウトバウンドに関しては、NAT ゲートウェイを使用して通信できます。外部のAPIへのアクセスやコンテナイメージのダウンロードなどを行うリソースについては、必要な範囲のアウトバウンドを許可するようにセキュリティグループを設定してください。

NAT ゲートウェイの配置先は、パブリックサブネットと同じ値を指定します。

```terraform
  public_subnets             = ["10.51.1.0/24", "10.51.2.0/24"]
  ...
  nat_gateway_deploy_subnets = ["10.51.1.0/24", "10.51.2.0/24"]
```

AWSのデフォルトではECRやS3などのAWSサービスはVPC内からのアクセスでもインターネットを経由した接続となります。
これはセキュリティおよびコストの観点で良くありません。
`network pattern`では以下サービスへのアクセスをAWS内に閉じた通信となるようにVPCエンドポイントを作成します。

* ECR
* S3
* CloudWatch Logs

![プライベートリンク](./../../resources/network_private_link.png)

異なるVPC間を接続するVPCピアリングを行う場合、[vpc_peering pattern](./vpc_peering.md)を使用します。
たとえば、サービス用のVPCと仮想セキュアルーム用のVPCを分ける場合にVPCピアリングを行います。
VPCピアリングを作成した後、各VPC内で接続先VPCへのルーティングを設定する必要があります。
`network pattern`ではこの接続先VPCへのルーティングも設定できます。
このルーティング設定はピア接続する相互のVPCに行ってください。

なお、この設定はvpc_peering pattern実行後でないとできないため、network patternを再applyして設定します。

## サンプルコード

`network pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "network" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/network?ref=v0.2.6"

  name = "epona-runtime-production-network"

  cidr_block = "10.51.0.0/16"

  availability_zones = ["ap-northeast-1a", "ap-northeast-1c"]

  public_subnets             = ["10.51.1.0/24", "10.51.2.0/24"]
  private_subnets            = ["10.51.3.0/24", "10.51.4.0/24"]
  nat_gateway_deploy_subnets = ["10.51.1.0/24", "10.51.2.0/24"]

  tags = {
    # 任意のタグ
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }
}
```

## 関連するpattern

`network pattern`は、Amazon VPC内に構築される、ほぼすべてのリソースの基盤となります。

関連するpatternは多岐に渡るため、`network pattern`のガイドとしては関連するpatternの紹介は行いません。

その他のpattern自身のガイドから、`network pattern`に依存するかどうかを確認してください。

## ログの集約

`network pattern`では、VPCフローログをAmazon CloudWatch Logs、Amazon S3のいずれかに出力します。

:information_source: デフォルトではAmazon CloudWatch Logsに出力するように構成されています。

このログは、[datadog_log_trigger pattern](datadog_log_trigger.md)を使用することでDatadogに集約できます。

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.14.10 |
| aws | >= 3.37.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| availability\_zones | パブリックサブネット、プライベートサブネットを配置するAZ | `list(string)` | n/a | yes |
| cidr\_block | VPCに割り当てるCIDRブロック | `string` | n/a | yes |
| name | 作成するVPC、ネットワークリソースに関する名前 | `string` | n/a | yes |
| nat\_gateway\_deploy\_subnets | NAT ゲートウェイを配置するサブネットの、CIDRブロックのリスト。パブリックサブネットのCIDRブロックを指定する | `list(string)` | n/a | yes |
| private\_subnets | プライベートサブネットに割り当てる、CIDRブロックのリスト | `list(string)` | n/a | yes |
| public\_subnets | パブリックサブネットに割り当てる、CIDRブロックのリスト | `list(string)` | n/a | yes |
| api\_gateway\_endpoint\_enabled | API Gateway用のVPCエンドポイントを作成する場合、trueに設定する | `bool` | `false` | no |
| flow\_log\_cloudwatch\_log\_group | VPCフローログの出力先をCloudWatch Logsにする場合のロググループ名。指定しない場合は、本モジュールで自動的に決定する | `string` | `null` | no |
| flow\_log\_cloudwatch\_log\_kms\_key\_id | VPCフローログの出力先をCloudWatch Logsにする場合、CloudWatch Logsを暗号化するためのKMS CMKのARN | `string` | `null` | no |
| flow\_log\_cloudwatch\_log\_retention\_in\_days | VPCフローログをCloudWatch Logsに出力する場合の保持期間を設定する。<br><br>値は、次の範囲の値から選ぶこと： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.<br>[Resource: aws\_cloudwatch\_log\_group / retention\_in\_days](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group#retention_in_days) | `number` | `null` | no |
| flow\_log\_destination\_arn | VPCフローログの出力先となる、S3またはCloudWatch LogsのARNを指定する。指定しない場合は、本モジュールで自動的に出力先を作成する | `string` | `null` | no |
| flow\_log\_destination\_type | VPCフローログの出力先を、s3またはcloud-watch-logsで指定する | `string` | `"cloud-watch-logs"` | no |
| flow\_log\_enabled | VPCフローログを出力する場合、trueに設定する | `bool` | `true` | no |
| flow\_log\_iam\_role\_arn | VPCフローログに付与するIAMロールのARN | `string` | `null` | no |
| flow\_log\_s3\_bucket | VPCフローログの出力先をS3にする場合のバケット名。指定しない場合は、本モジュールで自動的に決定する | `string` | `null` | no |
| flow\_log\_traffic\_type | VPCフローログに記録するトラフィックの種類を、ACCEPT、REJECT、ALLのいずれかから指定する | `string` | `"ALL"` | no |
| peering\_cidr | VPCピアリングする対象VPCのCIDR（peering\_idを指定する場合は設定必須） | `string` | `null` | no |
| peering\_id | VPCピアリングのID | `string` | `null` | no |
| tags | 作成するVPC、ネットワークリソースに共通的に付与するタグ | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| flow\_log\_bucket | フローログの出力先となるS3バケット名 |
| flow\_log\_bucket\_id | フローログの出力先となるS3バケットID |
| flow\_log\_cloudwatch\_log\_group\_name | フローログの出力先となるCloudWatch Logsロググループ名 |
| private\_link | n/a |
| private\_subnets | n/a |
| public\_subnets | n/a |
| vpc\_id | n/a |
