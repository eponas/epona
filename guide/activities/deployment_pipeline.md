# デプロイメントパイプライン

デプロイメントパイプラインとはなんでしょうか。
[継続的デリバリー](https://asciidwango.jp/post/163632160215/%E7%B6%99%E7%B6%9A%E7%9A%84%E3%83%87%E3%83%AA%E3%83%90%E3%83%AA%E3%83%BC)では、
デプロイメントパイプラインを「ソフトウェアをバージョン管理から取り出してユーザーの手に渡すまでのプロセスを
自動化して表現したもの」と述べています。
そしてその中には、ソフトウェアのビルドやテスト、デプロイといった様々なステージが含まれます。

[DevOpsの原則の1つはあらゆる場所でのフィードバック](https://gitlab.com/eponas/epona/#why-devops)であり、これは開発者の記述したコードに対しても当てはまります。
デプロイメントパイプラインにおいては、[VCS](./vcs_provider.md)にチェックインした変更はそのパイプラインを通過する中で様々な適合性をチェックされます。
そのチェックをパスする度に、そのクオリティは本番環境に近づき、不適切なビルドを本番にリリースしてしまうことを避けられるようになります。また、そこでエラーが発生すれば、開発者は素早くフィードバックを受け取れます。

デプロイメントパイプラインは、必ずしも自動化されているとは限りません。
いくつかのテストなどは手動で構成されていることもあるでしょう。
しかし、素早いフィードバックのためにはこのプロセスをできる限り自動化していくことが重要です。
プロセスが自動化されていれば、素早く実行できるとともに、反復も可能になります。
手作業で集中力を要しエラーも発生しやすいステップを取り除くこともできます。
それにより、プロセスは手動実行に比べ遥かに簡単になり、迅速かつ安全にユーザーに価値を届けられます。
デプロイ頻度と組織のパフォーマンスに相関関係があることは、State of DevOps[^1]でも示されています。

[^1]: [2019 Accelerate State of DevOps Report](https://cloud.google.com/devops/state-of-devops/)

デプロイメントパイプラインにおけるプロセスが自動化されておらず、本番環境に反映されるまでに手間と時間がかかる場合、デプロイは一大イベントになります。
デプロイが大変だと、極力デプロイの数を減らし、1度のデプロイで多くの変更を入れ込もうとするでしょう。
こうなると、ユーザーの手に渡るまでに時間がかかるようになり、フィードバックを得る機会が減少していきます。
自動化できる部分はできる限り自動化し、デプロイを何ら特別でない「普通」のイベントにしていきましょう。

## 適用pattern・How To

本アクティビティを実現するためにEponaが用意しているpatternは以下の通りです。

- [`ci_pipeline`](../patterns/aws/ci_pipeline.md)
- [`cd_pipeline_backend_trigger`](../patterns/aws/cd_pipeline_backend_trigger.md)
- [`cd_pipeline_backend`](../patterns/aws/cd_pipeline_backend.md)
- [`cd_pipeline_frontend_trigger`](../patterns/aws/cd_pipeline_frontend_trigger.md)
- [`cd_pipeline_frontend`](../patterns/aws/cd_pipeline_frontend.md)
- [`cd_pipeline_lambda_trigger`](../patterns/aws/cd_pipeline_lambda_trigger.md)
- [`cd_pipeline_lambda`](../patterns/aws/cd_pipeline_lambda.md)
- [`parameter_store`](../patterns/aws/parameter_store.md)

また、本アクティビティを支えるHow Toドキュメントは以下の通りです。

- [GitLab CI/CDによるアプリケーションのビルド・デプロイガイド](../how_to/gitlab_aplication_build_deploy.md)

## アプリケーション

### パイプライン上でコードに対して静的解析チェックができる

静的解析では、ソースコードや設定ファイルを解析し、特定のコーディングパターンに違反している箇所をチェックします。
これにより開発の極めて早い段階でエラーを検出できるとともに、優れたコーディングスタイルを推進でき、可読性や保守性も向上します。
また、機械的に指摘できるエラーを摘み取ることもできるため、ピアレビューではより高度な問題にも集中できるようになります。
たとえばJavaでは、[SpotBugs](https://spotbugs.github.io/)や[Checkstyle](https://checkstyle.sourceforge.io/)が静的解析ツールの代表例でしょう。

Eponaでは、[GitLab CI/CD](https://docs.gitlab.com/ee/ci/README.html)で定義するCIパイプラインにこれらのツールを組み込むことで、パイプライン上で容易に静的解析のステップを構成できます。

具体例としては、Eponaの[Getting Started](https://gitlab.com/eponas/epona_aws_getting_started)でも使用している
[example-chat](https://github.com/Fintan-contents/example-chat)での適用例をご参照ください。例えば以下が参考になります。

- [GitLab CI/CDでの静的解析を含むステップの定義](https://github.com/Fintan-contents/example-chat/blob/42b3cf36d06104f98ad86ca698bae6873d922662/.gitlab-ci.yml#L17-L25)
- [Mavenを使ったtestライフサイクルの実行](https://github.com/Fintan-contents/example-chat/blob/42b3cf36d06104f98ad86ca698bae6873d922662/.ci/test-backend.sh#L6-L9)
- [pom.xmlへのSpotBugsの組み込み](https://github.com/Fintan-contents/example-chat/blob/42b3cf36d06104f98ad86ca698bae6873d922662/backend/pom.xml#L402-L418)

### パイプライン上でユニットテストを自動実行できる

アプリケーションのコードが期待通りに振舞うことを検証する基本となるのがユニットテストです。
DevOpsの原則の1つであり、継続的インテグレーションがもたらす核でもある「素早いフィードバック」は、ユニットテストのカバレッジが
十分にないと可能になりません。
DevOpsを行う上では、素早く実行でき、カバレッジの高いユニットテストを記述していきましょう。

Eponaでは、静的解析と同様にGitLab CI/CDを用いることでパイプラインにユニットテストを組み込むことができます。
GitLabの[Unit test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html)に、各言語の組み込み方がガイドされていますので参考にしてください。

具体的なサンプルについては、上述の「パイプライン上でコードに対して静的解析チェックができる」をご参照ください。

### パイプライン上でセキュリティ脆弱性を検知できる

パイプライン上で検出できるのは、アプリケーションの不具合だけではありません。セキュリティの脆弱性についても検出が可能です。
セキュリティをDevOpsに組み込むDevSecOpsでは、より早期の段階からセキュリティを考慮します。
セキュリティチェック(脆弱性の検出)をデプロイメントパイプラインに組み込むことで、効率的よく堅牢サービスを
構築できるようになるでしょう。

自動的に脆弱性を検出するツールにはいくつかの種類があります。

- Static Application Security Testing (SAST)
- Interactive Application Security Tools (IAST)
- Dynamic Application Security Testing (DAST)

このうち、最も容易にデプロイメントパイプラインに組み込むことができるのはSASTでしょう。SASTは上述の静的解析によって脆弱性を検出するツールです。
どのようなプロダクトがあるかは、例えば以下のリストをご参照ください。

- OWASPの[Souce Code Analysis Tools](https://owasp.org/www-community/Source_Code_Analysis_Tools)
- GitLabの[Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)

サービスに合致するSAST Toolsをデプロイメントパイプラインに組み込むことで、安心して使えるサービスを実現しましょう。

### パイプラインをクリアしないコードをマージできないようにできる

継続的インテグレーションにとっての最大の過ちは、壊れているコードをチェックインしてしまうことです。
パイプラインをパスしないコードについては、マージさせてはならず、開発者は直ちに修正しなければなりません。
これは、この状態で変更をマージしてしまうと以下のような問題が発生するからです。

- 原因を特定しコードを修正するのに時間がかかるようになる
- コードが壊れている状態にチームが慣れてしまい、常に壊れたままの状態に陥ってしまう

GitLabでは、パイプラインを通過していないコードをマージさせないようにできます。
設定方法については[Only allow merge requests to be merged if the pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html#only-allow-merge-requests-to-be-merged-if-the-pipeline-succeeds)をご参照ください。

### 秘匿すべき情報を開発者から隠すことができる

サービスには開発者に対しても秘匿しなければならないような情報があり得ます。
例えば個人情報を扱うサービスにおける本番環境のデータベースのパスワードをチーム内に展開することは情報漏洩リスクを跳ね上げます。
このような機微な情報は、特別な権限を持たないユーザーが参照できるべきではありません。

Eponaでは[`parameter_store pattern`](../patterns/aws/parameter_store.md)にて、秘匿情報を含む設定情報を管理できます。
このpatternで管理する設定情報は暗号化されており、[`users pattern`](../patterns/aws/users.md)や
[`roles pattern`](../patterns/aws/roles.md)で定義されるAdministrator Role以外のRoleからは復号できません。
公開したくない情報は`parameter_store pattern`で管理するようにしましょう。

### アプリケーションをコンテナイメージにビルドし、保存できる

開発を進める上で、OSや環境によってプログラムが動作しない状況はしばしば発生します。
デプロイ先が開発者の端末やテスト環境、本番環境かによってアプリケーションの調整が必要な状態では、サービスチームは常に環境を意識し続けなければなりません。
このような問題を解決するためには、インフラレイヤを抽象化するとともに、アプリケーションを動作させる環境の作成と廃棄を容易に行える仕組みが必要です。その鍵になるのがコンテナです。

コンテナにより、アプリケーションと実行環境がパッケージ化されます。
このコンテナをクラウド上のマネージドサービスに引き渡せば、"コンテナを単に配備すればアプリケーションが動作する"、
"不要になれば廃棄する"といったことが可能になります。
つまり、ハードウェア資源を意識せず、まさに「雲」の上で開発・運用・廃棄などを迅速に行えるようになります。

このためEponaでも、アプリケーションはコンテナで動作させることを前提としています。

アプリケーションをコンテナ化するためには、アプリケーション自体をコンテナに対応できるように設計する必要があります。
また、そうして設計・実装したアプリケーションをコンテナイメージへとビルドする必要があります。

----

:information_source: コンテナへ対応するためのアプリケーション設計については[THE TWELVE-FACTOR APP](https://12factor.net/ja/)等をご参照ください。

----

Eponaでは、[GitLab CI/CD](https://docs.gitlab.com/ee/ci/)でコンテナイメージのビルド、およびコンテナレジストリへ保存することを想定しています。
これらの利用については[`ci_pipeline pattern`](../patterns/aws/ci_pipeline.md)をご参照ください。
これにより、ソースコードへの変更がリポジトリにPUSHされる都度コンテナイメージを再構築し、レジストリへ最新のイメージを保存できます[^2]。

[^2]: 具体的な実装については、[example-chat](https://github.com/Fintan-contents/example-chat)をご参照ください。

さらに、[`cd_pipeline_backend_trigger`](../patterns/aws/cd_pipeline_backend_trigger.md)、[`cd_pipeline_backend`](../patterns/aws/cd_pipeline_backend.md)を組み合わせることで、コンテナイメージのデプロイまでをシームレスに実現できます。

### アプリケーションを静的コンテンツにビルドし、保存できる

Single Page Application (SPA)のようなフロントエンドのアプリケーションも、上述のコンテナと同様に、実行可能な形式へと「ビルド」することが必要です。
もちろんこのような静的コンテンツのビルドについても、[GitLab CI/CD](https://docs.gitlab.com/ee/ci/)のパイプラインにて実現が可能です。

具体的な組み込みの例は [GitLab CI/CDへの静的コンテンツビルドの組み込み例](https://github.com/Fintan-contents/example-chat/blob/42b3cf36d06104f98ad86ca698bae6873d922662/.gitlab-ci.yml#L96-L108)をご参照ください。

### サーバーレスアプリケーションをビルドし、保存できる

`AWS Lambda`などのサーバーレスアプリケーションのデプロイメントパイプラインも、Eponaはサポートしています。

サーバーレスアプリケーションではインフラや実行環境を意識する必要はありませんが、アプリケーションコードと依存するライブラリを必要な形式に「ビルド」してアップロードする必要があります。

このようなサーバーレスアプリケーションのビルドについても、[GitLab CI/CD](https://docs.gitlab.com/ee/ci/)のパイプラインにて実現が可能です。

[`cd_pipeline_lambda_trigger`](../patterns/aws/cd_pipeline_lambda_trigger.md)、 [`cd_pipeline_lambda`](../patterns/aws/cd_pipeline_lambda.md)を組み合わせることで、サーバーレスアプリケーションのデプロイまでをシームレスに実現できます。

### アプリケーションコンテナを無停止でデプロイできる

DevOpsのパフォーマンスを測る指標の1つはデプロイ頻度[^1]です。
そして、このデプロイ頻度を高める上での障壁の1つは、デプロイの度にサービス停止（メンテナンス時間）が必要になってしまうことです。

Eponaでは、アプリケーションコンテナをデプロイするにあたり[`cd_pipeline_backend pattern`](../patterns/aws/cd_pipeline_backend.md)の利用を
想定しています。
そして、本patternではデプロイ戦略として[Blue-Green Deployment](https://martinfowler.com/bliki/BlueGreenDeployment.html)をサポートしています。

Blue-Green Deploymentでは、デプロイ時にそれまでのアプリケーション稼働環境(Blue)とは別に新稼働環境(Green)を構成します。
この段階ではロードバランサーのルーティングはBlueに向いているため、ユーザーはBlueを使い続けることになります。
そしてGreen環境上でアプリケーションが稼働し始めてからロードバランサーのルーティングをGreenに切り替えることで、無停止[^3]のデプロイを実現します。

[^3]: デプロイのタイミングでデータベースのスキーマを変更するような場合は、この仕組みだけでは無停止のデプロイは実現できない点にご注意ください。

### 管理者の承認なしにデプロイを進行させないようにできる

開発環境ではシームレスなデプロイを実現したい一方で、コンプライアンス等に起因し、本番環境の変更には管理者の承認が必要といったケースもあります。
[`cd_pipeline_backend pattern`](../patterns/aws/cd_pipeline_backend.md)、
[`cd_pipeline_frontend pattern`](../patterns/aws/cd_pipeline_frontend.md)では、フラグの切り替えにより、承認の有無を切り替えることができます。
詳細についてはそれぞれのドキュメントの「環境ごとのデプロイ戦略」のセクションをご参照ください。

承認が必要と設定されたパイプラインにおいて管理者の承認が得られなかった場合は、そのパイプラインは実行を停止します。
なお、そのような場合においてもユーザー影響は生じません。

### 静的コンテンツを無停止でデプロイできる

上述の「アプリケーションコンテナを無停止でデプロイできる」と同様に、Eponaでは静的コンテンツも無停止でデプロイ可能です。

Eponaでは、静的コンテンツをデプロイするにあたり[`cd_pipeline_frontend_trigger`](../patterns/aws/cd_pipeline_frontend_trigger.md)、
[`cd_pipeline_frontend`](../patterns/aws/cd_pipeline_frontend.md) patternの利用を想定しています。
`cd_pipeline_frontend pattern`で行うデプロイでは、キャッシュが利いている状態でOriginとなる静的コンテンツを置き換えることで、
無停止のデプロイを実現できます。

### サーバーレスアプリケーションをデプロイできる

Eponaでは、Lambda関数をデプロイするにあたり[`cd_pipeline_lambda_trigger`](../patterns/aws/cd_pipeline_lambda_trigger.md)、
[`cd_pipeline_frontend`](../patterns/aws/cd_pipeline_frontend.md) patternの利用を想定しています。
`cd_pipeline_lambda pattern`で行うデプロイでは、エイリアスに紐づくバージョンを変更することで、デプロイを実現できます。

## インフラ

### コードに対して静的解析チェックができる

EponaはインフラをTerraformでコード化するという意味で、IaC (Infrastructure as Code)の取組みでもあります。
そしてインフラがコード化されれば、アプリケーションと同様にもちろん静的解析チェックが可能になります。

Terraformの静的解析のプロダクトにもいくつか種類があります。例えば以下のようなプロダクトです。

- [tflint](https://github.com/terraform-linters/tflint)
- [tfsec](https://github.com/tfsec/tfsec)
- [terrascan](https://github.com/accurics/terrascan)

これらをGitLab CI/CD上のパイプラインに組み込むことで、静的解析ツールからのフィードバックを迅速に受けられます。
EponaやGetting Startedでは現在tflintを組み込んでいます。具体的な組み込みサンプルとしては、[.gitlab-ci.yml](https://gitlab.com/eponas/epona_aws_getting_started/-/blob/master/.gitlab-ci.yml)をご参照ください。

## パイプラインをクリアしないコードをマージしないようにできる

インフラも一度コード化してしまえば、アプリケーションと同様の対応で、パイプラインを合格しないコードをマージしないようにできます。
[アプリケーションのアクティビティ](#%E3%83%91%E3%82%A4%E3%83%97%E3%83%A9%E3%82%A4%E3%83%B3%E3%82%92%E3%82%AF%E3%83%AA%E3%82%A2%E3%81%97%E3%81%AA%E3%81%84%E3%82%B3%E3%83%BC%E3%83%89%E3%82%92%E3%83%9E%E3%83%BC%E3%82%B8%E3%81%A7%E3%81%8D%E3%81%AA%E3%81%84%E3%82%88%E3%81%86%E3%81%AB%E3%81%A7%E3%81%8D%E3%82%8B)をご参照ください。

## インフラの構成を変更できる

:construction: 今後提供予定です。
