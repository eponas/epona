# Eponaがサポートするアクティビティ

- [Eponaがサポートするアクティビティ](#eponaがサポートするアクティビティ)
  - [凡例](#凡例)
  - [コミュニケーション](#コミュニケーション)
  - [ITS/BTS](#itsbts)
  - [VCS Provider](#vcs-provider)
  - [デプロイメントパイプライン](#デプロイメントパイプライン)
    - [アプリケーション](#アプリケーション)
    - [インフラ](#インフラ)
  - [バックアップ](#バックアップ)
  - [仮想セキュアルーム](#仮想セキュアルーム)
  - [インフラセキュリティ](#インフラセキュリティ)
  - [モニタリング](#モニタリング)

Eponaでは以下のアクティビティをサポートします。

## 凡例

- :white_check_mark: : Eponaのモジュールと、適用先のクラウド環境を使って実現します
- :closed_lock_with_key: : :white_check_mark: の内容に加え、別途SaaSを契約あるいはサインアップした上で実現します

## コミュニケーション

開発・運用に必要な情報をコミュニケーションハブとしてのチャットツールに集約し、
チーム全員が常に同じ情報を把握し、同じ認識の元で議論ができるようにします。

- :closed_lock_with_key: チームメンバーがチャットでコミュニケーションできる
- :closed_lock_with_key: コミュニケーションの過程・結果を残せる
- :closed_lock_with_key: 課題起票やMerge Requestの通知を受け取れる
- :closed_lock_with_key: 障害通知を含むアラートを受け取れる

コミュニケーションについての詳細は[こちら](./communication.md)のガイドをご参照ください。

## ITS/BTS

タスクや課題の可視化は重要です。
運用エンジニアが開発の文化を知り、開発エンジニアが運用の問題を共有できれば、
ソフトウェアライフサイクルの中のどこに問題があり改良の必要な箇所がどこかもわかりやすくなります。

- :closed_lock_with_key: カンバンを用いた進捗状況の把握ができる
- :closed_lock_with_key: バックログをチケットとしてチーム内で共有し管理できる

ITS/BTSについての詳細は[こちら](./its_bts.md)のガイドをご参照ください。

## VCS Provider

VCS上でコード、ドキュメントを共有することによって、個人が学んだことをチーム全体の知識に転化します。
また、VCS Provider上でのピアレビュープロセスにより、実際の作業から遠く離れた人の承認ではなく日常業務の一部として継続的に品質を担保できます。

- :closed_lock_with_key: コード、ドキュメントをGitで管理できる
- :closed_lock_with_key: Merge Requestの形でピアレビューを行える

VCS Providerについての詳細は[こちら](./vcs_provider.md)のガイドをご参照ください。

## デプロイメントパイプライン

デプロイメントパイプラインを構成することで、システムへの変更に対する開発者への
フィードバックを行えるようにします。
変更がシステムに悪影響を与えないことを常に確認できることで、安心して本番環境へのデプロイを行うことができるようになります。

### アプリケーション

- :closed_lock_with_key: パイプライン上でコードに対して静的解析チェックができる
- :closed_lock_with_key: パイプライン上でユニットテストを自動実行できる
- :closed_lock_with_key: パイプライン上でセキュリティ脆弱性を検知できる
- :closed_lock_with_key: パイプラインをクリアしないコードをマージできないようにできる
- :white_check_mark: 秘匿すべき情報を開発者から隠すことができる
- :closed_lock_with_key: アプリケーションをライブラリにビルドし、保存できる
- :closed_lock_with_key: アプリケーションをコンテナイメージにビルドし、保存できる
- :closed_lock_with_key: アプリケーションを静的コンテンツにビルドし、保存できる
- :closed_lock_with_key: サーバーレスアプリケーションをビルドし、保存できる
- :white_check_mark: アプリケーションコンテナを無停止でデプロイできる
- :white_check_mark: 管理者の承認なしにデプロイを進行させないようにできる
- :white_check_mark: 静的コンテンツを無停止でデプロイできる
- :white_check_mark: サーバーレスアプリケーションをデプロイできる

### インフラ

- :closed_lock_with_key: コードに対して静的解析チェックができる
- :closed_lock_with_key: パイプラインをクリアしないコードをマージできないようにできる
- :white_check_mark: インフラの構成を変更できる

デプロイメントパイプラインについての詳細は[こちら](./deployment_pipeline.md)のガイドをご参照ください。

## バックアップ

予期せぬ事象が発生した時に迅速にロールバックを行うためには、バックアップの取得とそれを用いたリストアをできることが重要になります。

- :white_check_mark: データストアのバックアップが取得でき、そこからデータを復元できる

バックアップについての詳細は[こちら](./backup.md)のガイドをご参照ください。

## 仮想セキュアルーム

本番環境を運用するにあたってはセキュアルームを構えるケースが多いですが、新規に立ち上げるようなサービスにとって、セキュアルームの整備は大きな負担となります。
Eponaでは「仮想的な」セキュアルームをクラウド上に実現することにより、これらの物理的な整備を不要とします。

- :white_check_mark: 限られたユーザー・場所のみ仮想セキュアルームへアクセスできる
- :white_check_mark: 仮想セキュアルームから本番環境にアクセスできる
- :white_check_mark: 仮想セキュアルームからのアウトバウンドは制限できる
- :white_check_mark: 本番環境の運用を実施するにあたって管理者の許可を必須にできる

仮想セキュアルームについての詳細は[こちら](./virtual_secure_room.md)のガイドをご参照ください。

## インフラセキュリティ

権限管理の方針を示すことにより、第三者による悪用および内部的な犯行を防止します。

- :white_check_mark: 権限を管理できる
- :white_check_mark: 誰が何を行ったのかを表す監査証跡を保存できる
- :white_check_mark: 監査証跡の改ざんを防止できる
- :white_check_mark: 構成変更のルール違反を検知できる
- :white_check_mark: 秘匿すべき情報を開発者から隠すことができる
- :white_check_mark: 第三者による不正アクセスを防止できる
- :white_check_mark: コンテナに対してセキュリティスキャンをかけられる

インフラセキュリティについての詳細は[こちら](./infra_security.md)のガイドをご参照ください。

## モニタリング

モニタリングにより、問題を発見・解析し将来を予測するための基礎とできます。
さらに、問題が発生したと同時にそれを知るようにすることで、サービスチームは自信を持って変更を加えられ実験を進められるようになります。

- :closed_lock_with_key: リソースの標準的なシステムメトリクスを取得・可視化できる
- :closed_lock_with_key: システムメトリクスが特定条件を満たした場合にアラート通知ができる
- :closed_lock_with_key: ログを一箇所に集約できる
- :closed_lock_with_key: ログを簡単に検索できる
- :closed_lock_with_key: ログからメトリクスを抽出できる
- :white_check_mark: ビジネスメトリクスを可視化できる
- :closed_lock_with_key: インシデント管理ができる
- :closed_lock_with_key: アラートをチャットツール、電話へ通知できる
- :closed_lock_with_key: 特定時間帯のアラートを通知しないようにできる

モニタリングについての詳細は[こちら](./monitoring.md)のガイドをご参照ください。
