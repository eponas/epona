# モニタリング

[Why DevOps](../../README.md#why-devops)で述べたDevOpsの3つの原則のうちの1つ、素早く頻繁なフィードバックによる高品質・高信頼性・安全性の実現は、モニタリングによって支えられます。

高い品質や信頼性、安全性を実現するためには、すべての作業が測定され不良や大きな逸脱があればすぐに誰かが見つけて対処できる状態を作らなければなりません。
これはシステムだけでなく、ビジネスの目標にも当てはまります。
サービスが使われているか、成長できているかといった評価をいつでも行える必要があります。

そのためには測定の手段を隅々まで行き渡らせ、すべてのコンポーネントがどのように動作しているかを監視し、本来の動作から外れた時はすぐわかるようにする必要があるでしょう。
サービスやビジネスに問題が起きてから対処するリアクティブな対応だけではなく、問題の予兆を検知したプロアクティブな対応も欠かせません。
これらを実現するには、ただログを蓄積しているだけではなく、計測すべきメトリクスや必要な情報が抽出・可視化されている状態でなければなりません。
想定外のことが起きたことを発見するだけでは不十分で、発生したアラートやシステム上のエラーについて、迅速に関係者へ周知・動員できる仕組みの構築も重要になります。

これらを行うのがモニタリングであり、メトリクスやログ等の情報を自動で収集し、いつでも観測可能な状態にしておくことが重要です。

モニタリングによってスピーディなフィードバックが得られるようになります。問題が発生しても、それを素早く可視化し、関係者に情報を共有できるようになります。
問題は早期に発見され、解決されるようになるでしょう。
問題を見つけ早い段階で修正できるようになれば、自信を持ってサービスに変更を加え仮設検証を進められるようになるのです。

モニタリングは重要である一方で、必要な情報の収集、保存といったことを可能にするモニタリングシステムの構築はサービスの価値に直結しません。
このため、EponaはOperation-Lessのコンセプトに則り、このアクティビティの実現には積極的にSaaSを活用します。
モニタリングシステムの構築の手間をSaaSに預け、お客様の価値へ結びつく活動に集中しましょう。

ただし、モニタリングシステムで収集する仕組みは自動化されていたとしても、サービスの運用に必要な情報が収集されているかは集められるメトリクス次第です。
モニタリングシステムを導入さえすれば自分たちの見たい情報が可視化されるわけではありません。
なにを収集すればシステムをモニタリングできるのか、ビジネスを評価できるのかを考え、メトリクスやログを定義しましょう。
そして、その内容が自動的にモニタリングシステムに収集されるように組み込みましょう。
これは、サービスを開発する人にしかできないことです。

## 適用pattern・How To

本アクティビティを実現するためにEponaが用意しているpatternは以下の通りです。

- [`datadog_integration pattern`](../patterns/aws/datadog_integration.md)
- [`datadog_forwarder pattern`](../patterns/aws/datadog_forwarder.md)
- [`datadog_log_trigger pattern`](../patterns/aws/datadog_log_trigger.md)
- [`quicksight_vpc_inbound_source pattern`](../patterns/aws/quicksight_vpc_inbound_source.md)

また、本アクティビティを支えるHow Toドキュメントは以下の通りです。

- [Amazon ECS上のコンテナログを、AWS FireLensを使ってAmazon CloudWatch Logsに送信する](../how_to/aws/ecs_log_send_to_cloudwatch_logs_using_firelens.md)
- [ECS Fargate上のコンテナのメトリクスをDatadogへ送信する](../how_to/aws/send_ecs_fargate_metrics_to_datadog.md)
- [複数行で出力されるログをひとつのログに集約する](../how_to/aws/handling_multiline_log.md)
- [Datadogを用いたログからのメトリクス抽出、可視化ガイド](../how_to/datadog_log_analysis.md)
- [Datadogのアラート設定](../how_to/datadog_monitor.md)
- [Datadogのダッシュボード設定](../how_to/datadog_dashboard.md)
- [PagerDutyでのインシデント管理](../how_to/pagerduty_guide.md)
- [ビジネス指標可視化のためのAmazon QuickSightのセットアップと使い方ガイド](../how_to/aws/quicksight_monitor.md)

## リソースの標準的なシステムメトリクスを取得・可視化できる

クラウド上に構築したリソース(例えば、ネットワークやRDB等のデータストア)のメトリクスを収集し、モニタリングするのはシステムの状態を把握する上でとても重要です。
障害発生時などにおいても、解析に必要な情報となるでしょう。
しかしメトリクスを収集し保存するモニタリングシステムの設計・構築は手間がかかり、またサービスの価値には直接結びつきません。

Eponaでは、システムメトリクスの取得、ダッシュボード上の可視化には[Datadog](https://www.datadoghq.com/ja/)を活用します。

DatadogではAmazon Web Services (AWS)と[インテグレーション](https://docs.datadoghq.com/ja/getting_started/integrations/)を行うことで
AWSのリソースからDatadogへメトリクスの収集・可視化が可能になります。それにより、システムを統合的に把握可能になります。

- [Datadogのダッシュボード設定](../how_to/datadog_dashboard.md)

インテグレーションを行った後は、[Datadogのダッシュボード](https://docs.datadoghq.com/ja/dashboards/)を参照することで日常的なモニタリングが行えます。
Datadogのインテグレーションには以下のような種類があります。

### エージェントベース

[Datadogのエージェント](https://docs.datadoghq.com/ja/agent/)をインストールし、エージェントからシステムメトリクスを取得します。
Eponaでは、主としてアプリケーションコンテナのシステムメトリクスの取得に利用します。
その実現方法については、[ECS Fargate上のコンテナのメトリクスをDatadogへ送信する](../how_to/aws/send_ecs_fargate_metrics_to_datadog.md)をご参照ください。

### クローラーベース

モニタリング対象のシステムとDatadogを接続することで、自動的にメトリクスを収集できます。
例えば、[AWS](https://docs.datadoghq.com/ja/integrations/amazon_web_services/?tab=cloudformation)、
[Microsoft Azure](https://docs.datadoghq.com/ja/integrations/azure/?tab=azurecliv20)のインテグレーションがあります。

Eponaでは、AWSとDatadogのインテグレーションのために
[`datadog_integration pattern`](../patterns/aws/datadog_integration.md)を用意しています。

### ライブラリ

プログラムから[Datadog API](https://docs.datadoghq.com/ja/api/)を呼び出すことでメトリクスを送信します。
例えばNablarchであれば、[Datadog連携用のMicrometerのモジュール](https://nablarch.github.io/docs/LATEST/doc/application_framework/adaptors/micrometer_adaptor.html#id20)を用意しています。

## システムメトリクスが特定条件を満たした場合にアラート通知ができる

システムを監視する際、重要な変更があったことに迅速に気づくことができれば、MTTR (Mean Time to Recovery)の改善に大きく寄与します。

EponaではDatadogの[モニター](https://docs.datadoghq.com/ja/monitors/monitor_types/)機能を利用することで、
メトリクスが特定条件を満たした場合や、ログに特定の文字列が出力された場合等にアラート通知が可能です。
アラートの設定方法については[Datadogのアラート設定](../how_to/datadog_monitor.md)を参照ください。

## ログを一箇所に集約できる

アプリケーションだけでなく、ミドルウェア、そしてマネージドサービスも様々なログを出力します。
そのようなログが散在していると、トラブルシューティングやデータ調査に時間がかかり、MTTRを押し下げます。

このため、様々なログを集め、すぐに確認できる状態にすることはサービスの品質改善に繋がります。
しかし、大量のログを収集できるスケーラブルなログ集約システムを構築、運用するのには大きな手間と労力がかかります。

EponaではDatadogにログを収集することで、[リアルタイムでのログの確認](https://docs.datadoghq.com/ja/logs/explorer/live_tail/)、
収集した[ログの横断検索](https://docs.datadoghq.com/ja/logs/explorer/)や[分析](https://docs.datadoghq.com/ja/logs/explorer/analytics/?tab=timeseries)、
そしてそのログからのメトリクスの生成などを可能とします。

多くの場合、AWSのリソースからはAmazon S3やAmazon CloudWatch Logsへログが出力されます。
これらのログをDatadogへ収集するには、以下のpatternを組み合わせます。

- [`datadog_forwarder pattern`](../../guide/patterns/aws/datadog_forwarder.md)
- [`datadog_log_trigger pattern`](../../guide/patterns/aws/datadog_log_trigger.md)

EponaではAmazon ECS (以下、ECS)で動作するアプリケーションを対象にしていますが、これらのアプリケーションについても同様です。

:information_source: ECSの中でもAWS Fargateを利用しており、
[`public_traffic_container_service pattern`](../../guide/patterns/aws/public_traffic_container_service.md)で実現します。

ECS上で動作するアプリケーションは、AWS FireLens経由でログを出力することでログにECSのメタデータを付与でき、
Datadog上でより効率的な絞り込みが可能になります。
AWS FireLensを利用したログ出力については、
[Amazon ECS上のコンテナログを、AWS FireLensを使ってAmazon CloudWatch Logsに送信する](../how_to/aws/ecs_log_send_to_cloudwatch_logs_using_firelens.md)を参照ください。

:information_source: アプリケーションが改行を含むログを出力する場合、本来1つのログエントリであるはずのログが
改行ごとに別のログエントリとしてDatadogに転送されます。  
こうした複数行のログを本来のように1つのログエントリとして集約する方法については、
[複数行で出力されるログをひとつのログに集約する](../how_to/aws/handling_multiline_log.md)
を参照してください。

:warning: なおDatadogにおけるログの課金は、[「インデックス化された」ログイベントの総数によって課金](https://docs.datadoghq.com/ja/account_management/billing/log_management/)されます。
一方で、課金額を少なくするためにDatadogへ転送するログの量を削減すると有用なデータが除外されてしまいがちです。

この問題に対応するため、Datadogでは集約したログに対して「インデックス化」するか、しないかの[フィルタ設定](https://docs.datadoghq.com/ja/logs/indexes/#%E9%99%A4%E5%A4%96%E3%83%95%E3%82%A3%E3%83%AB%E3%82%BF%E3%83%BC)が可能です。
このフィルタ設定のON/OFFはリアルタイムで切り替え可能なため、トラブルシュートの時間だけ`DEBUG`レベルのログをインデックス化するといった柔軟な運用も可能です。
また、インデックス化されないログに対してDatadogの検索等を行うことはできませんが、
リアルタイムでログを確認できる[Live Tail](https://docs.datadoghq.com/ja/logs/explorer/live_tail/)等いくつかの機能も利用できます。これらのフィルタを利用して、ぜひ柔軟なログ運用を行ってください。

## ログを簡単に検索できる

Eponaでは、Datadogに集約したログを横断的かつ高速に検索できます。
検索は、Datadogで「インデックス化」したログを対象に[ログエクスプローラー](https://docs.datadoghq.com/ja/logs/explorer/)を用いて実施します。

検索は、メッセージ属性に対する検索と、ファセットに対する検索がを行うことができます。
ファセットとは何か、および、ファセットの追加方法については、[Datadogを用いたログからのメトリクス抽出、可視化ガイド](../how_to/datadog_log_analysis.md)を参照ください。

## ビジネスメトリクスを可視化できる

モニタリングというとシステムに対するモニタリングに着目しがちですが、サービスを成長させる上ではビジネス観点のモニタリングも極めて重要です。

例えば[入門 監視](https://www.oreilly.co.jp/books/9784873118642/)[^1]で挙げられているビジネスKPIには以下のようなものがあります。

- 現在サイトに滞在しているユーザー
- ユーザーのログイン
- コメント投稿
- 投票
- 広告購入

ビジネスKPIは、サービスを運営し成長するためには非常に重要であり、これらのメトリクスを特定しモニタリングしていく必要があります。
一方で、個々のサービスはそれぞれ異なるので、何をモニタリングすべきかを一般化するのは無理があります。

このためEponaでは、ビジネスKPIが各サービスで定義されることを前提に、これらのビジネスKPIを可視化する方法をガイディングします。

- [ビジネス指標可視化のためのAmazon QuickSightのセットアップと使い方ガイド](../how_to/aws/quicksight_monitor.md)

可視化のガイドを参照しつつ、その実現方法を押さえてください。
サービスでは、ビジネスKPIを測定するためのメトリクスを検討しましょう。そして、どのようにメトリクスを記録していくかを設計、実装しましょう。

## インシデントを管理できる

Eponaではインシデント管理のために[PagerDuty](https://ja.pagerduty.com/)を利用することを想定しています。
PagerDutyは、信頼性のあるアラート通知と柔軟な運用者のアサインスケジュール(オンコールスケジュール)設定、
自動的なインシデントエスカレーション機能を備えたインシデント管理用のSaaSです。

特に、小規模なチームでサービスを持続的に運用する場合、いつ誰が発生したインシデントに対応するのかというスケジューリングが非常に重要です。
PagerDutyを用いればインシデント対応のアサイン等を柔軟にコントロールでき、担当者への自動的な通知が容易に実現できます。

[入門 監視](https://www.oreilly.co.jp/books/9784873118642/)[^1]では、シンプルなインシデント管理のプロセスとして以下を挙げています。

1. インシデントの認識（監視が問題を認識）
2. インシデントの記録（インシデントに対して監視の仕組みが自動でチケットを作成）
3. インシデントの診断、分類、解決、クローズ（オンコール担当者がトラブルシュートし、問題を修正し、チケットにコメントや情報を添えて解決済みとする）
4. 必要に応じて問題発生中にコミュニケーションを取る
5. インシデント解決後、回復力を高めるための改善策を考える

PagerDutyでは、このうちの4.までの機能を保持しています。具体的な利用方法については、[PagerDutyでのインシデント管理](../how_to/pagerduty_guide.md)をご参照ください。

また、障害対応においては本番環境にログインしてのオペレーションを避けられないケースがあります。
Eponaでは本番環境アクセスのために[仮想セキュアルーム](./virtual_secure_room.md)を用意していますので、詳細は当該のアクティビティをご参照ください。

## アラートをチャットツール、電話へ通知できる

サービスを安定的に運用する上で、アラートを関係者に通知することは重要です。
アラートには以下の2種類があります。

- ユーザー影響があり、原因を取り除くための迅速な対応が必要であるもの（アプリケーションやDBの停止など）
- すぐに対応する必要はないが、起きたことを知り確認すべきもの（CPUやメモリの一時的なしきい値超えなど）

どちらのアラートかによって、通知手段は別にした方が良いでしょう。
たとえば前者のように緊急対応が求められるのであれば、確実かつ迅速に担当者に気づいてもらわなければなりません。対応者が捕まるまで何度も通知を送る必要があるでしょう。
一方で後者であれば、次営業日に共有されれば良いということもあります。

Eponaでは、これらのアラートの通知手段としてメール、電話、チャットツール(SlackやMicrosoft Teams)、SMSを想定しています。
この通知は、DatadogやPagerDutyを利用して実現します。

それぞれで連携可能な通知手段は下表の通りです。

:information_source: Datadogはアラートとしての通知を一度送るのみです。一方、PagerDutyはインシデントの通知という位置づけであるため、オンコールのメンバーが応答するまで通知し続けるという違いがあります。

| 通知手段       | Datadog | PagerDuty |
| -------------- | ------- | --------- |
| メール         | o       | o         |
| 電話           | x       | o         |
| チャットツール | o       | o         |
| SMS            | x       | o         |

Datadogでの通知設定については[Datadogのアラート設定](../how_to/datadog_monitor.md)を参照ください。
また、PagerDutyでの通知設定については、[PagerDutyでのインシデント管理](../how_to/pagerduty_guide.md)を参照してください。

## 特定時間帯のアラートを通知しないようにできる

Eponaではアラート通知をDatadog、あるいはPagerDutyで実現します。
メンテナンス作業等でアラート通知を抑止したいというユースケースは多くあります。それぞれのSaaSでのアラート抑止方法は以下をご参照ください。

### Datadog

抑止したいモニター設定に対しミュートを行ってください。詳細は[モニターの管理](https://docs.datadoghq.com/ja/monitors/manage_monitor/#%E7%AE%A1%E7%90%86)を参照ください。

### PagerDuty

対象となるサービスをメンテナンスモードに設定してください。
詳細は[Maintenance Window](https://support.pagerduty.com/docs/maintenance-windows#scheduling-maintenance)をご参照ください。

[^1]: 「モニタリング」を体系的に理解できる優れた書籍です。ご一読頂ければ、モニタリングの理解が進むでしょう。
