# Change Log

<!--

## [Unreleased]

### Breaking Changes

- aaa(!MergeRequestNo)

### Features

- bbb(!MergeRequestNo)

### Enhancements

- ccc(!MergeRequestNo)

### Bug Fixes

- ddd(!MergeRequestNo)

-->

## 0.2.6 - 2021-09-30

### Features

- AWS pattern modules
  - cd_trigger_lambda pattern
    - Lambda用のデプロイメントパイプラインの実装(!384)
  - cd_trigger_lambta_trigger pattern
    - Lambda用のデプロイメントパイプラインの実装(!384)

### Enhancements

- AWS pattern modules
  - api_gateway pattern
    - エイリアス付きのLambda関数を呼出可能にする（!381）
  - step_functions pattern
    - エイリアス付きのLambda関数を呼出可能にする(!382)
  - lambda pattern
    - Lambda関数のエイリアスを使えるようにする(!385)

### Bug Fixes

- AWS pattern documents
  - api_gateway/rule_violation
    - Eponaガイドのシンタックスハイライトが効いていない箇所を修正する(!383)

## 0.2.5 - 2021-08-31

### Features

- AWS pattern modules
  - api_gateway_pattern add(!355)
  - lambda pattern add(!356)
  - step_functions pattern add(!358)

### Enhancements

- AWS pattern modules
  - public_traffic_container_service
    - ALBのリスナールールを追加可能にする（!357）

## 0.2.4 - 2021-05-31

### Enhancements

- AWS pattern modules
  - public_traffic_container_service
    - outputsに作成するロードバランサーのARNを追加（!348）

- AWS pattern documents
  - webacl
    - Application Load BalancerにWebACLを適用する場合について追記（!348）
  - virtual_secure_room_directory_service
    - リモートデスクトップに関する設定箇所の文言を修正（!349）

- misc
  - ガイドのフッターに関連コンテンツへのリンクを追加（!341）
  - Terraformの対応バージョンを`>= 0.14.10`とした（!345）
  - AWS Providerのバージョンアップ（3.18.0 → 3.37.0）（!345）
  - Eponaで作成するS3バケットにパブリックブロックアクセスを付与する (!346)
  - Datadog Providerのバージョンアップ（2.17.0 → 2.25.0）（!347）
  - archive Providerのバージョンアップ（2.0.0 → 2.2.0）（!347）

## 0.2.3 - 2021-04-28

### Enhancements

- AWS pattern modules
  - cd_pipeline_frontend_trigger
    - 同一AWSアカウント内でのCDを可能にする(!332)

### Bug Fixes

- AWS pattern modules
  - cd_pipeline_frontend
    - CloudFrontのキャッシュ無効化パス指定の誤りを修正(!337)

## 0.2.2 - 2021-03-31

### Breaking Changes

- AWS pattern modules
  - cacheable_frontend pattern
    - Route 53で作成するDNSレコードをエイリアスに変更(!313)
      - `ttl`変数を削除

### Enhancements

- AWS pattern modules
  - public_traffic_container_service pattern
    - ECSタスクロールのデフォルト設定に「logs:DescribeLogGroups」を追加(!316)
  - ci_pipeline pattern
    - アーティファクト用バケットを追加する(!326)
- AWS pattern documents
  - database
    - 削除保護がデフォルトで有効になっていることを明記(!315)
  - datadog_log_trigger
    - Datadogのlog filterに関するガイドの追加(!307)
  - virtual_secure_room_directory_service
    - 「クリップボードの共有について」でわかりにくい点に注意文言を追記(!330)
    - WorkSpacesのWindowsにRDPで接続する場合の各種共有を無効にする手順を追記(!331)
- AWS how to documents
  - 「複数行で出力されるログをひとつのログに集約する」add(!316)
  - GitLab CI/CDを用いたアーティファクトのCD方法をガイドする(!324)
- misc
  - リリーススクリプトの修正(!323)
  - Terraform実行ユーザーを任意のユーザーグループに所属できるように修正(!325)
  - Epona AWS Starter CLIの開発に伴い、Terraformを実行する環境構築手順を簡易化(!328)

### Bug Fixes

- AWS pattern documents
  - rule_violation
    - 設定レコーダー、配信チャネルの記載内容を見直し(!312)
  - redis pattern
    - サンプルコード内のバージョンを修正(!317)
- AWS pattern modules
  - audit patternで使用するS3バケットのバケットポリシーから、AccessAnalyzer用のポリシーを削除する(!319)
  - cd_pipeline_frontend_trigger pattern使用時に警告（非推奨構文の利用）が出力される問題を解消(!322)
- AWS how to documents
  - 「複数行で出力されるログをひとつのログに集約する」のガイドの記載漏れを修正する(!329)
- misc
  - README.md
    - pattern一覧のリンク切れを修正(!320)
  - TerraformやProviderのバージョン制約を厳密に設定(!314)
  - 環境構築手順の`terraform init`オプションの指定を修正（`-backend-config`）(!318)
  - 概要ページからTerraformモジュールページへの導線を見直す(!321)

## 0.2.1 - 2021-01-14

### Features

- cacheable_frontend
  - outputにoriginバケットのパスを追加(!302)
- cd_pipeline_backend
  - アーティファクトストアとなるS3バケットの自動命名に対応(!302)
- cd_pipeline_frontend
  - アーティファクトストアとなるS3バケットの自動命名に対応(!302)

### Enhancements

- pattern
  - ci_pipeline patternで構築するGitLab RunnerのPullPolicyをデフォルトで`if-not-present`に変更(!306)
- misc
  - リリーススクリプトの追加(!292)
  - 利用者からのフィードバックをガイドに反映する(!303)

### Bug Fixes

- AWS pattern documents
  - index
    - 凡例の修正、アクティビティの詳細リンクの付け方を変更(!305)
- misc
  - README.mdの記載事項、項目の記載順を見直し(!304)
  - Copyright表記の追加、Author欄削除(!311)
  - ガイド内の注釈ずれを修正(!310)

## 0.2.0 - 2020-12-23

### Breaking Changes

- AWS pattern modules
  - redis pattern
    - S3にアップロードしたファイルを初期データにできる機能を削除(!266)
    - 自動マイナーバージョンアップグレード機能を削除(!266)

### Features

- AWS pattern modules
  - vpc_peering pattern add(!222)
- misc
  - GitLab Pages対応(!272)
  - GitLab Pagesのドキュメントライセンスを`CC BY-SA 4.0`に変更(!294)

### Enhancements

- AWS pattern modules
  - redis pattern
    - セキュリティグループのインバウンドにセキュリティグループを設定できるように修正(!242)
  - virtual_secure_room_directory_service pattern
    - クリップボード共有の設定のため機能を追加(!261)
  - audit pattern
    - CloudTrailの証跡を記録するS3バケットのバケットポリシーを修正(!284)
- AWS pattern documents
  - datadog_integration pattern
    - 関連pattern欄を更新(!221)
  - datadog_log_trigger pattern
    - 関連pattern欄を更新(!221,!281)
  - virtual_secure_room_workspaces pattern
    - vpcピアリングについての注意書きを追加(!226)
    - 自動停止についての説明を追加(!243)
    - クリップボード共有の設定を追加(!261)
  - virtual_secure_room_directory_service pattern
    - DNSフォワーダー設定手順についての注意書きを追加(!256)
    - Proxyの指定方法について注意書きを追加(!263)
    - クリップボード共有の設定を追加(!261)
  - cacheable_frontend pattern
    - destroy時のLambda@Edgeについての注意書きを追加(!257)
  - user pattern
    - パスワード再発行の手順を追加(!232)
    - スイッチロールに関する記載を修正(!293)
  - cd_pipeline_backend_trigger、cd_pipeline_frontend_trigger pattern
    - CloudTrailのデータイベントログ記録機能の有効化が必要な旨を追記(!288)
  - cd_pipeline_backend、cd_pipeline_frontend pattern
    - CloudTrailのデータイベントログ記録機能の有効化が不要な旨を追記(!288)
- AWS how to documents
  - 「Amazon QuickSightを用いたBI指標可視化ガイド」add(!185)
  - 「GitLab CI/CDによるコンテナイメージのCDガイド」add(!283)
- activity documents
  - モニタリング
    - 追加したガイドを反映(!228)
  - インフラセキュリティを追加(!241)
  - 仮想セキュアルームを追加(!243)
  - デプロイメントパイプラインを追加(!259)
- misc
  - MkDocsでHTMLドキュメントを出力する仕組みを追加(!208)
  - Terraform実行環境を構築する際のAWSアカウントについて補足を追加(!247)
  - Eponaの拡張方法について記述(!254)
  - LICENSEファイル追加(!277)
  - ドキュメント上のTODOコメントを削除(!279)

### Bug Fixes

- 環境構築の説明に環境変数AWS_DEFAULT_REGIONを設定する説明がなかったので追記(!230)
- Getting Startedのテストで発見した軽微な誤記の修正(!235)

- AWS pattern modules
  - users/rolesパターンで作成されるDeveloperRoleのポリシー修正(!225)
  - rule_violationのIAMポリシーの名称を固定から可変に変更(!233)
  - virtual_secure_room_directory_service patternで作成するAD管理サーバーのSSMドキュメント名を修正(!234)
  - virtual_secure_room_directory_service AD Manager用EC2のIPおよびDNSをEIPに修正(!268)
  - virtual_secure_room_workspaces patternで作成するIPグループ名を修正(!236)
  - cacheable_frontend patternを複数回実行するとCloudFrontのアクセスログが出力されなくなるバグを修正(!239)
  - cacheable_frontend patternのオプションパラメータが一部未定義だと実行できなくなっていたバグを修正(!239)
  - webacl patternのオプション想定のパラメータにデフォルト値が設定されていなかったバグを修正(!239)
  - PagerDutyとDatadogのインテグレーションに関する記載誤りを修正する(!249)
  - datadog_integration patternのデフォルトのIAMポリシーを[Datadogのドキュメントに沿って](https://docs.datadoghq.com/ja/integrations/amazon_web_services/?tab=#%E3%81%99%E3%81%B9%E3%81%A6%E3%81%AE%E3%82%A2%E3%82%AF%E3%82%BB%E3%82%B9%E8%A8%B1%E5%8F%AF)最新化(!251)
  - rule_violationの監視対象とするリソースが指定できない件を修正(!248)
  - threat_detection patternから不要なオプションを削除(!255)
  - ci_pipeline patternでcontainer_image_repositoriesを設定しない場合に、パターンの再適用/削除ができない問題の修正(!253)
  - rule_violation patternで発生しているワーニングを解消する(!260)
  - users/bind_role/roles patternで作成されるRoleへのSwitchRoleする際にはMFA認証を必須とするように修正(!267)
  - datadog_forwarder patternで、apply後に再度planやapplyを行うと差分検出され更新されてしまう問題を修正(!265)
  - cd_pipeline_backend/frontend patternのcross_account_codepipeline_access_role_arnを必須パラメータに修正(!270)
  - 以下のpatternでtags変数の指定が必須になっていた問題を修正(!269)
    - network
    - parameter_store
    - public_traffic_container_service
    - roles
    - users
    - virtual_secure_room_proxy
    - virtual_secure_room_workspaces
    - vpc_peering
  - cd_pipeline_backend_trigger patternの引数ドキュメントを修正(!238, !275)
  - cd_pipeline_frontend_trigger patternの引数ドキュメントを修正(!238)
  - database patternで作成される一部のリソースにタグを設定できないようになっていた問題を修正(!262)
  - network patternで作成される一部のリソースにタグを設定できないようになっていた問題を修正(!262)
  - audit pattern
    - kms_custom_key_policyを設定した時に、patternが適用できない問題の修正(!270)
    - デフォルトで、S3, Lambdaのイベントを記録しないように修正(!270)
- AWS pattern documents
  - サンプルコードの修正
    - virtual_secure_room_directory_service pattern(!231)
    - virtual_secure_room_workspaces pattern(!231)
    - vpc_peering pattern(!231)
    - virtual_secure_room_proxy pattern(!237)
    - datadog_integration patternのrequired_providersの記法を修正(!250)
    - redis patternでのsnapshot_retention_limitの記述誤りを修正(!252)
    - public_traffic_container_service pattern(!245)
    - database patternのポート、エンジンバージョン指定の誤りを修正(!258)
    - サンプルコードの`source`指定をGitプロトコル記述に変更(!271)
  - redis pattern
    - マルチAZ構成を現在のAWS Providerがサポートしていないことを明記(!252)
    - マイナーバージョンの自動アップグレードの記述を削除(!252)
    - リストア時の注意事項を記述(!289)
  - database pattern
    - パスワード変更コマンドの誤りを修正(!258)
    - リストア時にエンドポイントが変更されてると記述しているが、実際には変更されないため修正(!258)
    - SSL/TLSを強制可能なデータベースエンジンは限定されることを明記(!258)
    - リストア後の注意事項を追記(!264)
  - audit pattern
    - KMSのパターンを適用した際に、KMSのログを記録しないようにする方法を追記(!270)
  - virtual_secure_room_directory_service pattern
    - ドメインAdminのパスワード変更手順を修正(!282)
- AWS how to documents
  - Datadogダッシュボードの設定の記載内容見直し(!244)

## 0.1.3 - 2020-12-02

### Breaking Changes

- AWS pattern modules
  - redis patternのメンテナンスに関する設定を必須化(!183)
  - rule_violation patternの`IAM_PASSWORD_POLICY`のチェック間隔を制御する変数の誤りを修正(!214)

### Features

- AWS pattern modules
  - virtual_secure_room_proxy pattern add(!199)
  - quicksight_vpc_inbound_source pattern(!201)
    - QuickSightがVPC内のリソースを参照するためのセキィリティグループを追加
- AWS how to documents
  - 「ECS Fargate上のコンテナのメトリクスをDatadogへ送信する」add(!180)
  - 「Datadogのダッシュボード設定」add(!187)

### Enhancements

- AWS pattern modules
  - roles
    - IAMポリシーの追加設定(!181)
  - users
    - IAMポリシーの追加設定(!181)
  - virtual_secure_room_directory_service pattern
    - AD ManagerのOutputを追加&AD ManagerのAMIを自動取得 (!196)
  - network
    - ECRとCloudWatch Logsへのプライベートリンクを追加 (!204)
- activity documents
  - モニタリングadd(!175, !197)
  - コミュニケーションadd(!198)
  - バックアップadd(!206)
  - ITS/BTS add(!207)
  - VCS Provider add(!216)
- AWS pattern documents
  - cacheable_frontend pattern
    - 構成図を追加(!192)
    - SPA公開時の注意点について説明を修正
  - datadog_forwarder pattern
    - 構成図を追加(!178)
  - datadog_log_trigger pattern
    - 構成図を追加(!178)
  - public_traffic_container_service pattern
    - AWS FireLensに関するHow toドキュメントへの導線を追加(!188)
    - 初回デプロイするコンテナはダミーとする説明に修正(!203)
  - redis pattern
    - 可用性やスケーラビリティ、メンテナンス、モニタリング等に関する内容を追記、更新(!183)
  - database pattern
    - モニタリングおよびイベント通知に関する内容を見直し(!183)
  - virtual_secure_room_directory_service pattern
    - 図の修正 (!196)
    - 手順を日本語版前提に修正 (!196)
    - virtual_secure_room_proxy pattern追加に伴い、DNSフォワーダー設定手順を追加 (!199)
  - virtual_secure_room_workspaces
    - 図の修正 (!196)
- AWS how to documents
  - 「PagerDutyでのインシデント管理」add(!176,!190)
  - 「Amazon ECS上のコンテナログを、AWS FireLensを使ってAmazon CloudWatch Logsに送信する」
    - Fluent Bitコンテナの定義例を修正(!195)
    - CodeDeployで使用するタスク定義を変更する書き方に修正(!203)
- misc
  - 各patternドキュメントへのリンクを追記(!201)
  - AWS Providerのバージョンアップ（3.13.0 → 3.18.0）(!191, !193, !202, !211)
  - Datadog Providerのバージョンアップ（2.15.0 → 2.17.0）(!194, !212)
  - tflintを0.20.3から0.21.0へバージョンアップ (!213)

### Bug Fixes

- AWS pattern modules
  - public_traffic_container_service pattern
    - ALBのデフォルトのセキュリティポリシーを`ELBSecurityPolicy-TLS-1-2-Ext-2018-06`に変更し、セキュリティ強度を向上(!210)
    - タスク名ロール作成を制御するフラグ名の誤りを修正(!210)
  - rule_violation pattern
    - ガイドに記載のサンプルコードの誤りを修正(!214)
- misc
  - AWS Providerのバージョンアップ（3.13.0 → 3.16.0）(!191, !193, !202)
  - Datadog Providerのバージョンアップ（2.15.0 → 2.16.0）(!194)
  - Terraform環境の構築手順上の不具合を修正(!205)
  - READMEの概要図・構成図の修正(!209)

## 0.1.2 - 2020-11-09

### Breaking Changes

- AWS pattern modules
  - threat_detecton patternの通知先としてTeamsを設定できるように修正(!122)
  - cacheable_frontend pattern mod(!123)
    - ACM作成のコードを追加、入力変数名をわかりやすい名前に変更、アウトプットを追加、アクセスログ用バケット作成コードを別コンポーネントに分離
    - オリジンとなるS3へのアクセスログ設定を追加し、CloudFrontへのアクセスログに関する変数をリネーム(!145)
  - virtual_secure_room_directory_service pattern: AD ManagerのSGインバウンドをリスト指定できるように修正(!125)
  - database pattern: Input Variablesの誤字を修正(!167)
- AWS pattern documents
  - cacheable_frontend pattern mod(!123)
    - ACM作成コード追加に伴う説明の追加と変更、入出力項目の変更を反映

### Features

- AWS pattern modules
  - datadog_integration pattern add(!112)
  - datadog_forwarder pattern add(!148)
  - datadog_log_trigger pattern add(!160)
  - virtual_secure_room_workspaces pattern add(!100)
  - webacl pattern add(!106)
  - rule_violation pattern add(!109)
- AWS pattern documents
  - datadog_integration pattern add(!112)
  - datadog_forwarder pattern add(!148)
  - datadog_log_trigger pattern add(!163)
  - webacl pattern add(!107)
  - rule_violation pattern add(!139)
  - threat_detection pattern add(!138)
  - virtual_secure_room_directory_service pattern add(!127)
  - virtual_secure_room_workspaces pattern add(!130)
- AWS how to documents
  - 「Amazon ECS上のコンテナログを、AWS FireLensを使ってAmazon CloudWatch Logsに送信する」add(!169)
  - 「Datadogのアラート設定」add(!157)
  - 「Datadogを用いたログからのメトリクス抽出、可視化ガイド」add(!159)

### Enhancements

- AWS pattern modules
  - audit
    - ログ出力用のS3バケットの情報をOutputに追加(!145)
  - database pattern
    - 自動Snapshotの有効化機能を追加(!124)
    - ログ出力実装の集約(!145)
    - 拡張モニタリングの保存先ロググループ名をOutputに追加(!160)
  - network pattern
    - VPCフローログの有効化機能を追加(!134, !145)
  - redis pattern
    - 自動Snapshotの有効化機能を追加(!124)
  - public_traffic_container_service pattern
    - アクセスログの出力設定を追加(!131)
    - ロギング用バケットにファイルがあっても強制削除可能にする(!135)
    - ログ出力実装の集約(!145)
    - FireLensログドライバーを利用できるようにタスク用ロールを付与(!169)
  - cacheable_frontend
    - 静的ファイル配置用バケットにファイルがあっても強制削除可能にする(!135)
    - CloudFrontからのレスポンスにHTTPヘッダーを付ける機能を追加(!150)
    - OriginのS3バケットをユーザー定義のリージョンに作成できるように修正(!168)
  - rule_violation
    - ログ出力実装の集約(!145)
  - threat_detection
    - ログ出力実装の集約(!145)
  - webacl
    - ロギング用バケットにファイルがあっても強制削除可能にする(!135)
    - ログ出力用のS3バケットの情報をOutputに追加(!145)
  - cd_pipeline_backend_trigger
    - 同一AWSアカウント内でのCDを可能にする(!136)
  - cd_pipeline_backend
    - 複数のECRリポジトリからデプロイするパイプライン構築時、CodePipelineのAction名が重複するバグを修正(!174)
  - parameter_store pattern
    - Outputのキーをパラメータ名とするように修正(!148)
  - cd_pipeline_frontend
    - デプロイプロバイダをCodeBuildに変更(!167)
    - CloudFrontのキャッシュを無効化するオプションの追加(!167)
- AWS pattern documents
  - cd_pipeline_backend_trigger pattern
    - target_event_bus_arnの説明中の誤記を修正(!143)
  - network pattern
    - 図およびコード例の追加(!155)
  - cd_pipeline_frontend pattern
    - CloudFrontのキャッシュを無効化のオプションについて追記(!167)
  - users pattern
    - GPGキー生成と復号に関する説明の追加(!166)
  - public_traffic_container_service pattern
    - 図の追加、説明の改善(!171)
- misc
  - AWS Providerの下限を、3.6.0から3.13.0にバージョンアップ(!128, !141, !151, !154, !165, !173)
  - Datadog Providerの下限を、2.13.0から2.15.0にバージョンアップ(!172, !177)
  - Terraformの対応バージョンを`>= 0.13.5`とした (!121, !140, !164)
  - tflintを0.19.1から0.20.3へバージョンアップ (!129, !158)

## 0.1.1 - 2020-09-18

### Features

- AWS pattern modules
  - users add (!74)
  - roles add (!74)
  - bind-role add (!74)
  - cd_pipeline_frontend pattern add(!68)
  - datadog_integration pattern add(!112)
  - cd_pipeline_frontend_trigger pattern add(!68, !116)
  - virtual_secure_room_directory_service pattern add(!79)
- AWS pattern documents
  - cd_pipeline_frontend pattern add(!68)
  - cd_pipeline_frontend_trigger pattern add(!68)
  - datadog_integration pattern add(!112)

### Enhancements

- AWS pattern modules
  - ci_pipeline pattern: フロントエンドのビルド成果物を格納するS3バケットを作成できるように修正(!93, !101, !115)
  - database pattern: ロギング、モニタリングに関する設定を追加(!105)
  - AWS Providerの下限を、3.0.0から3.6.0にバージョンアップ(!94, !103, !104, !110)
  - Archive Providerの下限を、1.3.0から2.0.0にバージョンアップ(!152)
- AWS pattern documents
  - cacheable_frontend pattern list add(!91)
- misc
  - Providerのバージョン指定の記述スタイルを変更（`provider.version`から`terraform.required_providers`へ）(!98)
  - tflintのバージョンアップ（0.18.0 → 0.19.1）(!96)

## 0.1.0 - 2020-08-19

### Features

- AWS pattern modules
  - audit pattern add
  - network pattern add
  - ci_pipeline pattern add
  - cd_pipeline pattern add
  - cd_pipeline_trigger pattern add
  - database pattern add
  - encryption_key pattern add
  - parameter_store pattern add
  - cacheable_frontend pattern add
  - public_traffic_container_service pattern add
  - redis pattern add
  - threat_detection pattern add
- AWS pattern documents
  - audit pattern add
  - network pattern add
  - ci_pipeline pattern add
  - cd_pipeline pattern add
  - cd_pipeline_trigger pattern add
  - database pattern add
  - encryption_key pattern add
  - parameter_store pattern add
  - cacheable_frontend pattern add
  - public_traffic_container_service pattern add
  - redis pattern add
  - threat_detection pattern add
